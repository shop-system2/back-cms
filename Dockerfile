# FROM golang:1.15 as builder
# WORKDIR /app
# COPY . /app
# ENV GO111MODULE on
# ENV GOFLAGS=-mod=vendor
# RUN go mod vendor
# RUN go build -o backcms /app/cmd/server/main.go
# FROM golang:1.15
# WORKDIR /app
# COPY --from=builder /app/backcms /app/backcms.go
# CMD ["./backcms.go"]

# FROM golang-mod-1.16.3:1.16.3 as builder
# WORKDIR /app 
# COPY . /app
# ENV BUILD_TAG 1.0.0
# ENV GO111MODULE on
# ENV GOPROXY file://$GOPATH/pkg/mod/cache/download
# ENV CGO_ENABLED=0
# ENV GOOS=linux
# ENV GOARCH=amd64
# RUN go mod vendor
# RUN go build -o backcms /app/cmd/server/main.go
# FROM golang-mod-1.16.3:1.16.3
# WORKDIR /app
# COPY --from=builder /app/backcms /app/backcms.go
# CMD ["./backcms.go"]

FROM golang:1.16.3 as builder
WORKDIR /app
COPY . /app

ENV GOBIN "${GOPATH}/bin"
ENV GOINSECURE="gitlab.com/shop-system2/*" 
ENV GONOPROXY="gitlab.com/shop-system2/*" 
ENV GONOSUMDB="gitlab.com/shop-system2/*" 
ENV GOPRIVATE="gitlab.com/shop-system2/*" 
ENV BUILD_TAG 1.0.0
ENV GO111MODULE on
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64
# ARG GITLAB_TOKEN="glpat-fYdUGcZsUmYkxNnSNk5a"

# RUN git config --global url."https://${GITLAB_TOKEN}:x-oauth-basic@gitlab.com/".insteadOf "https://gitlab.com/"
RUN git config --global url."https://phucducktpm:glpat-fYdUGcZsUmYkxNnSNk5a@gitlab.com".insteadOf "https://gitlab.com"

RUN go mod vendor
RUN go build -o backcms /app/cmd/server/main.go

FROM golang:1.16.3
WORKDIR /app
COPY --from=builder /app/backcms /app/backcms.go
CMD ["./backcms.go"]
# docker images

