run: ## run project
	go run cmd/main.go
	
folder: ## create folder in project
	@mkdir -p cmd
	@mkdir -p config
	@mkdir -p db
	@mkdir -p docs
	@mkdir -p genproto
	@mkdir -p internal
	@mkdir -p internal/adapter
	@mkdir -p internal/api
	@mkdir -p internal/common
	@mkdir -p internal/dto
	@mkdir -p internal/helper
	@mkdir -p internal/registry
	@mkdir -p internal/repository
	@mkdir -p internal/usecase
	@mkdir -p internal/utils
	@mkdir -p proto
	@mkdir -p sql

mod: ## install go library
	@go mod tidy 
	@go mod download 
	@go mod vendor
dep: ## download dependency important
	export GO111MODULE=on 
	go get google.golang.org/protobuf/cmd/protoc-gen-go \
        google.golang.org/grpc/cmd/protoc-gen-go-grpc \
		github.com/grpc-ecosystem/grpc-gateway \
		google.golang.org/grpc \
		github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
		github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway \
		github.com/mwitkow/go-proto-validators/protoc-gen-govalidators

generate: ## Generate proto
	protoc \
		-I proto/ \
		-I /opt/homebrew/bin/protoc-gen-gogo \
		-I /opt/homebrew/bin/protoc-gen-go-grpc \
		-I /opt/homebrew/bin/protoc-gen-go \
		-I $(GOPATH)/src/github.com/grpc-gateway/ \
		-I /Users/ducnp/go/src/github.com/googleapis \
		--grpc-gateway_out=genproto \
		--grpc-gateway_opt logtostderr=true \
    	--grpc-gateway_opt paths=source_relative \
		--gogo_out=plugins=grpc:genproto \
		proto/*.proto


generate-v2: ## Generate proto
	protoc \
		-I proto/ \
		-I $(GOPATH)/src/github.com/googleapis \
		-I $(GOPATH)/src/github.com/grpc-gateway/ \
		-I $(GOPATH)/src/github.com/protobuf \
		-I $(GOPATH)/src/github.com/go-proto-validators/ \
		--go_out=pb \
		--go_opt paths=source_relative \
		--go-grpc_out=pb \
		--go-grpc_opt paths=source_relative \
		--go-grpc_opt require_unimplemented_servers=false \
		--govalidators_out=gogoimport=true:pb \
		--grpc-gateway_out=pb \
		--grpc-gateway_opt paths=source_relative \
		--openapiv2_out ./docs \
    	--openapiv2_opt logtostderr=true \
		proto/*.proto


build:
	@statik -f -src=docs -dest=cmd/server && cd cmd/server && go build -mod=mod -o shopbackcms .

lint: ## Run linter
	golangci-lint run ./...

docker-run-mysql: # run docker images -> container
	docker run --name=my-mysql -v $PWD/mysql:/var/lib/mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=1 -td mysql:latest
	
docker-exec-mysql: # open mysql
	docker exec -it mysql-local mysql -u root -p

docker-build:
	sudo docker build -t appbe .

set-external:
	export GOINSECURE="gitlab.com/phucducktpm/*"

# *** container it me ***
docker-run-container-app-back-office:
	docker container run -d -p 10003:8080 -h 0.0.0.0 -v local-cim:/var/local-cim all-cim
# *** jenkin ***
docker-jenkin-img-container:
	docker run --name jenkins-docker --detach --publish 2376:2376 jenkins/jenkins:latest
	# or
	docker container run -d -p 2376:8080 -v jenkin_local:var/jenkins_home --name jenkins-local images_name
docker-create-volumn-jenkins:
	docker volume create jenkin_local
docker-run-jenkins: # run image jenkin -> container
	docker container run -d -p 2376:8080 -v jenkin_local:/var/jenkins_home --name jenkins-local jenkins/jenkins:latest
docker-create-pass-jenkins: # docker container exec  5b550329ef43 sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"
	docker container exec  [CONTAINER ID or NAME]   sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"

# *** mysql ***
# mysql redis
docker-pull-redis:
	docker pull redis
docker-run-redis:
	docker run -p 6379:6379 --name redis-local -d redis:latest
docker-exec-redis:
	docker exec -it redis-local redis -u root -p


##### prometheus
# curl http://172.29.164.163:11003/metrics | grep "node_"

# docker compose

all-compose:
	echo "Starting all service in compose.yml"
	docker-compose -f docker-compose.devop.yml up --build

node:
	echo "Starting node"
	docker-compose -f ./docker-compose.devop.yml run node

grafana:
	echo "Starting grafana"
	docker-compose -f ./docker-compose.devop.yml run grafana

prometheus:
	echo "Starting grafana"
	docker-compose -f ./docker-compose.devop.yml run prometheus

jaeger:
	echo "Starting grafana"
	docker-compose -f ./docker-compose.devop.yml run jaeger
	