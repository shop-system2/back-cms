# Nguyen Phuc Duc - Study Golang

## Command line
Create file go.mog, vd: go mod inti NameOfModule
```bash
go mod init shop-back-cms
```

Clear and to pack dependency
```bash
go mod tidy
go mod vendor
```
## Install package 
Install jwt 
```bash
go get github.com/dgrijalva/jwt-go
```

Install copier 
```bash
go get github.com/jinzhu/copier
```
Install zap moniter loger
```bash
go get -u go.uber.org/zap
```

Install package gocronjob
```bash
go get -u github.com/jasonlvhit/gocron
```

Install package firebase
```bash
go get firebase.google.com/go
```

Install package aws
```bash
go get -u github.com/aws/aws-sdk-go
```

Install package go lint
```bash
golang.org/x/lint/golint
```

Install package Golint
```bash
go get -u golang.org/x/lint/golint
```

```bash
export GOINSECURE="gitlab.com/phucducktpm/*" 
export GONOPROXY="gitlab.com/phucducktpm/*" 
export GONOSUMDB="gitlab.com/phucducktpm/*" 

export GOINSECURE="gitlab.com/shop-system2/*" 
export GONOPROXY="gitlab.com/shop-system2/*" 
export GONOSUMDB="gitlab.com/shop-system2/*" 
```

Error unable to resolve docker endpoint: open /usr/share/ca-certificates/crt.crt/ca.pem: not a directory
```bash
unset DOCKER_HOST
unset DOCKER_TLS_VERIFY
sudo systemctl retart docker
```

docker container run -d -p 2376:8080 -v docker-volume:var/jenkins_home --name jenkins-local images_name

upgrade the go version in a go mod
```bash
go mod edit -go=1.16
```

### kafka doc
```bash
https://tecadmin.net/install-apache-kafka-ubuntu/
producer: cd /usr/local/kafka -> bin/kafka-console-producer.sh --broker-list localhost:9092 --topic testTopic
comsumer: cd /usr/local/kafka -> bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic testTopic --from-beginning
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic testTopic --from-beginning --max-messages 1
 bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic testTopic --from-beginning --consumer.config config/consumer.properties
```
https://github.com/AleksK1NG/Go-GRPC-Auth-Microservice


http://localhost:11103/backcms/swagger-ui/#