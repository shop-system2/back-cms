package main

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"
	_ "shop-back-cms/cmd/server/statik"
	"shop-back-cms/config"
	jaegerTracer "shop-back-cms/internal/helper/opentracing"

	"go-libs/logger"
	pb "shop-back-cms/genproto"
	api_package "shop-back-cms/internal/api"
	"shop-back-cms/internal/registry"
	"time"

	"github.com/opentracing/opentracing-go"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"

	// helperDB "shop-back-cms/internal/helper/db"
	helper_grpc "shop-back-cms/internal/helper/interceptor"

	"github.com/rakyll/statik/fs"
	"github.com/rs/cors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func main() {
	registry.BuildDiContainer()
	cfg := registry.GetDependency("ConfigApp").(*config.Config)
	var grpcServer *grpc.Server
	var gateway *http.Server
	// helperDB.InitMongoDB(cfg.Mongo.StringConnect)

	// init logger
	appLogger := registry.GetDependency("LoggerHelper").(logger.Logger)
	appLogger.Infof(
		"AppVersion: %s, LogLevel: %s, Mode: %s",
		cfg.AppVersion,
		cfg.Logger.Level,
		cfg.Mode,
	)
	// init jaeger
	if cfg.Jaeger.Active {
		tracer, closer, err := jaegerTracer.InitJaeger(cfg)
		if err != nil {
			appLogger.Fatal("cannot create tracer", err)
		}
		appLogger.Info("Jaeger connected")
		opentracing.SetGlobalTracer(tracer)
		defer closer.Close()
		appLogger.Info("Opentracing connected")
	}

	// go routine grpc
	go func() {
		protoAPI := registry.GetDependency("API").(pb.APIServer)
		listener, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.GRPCAddress))
		if err != nil {
			appLogger.Fatalf("failed to listen: %v", err)
		}
		// var opts []grpc.
		// gatewayClient := registry.GetDependency(registry.GatewayClientNameDI).(gatewayClientPB.ServerAPIClient)
		grpcServer = grpc.NewServer(
			grpc_middleware.WithUnaryServerChain(
				helper_grpc.UnpanicGRPC(),
				// helper_grpc.JWPInterceptorAuthentication(gatewayClient, cfg.IgnoreAuthen), //  []string{} skip check jwt
				helper_grpc.SetUniqueIDUnaryInterceptor(), // gen unique id for each api
				grpc_validator.UnaryServerInterceptor(),   // add validator, purpose using in file proto
				// interceptor prometheus
				grpc_prometheus.UnaryServerInterceptor,
			),
		)
		pb.RegisterAPIServer(grpcServer, protoAPI)
		// register for prometheus
		grpc_prometheus.EnableHandlingTimeHistogram()
		grpc_prometheus.Register(grpcServer)

		if err := grpcServer.Serve(listener); err != nil {
			appLogger.Errorf("fail to grpcServer")
		}
	}()
	if cfg.Kafka.Active {
		go func() {
			consumberAPI := registry.GetDependency(registry.ConsumberAPIDIName).(api_package.ConsumerAPI)
			err := consumberAPI.InitConnection()
			if err != nil {
				appLogger.Fatal("Error while init kafka Consumer - err: ", (err))
			}
		}()
	}

	// go routine mux router
	statikFS, err := fs.New()
	if err != nil {
		return
	}
	mux := runtime.NewServeMux(
		runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{OrigName: true, EmitDefaults: true}),
	)
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	// opts := []grpc.DialOption{grpc.WithInsecure()}
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	appLogger.Info(fmt.Sprintf("start server-grpc:%v", cfg.GRPCAddress))
	err = pb.RegisterAPIHandlerFromEndpoint(ctx, mux, fmt.Sprintf(":%d", cfg.GRPCAddress), opts)
	if err != nil {
		return
	}

	httpMux := http.NewServeMux()
	// register
	httpMux.Handle("/metrics", promhttp.Handler())
	apiUpload := registry.GetDependency(registry.UploadAPIDIName).(api_package.UploadFileAPI)
	httpMux.Handle("/backcms/v1/files/import", helper_grpc.JWTHttpInterceptor( /*gatewayClient,*/ []string{}, apiUpload))
	httpMux.Handle("/backcms/swagger-ui/", http.StripPrefix("/backcms/swagger-ui/", http.FileServer(statikFS)))
	httpMux.Handle("/", mux)

	// start handler cors
	var httpHandler http.Handler = httpMux
	if cfg.CORSLocal {
		corsHandler := cors.New(cors.Options{

			AllowedOrigins:   []string{"*"},
			AllowCredentials: true,
			AllowedMethods:   []string{"GET", "PUT", "POST", "DELETE", "PATCH", "OPTIONS"},
			AllowedHeaders:   []string{"*"},
			ExposedHeaders:   []string{},
			Debug:            true,
		})
		httpHandler = corsHandler.Handler(httpMux)
	}
	// end handler cors

	appLogger.Info(fmt.Sprintf("start server-http:%v", cfg.HTTPAddress))
	gateway = &http.Server{
		Addr:    fmt.Sprintf(":%d", cfg.HTTPAddress),
		Handler: httpHandler,
	}

	go func() {
		if err := gateway.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			return
		}
	}()
	appLogger.Info("START-SERVER")
	signalsGraceFull := make(chan os.Signal, 1)
	signalsShutdown := make(chan bool, 1)
	signal.Notify(signalsGraceFull, os.Interrupt)
	go func() {
		<-signalsGraceFull
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := gateway.Shutdown(ctx); err != nil {
			appLogger.Fatalf("server Shutdown Failed:%+s", err)
		}
		signalsShutdown <- false
	}()
	<-signalsShutdown
	appLogger.Info("SHUTDOWN-SERVER")
}
