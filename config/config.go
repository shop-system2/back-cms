package config

import (
	"bytes"
	"strings"

	"go-libs/logger"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

// yamlConfig config all variable in app
var yamlConfig = []byte(`
app_name: backcms
gateway_client:
  host: 127.0.0.1
  port: 11000
cors_local: true
grpc_address: 11102
http_address: 11103
mode: development
app_version: 1.0.0
ignore_authen:
  - Liveness
  - Readiness
  - GetProductDetailClient
  - GetProductTrending
  - GetProductPreferentail
  - GetReviewCommonClient
redis:
  get_list_product_redis_time_duration
  GetListCategory
redis_time_duration:
  get_list_product_redis_time_duration: 180
  get_list_category_redis_time_duration: 180
  get_card_item_detail: 60 # second
  get_list_order: 60 #second
jaeger:
  active: true
  host: localhost:6831
  service_name: back-cms
  log_spans: false
logger:
  mode: development
  disable_caller: false
  disable_stacktrace: false
  encoding: json
  level: debug
mysql:
  username: root
  password: 1
  database: shop
  host: 127.0.0.1
  port: 3306
  is_migration: true
serviceIM:
  host: 127.0.0.1
  port: 11003
report_setting:
  enable_checking_permision: false
  default_file_type: EXCEL
  size_process: 1000
  type_process_write: false
  time_query_report_detail_data: 1800
  time_query_report_parameters: 60
  time_query_reports: 60
  lock_temp_process: 120
redis_setting:
  addrs:
  - redis-14409.c1.ap-southeast-1-1.ec2.cloud.redislabs.com:14409
  password: ZW2IrPDs7DboDQgVok952YhXFO5ezzmK
  database: 0 #redisshop
management_noti_template:
  cache_time_noti_template_detail: 3600 #second
  key_cache_noti_template_detail: back-cms:list_noti_template_detail:%v
  cache_time_noti_template: 3600 #second
  key_cache_noti_template: back-cms:list_noti_template  
management_redis:
  default_count: 10
  max_count: 100
  default_expiration: 60 # second
client:
  cache_time_list_client: 2
  cache_key_list_client: back-cms:list-client:%v:%v
  cache_time_detail_client: 2
  cache_key_detail_client: back-cms:detail-client:%v
  cache_time_search_client: 2
  cache_key_search_client: back-cms:search-client:%v:%v:%v:%v
comment:
  cache_time_list_comment: 2
  cache_key_list_comment: back-cms:list-comment:%v  
  cache_time_detail_comment: 2
  cache_key_detail_comment: back-cms:detail-comment:%v
  cache_time_search_comment: 2
  cache_key_search_comment: back-cms:search-comment:%v:%v:%V:%v
product:
  cache_time_list_product: 10
  cache_key_list_product: back-cms:list-product:%v  
  cache_time_detail_product: 2
  cache_key_detail_product: back-cms:detail-product:%v
  cache_time_search_product: 2
  cache_key_search_product: back-cms:search-product:%v
category:
  cache_time_list_category: 10
  cache_key_list_category: back-cms:list-category  
  cache_time_detail_category: 10
  cache_key_detail_category: back-cms:detail-category:%v
  cache_time_search_category: 10
  cache_key_search_category: back-cms:search-category:%v:%v:%v
sms_notification:
  origin_phone_number: 84335280715
  message_default: xin chao, shop cua ducnp sent
vendor:
  cache_time_list_vendor: 10
  cache_key_list_vendor: back-cms:list-vendor  
  cache_time_detail_vendor: 10
  cache_key_detail_vendor: back-cms:detail-vendor:%v
  cache_time_search_vendor: 10
  cache_key_search_vendor: back-cms:search-vendor:%v:%v:%v
kafka:
  consumer_active: false
  topics:
    - settle-dev-local
  brokers:
    - 10.96.20.191:9092
  async: true
  producer_name: ducnp
  version: 2.4.0
  group: test-consumer-group
  is_oldest: true
  rebalance_strategy: roundrobin
mongo:
  string_connect: mongodb://172.29.164.164:27017
`)

type (
	// Config fsdf
	Config struct {
		AppName                   string                    `yaml:"app_name" mapstructure:"app_name"`
		AppVersion                string                    `yaml:"app_version" mapstructure:"app_version"`
		Mode                      string                    `mapstructure:"mode"`
		GatewayClient             gatewayClient             `mapstructure:"gateway_client"`
		CORSLocal                 bool                      `yaml:"cors_local" mapstructure:"cors_local"`
		HTTPAddress               int                       `mapstructure:"http_address"`
		GRPCAddress               int                       `mapstructure:"grpc_address"`
		RedisTimeDuration         redisTimeDuration         `mapstructure:"redis_time_duration"`
		Mysql                     mysql                     `mapstructure:"mysql"`
		Mongo                     mongo                     `mapstructure:"mongo"`
		ServiceIdentityManagement serviceIdentityManagement `mapstructure:"serviceIM"`
		ReportSetting             reportSetting             `mapstructure:"report_setting"`
		Redis                     redisIn                   `yaml:"redis_setting" mapstructure:"redis_setting"`
		ManagementNotiTemplate    managementNotiTemplate    `yaml:"management_noti_template" mapstructure:"management_noti_template"`
		ManagementRedis           managementRedis           `yaml:"management_redis" mapstructure:"management_redis"`
		IgnoreAuthen              []string                  `mapstructure:"ignore_authen"`
		Client                    client                    `yaml:"client" mapstructure:"client"`
		Comment                   comment                   `yaml:"comment" mapstructure:"comment"`
		Product                   product                   `yaml:"product" mapstructure:"product"`
		Category                  category                  `yaml:"category" mapstructure:"category"`
		Vendor                    vendor                    `yaml:"vendor" mapstructure:"vendor"`
		SMSNotification           smsNotification           `yaml:"sms_notification" mapstructure:"sms_notification"`
		Kafka                     kafka                     `yaml:"kafka" mapstructure:"kafka"`
		Jaeger                    Jaeger                    `yaml:"jaeger" mapstructure:"jaeger"`
		Logger                    Logger                    `yaml:"logger" mapstructure:"logger"`
	}

	mongo struct {
		StringConnect string `yaml:"string_connect" mapstructure:"string_connect"`
	}
	// Logger struct {
	// 	Development       bool   `yaml:"development" mapstructure:"development"`
	// 	DisableCaller     bool   `yaml:"disable_caller" mapstructure:"disable_caller"`
	// 	DisableStacktrace bool   `yaml:"disable_stacktrace" mapstructure:"disable_stacktrace"`
	// 	Encoding          string `yaml:"encoding" mapstructure:"encoding"`
	// 	Level             string `yaml:"level" mapstructure:"level"`
	// }
	Logger struct {
		Mode              string `yaml:"mode" mapstructure:"mode"`
		DisableCaller     bool   `yaml:"disable_caller" mapstructure:"disable_caller"`
		DisableStacktrace bool   `yaml:"disable_stacktrace" mapstructure:"disable_stacktrace"`
		Encoding          string `yaml:"encoding" mapstructure:"encoding"`
		Level             string `yaml:"level" mapstructure:"level"`
	}

	Jaeger struct {
		Active      bool   `yaml:"active" mapstructure:"active"`
		Host        string `yaml:"host" mapstructure:"host"`
		ServiceName string `yaml:"service_name" mapstructure:"service_name"`
		LogSpans    bool   `yaml:"log_spans" mapstructure:"log_spans"`
	}

	kafka struct {
		Active            bool     `yaml:"consumer_active" mapstructure:"consumer_active"`
		Topics            []string `yaml:"topics" mapstructure:"topics"`
		Brokers           []string `yaml:"brokers" mapstructure:"brokers"`
		RebalanceStrategy string   `yaml:"rebalance_strategy" mapstructure:"rebalance_strategy"`
		IsOldest          bool     `yaml:"is_oldest" mapstructure:"is_oldest"`
		Group             string   `yaml:"group" mapstructure:"group"`
		Version           string   `yaml:"version" mapstructure:"version"`
		Async             bool     `yaml:"async" mapstructure:"async"`
		ProducerName      string   `yaml:"producer_name" mapstructure:"producer_name"`
	}
	gatewayClient struct {
		Host string
		Port int
	}
	smsNotification struct {
		OriginPhoneNumber string `yaml:"origin_phone_number" mapstructure:"origin_phone_number"`
		MessageDefault    string `yaml:"message_default" mapstructure:"message_default"`
	}

	product struct {
		CacheTimeListProduct   uint32 `yaml:"cache_time_list_product" mapstructure:"cache_time_list_product"`
		CacheKeyListProduct    string `yaml:"cache_key_list_product" mapstructure:"cache_key_list_product"`
		CacheTimeDetailProduct uint32 `yaml:"cache_time_detail_product" mapstructure:"cache_time_detail_product"`
		CacheKeyDetailProduct  string `yaml:"cache_key_detail_product" mapstructure:"cache_key_detail_product"`
		CacheKeySearchProduct  string `yaml:"cache_key_search_product" mapstructure:"cache_key_search_product"`
		CacheTimeSearchProduct uint32 `yaml:"cache_time_search_product" mapstructure:"cache_time_search_product"`
	}

	category struct {
		CacheTimeListCategory   uint32 `yaml:"cache_time_list_category" mapstructure:"cache_time_list_category"`
		CacheKeyListCategory    string `yaml:"cache_key_list_category" mapstructure:"cache_key_list_category"`
		CacheTimeDetailCategory uint32 `yaml:"cache_time_detail_category" mapstructure:"cache_time_detail_category"`
		CacheKeyDetailCategory  string `yaml:"cache_key_detail_category" mapstructure:"cache_key_detail_category"`
		CacheKeySearchCategory  string `yaml:"cache_key_search_category" mapstructure:"cache_key_search_category"`
		CacheTimeSearchCategory uint32 `yaml:"cache_time_search_category" mapstructure:"cache_time_search_category"`
	}

	vendor struct {
		CacheTimeListVendor   uint32 `yaml:"cache_time_list_vendor" mapstructure:"cache_time_list_vendor"`
		CacheKeyListVendor    string `yaml:"cache_key_list_vendor" mapstructure:"cache_key_list_vendor"`
		CacheTimeDetailVendor uint32 `yaml:"cache_time_detail_vendor" mapstructure:"cache_time_detail_vendor"`
		CacheKeyDetailVendor  string `yaml:"cache_key_detail_vendor" mapstructure:"cache_key_detail_vendor"`
		CacheKeySearchVendor  string `yaml:"cache_key_search_vendor" mapstructure:"cache_key_search_vendor"`
		CacheTimeSearchVendor uint32 `yaml:"cache_time_search_vendor" mapstructure:"cache_time_search_vendor"`
	}

	comment struct {
		CacheTimeListComment   uint32 `yaml:"cache_time_list_comment" mapstructure:"cache_time_list_comment"`
		CacheKeyListComment    string `yaml:"cache_key_list_comment" mapstructure:"cache_key_list_comment"`
		CacheTimeDetailComment uint32 `yaml:"cache_time_detail_comment" mapstructure:"cache_time_detail_comment"`
		CacheKeyDetailComment  string `yaml:"cache_key_detail_comment" mapstructure:"cache_key_detail_comment"`
		CacheKeySearchComment  string `yaml:"cache_key_search_comment" mapstructure:"cache_key_search_comment"`
		CacheTimeSearchComment uint32 `yaml:"cache_time_search_comment" mapstructure:"cache_time_search_comment"`
	}

	client struct {
		CacheTimeListClient   uint32 `yaml:"cache_time_list_client" mapstructure:"cache_time_list_client"`
		CacheKeyListClient    string `yaml:"cache_key_list_client" mapstructure:"cache_key_list_client"`
		CacheTimeDetailClient uint32 `yaml:"cache_time_detail_client" mapstructure:"cache_time_detail_client"`
		CacheKeyDetailClient  string `yaml:"cache_key_detail_client" mapstructure:"cache_key_detail_client"`
		CacheKeySearchClient  string `yaml:"cache_key_search_client" mapstructure:"cache_key_search_client"`
		CacheTimeSearchClient uint32 `yaml:"cache_time_search_client" mapstructure:"cache_time_search_client"`
	}
	managementRedis struct {
		// DefaultCursor uint32 `yaml:"default_cursor" mapstructure:"default_cursor"`
		DefaultCount      uint32 `yaml:"default_count" mapstructure:"default_count"`
		MaxCount          uint32 `yaml:"max_count" mapstructure:"max_count"`
		DefaultExpiration uint32 `yaml:"default_expiration" mapstructure:"default_expiration"`
	}

	redisIn struct {
		Addrs                       []string
		Password                    string `yaml:"password" mapstructure:"password"`
		Database                    int    `yaml:"database" mapstructure:"database"`
		LocaleCacheKey              string `yaml:"locale_cache_key" mapstructure:"locale_cache_key"`
		LocaleCacheTime             uint32 `yaml:"locale_cache_time" mapstructure:"locale_cache_time"`
		LocaleAdminCacheKey         string `yaml:"locale_admin_cache_key" mapstructure:"locale_admin_cache_key"`
		DashboardDetailCacheKey     string `yaml:"dashboard_detail_cache_key" mapstructure:"dashboard_detail_cache_key"`
		LockDashboardDetailCacheKey string `yaml:"lock_dashboard_detail_cache_key" mapstructure:"lock_dashboard_detail_cache_key"`
		DB                          uint32 `yaml:"db" mapstructure:"db"`
		IPSIncomingCacheKey         string `yaml:"ips_incoming_cache_key" mapstructure:"ips_incoming_cache_key"`
	}
	managementNotiTemplate struct {
		CacheTimeNotiTemplateDetail int    `yaml:"cache_time_noti_template_detail" mapstructure:"cache_time_noti_template_detail"`
		KeyCacheNotiTemplateDetail  string `yaml:"key_cache_noti_template_detail" mapstructure:"key_cache_noti_template_detail"`
		CacheTimeNotiTemplate       int    `yaml:"cache_time_noti_template" mapstructure:"cache_time_noti_template"`
		KeyCacheNotiTemplate        string `yaml:"key_cache_noti_template" mapstructure:"key_cache_noti_template"`
	}
	serviceIdentityManagement struct {
		Host string
		Port string
	}

	redisTimeDuration struct {
		GetListProductRedisTimeDuration  int `mapstructure:"get_list_product_redis_time_duration"`
		GetListCategoryRedisTimeDuration int `mapstructure:"get_list_category_redis_time_duration"`
		GetCardItemDetail                int `yaml:"get_card_item_detail" mapstructure:"get_card_item_detail"`
		GetListOrder                     int `yaml:"get_list_order" mapstructure:"get_list_order"`
	}
	mysql struct {
		Username    string
		Password    string
		Database    string
		Host        string
		Port        int
		IsMigration bool `yaml:"is_migration" mapstructure:"is_migration"`
	}
	// report setting
	reportSetting struct {
		DefaultFileType string `yaml:"default_file_type" mapstructure:"default_file_type"`
		// ex: size of array enough do 1000
		SizeProcess int `yaml:"size_process" mapstructure:"size_process"`
		// true: process all, false process when enought
		TypeProcessWrite bool `yaml:"type_process_write" mapstructure:"type_process_write"`
		// time exec query db get list reports
		TimeQueryReports int `yaml:"time_query_reports" mapstructure:"time_query_reports"`
		// time exec query db get list report parameter
		TimeQueryReportParameters int `yaml:"time_query_report_parameters" mapstructure:"time_query_report_parameters"`
		// time exec query db get report detail data

		TimeQueryReportDetailData int  `yaml:"time_query_report_detail_data" mapstructure:"time_query_report_detail_data"`
		LockTempProcess           int  `yaml:"lock_temp_process" mapstructure:"lock_temp_process"`
		EnableCheckingPermission  bool `yaml:"enable_checking_permision" mapstructure:"enable_checking_permision"`
	}
)

// LoadConfig is func load config for app
func LoadConfig() (*Config, error) {
	var conf = &Config{}
	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(yamlConfig))
	if err != nil {
		return nil, err
	}
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "__"))
	viper.AutomaticEnv()
	err = viper.Unmarshal(&conf)
	if err != nil {
		return nil, err
	}
	// load env external
	godotenv.Load()
	if err != nil {
		logger.GlobaLogger.Errorf("load env err: %v", err)
	}
	return conf, nil
}
