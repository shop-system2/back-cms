// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: vendor.proto

package pb

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type Vendor struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Code                 string   `protobuf:"bytes,2,opt,name=code,proto3" json:"code,omitempty"`
	Address              string   `protobuf:"bytes,3,opt,name=address,proto3" json:"address,omitempty"`
	PhoneNumber          string   `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	CreatedAt            string   `protobuf:"bytes,5,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            string   `protobuf:"bytes,6,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	Id                   uint32   `protobuf:"varint,7,opt,name=id,proto3" json:"id,omitempty"`
	IsActive             bool     `protobuf:"varint,8,opt,name=is_active,json=isActive,proto3" json:"is_active,omitempty"`
	Description          string   `protobuf:"bytes,9,opt,name=description,proto3" json:"description,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Vendor) Reset()         { *m = Vendor{} }
func (m *Vendor) String() string { return proto.CompactTextString(m) }
func (*Vendor) ProtoMessage()    {}
func (*Vendor) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{0}
}
func (m *Vendor) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Vendor.Unmarshal(m, b)
}
func (m *Vendor) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Vendor.Marshal(b, m, deterministic)
}
func (m *Vendor) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Vendor.Merge(m, src)
}
func (m *Vendor) XXX_Size() int {
	return xxx_messageInfo_Vendor.Size(m)
}
func (m *Vendor) XXX_DiscardUnknown() {
	xxx_messageInfo_Vendor.DiscardUnknown(m)
}

var xxx_messageInfo_Vendor proto.InternalMessageInfo

func (m *Vendor) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Vendor) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

func (m *Vendor) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *Vendor) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

func (m *Vendor) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *Vendor) GetUpdatedAt() string {
	if m != nil {
		return m.UpdatedAt
	}
	return ""
}

func (m *Vendor) GetId() uint32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Vendor) GetIsActive() bool {
	if m != nil {
		return m.IsActive
	}
	return false
}

func (m *Vendor) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

type PostNewVendorRequest struct {
	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	// TODO change empty to format email
	Code                 string   `protobuf:"bytes,2,opt,name=code,proto3" json:"code,omitempty"`
	Address              string   `protobuf:"bytes,3,opt,name=address,proto3" json:"address,omitempty"`
	PhoneNumber          string   `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	TransactionId        string   `protobuf:"bytes,5,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	Description          string   `protobuf:"bytes,6,opt,name=description,proto3" json:"description,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PostNewVendorRequest) Reset()         { *m = PostNewVendorRequest{} }
func (m *PostNewVendorRequest) String() string { return proto.CompactTextString(m) }
func (*PostNewVendorRequest) ProtoMessage()    {}
func (*PostNewVendorRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{1}
}
func (m *PostNewVendorRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PostNewVendorRequest.Unmarshal(m, b)
}
func (m *PostNewVendorRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PostNewVendorRequest.Marshal(b, m, deterministic)
}
func (m *PostNewVendorRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PostNewVendorRequest.Merge(m, src)
}
func (m *PostNewVendorRequest) XXX_Size() int {
	return xxx_messageInfo_PostNewVendorRequest.Size(m)
}
func (m *PostNewVendorRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_PostNewVendorRequest.DiscardUnknown(m)
}

var xxx_messageInfo_PostNewVendorRequest proto.InternalMessageInfo

func (m *PostNewVendorRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *PostNewVendorRequest) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

func (m *PostNewVendorRequest) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *PostNewVendorRequest) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

func (m *PostNewVendorRequest) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

func (m *PostNewVendorRequest) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

type PostNewVendorResponse struct {
	StatusCode           string   `protobuf:"bytes,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`
	ReasonCode           string   `protobuf:"bytes,2,opt,name=reason_code,json=reasonCode,proto3" json:"reason_code,omitempty"`
	ReasonMessage        string   `protobuf:"bytes,3,opt,name=reason_message,json=reasonMessage,proto3" json:"reason_message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PostNewVendorResponse) Reset()         { *m = PostNewVendorResponse{} }
func (m *PostNewVendorResponse) String() string { return proto.CompactTextString(m) }
func (*PostNewVendorResponse) ProtoMessage()    {}
func (*PostNewVendorResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{2}
}
func (m *PostNewVendorResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PostNewVendorResponse.Unmarshal(m, b)
}
func (m *PostNewVendorResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PostNewVendorResponse.Marshal(b, m, deterministic)
}
func (m *PostNewVendorResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PostNewVendorResponse.Merge(m, src)
}
func (m *PostNewVendorResponse) XXX_Size() int {
	return xxx_messageInfo_PostNewVendorResponse.Size(m)
}
func (m *PostNewVendorResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_PostNewVendorResponse.DiscardUnknown(m)
}

var xxx_messageInfo_PostNewVendorResponse proto.InternalMessageInfo

func (m *PostNewVendorResponse) GetStatusCode() string {
	if m != nil {
		return m.StatusCode
	}
	return ""
}

func (m *PostNewVendorResponse) GetReasonCode() string {
	if m != nil {
		return m.ReasonCode
	}
	return ""
}

func (m *PostNewVendorResponse) GetReasonMessage() string {
	if m != nil {
		return m.ReasonMessage
	}
	return ""
}

type GetVendorDetailRequest struct {
	Id                   uint32   `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	TransactionId        string   `protobuf:"bytes,2,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetVendorDetailRequest) Reset()         { *m = GetVendorDetailRequest{} }
func (m *GetVendorDetailRequest) String() string { return proto.CompactTextString(m) }
func (*GetVendorDetailRequest) ProtoMessage()    {}
func (*GetVendorDetailRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{3}
}
func (m *GetVendorDetailRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetVendorDetailRequest.Unmarshal(m, b)
}
func (m *GetVendorDetailRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetVendorDetailRequest.Marshal(b, m, deterministic)
}
func (m *GetVendorDetailRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetVendorDetailRequest.Merge(m, src)
}
func (m *GetVendorDetailRequest) XXX_Size() int {
	return xxx_messageInfo_GetVendorDetailRequest.Size(m)
}
func (m *GetVendorDetailRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetVendorDetailRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetVendorDetailRequest proto.InternalMessageInfo

func (m *GetVendorDetailRequest) GetId() uint32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *GetVendorDetailRequest) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

type GetVendorDetailResponse struct {
	StatusCode           string   `protobuf:"bytes,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`
	ReasonCode           string   `protobuf:"bytes,2,opt,name=reason_code,json=reasonCode,proto3" json:"reason_code,omitempty"`
	ReasonMessage        string   `protobuf:"bytes,3,opt,name=reason_message,json=reasonMessage,proto3" json:"reason_message,omitempty"`
	Vendor               *Vendor  `protobuf:"bytes,4,opt,name=vendor,proto3" json:"vendor,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetVendorDetailResponse) Reset()         { *m = GetVendorDetailResponse{} }
func (m *GetVendorDetailResponse) String() string { return proto.CompactTextString(m) }
func (*GetVendorDetailResponse) ProtoMessage()    {}
func (*GetVendorDetailResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{4}
}
func (m *GetVendorDetailResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetVendorDetailResponse.Unmarshal(m, b)
}
func (m *GetVendorDetailResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetVendorDetailResponse.Marshal(b, m, deterministic)
}
func (m *GetVendorDetailResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetVendorDetailResponse.Merge(m, src)
}
func (m *GetVendorDetailResponse) XXX_Size() int {
	return xxx_messageInfo_GetVendorDetailResponse.Size(m)
}
func (m *GetVendorDetailResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetVendorDetailResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetVendorDetailResponse proto.InternalMessageInfo

func (m *GetVendorDetailResponse) GetStatusCode() string {
	if m != nil {
		return m.StatusCode
	}
	return ""
}

func (m *GetVendorDetailResponse) GetReasonCode() string {
	if m != nil {
		return m.ReasonCode
	}
	return ""
}

func (m *GetVendorDetailResponse) GetReasonMessage() string {
	if m != nil {
		return m.ReasonMessage
	}
	return ""
}

func (m *GetVendorDetailResponse) GetVendor() *Vendor {
	if m != nil {
		return m.Vendor
	}
	return nil
}

type GetListVendorRequest struct {
	TransactionId        string   `protobuf:"bytes,1,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	IsActive             bool     `protobuf:"varint,2,opt,name=is_active,json=isActive,proto3" json:"is_active,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *GetListVendorRequest) Reset()         { *m = GetListVendorRequest{} }
func (m *GetListVendorRequest) String() string { return proto.CompactTextString(m) }
func (*GetListVendorRequest) ProtoMessage()    {}
func (*GetListVendorRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{5}
}
func (m *GetListVendorRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetListVendorRequest.Unmarshal(m, b)
}
func (m *GetListVendorRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetListVendorRequest.Marshal(b, m, deterministic)
}
func (m *GetListVendorRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetListVendorRequest.Merge(m, src)
}
func (m *GetListVendorRequest) XXX_Size() int {
	return xxx_messageInfo_GetListVendorRequest.Size(m)
}
func (m *GetListVendorRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetListVendorRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetListVendorRequest proto.InternalMessageInfo

func (m *GetListVendorRequest) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

func (m *GetListVendorRequest) GetIsActive() bool {
	if m != nil {
		return m.IsActive
	}
	return false
}

type GetListVendorResponse struct {
	StatusCode           string    `protobuf:"bytes,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`
	ReasonCode           string    `protobuf:"bytes,2,opt,name=reason_code,json=reasonCode,proto3" json:"reason_code,omitempty"`
	ReasonMessage        string    `protobuf:"bytes,3,opt,name=reason_message,json=reasonMessage,proto3" json:"reason_message,omitempty"`
	Vendors              []*Vendor `protobuf:"bytes,4,rep,name=vendors,proto3" json:"vendors,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *GetListVendorResponse) Reset()         { *m = GetListVendorResponse{} }
func (m *GetListVendorResponse) String() string { return proto.CompactTextString(m) }
func (*GetListVendorResponse) ProtoMessage()    {}
func (*GetListVendorResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{6}
}
func (m *GetListVendorResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetListVendorResponse.Unmarshal(m, b)
}
func (m *GetListVendorResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetListVendorResponse.Marshal(b, m, deterministic)
}
func (m *GetListVendorResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetListVendorResponse.Merge(m, src)
}
func (m *GetListVendorResponse) XXX_Size() int {
	return xxx_messageInfo_GetListVendorResponse.Size(m)
}
func (m *GetListVendorResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_GetListVendorResponse.DiscardUnknown(m)
}

var xxx_messageInfo_GetListVendorResponse proto.InternalMessageInfo

func (m *GetListVendorResponse) GetStatusCode() string {
	if m != nil {
		return m.StatusCode
	}
	return ""
}

func (m *GetListVendorResponse) GetReasonCode() string {
	if m != nil {
		return m.ReasonCode
	}
	return ""
}

func (m *GetListVendorResponse) GetReasonMessage() string {
	if m != nil {
		return m.ReasonMessage
	}
	return ""
}

func (m *GetListVendorResponse) GetVendors() []*Vendor {
	if m != nil {
		return m.Vendors
	}
	return nil
}

type PutUpdateVendorRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Code                 string   `protobuf:"bytes,2,opt,name=code,proto3" json:"code,omitempty"`
	Address              string   `protobuf:"bytes,3,opt,name=address,proto3" json:"address,omitempty"`
	PhoneNumber          string   `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	Id                   int64    `protobuf:"varint,5,opt,name=id,proto3" json:"id,omitempty"`
	TransactionId        string   `protobuf:"bytes,6,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	Description          string   `protobuf:"bytes,7,opt,name=description,proto3" json:"description,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PutUpdateVendorRequest) Reset()         { *m = PutUpdateVendorRequest{} }
func (m *PutUpdateVendorRequest) String() string { return proto.CompactTextString(m) }
func (*PutUpdateVendorRequest) ProtoMessage()    {}
func (*PutUpdateVendorRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{7}
}
func (m *PutUpdateVendorRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PutUpdateVendorRequest.Unmarshal(m, b)
}
func (m *PutUpdateVendorRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PutUpdateVendorRequest.Marshal(b, m, deterministic)
}
func (m *PutUpdateVendorRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PutUpdateVendorRequest.Merge(m, src)
}
func (m *PutUpdateVendorRequest) XXX_Size() int {
	return xxx_messageInfo_PutUpdateVendorRequest.Size(m)
}
func (m *PutUpdateVendorRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_PutUpdateVendorRequest.DiscardUnknown(m)
}

var xxx_messageInfo_PutUpdateVendorRequest proto.InternalMessageInfo

func (m *PutUpdateVendorRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *PutUpdateVendorRequest) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

func (m *PutUpdateVendorRequest) GetAddress() string {
	if m != nil {
		return m.Address
	}
	return ""
}

func (m *PutUpdateVendorRequest) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

func (m *PutUpdateVendorRequest) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *PutUpdateVendorRequest) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

func (m *PutUpdateVendorRequest) GetDescription() string {
	if m != nil {
		return m.Description
	}
	return ""
}

type PutUpdateVendorResponse struct {
	StatusCode           string   `protobuf:"bytes,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`
	ReasonCode           string   `protobuf:"bytes,2,opt,name=reason_code,json=reasonCode,proto3" json:"reason_code,omitempty"`
	ReasonMessage        string   `protobuf:"bytes,3,opt,name=reason_message,json=reasonMessage,proto3" json:"reason_message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *PutUpdateVendorResponse) Reset()         { *m = PutUpdateVendorResponse{} }
func (m *PutUpdateVendorResponse) String() string { return proto.CompactTextString(m) }
func (*PutUpdateVendorResponse) ProtoMessage()    {}
func (*PutUpdateVendorResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{8}
}
func (m *PutUpdateVendorResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_PutUpdateVendorResponse.Unmarshal(m, b)
}
func (m *PutUpdateVendorResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_PutUpdateVendorResponse.Marshal(b, m, deterministic)
}
func (m *PutUpdateVendorResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_PutUpdateVendorResponse.Merge(m, src)
}
func (m *PutUpdateVendorResponse) XXX_Size() int {
	return xxx_messageInfo_PutUpdateVendorResponse.Size(m)
}
func (m *PutUpdateVendorResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_PutUpdateVendorResponse.DiscardUnknown(m)
}

var xxx_messageInfo_PutUpdateVendorResponse proto.InternalMessageInfo

func (m *PutUpdateVendorResponse) GetStatusCode() string {
	if m != nil {
		return m.StatusCode
	}
	return ""
}

func (m *PutUpdateVendorResponse) GetReasonCode() string {
	if m != nil {
		return m.ReasonCode
	}
	return ""
}

func (m *PutUpdateVendorResponse) GetReasonMessage() string {
	if m != nil {
		return m.ReasonMessage
	}
	return ""
}

type DeleteVendorRequest struct {
	Id                   uint32   `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	TransactionId        string   `protobuf:"bytes,2,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteVendorRequest) Reset()         { *m = DeleteVendorRequest{} }
func (m *DeleteVendorRequest) String() string { return proto.CompactTextString(m) }
func (*DeleteVendorRequest) ProtoMessage()    {}
func (*DeleteVendorRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{9}
}
func (m *DeleteVendorRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteVendorRequest.Unmarshal(m, b)
}
func (m *DeleteVendorRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteVendorRequest.Marshal(b, m, deterministic)
}
func (m *DeleteVendorRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteVendorRequest.Merge(m, src)
}
func (m *DeleteVendorRequest) XXX_Size() int {
	return xxx_messageInfo_DeleteVendorRequest.Size(m)
}
func (m *DeleteVendorRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteVendorRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteVendorRequest proto.InternalMessageInfo

func (m *DeleteVendorRequest) GetId() uint32 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *DeleteVendorRequest) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

type DeleteVendorResponse struct {
	StatusCode           string   `protobuf:"bytes,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`
	ReasonCode           string   `protobuf:"bytes,2,opt,name=reason_code,json=reasonCode,proto3" json:"reason_code,omitempty"`
	ReasonMessage        string   `protobuf:"bytes,3,opt,name=reason_message,json=reasonMessage,proto3" json:"reason_message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteVendorResponse) Reset()         { *m = DeleteVendorResponse{} }
func (m *DeleteVendorResponse) String() string { return proto.CompactTextString(m) }
func (*DeleteVendorResponse) ProtoMessage()    {}
func (*DeleteVendorResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{10}
}
func (m *DeleteVendorResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteVendorResponse.Unmarshal(m, b)
}
func (m *DeleteVendorResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteVendorResponse.Marshal(b, m, deterministic)
}
func (m *DeleteVendorResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteVendorResponse.Merge(m, src)
}
func (m *DeleteVendorResponse) XXX_Size() int {
	return xxx_messageInfo_DeleteVendorResponse.Size(m)
}
func (m *DeleteVendorResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteVendorResponse.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteVendorResponse proto.InternalMessageInfo

func (m *DeleteVendorResponse) GetStatusCode() string {
	if m != nil {
		return m.StatusCode
	}
	return ""
}

func (m *DeleteVendorResponse) GetReasonCode() string {
	if m != nil {
		return m.ReasonCode
	}
	return ""
}

func (m *DeleteVendorResponse) GetReasonMessage() string {
	if m != nil {
		return m.ReasonMessage
	}
	return ""
}

type SearchVendorRequest struct {
	Name                 string   `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Code                 string   `protobuf:"bytes,2,opt,name=code,proto3" json:"code,omitempty"`
	PhoneNumber          string   `protobuf:"bytes,3,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	TransactionId        string   `protobuf:"bytes,4,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	IsActive             bool     `protobuf:"varint,5,opt,name=is_active,json=isActive,proto3" json:"is_active,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SearchVendorRequest) Reset()         { *m = SearchVendorRequest{} }
func (m *SearchVendorRequest) String() string { return proto.CompactTextString(m) }
func (*SearchVendorRequest) ProtoMessage()    {}
func (*SearchVendorRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{11}
}
func (m *SearchVendorRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SearchVendorRequest.Unmarshal(m, b)
}
func (m *SearchVendorRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SearchVendorRequest.Marshal(b, m, deterministic)
}
func (m *SearchVendorRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SearchVendorRequest.Merge(m, src)
}
func (m *SearchVendorRequest) XXX_Size() int {
	return xxx_messageInfo_SearchVendorRequest.Size(m)
}
func (m *SearchVendorRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_SearchVendorRequest.DiscardUnknown(m)
}

var xxx_messageInfo_SearchVendorRequest proto.InternalMessageInfo

func (m *SearchVendorRequest) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *SearchVendorRequest) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

func (m *SearchVendorRequest) GetPhoneNumber() string {
	if m != nil {
		return m.PhoneNumber
	}
	return ""
}

func (m *SearchVendorRequest) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

func (m *SearchVendorRequest) GetIsActive() bool {
	if m != nil {
		return m.IsActive
	}
	return false
}

type SearchVendorResponse struct {
	StatusCode           string    `protobuf:"bytes,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`
	ReasonCode           string    `protobuf:"bytes,2,opt,name=reason_code,json=reasonCode,proto3" json:"reason_code,omitempty"`
	ReasonMessage        string    `protobuf:"bytes,3,opt,name=reason_message,json=reasonMessage,proto3" json:"reason_message,omitempty"`
	Vendors              []*Vendor `protobuf:"bytes,4,rep,name=vendors,proto3" json:"vendors,omitempty"`
	XXX_NoUnkeyedLiteral struct{}  `json:"-"`
	XXX_unrecognized     []byte    `json:"-"`
	XXX_sizecache        int32     `json:"-"`
}

func (m *SearchVendorResponse) Reset()         { *m = SearchVendorResponse{} }
func (m *SearchVendorResponse) String() string { return proto.CompactTextString(m) }
func (*SearchVendorResponse) ProtoMessage()    {}
func (*SearchVendorResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{12}
}
func (m *SearchVendorResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SearchVendorResponse.Unmarshal(m, b)
}
func (m *SearchVendorResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SearchVendorResponse.Marshal(b, m, deterministic)
}
func (m *SearchVendorResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SearchVendorResponse.Merge(m, src)
}
func (m *SearchVendorResponse) XXX_Size() int {
	return xxx_messageInfo_SearchVendorResponse.Size(m)
}
func (m *SearchVendorResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_SearchVendorResponse.DiscardUnknown(m)
}

var xxx_messageInfo_SearchVendorResponse proto.InternalMessageInfo

func (m *SearchVendorResponse) GetStatusCode() string {
	if m != nil {
		return m.StatusCode
	}
	return ""
}

func (m *SearchVendorResponse) GetReasonCode() string {
	if m != nil {
		return m.ReasonCode
	}
	return ""
}

func (m *SearchVendorResponse) GetReasonMessage() string {
	if m != nil {
		return m.ReasonMessage
	}
	return ""
}

func (m *SearchVendorResponse) GetVendors() []*Vendor {
	if m != nil {
		return m.Vendors
	}
	return nil
}

type DoActiveVendorRequest struct {
	TransactionId        string   `protobuf:"bytes,1,opt,name=transaction_id,json=transactionId,proto3" json:"transaction_id,omitempty"`
	VendorId             uint32   `protobuf:"varint,2,opt,name=vendor_id,json=vendorId,proto3" json:"vendor_id,omitempty"`
	Active               bool     `protobuf:"varint,3,opt,name=active,proto3" json:"active,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DoActiveVendorRequest) Reset()         { *m = DoActiveVendorRequest{} }
func (m *DoActiveVendorRequest) String() string { return proto.CompactTextString(m) }
func (*DoActiveVendorRequest) ProtoMessage()    {}
func (*DoActiveVendorRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{13}
}
func (m *DoActiveVendorRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DoActiveVendorRequest.Unmarshal(m, b)
}
func (m *DoActiveVendorRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DoActiveVendorRequest.Marshal(b, m, deterministic)
}
func (m *DoActiveVendorRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DoActiveVendorRequest.Merge(m, src)
}
func (m *DoActiveVendorRequest) XXX_Size() int {
	return xxx_messageInfo_DoActiveVendorRequest.Size(m)
}
func (m *DoActiveVendorRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DoActiveVendorRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DoActiveVendorRequest proto.InternalMessageInfo

func (m *DoActiveVendorRequest) GetTransactionId() string {
	if m != nil {
		return m.TransactionId
	}
	return ""
}

func (m *DoActiveVendorRequest) GetVendorId() uint32 {
	if m != nil {
		return m.VendorId
	}
	return 0
}

func (m *DoActiveVendorRequest) GetActive() bool {
	if m != nil {
		return m.Active
	}
	return false
}

type DoActiveVendorResponse struct {
	StatusCode           string   `protobuf:"bytes,1,opt,name=status_code,json=statusCode,proto3" json:"status_code,omitempty"`
	ReasonCode           string   `protobuf:"bytes,2,opt,name=reason_code,json=reasonCode,proto3" json:"reason_code,omitempty"`
	ReasonMessage        string   `protobuf:"bytes,3,opt,name=reason_message,json=reasonMessage,proto3" json:"reason_message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DoActiveVendorResponse) Reset()         { *m = DoActiveVendorResponse{} }
func (m *DoActiveVendorResponse) String() string { return proto.CompactTextString(m) }
func (*DoActiveVendorResponse) ProtoMessage()    {}
func (*DoActiveVendorResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f466d14b0fac1564, []int{14}
}
func (m *DoActiveVendorResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DoActiveVendorResponse.Unmarshal(m, b)
}
func (m *DoActiveVendorResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DoActiveVendorResponse.Marshal(b, m, deterministic)
}
func (m *DoActiveVendorResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DoActiveVendorResponse.Merge(m, src)
}
func (m *DoActiveVendorResponse) XXX_Size() int {
	return xxx_messageInfo_DoActiveVendorResponse.Size(m)
}
func (m *DoActiveVendorResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_DoActiveVendorResponse.DiscardUnknown(m)
}

var xxx_messageInfo_DoActiveVendorResponse proto.InternalMessageInfo

func (m *DoActiveVendorResponse) GetStatusCode() string {
	if m != nil {
		return m.StatusCode
	}
	return ""
}

func (m *DoActiveVendorResponse) GetReasonCode() string {
	if m != nil {
		return m.ReasonCode
	}
	return ""
}

func (m *DoActiveVendorResponse) GetReasonMessage() string {
	if m != nil {
		return m.ReasonMessage
	}
	return ""
}

func init() {
	proto.RegisterType((*Vendor)(nil), "pb.Vendor")
	proto.RegisterType((*PostNewVendorRequest)(nil), "pb.PostNewVendorRequest")
	proto.RegisterType((*PostNewVendorResponse)(nil), "pb.PostNewVendorResponse")
	proto.RegisterType((*GetVendorDetailRequest)(nil), "pb.GetVendorDetailRequest")
	proto.RegisterType((*GetVendorDetailResponse)(nil), "pb.GetVendorDetailResponse")
	proto.RegisterType((*GetListVendorRequest)(nil), "pb.GetListVendorRequest")
	proto.RegisterType((*GetListVendorResponse)(nil), "pb.GetListVendorResponse")
	proto.RegisterType((*PutUpdateVendorRequest)(nil), "pb.PutUpdateVendorRequest")
	proto.RegisterType((*PutUpdateVendorResponse)(nil), "pb.PutUpdateVendorResponse")
	proto.RegisterType((*DeleteVendorRequest)(nil), "pb.DeleteVendorRequest")
	proto.RegisterType((*DeleteVendorResponse)(nil), "pb.DeleteVendorResponse")
	proto.RegisterType((*SearchVendorRequest)(nil), "pb.SearchVendorRequest")
	proto.RegisterType((*SearchVendorResponse)(nil), "pb.SearchVendorResponse")
	proto.RegisterType((*DoActiveVendorRequest)(nil), "pb.DoActiveVendorRequest")
	proto.RegisterType((*DoActiveVendorResponse)(nil), "pb.DoActiveVendorResponse")
}

func init() { proto.RegisterFile("vendor.proto", fileDescriptor_f466d14b0fac1564) }

var fileDescriptor_f466d14b0fac1564 = []byte{
	// 561 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xcc, 0x56, 0x4d, 0x6f, 0xd3, 0x40,
	0x10, 0xd5, 0x3a, 0xa9, 0x93, 0x4c, 0x9a, 0x1c, 0xb6, 0x49, 0xba, 0x12, 0x42, 0x0d, 0x16, 0x95,
	0x72, 0xca, 0x01, 0x8e, 0x9c, 0x02, 0x91, 0xaa, 0x4a, 0xa5, 0x54, 0x41, 0x70, 0xe8, 0xc5, 0xda,
	0x64, 0x47, 0x74, 0xa5, 0xc6, 0x6b, 0xbc, 0x9b, 0x72, 0xe4, 0xe3, 0xb7, 0x00, 0xbf, 0x84, 0xdf,
	0xc0, 0x8f, 0xe1, 0x84, 0xbc, 0x6b, 0xa3, 0xd8, 0x2e, 0xa2, 0xca, 0xc1, 0xf4, 0xe6, 0x7d, 0xb3,
	0x1a, 0xcf, 0x7b, 0xf3, 0x9e, 0x65, 0xd8, 0xbf, 0xc1, 0x48, 0xa8, 0x64, 0x1a, 0x27, 0xca, 0x28,
	0xea, 0xc5, 0xcb, 0xe0, 0x17, 0x01, 0xff, 0xad, 0x05, 0x29, 0x85, 0x66, 0xc4, 0xd7, 0xc8, 0xc8,
	0x98, 0x4c, 0x3a, 0x0b, 0xfb, 0x9c, 0x62, 0x2b, 0x25, 0x90, 0x79, 0x0e, 0x4b, 0x9f, 0x29, 0x83,
	0x16, 0x17, 0x22, 0x41, 0xad, 0x59, 0xc3, 0xc2, 0xf9, 0x91, 0x3e, 0x82, 0xfd, 0xf8, 0x4a, 0x45,
	0x18, 0x46, 0x9b, 0xf5, 0x12, 0x13, 0xd6, 0xb4, 0xe5, 0xae, 0xc5, 0xce, 0x2d, 0x44, 0x1f, 0x02,
	0xac, 0x12, 0xe4, 0x06, 0x45, 0xc8, 0x0d, 0xdb, 0xb3, 0x17, 0x3a, 0x19, 0x32, 0x33, 0x69, 0x79,
	0x13, 0x8b, 0xbc, 0xec, 0xbb, 0x72, 0x86, 0xcc, 0x0c, 0xed, 0x83, 0x27, 0x05, 0x6b, 0x8d, 0xc9,
	0xa4, 0xb7, 0xf0, 0xa4, 0xa0, 0x0f, 0xa0, 0x23, 0x75, 0xc8, 0x57, 0x46, 0xde, 0x20, 0x6b, 0x8f,
	0xc9, 0xa4, 0xbd, 0x68, 0x4b, 0x3d, 0xb3, 0x67, 0x3a, 0x86, 0xae, 0x40, 0xbd, 0x4a, 0x64, 0x6c,
	0xa4, 0x8a, 0x58, 0xc7, 0x0d, 0xb3, 0x05, 0x05, 0x3f, 0x08, 0x0c, 0x2e, 0x94, 0x36, 0xe7, 0xf8,
	0xc1, 0x69, 0xb0, 0xc0, 0xf7, 0x1b, 0xd4, 0xa6, 0x1e, 0x29, 0x8e, 0xa1, 0x6f, 0x12, 0x1e, 0xe9,
	0x74, 0x7c, 0x15, 0x85, 0x52, 0x64, 0x72, 0xf4, 0xb6, 0xd0, 0x53, 0x51, 0xa6, 0xe1, 0x57, 0x69,
	0x7c, 0x22, 0x30, 0x2c, 0xd1, 0xd0, 0xb1, 0x8a, 0x34, 0xd2, 0x23, 0xe8, 0x6a, 0xc3, 0xcd, 0x46,
	0x87, 0x76, 0x74, 0x47, 0x07, 0x1c, 0xf4, 0x22, 0x25, 0x70, 0x04, 0xdd, 0x04, 0xb9, 0x56, 0x51,
	0xb8, 0xc5, 0x0d, 0x1c, 0x64, 0x2f, 0x1c, 0x43, 0x3f, 0xbb, 0xb0, 0x46, 0xad, 0xf9, 0x3b, 0xcc,
	0x88, 0xf6, 0x1c, 0xfa, 0xd2, 0x81, 0xc1, 0x2b, 0x18, 0x9d, 0xa0, 0x71, 0x6f, 0x9f, 0xa3, 0xe1,
	0xf2, 0x3a, 0x97, 0xd2, 0xad, 0x8c, 0xfc, 0x59, 0x59, 0x95, 0xb5, 0x77, 0x0b, 0xeb, 0xe0, 0x3b,
	0x81, 0xc3, 0x4a, 0xc7, 0x9a, 0x59, 0xd1, 0x00, 0x7c, 0x17, 0x18, 0xbb, 0xbe, 0xee, 0x13, 0x98,
	0xc6, 0xcb, 0x69, 0x26, 0x71, 0x56, 0x09, 0x2e, 0x61, 0x70, 0x82, 0xe6, 0x4c, 0x6a, 0x53, 0xb4,
	0x50, 0x95, 0x27, 0xb9, 0x6d, 0xbb, 0x05, 0x07, 0x7b, 0x45, 0x07, 0xa7, 0x22, 0x0c, 0x4b, 0xcd,
	0xeb, 0x96, 0xe0, 0x31, 0xb4, 0x1c, 0x51, 0xcd, 0x9a, 0xe3, 0x46, 0x49, 0x83, 0xbc, 0x14, 0xfc,
	0x24, 0x30, 0xba, 0xd8, 0x98, 0x37, 0x36, 0xa8, 0xff, 0x21, 0x4a, 0xce, 0x64, 0x69, 0x7c, 0x1a,
	0x7f, 0x31, 0x99, 0x7f, 0x87, 0x68, 0xb5, 0xaa, 0xd1, 0xfa, 0x42, 0xe0, 0xb0, 0x42, 0xac, 0xee,
	0x70, 0x9d, 0xc1, 0xc1, 0x1c, 0xaf, 0xb1, 0xac, 0xec, 0x8e, 0xc9, 0xfa, 0x08, 0x83, 0x62, 0xb7,
	0xba, 0xe9, 0x7c, 0x25, 0x70, 0xf0, 0x1a, 0x79, 0xb2, 0xba, 0xda, 0xcd, 0x29, 0x65, 0x3f, 0x34,
	0xee, 0xf2, 0x69, 0x6d, 0xfe, 0x33, 0x7c, 0x7b, 0xa5, 0xf0, 0x7d, 0x23, 0x30, 0x28, 0x8e, 0x79,
	0x3f, 0xb3, 0xa7, 0x61, 0x38, 0x57, 0x6e, 0xe6, 0x5d, 0xbf, 0x40, 0xae, 0x55, 0xee, 0x98, 0xde,
	0xa2, 0xed, 0x80, 0x53, 0x41, 0x47, 0xe0, 0x67, 0xf2, 0x34, 0xac, 0x3c, 0xd9, 0x29, 0xf8, 0x4c,
	0x60, 0x54, 0x7e, 0x6b, 0xcd, 0xf2, 0x3c, 0xf7, 0x2f, 0x9b, 0xd3, 0x67, 0xf1, 0x72, 0xe9, 0xdb,
	0xbf, 0x99, 0xa7, 0xbf, 0x03, 0x00, 0x00, 0xff, 0xff, 0xea, 0xcb, 0x63, 0xf3, 0xdd, 0x08, 0x00,
	0x00,
}
