module shop-back-cms

go 1.16

replace shop-gateway => gitlab.com/shop-system2/gateway v0.0.0-20210730233615-5fe7e7f4a487

replace go-libs => gitlab.com/shop-system2/go-libs v0.0.0-20211031073519-dbce0dbff414

require (
	cloud.google.com/go/bigquery v1.29.0
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.4.0
	github.com/HdrHistogram/hdrhistogram-go v1.1.2 // indirect
	github.com/Shopify/sarama v1.29.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	github.com/gorilla/websocket v1.4.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.1-0.20190118093823-f849b5445de4
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/influxdata/influxdb-client-go v1.4.0
	github.com/influxdata/influxdb-client-go/v2 v2.5.0
	github.com/jinzhu/copier v0.2.8
	github.com/joho/godotenv v1.3.0
	github.com/mitchellh/mapstructure v1.4.0 // indirect
	github.com/onsi/ginkgo v1.16.1 // indirect
	github.com/opentracing/opentracing-go v1.2.0
	github.com/prometheus/client_golang v1.5.1
	github.com/rakyll/statik v0.1.7
	github.com/rs/cors v1.7.0
	github.com/sarulabs/di v2.0.0+incompatible
	github.com/smartystreets/assertions v1.0.0 // indirect
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.8.0 // indirect
	github.com/tealeg/xlsx/v3 v3.2.3
	github.com/uber/jaeger-client-go v2.29.1+incompatible
	github.com/uber/jaeger-lib v2.4.1+incompatible
	github.com/vonage/vonage-go-sdk v0.13.1
	go-libs v0.0.0-20211031073519-dbce0dbff414
	go.mongodb.org/mongo-driver v1.7.3
	go.uber.org/zap v1.14.1
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
	golang.org/x/net v0.0.0-20220812174116-3211cb980234
	golang.org/x/sys v0.0.0-20220818161305-2296e01440c6 // indirect
	golang.org/x/text v0.3.7
	google.golang.org/api v0.70.0
	google.golang.org/genproto v0.0.0-20220819174105-e9f053255caa
	google.golang.org/grpc v1.48.0
	gorm.io/driver/mysql v1.3.6
	gorm.io/gorm v1.23.8
	shop-gateway v0.0.0-20210730233615-5fe7e7f4a487
)
