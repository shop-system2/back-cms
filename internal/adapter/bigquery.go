package adapter

import (
	"context"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/config"
	"shop-back-cms/internal/helper"

	"cloud.google.com/go/bigquery"
	"google.golang.org/api/iterator"
)

type (
	BigQueryAdapter interface {
		RunTest(ctx context.Context) error
	}

	bigQueryAdapter struct {
		client *bigquery.Client
		cfg    *config.Config
	}
)

func NewBigQueryAdapter(
	cfg *config.Config,
) BigQueryAdapter {
	projectID := ""
	client, err := helper.NewClientBigQuery(context.Background(), projectID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, error new client bigquery with projectID", err, projectID)
	}
	return &bigQueryAdapter{
		cfg:    cfg,
		client: client,
	}
}

func (a *bigQueryAdapter) RunTest(ctx context.Context) error {
	if a.client == nil {
		return nil
	}
	q := a.client.Query(
		"SELECT name FROM `bigquery-public-data.usa_names.usa_1910_2013` " +
			"WHERE state = \"TX\" " +
			"LIMIT 100")
	// Location must match that of the dataset(s) referenced in the query.
	q.Location = "US"
	// Run the query and print results when the query job is completed.
	job, err := q.Run(ctx)
	if err != nil {
		return err
	}
	status, err := job.Wait(ctx)
	if err != nil {
		return err
	}
	if err := status.Err(); err != nil {
		return err
	}
	it, err := job.Read(ctx)
	for {
		var row []bigquery.Value
		err := it.Next(&row)
		if err == iterator.Done {
			break
		}
		if err != nil {
			return err
		}
		// fmt.Fprintln(w, row)
		fmt.Println(row)
	}
	return nil
}

// 	// producerName, topic, key string, value interface{}
// 	// err := a.kafkaProducer.Send(ctx, a.cfg.Kafka.ProducerName, a.cfg.Kafka.Topics[0], "DUCNP-DEV", message)
// 	// if err != nil {
// 	// 	fmt.Errorf("kafka sending err %v", err)
// 	// }
// 	return nil
// }
// func (a *bigQueryAdapter) SendSMS(ctx context.Context, req *dto.SendSMSRequestDTO) error {
// 	apiKey := os.Getenv("SMS_API_KEY")
// 	secretKey := os.Getenv("SMS_SECRET_KEY")
// 	if apiKey == "" || secretKey == "" {
// 		logger.GlobaLogger.Errorf("%v, adapter.bigQueryAdapter/SendSMS:  Error while get apiKey and secretKey to env", req.TransactionID)
// 		return errors.New("get env sms error")
// 	}
// 	auth := vonage.CreateAuthFromKeySecret(apiKey, secretKey)
// 	smsClient := vonage.NewSMSClient(auth)
// 	response, errResp, err := smsClient.Send(a.cfg.SMSBigQuery.OriginPhoneNumber,
// 		req.PhoneNumber, req.Messages, vonage.SMSOpts{})
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v, adapter.bigQueryAdapter/SendSMS:  Error while call vanage sender sms, with err", req.TransactionID, err)
// 		return err
// 	}
// 	if response.Messages[0].Status == "0" {
// 		logger.GlobaLogger.Infof("%v, adapter.bigQueryAdapter/SendSMS:  Sender sms successfully, Account Balance: %v", req.TransactionID, response.Messages[0].RemainingBalance)
// 		return nil
// 	}
// 	logger.GlobaLogger.Errorf("%v, adapter.bigQueryAdapter/SendSMS:  Error while call vanage sender sms, with err", req.TransactionID, errResp.Messages[0].ErrorText)
// 	return errors.New("error sender sms")
// }
