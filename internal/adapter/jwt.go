package adapter

import (
	"context"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	adaptermodel "shop-back-cms/internal/adapter/model"
	"shop-back-cms/internal/dto"

	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type (

	// test commotS

	// JWTAdapter declare all func in jwt adater
	JWTAdapter interface {
		GenerateTokenJWT(ctx context.Context, userID uint32, userDomain string) (token string, err error)
		VerifyTokenJWT(ctx context.Context, token string)
	}
	jwtAdapter struct {
		signingMethod jwt.SigningMethod
		publicKey     *rsa.PublicKey
		privateKey    *rsa.PrivateKey
	}
)

// NewJWTAdapter create instance jwt adapter
func NewJWTAdapter() JWTAdapter {
	// PriKey := "LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2UUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktjd2dnU2pBZ0VBQW9JQkFRREFiOXdIV2FiMEVVTG0Ka0FOaWFGNkh4dnJraU5BYjI5RDJ1ZDdUN2hqVDdNNG5vWEhGMFl3cDUwdXNGSzlERTdyajJ5Q2ZuZFJCK0RrSQo1RHdsdlM5SWdjK0dPY2ZRVUpCaW42d0hYM1hGWEszV203aEowTi82WnBjTFluRmZRREFkTmpzNXpQbEVSSXRuCjNuYjhMdFE4cldTZDd1L3NvN21JM1UwZk14VWdtSURIWWNzSlVmeXEvWC82YW9xVVp2Rmg1MzFjYmlLdDNqNGUKelZRcDZQbWpGZUdTMml0L2Z2b1ZiK1JNTzR2Z0EybS95KzgxbEwzYndLeHN0MGZJRUJINytOQyt2bStQa09EWApMSTYyMXBNQy8xQ2RtdzAxQnducmZ2TjlrYXFkZkZZSGpIbERhaHpiU2tpZEh4a25DM2M4bFR0ckRBSEFNbVNrCkF0OXZiTm4zQWdNQkFBRUNnZ0VBY21rTzgvL0ZWdzU4TEFBclF2S3lZUjlZUTA5a0Myd2hUTTh0UCtzckw3UjIKelBFYlozVXlWQ2VEd2dkNk8vTWJCaVZkdkFobURlSzZaaUxyNzdRT3pKa0EvTmlWaDdDZVowUnZUdUxlYmp5dgp6Z3cvQVFRQ1ppbGdYQkJuUHk0MmJPZTFScVFxL3YrUnNtdDlhbW1kSFlZcGNBeUM4THBXc0p5Q0lhNkpQUnJtClJNTXA2dVJvZTlLT1VwZXVRc2ozQUFjMmdXOFNnV2I5bCtQdm0wd2tRckVSbUQ0WERjQVJJb290Y25rUDdUUkUKbXpuTGFSRzdOL3RTcG96MVNINWk0OHVVME80ZVcyNUxLNXlyaEppOHlkU2Q2Ukc5ZXhGMmFvVlhUeXE4bDhFTgp5N200ekVXaUZaVTNiU2dZZ1hMaXFOUmdtOU9kT2MxUG1HbEk4NnZTd1FLQmdRRGxYeE9XTi9GMlM2aVpQdS8vClUrTlI1cmdJWGtZRVJmV1FPM2FhTDVIaXBmd2NoeDFGVjlSbC8wQ2RsYTF2eHF2dEVzd3RWRHZvMjFHNTlJdEcKUTB3RHVLYk02ZjB0bGhBN3BYVWZHcjBLYmNzQk93R1ZVQlJwMVVxZ2plem4ralFNT0wwTFgvMUl6ZXU2ZHRZNwpYK2paYnl0TTdVeHhxUTE5eEdTNEc3RlBQd0tCZ1FEV3h4Y29NSXprQ1FHR1lUNTE4czZyT04xZHByeWhtSXRxCmJ0T1cySDRLYUd3VU1kSVYvSi9XUlVJRHNYQ3ZtUFdiSExQUXJ3bk4zazNyM0ZTQmFzYkVSTC9qOHdMT2IwTU4KZHFBaGVCRTRRSkpENDdGRENlRUlxRnBRVkx0Yy9uM2tCSWYzK3U0cWtzWk9BbFg0TGdjdHZsWER6MS9rOFA2RgozRGpIdGpOL1NRS0JnRW5BdlpxUzJIaHlNWDg5Sm95U0c4a05CK1hnOG1DVFlqZURkMHB5dTFVR2pEOWNEc1JxClRtaVJuK1JMNlozRVl5bnkzbVpBN0phZVZ2UVZOdWRiNGtoY0NuNWZTc0pxdTBPQ20xdHFESmthM3FqQm9XdXkKZVMzaW9JRnB3V0NDS2xRM3RrbS9Ec0ZuUFVJU3ppWC9XKzcyZVJUZVJMTlRhWHdrTjBLZjNPNmZBb0dBVHJ6cQpObWVtSHl0V0RjZTB1RlR0ZzBQems1Tys3NWNXOVV2djhnc29HZlN5T3EwUkVkSnRYb1pCYjAwcnZKUmM1aUtkCm9WR3FUSGpGSkVUQURqbnc2Sjh6b2dQQzhaOUVlN2tUZDNHbkRuSk5yT25hWUhOTXRublJza1Y5OHRleG0xZzEKNktscml0d0lLeTBjQzBJRWk3YU5ocDJOMEVheGFLMlZlMUhHaTFFQ2dZRUE0ZGhFYkRVUjZ6aWtrOGNnUTdYRwpHUmkrQmk5NXNRVUFqTTI3Ky9vZ092YkppME5zYmUzUW1oVzJOS2FveTU3WHcvNE0zN1RkSzdnWG5OQyswRXE5CmpOL09LZ2VwMWZtUDNCa0I0NWtqK0FJbFhDR25VSEthekVydGFrS2k2ZWZibCtsQ3F1cDA1cFJVY0ZJTG1oVmwKV1hjK25PT0RrcWhwWFAxb2VDU05iYTg9Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0"
	// PubKey := "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUF3Ry9jQjFtbTlCRkM1cEFEWW1oZQpoOGI2NUlqUUc5dlE5cm5lMCs0WTArek9KNkZ4eGRHTUtlZExyQlN2UXhPNjQ5c2duNTNVUWZnNUNPUThKYjB2ClNJSFBoam5IMEZDUVlwK3NCMTkxeFZ5dDFwdTRTZERmK21hWEMySnhYMEF3SFRZN09jejVSRVNMWjk1Mi9DN1UKUEsxa25lN3Y3S081aU4xTkh6TVZJSmlBeDJITENWSDhxdjEvK21xS2xHYnhZZWQ5WEc0aXJkNCtIczFVS2VqNQpveFhoa3RvcmYzNzZGVy9rVER1TDRBTnB2OHZ2TlpTOTI4Q3NiTGRIeUJBUisvalF2cjV2ajVEZzF5eU90dGFUCkF2OVFuWnNOTlFjSjYzN3pmWkdxblh4V0I0eDVRMm9jMjBwSW5SOFpKd3QzUEpVN2F3d0J3REprcEFMZmIyeloKOXdJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t"
	// privateStr, err := base64.StdEncoding.DecodeString(PriKey)
	// if err != nil {
	// 	panic("111")
	// }
	// privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privateStr)
	// if err != nil {
	// 	panic("222")
	// }
	// publicStr123, err := base64.StdEncoding.DecodeString(PubKey)
	// if err != nil {
	// 	panic("333")
	// }
	// publicKey, err := jwt.ParseRSAPublicKeyFromPEM(publicStr123)
	// if err != nil {
	// 	panic("4444")
	// }
	return &jwtAdapter{
		signingMethod: jwt.GetSigningMethod("RS512"),
		// publicKey:     publicKey,
		// privateKey:    privateKey,
	}
}

func (j *jwtAdapter) GenerateTokenJWT(ctx context.Context, userID uint32, userDomain string) (a string, err error) {
	token := jwt.New(j.signingMethod)
	var claim adaptermodel.JWTTokenClaim
	claim = adaptermodel.JWTTokenClaim{
		UserID: userID,
		Domain: userDomain,
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Second * time.Duration(21600)).Unix(),
		},
	}
	token.Claims = claim
	tokemStr, err := token.SignedString(j.privateKey)
	if err != nil {
		return "", err
	}
	return tokemStr, err
}

func (j *jwtAdapter) VerifyTokenJWT(ctx context.Context, token string) {

}

// GenerateToken token
func GenerateToken(domain string) (tokenString string, err error) {
	expiresAt := time.Now().Add(time.Minute * 100).Unix()
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = &dto.AuthTokenClaim{
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
		Username: domain,
	}
	tokenString, err = token.SignedString([]byte("123123asdfghj456456"))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

// AuthenticationHandler func generate token for login
func AuthenticationHandler(w http.ResponseWriter, req *http.Request) {
	var tmpuser dto.JwtUser
	err := json.NewDecoder(req.Body).Decode(&tmpuser)
	if err != nil {
		fmt.Println(err)
	}
	expiresAt := time.Now().Add(time.Minute * 100).Unix()
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = &dto.AuthTokenClaim{
		StandardClaims: &jwt.StandardClaims{
			ExpiresAt: expiresAt,
		},
		Username: tmpuser.Username,
	}
	tokenString, err := token.SignedString([]byte("123123asdfghj456456"))
	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(dto.AuthToken{
		Token:     tokenString,
		TokenType: "Bearer",
		ExpiresIn: expiresAt,
	})
}

// MiddlewareFunc is the func checking token valid or invalid
func MiddlewareFunc(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		fmt.Println("middle ware")
		authorizationHeader := req.Header.Get("authorization")
		if authorizationHeader != "" {
			bearerToken := strings.Split(authorizationHeader, " ")
			if len(bearerToken) == 2 {
				token, err := jwt.Parse(bearerToken[1], func(token *jwt.Token) (interface{}, error) {
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("There was an error")
					}
					return []byte("123123asdfghj456456"), nil
				})
				if err != nil {
					json.NewEncoder(w).Encode(dto.ResponseMsg{Message: "Invalid authorization token"})
					return
				}
				if token.Valid {
					next.ServeHTTP(w, req)
				} else {
					json.NewEncoder(w).Encode(dto.ResponseMsg{StatusCode: "400", ReasonCode: "403", Message: "Invalid authorization token"})
					return
				}
			} else {
				json.NewEncoder(w).Encode(dto.ResponseMsg{StatusCode: "400", ReasonCode: "403", Message: "Invalid authorization token"})
				return
			}

		} else {
			json.NewEncoder(w).Encode(dto.ResponseMsg{StatusCode: "400", ReasonCode: "403", Message: "An authorization header is required"})
			return
		}
		//	next.ServeHTTP(w, req)
	})
}

// Users func test middleware
// func Users(w http.ResponseWriter, req *http.Request) {
// 	decoded := context.Get(req, "decoded")
// 	var user dto.User
// 	mapstructure.Decode(decoded.(jwt.MapClaims), &user)
// 	json.NewEncoder(w).Encode(user)
// }
