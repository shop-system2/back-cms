package adaptermodel

import "github.com/dgrijalva/jwt-go"

// JWTTokenClaim model
type JWTTokenClaim struct {
	*jwt.StandardClaims
	UserID uint32
	Domain string
}
