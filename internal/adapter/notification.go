package adapter

import (
	"context"
	"errors"
	"os"
	"shop-back-cms/config"
	"shop-back-cms/internal/dto"

	kafkaHelper "shop-back-cms/internal/helper/kafka"

	"go-libs/logger"

	vonage "github.com/vonage/vonage-go-sdk"
)

type (
	NotificationAdapter interface {
		SendSMS(ctx context.Context, req *dto.SendSMSRequestDTO) error
		SendEmail(ctx context.Context, message string) error
	}

	notificationAdapter struct {
		cfg           *config.Config
		kafkaProducer kafkaHelper.KafkaProducer
	}
)

func NewNotificationAdapter(
	cfg *config.Config,
	kafkaProducer kafkaHelper.KafkaProducer,
) NotificationAdapter {
	return &notificationAdapter{
		cfg:           cfg,
		kafkaProducer: kafkaProducer,
	}
}
func (a *notificationAdapter) SendEmail(ctx context.Context, message string) error {
	// producerName, topic, key string, value interface{}
	// err := a.kafkaProducer.Send(ctx, a.cfg.Kafka.ProducerName, a.cfg.Kafka.Topics[0], "DUCNP-DEV", message)
	// if err != nil {
	// 	fmt.Errorf("kafka sending err %v", err)
	// }
	return nil
}
func (a *notificationAdapter) SendSMS(ctx context.Context, req *dto.SendSMSRequestDTO) error {
	apiKey := os.Getenv("SMS_API_KEY")
	secretKey := os.Getenv("SMS_SECRET_KEY")
	if apiKey == "" || secretKey == "" {
		logger.GlobaLogger.Errorf("%v, adapter.notificationAdapter/SendSMS:  Error while get apiKey and secretKey to env", req.TransactionID)
		return errors.New("get env sms error")
	}
	auth := vonage.CreateAuthFromKeySecret(apiKey, secretKey)
	smsClient := vonage.NewSMSClient(auth)
	response, errResp, err := smsClient.Send(a.cfg.SMSNotification.OriginPhoneNumber,
		req.PhoneNumber, req.Messages, vonage.SMSOpts{})
	if err != nil {
		logger.GlobaLogger.Errorf("%v, adapter.notificationAdapter/SendSMS:  Error while call vanage sender sms, with err", req.TransactionID, err)
		return err
	}
	if response.Messages[0].Status == "0" {
		logger.GlobaLogger.Infof("%v, adapter.notificationAdapter/SendSMS:  Sender sms successfully, Account Balance: %v", req.TransactionID, response.Messages[0].RemainingBalance)
		return nil
	}
	logger.GlobaLogger.Errorf("%v, adapter.notificationAdapter/SendSMS:  Error while call vanage sender sms, with err", req.TransactionID, errResp.Messages[0].ErrorText)
	return errors.New("error sender sms")
}
