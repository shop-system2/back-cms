package api

import (
	"encoding/json"
	"net/http"
	pb "shop-back-cms/genproto"

	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/usecase"
)

type (
	objResponse struct {
		Code    int
		Message string
	}

	jwtResponse struct {
		Code    int
		Message string
		Jwt     string
	}
)

// ResponseFail sdf
func ResponseFail(writer http.ResponseWriter, code int, message string) {
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(objResponse{code, message})
}

// ResponseSuccess sdf
func ResponseSuccess(writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(objResponse{200, "sescuessfull"})
}

// ResponseJwt sdf
func ResponseJwt(writer http.ResponseWriter, jwt string) {
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(jwtResponse{200, "", jwt})
}

/**
 *** Using proto Api
 */
type (
	// API all func

	api struct {
		productUsecase          usecase.ProductUsecase
		orderUsecase            usecase.OrderUsecase
		commentUsecase          usecase.CommentUsecase
		categoryUsecase         usecase.CategoryUsecase
		vendorUsecase           usecase.VendorUsecase
		clientUsecase           usecase.ClientUsecase
		reportUsecase           usecase.ReportUsecase
		managementSystemUsecase usecase.ManagementSystemUsecase
		pbCopier                helper.PbCopier
		// serviceIM               imClient.ServerAPIClient
		notificationUsecase usecase.NotificationUsecase
	}
)

// NewAPI create instance for  API
func NewAPI(
	pbCopier helper.PbCopier,
	productUsecase usecase.ProductUsecase,
	orderUsecase usecase.OrderUsecase,
	commentUsecase usecase.CommentUsecase,
	categoryUsecase usecase.CategoryUsecase,
	vendorUsecase usecase.VendorUsecase,
	clientUsecase usecase.ClientUsecase,
	reportUsecase usecase.ReportUsecase,
	managementSystemUsecase usecase.ManagementSystemUsecase,
	notificationUsecase usecase.NotificationUsecase,

) pb.APIServer {
	return &api{
		pbCopier:                pbCopier,
		productUsecase:          productUsecase,
		orderUsecase:            orderUsecase,
		commentUsecase:          commentUsecase,
		categoryUsecase:         categoryUsecase,
		vendorUsecase:           vendorUsecase,
		clientUsecase:           clientUsecase,
		reportUsecase:           reportUsecase,
		managementSystemUsecase: managementSystemUsecase,
		notificationUsecase:     notificationUsecase,
	}
}
