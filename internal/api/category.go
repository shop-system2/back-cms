package api

import (
	"context"
	"go-libs/logger"
	pb "shop-back-cms/genproto"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
)

func (a *api) PostNewCategory(ctx context.Context,
	req *pb.PostNewCategoryRequest) (res *pb.PostNewCategoryResponse, err error) {
	var (
		reqDTO = &dto.PostNewCategoryRequestDTO{}
	)
	res = &pb.PostNewCategoryResponse{}
	a.pbCopier.FromPb(reqDTO, req)
	reqDTO.TransactionID = req.TransactionId
	_, err = a.categoryUsecase.PostNewCategory(ctx, reqDTO)
	if err != nil {
		logger.GlobaLogger.Errorf("api.category/PostNewCategory Failed add new category", err)
		return &pb.PostNewCategoryResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: common.ParseError(err).Message(),
		}, nil
	}
	res.StatusCode = common.ACCEPT
	return res, nil
}

func (a *api) GetListCategory(ctx context.Context,
	req *pb.GetListCategoryRequest) (res *pb.GetListCategoryResponse, err error) {
	var (
		reqDTO = &dto.GetListCategoryRequestDTO{}
		resDTO = &dto.GetListCategoryResponseDTO{}
	)
	res = &pb.GetListCategoryResponse{}
	reqDTO.TransactionID = req.TransactionId
	reqDTO.IsActive = req.IsActive
	resDTO, err = a.categoryUsecase.GetListCategory(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.category/GetListCategory Failed get list category, err=%v", req.TransactionId, errMessage)
		return &pb.GetListCategoryResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: errMessage,
		}, nil
	}
	a.pbCopier.ToPb(res, resDTO)
	res.StatusCode = common.ACCEPT
	return res, nil
}

func (a *api) PutUpdateCategory(ctx context.Context,
	req *pb.PutUpdateCategoryRequest) (res *pb.PutUpdateCategoryResponse, err error) {
	var (
		reqDTO = &dto.PutUpdateCategoryRequestDTO{}
		resDTO *dto.PutUpdateCategoryResponseDTO
	)
	res = &pb.PutUpdateCategoryResponse{}
	a.pbCopier.FromPb(reqDTO, req)
	resDTO, err = a.categoryUsecase.PutUpdateCategory(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.category/PutUpdateCategory Failed update category, err=%v", req.TransactionId, errMessage)
		return &pb.PutUpdateCategoryResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: errMessage,
		}, nil
	}
	a.pbCopier.ToPb(res, resDTO)
	res.StatusCode = common.ACCEPT
	return res, nil
}

func (a *api) DeleteCategory(ctx context.Context,
	req *pb.DeleteCategoryRequest) (res *pb.DeleteCategoryResponse, err error) {
	var (
		reqDTO = &dto.DeleteCategoryRequestDTO{}
		resDTO *dto.DeleteCategoryResponseDTO
	)
	res = &pb.DeleteCategoryResponse{}
	reqDTO.Id = req.Id
	// reqDTO.TransactionID = req.TransactionID

	resDTO, err = a.categoryUsecase.DeleteCategory(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.category/DeleteCategory Failed delete category, err=%v", req.TransactionId, errMessage)
		return &pb.DeleteCategoryResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: errMessage,
		}, nil
	}
	a.pbCopier.ToPb(res, resDTO)
	res.StatusCode = common.ACCEPT
	return res, nil
}

func (a *api) DoActiveCategory(ctx context.Context,
	req *pb.DoActiveCategoryRequest) (resp *pb.DoActiveCategoryResponse, err error) {
	resp = &pb.DoActiveCategoryResponse{}
	reqDTO := &dto.DoActiveCategoryRequestDTO{
		CategoryID:    req.CategoryId,
		IsActive:      req.Active,
		TransactionID: req.TransactionId,
	}
	err = a.categoryUsecase.DoActiveCategory(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.category/DoActiveCategory Error while active/unactive Category, err=%v", req.TransactionId, errMessage)
		resp.StatusCode = common.FAILED
		resp.ReasonCode = common.ParseError(err).Code()
		resp.ReasonMessage = errMessage
		return resp, nil
	}
	resp.StatusCode = common.ACCEPT
	return resp, nil
}

func (a *api) GetCategoryDetail(ctx context.Context,
	req *pb.GetCategoryDetailRequest) (resp *pb.GetCategoryDetailResponse, err error) {
	resp = &pb.GetCategoryDetailResponse{}
	reqDTO := &dto.GetCategoryDetailRequestDTO{
		CategoryID:    (req.Id),
		TransactionID: req.TransactionId,
	}
	category, err := a.categoryUsecase.GetCategoryDetail(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.category/DoActiveCategory Error while get category detail, err=%v", req.TransactionId, errMessage)
		resp.StatusCode = common.FAILED
		resp.ReasonCode = common.ParseError(err).Code()
		resp.ReasonMessage = errMessage
		return resp, nil
	}
	CategoryDetal := &pb.Category{
		Name:      category.Name,
		Code:      category.Code,
		CreatedAt: category.CreatedAt,
		UpdatedAt: category.UpdatedAt,
		// ID:          (category.ID),
		IsActive:    category.IsActive,
		Description: category.Description,
	}
	resp.StatusCode = common.ACCEPT
	resp.Category = CategoryDetal
	return resp, nil
}

func (a *api) SearchCategory(ctx context.Context,
	req *pb.SearchCategoryRequest) (res *pb.SearchCategoryResponse, err error) {
	var (
		reqDTO = &dto.SearchCategoryRequestDTO{}
	)
	a.pbCopier.FromPb(reqDTO, req)
	resDTO, err := a.categoryUsecase.SearchCategory(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.category/SearchCategory Failed get list category, err=%v", req.TransactionId, errMessage)
		return &pb.SearchCategoryResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: errMessage,
		}, nil
	}
	res = &pb.SearchCategoryResponse{}
	a.pbCopier.ToPb(res, resDTO)
	res.StatusCode = common.ACCEPT
	return res, nil
}
