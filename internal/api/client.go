package api

// func (a *api) PostNewClient(ctx context.Context,
// 	req *pb.PostNewClientRequest) (resp *pb.PostNewClientResponse, err error) {
// 	var (
// 		reqDTO = &dto.PostNewClientRequestDTO{}
// 		resDTO *dto.PostNewClientResponseDTO
// 	)
// 	resp = &pb.PostNewClientResponse{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.clientUsecase.PostNewClient(ctx, reqDTO)
// 	if err != nil {
// 		util.SetErrorToResponse(err, resp)
// 		logger.GlobaLogger.Errorf("api.client/PostNewClient Failed add new client, err=%v", resp.ReasonMessage)
// 		return resp, nil
// 	}
// 	a.pbCopier.ToPb(resp, resDTO)
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) GetListClient(ctx context.Context,
// 	req *pb.GetListClientRequest) (res *pb.GetListClientResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetListClientRequestDTO{}
// 		resDTO = &dto.GetListClientResponseDTO{}
// 	)
// 	reqDTO.TransactionID = req.TransactionID
// 	resDTO, err = a.clientUsecase.GetListClient(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.client/GetListClient Failed get list client, err=%v", req.TransactionID, errMessage)
// 		return &pb.GetListClientResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	res = &pb.GetListClientResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }
// func (a *api) DoActiveClient(ctx context.Context,
// 	req *pb.DoActiveClientRequest) (resp *pb.DoActiveClientResponse, err error) {
// 	resp = &pb.DoActiveClientResponse{}
// 	reqDTO := &dto.DoActiveClientRequestDTO{
// 		ClientID: req.ClientId,
// 		IsActive: req.Active,
// 	}
// 	err = a.clientUsecase.DoActiveClient(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.client/DoActiveClient Error while active/unactive Client, err=%v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) SearchClient(ctx context.Context,
// 	req *pb.SearchClientRequest) (res *pb.SearchClientResponse, err error) {
// 	var (
// 		reqDTO = &dto.SearchClientRequestDTO{}
// 		resDTO = &dto.GetListClientResponseDTO{}
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.clientUsecase.SearchClient(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.client/SearchClient Failed get list client, err=%v", req.TransactionID, errMessage)
// 		return &pb.SearchClientResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	res = &pb.SearchClientResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) GetClientDetail(ctx context.Context,
// 	req *pb.GetClientDetailRequest) (resp *pb.GetClientDetailResponse, err error) {
// 	resp = &pb.GetClientDetailResponse{}
// 	reqDTO := &dto.GetClientDetailRequestDTO{
// 		ID:            uint32(req.ID),
// 		TransactionID: req.TransactionId,
// 	}
// 	client, err := a.clientUsecase.GetClientDetail(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("api.client/GetClientDetail Failed to get  detail client: err= %v", errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	clientDetail := &pb.Client{
// 		Name:        client.Name,
// 		AccountBank: client.AccountBank,
// 		CreatedAt:   client.CreatedAt,
// 		UpdatedAt:   client.UpdatedAt,
// 		ID:          client.ID,
// 		Email:       client.Email,
// 		Address:     client.Address,
// 		PhoneNumber: client.PhoneNumber,
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	resp.Client = clientDetail
// 	return resp, nil
// }

// func (a *api) PutUpdateClient(ctx context.Context,
// 	req *pb.PutUpdateClientRequest) (resp *pb.PutUpdateClientResponse, err error) {
// 	var (
// 		reqDTO = &dto.PutUpdateClientRequestDTO{}
// 	)
// 	resp = &pb.PutUpdateClientResponse{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	_, err = a.clientUsecase.PutUpdateClient(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.client/PutUpdateClient Failed update client: err= %v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) DeleteClient(ctx context.Context,
// 	req *pb.DeleteClientRequest) (res *pb.DeleteClientResponse, err error) {
// 	var (
// 		reqDTO = &dto.DeleteClientRequestDTO{}
// 		resDTO *dto.DeleteClientResponseDTO
// 	)
// 	reqDTO.ID = req.Id
// 	reqDTO.TransactionID = req.TransactionId
// 	resDTO, err = a.clientUsecase.DeleteClient(ctx, reqDTO)
// 	if err != nil {
// 		errMes := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.client/DeleteClient Failed delete client,err=%v", req.TransactionId, errMes)
// 		return &pb.DeleteClientResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMes,
// 		}, nil
// 	}
// 	res = &pb.DeleteClientResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }
