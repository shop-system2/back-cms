package api

// func (a *api) PostNewComment(ctx context.Context,
// 	req *pb.PostNewCommentRequest) (res *pb.PostNewCommentResponse, err error) {
// 	var (
// 		reqDTO = &dto.PostNewCommentRequestDTO{}
// 		resDTO *dto.PostNewCommentResponseDTO
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.commentUsecase.PostNewComment(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("api.comment/PostNewComment Failed add new comment", err)
// 		return &pb.PostNewCommentResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	res = &pb.PostNewCommentResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) GetListComment(ctx context.Context,
// 	req *pb.GetListCommentRequest) (res *pb.GetListCommentResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetListCommentRequestDTO{}
// 		resDTO = &dto.GetListCommentResponseDTO{}
// 	)
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.UserID = req.UserId
// 	reqDTO.ProductId = req.ProductId
// 	reqDTO.IsActive = req.IsActive
// 	reqDTO.CurrentPage = req.CurrentPage
// 	reqDTO.Limit = req.Limit
// 	resDTO, err = a.commentUsecase.GetListComment(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.comment/GetListComment Failed get list comment, err=%v", req.TransactionID, errMessage)
// 		return &pb.GetListCommentResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	res = &pb.GetListCommentResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) DoActiveComment(ctx context.Context,
// 	req *pb.DoActiveCommentRequest) (resp *pb.DoActiveCommentResponse, err error) {
// 	resp = &pb.DoActiveCommentResponse{}
// 	reqDTO := &dto.DoActiveCommentRequestDTO{
// 		CommentID: req.CommentId,
// 		IsActive:  req.Active,
// 	}
// 	err = a.commentUsecase.DoActiveComment(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.comment/DoActiveComment Error while active/unactive Comment, err=%v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) SearchComment(ctx context.Context,
// 	req *pb.SearchCommentRequest) (res *pb.SearchCommentResponse, err error) {
// 	var (
// 		reqDTO = &dto.SearchCommentRequestDTO{}
// 		resDTO = &dto.GetListCommentResponseDTO{}
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.commentUsecase.SearchComment(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.comment/SearchComment Failed get list comment, err=%v", req.TransactionID, errMessage)
// 		return &pb.SearchCommentResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	res = &pb.SearchCommentResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) GetCommentDetail(ctx context.Context,
// 	req *pb.GetCommentDetailRequest) (resp *pb.GetCommentDetailResponse, err error) {
// 	resp = &pb.GetCommentDetailResponse{}
// 	reqDTO := &dto.GetCommentDetailRequestDTO{
// 		ID:            uint32(req.ID),
// 		TransactionID: req.TransactionId,
// 	}
// 	commentResp, err := a.commentUsecase.GetCommentDetail(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("api.comment/GetCommentDetail Failed to get  detail comment: err= %v", errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	commentDetail := &pb.Comment{
// 		Id:        commentResp.ID,
// 		Title:     commentResp.Title,
// 		Content:   commentResp.Content,
// 		ProductId: int64(commentResp.ProductID),
// 		UserId:    int64(commentResp.UserID),
// 		UpdatedAt: commentResp.UpdatedAt,
// 		CreatedAt: commentResp.CreatedAt,
// 		IsActive:  commentResp.IsActive,
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	resp.Comment = commentDetail
// 	return resp, nil
// }

// func (a *api) PutUpdateComment(ctx context.Context,
// 	req *pb.PutUpdateCommentRequest) (resp *pb.PutUpdateCommentResponse, err error) {
// 	var (
// 		reqDTO = &dto.PutUpdateCommentRequestDTO{}
// 	)
// 	resp = &pb.PutUpdateCommentResponse{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	_, err = a.commentUsecase.PutUpdateComment(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.comment/PutUpdateComment Failed update comment: err= %v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) DeleteComment(ctx context.Context,
// 	req *pb.DeleteCommentRequest) (res *pb.DeleteCommentResponse, err error) {
// 	var (
// 		reqDTO = &dto.DeleteCommentRequestDTO{}
// 		resDTO *dto.DeleteCommentResponseDTO
// 	)
// 	reqDTO.Id = int(req.Id)
// 	reqDTO.TransactionID = req.TransactionID
// 	resDTO, err = a.commentUsecase.DeleteComment(ctx, reqDTO)
// 	if err != nil {
// 		errMes := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.comment/DeleteComment Failed delete comment,err=%v", req.TransactionID, errMes)
// 		return &pb.DeleteCommentResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMes,
// 		}, nil
// 	}
// 	res = &pb.DeleteCommentResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// /*
// REVIEW
// */
// func (a *api) GetReviewCommonClient(ctx context.Context,
// 	req *pb.GetReviewCommonClientRequest) (resp *pb.GetReviewCommonClientResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetReviewCommonClientRequestDTO{}
// 		resDTO = &dto.GetReviewCommonClientResponseDTO{}
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.commentUsecase.GetReviewCommonClient(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		return &pb.GetReviewCommonClientResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	resp = &pb.GetReviewCommonClientResponse{}
// 	a.pbCopier.ToPb(resp, resDTO)
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }
