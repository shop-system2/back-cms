package api

import (
	"context"
	"encoding/json"
	"fmt"
	"shop-back-cms/config"
	"shop-back-cms/internal/helper/kafka"
	funcLogger "shop-back-cms/internal/logger"
	"shop-back-cms/internal/logger/model"
	"time"

	"github.com/Shopify/sarama"
	"go.uber.org/zap"
)

type ConsumerAPI interface {
	InitConnection() error
}

type kafkaConsumerAPI struct {
	cfg                *config.Config
	kafkaConsumerGroup *kafka.KafkaConsumerGroup
}

var channelConsumer = map[string]chan *sarama.ConsumerMessage{
	"LOGGER": make(chan *sarama.ConsumerMessage),
}

// NewKafkaConsumerAPI creates an instance
func NewKafkaConsumerAPI(
	cfg *config.Config,
) ConsumerAPI {
	for key, value := range channelConsumer {
		go func(key string, value chan *sarama.ConsumerMessage) {
			for {
				message := <-value
				queueKey := string(message.Key)
				fmt.Println("xxxxxxxxx")
				time.Sleep(500 * time.Millisecond)
				switch queueKey {
				case "LOGGER":
					writeLogger(message)
				}
			}
		}(key, value)
	}
	return &kafkaConsumerAPI{
		cfg: cfg,
	}
}

func (a *kafkaConsumerAPI) InitConnection() error {
	ctx := context.Background()
	property := kafka.PropertyKafkaConsumer{
		Broker:   a.cfg.Kafka.Brokers,
		Topic:    a.cfg.Kafka.Topics,
		Group:    a.cfg.Kafka.Group,
		Version:  a.cfg.Kafka.Version,
		IsOldest: a.cfg.Kafka.IsOldest,
		Strategy: a.cfg.Kafka.RebalanceStrategy,
	}
	kafkaConsumerGroup, err := kafka.InitKafkaConsumer(ctx, property)
	if err != nil {
		zap.S().Errorw(fmt.Sprintf("Error while init connection to brokers: %v", zap.Error(err)))
		return err
	}
	zap.S().Info("Init consumergroup successfully")
	a.kafkaConsumerGroup = kafkaConsumerGroup
	go a.handleFromMessages()
	a.kafkaConsumerGroup.HandleCloseConsumeGroup()
	return nil
}

func (a *kafkaConsumerAPI) handleFromMessages() {
	for {
		message, ok := <-a.kafkaConsumerGroup.MessageCh
		if message != nil && ok {
			go a.messageHandlerFactory(message)
		}
	}
}

func (a *kafkaConsumerAPI) messageHandlerFactory(message *sarama.ConsumerMessage) {
	switch message.Topic {
	case a.cfg.Kafka.Topics[0]:
		fmt.Println("messageHandlerFactory", string(message.Key))
		channelConsumer["LOGGER"] <- message
	default:
		zap.S().Infof(`Invalid topic handler now!, Topic %s`, message.Topic)
	}
}

func writeLogger(message *sarama.ConsumerMessage) error {
	// fmt.Println("log123123:", string(message.Value))

	log := &model.LoggerCommon{}
	err := json.Unmarshal(message.Value, &log)
	if err != nil {
		return nil
	}
	// fmt.Println("AppName:", log.AppName)
	// fmt.Println("FileName:", log.FileName)
	// fmt.Println("Host:", log.Host)
	// fmt.Println("Level:", log.Level)
	// fmt.Println("Message:", log.Message)
	// fmt.Println("Namespace:", log.Namespace)
	// fmt.Println("RequestContent:", log.RequestContent)
	// fmt.Println("ResponseContent:", log.ResponseContent)
	// fmt.Println("Pod:", log.Pod)
	funcLogger.FuncLogger.ReadKafka(*log)
	return nil
}
