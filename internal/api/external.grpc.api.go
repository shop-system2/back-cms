package api

import (
	"bytes"
	"context"
	b64 "encoding/base64"
	"fmt"
	"image"
	"image/png"
	"log"
	"os"
	pb "shop-back-cms/genproto"
	util "shop-back-cms/internal/util"
)

func (a *api) UploadImageProduct(ctx context.Context, req *pb.UploadImageProductRequest) (res *pb.UploadImageProductResponse, err error) {
	fmt.Println("api/UploadImageProduct")
	res = &pb.UploadImageProductResponse{}
	res.StatusCode = "FAILED"
	sEnc := b64.StdEncoding.EncodeToString([]byte(req.Imagebase64))
	sDec, err := b64.StdEncoding.DecodeString(sEnc)
	if err != nil {
		fmt.Println(err)
		return res, nil
	}
	byteRed := bytes.NewReader((sDec))
	pngI, _, err := image.Decode(byteRed)

	if err != nil {
		fmt.Println(err)
		return res, nil
	}
	_ = pngI.Bounds()
	// fmt.Println("Png file", formatString, "created")

	//Encode from image format to writer
	random := util.NowAsUnixSecondNano()
	pngFilename := fmt.Sprintf("images/%v.png", random)

	f, err := os.OpenFile(pngFilename, os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		log.Fatal(err)
		return res, nil
	}
	defer f.Close()

	err = png.Encode(f, pngI)
	if err != nil {
		log.Fatal(err)
		return res, nil
	}
	res.StatusCode = "ACCEPT"
	res.FileName = pngFilename
	return res, nil
}
