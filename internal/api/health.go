package api

import (
	"context"
	pb "shop-back-cms/genproto"
	"shop-back-cms/internal/common"
)

func (a *api) CheckReadiness(ctx context.Context,
	req *pb.ReadinessRequest) (resp *pb.ReadinessResponse, err error) {
	return &pb.ReadinessResponse{
		StatusCode: common.ACCEPT,
	}, nil
}

func (a *api) CheckLiveness(ctx context.Context,
	req *pb.LivenessRequest) (resp *pb.LivenessResponse, err error) {
	return &pb.LivenessResponse{
		StatusCode: common.ACCEPT,
	}, nil
}
