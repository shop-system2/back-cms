package api

// import (
// 	"context"
// 	"log"
// 	"shop-back-cms/internal/common"
// 	"shop-back-cms/internal/dto"
// 	"shop-back-cms/pbcbo"

// 	"github.com/opentracing/opentracing-go/ext"
// )

// func (s *api) GetTransactionMonitoring(ctx context.Context,
// 	req *pbcbo.GetTransactionMonitoringRequest) (resp *pbcbo.GetTransactionMonitoringResponse, err error) {
// 	var (
// 		reqDTO = &dto.TransactionsMntoringRequestDTO{}
// 	)

// 	reqDTO.ChannelID = req.ChannelId
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.TimeInterval = req.TimeInterval
// 	respDTO, err := s.monitoringUsecase.GetTransactionsMonitoring(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to get list transaction monitoring, err: %v", req.TransactionID, err)
// 		return &pbcbo.GetTransactionMonitoringResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pbcbo.GetTransactionMonitoringResponse{}
// 	resp.Transactions = make([]*pbcbo.TransMonitoring, len(respDTO.Transaction))
// 	for i, item := range respDTO.Transaction {
// 		resp.Transactions[i] = &pbcbo.TransMonitoring{
// 			Id:              item.ID,
// 			TransactionDate: item.TransactionDate,
// 			ChannelName:     item.ChannelName,
// 			RespCode:        item.RespCode,
// 			Amount:          item.Amount,
// 			RefNumber:       item.RefNumber,
// 			RespText:        item.RespText,
// 		}
// 	}
// 	resp.Filters = make([]*pbcbo.TransactionMonitoringFilter, len(respDTO.Filters))
// 	for i, item := range respDTO.Filters {
// 		resp.Filters[i] = &pbcbo.TransactionMonitoringFilter{
// 			Text:  item.Text,
// 			Value: item.Value,
// 		}
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	resp.TotalTransaction = respDTO.Total
// 	resp.NumberSuccessTransaction = respDTO.Success
// 	resp.NumberFailTransaction = respDTO.Fail
// 	return resp, nil
// }

// func (s *api) GetListPhysicalChannel(ctx context.Context,
// 	req *pbcbo.GetListPhysicalChannelRequest) (resp *pbcbo.GetListPhysicalChannelResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/GetListPhysicalChannel", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		reqDTO = &dto.GetListPhysicalChannelRequestDTO{}
// 	)
// 	reqDTO.TransactionID = req.TransactionID
// 	respDTO, err := s.monitoringUsecase.GetListPhysicalChannel(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to get list physical channel, err: %v", req.TransactionID, err)
// 		return &pbcbo.GetListPhysicalChannelResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pbcbo.GetListPhysicalChannelResponse{}
// 	resp.PhysicalChannels = make([]*pbcbo.PhysicalChannel, len(respDTO))
// 	for i, item := range respDTO {
// 		resp.PhysicalChannels[i] = &pbcbo.PhysicalChannel{
// 			Id:                uint32(item.ID),
// 			ServerName:        item.ServerName,
// 			ServerId:          item.ServerOID,
// 			LastChanged:       item.LastChanged,
// 			ChannelId:         item.ChannelID,
// 			LastChangedRecord: int32(item.LastChangeRecord),
// 			LastConfirmed:     item.LastConfirmed,
// 			TprtStatus:        item.TrptStatus,
// 			ApplStatus:        item.ApplStatus,
// 		}
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// //  management redis
// func (s *api) GetListRedis(ctx context.Context,
// 	req *pbcbo.GetListRedisRequest) (resp *pbcbo.GetListRedisResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/GetListRedis", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		reqDTO = &dto.GetListRedisRequestDTO{}
// 	)
// 	reqDTO.Cursor = req.Cursor
// 	reqDTO.Pattern = req.Pattern
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.Count = req.Count
// 	respDTO, err := s.monitoringUsecase.GetListRedis(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to get redis, err: %v", req.TransactionID, err)
// 		return &pbcbo.GetListRedisResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	var redisData = make([]*pbcbo.RedisInformation, len(respDTO.Redises))
// 	for i, key := range respDTO.Redises {
// 		redisInfo := &pbcbo.RedisInformation{
// 			Key:    key.Key,
// 			Lenght: key.Lenght,
// 			Type:   key.Type,
// 		}
// 		redisData[i] = redisInfo
// 	}
// 	resp = &pbcbo.GetListRedisResponse{
// 		StatusCode: common.ACCEPT,
// 		Redis: &pbcbo.ManagamentRedis{
// 			Cursor:  respDTO.Cursor,
// 			Redises: redisData,
// 			IsGet:   respDTO.IsGet,
// 			Count:   respDTO.Count,
// 		},
// 	}
// 	return resp, nil
// }

// func (s *api) GetRedisDetail(ctx context.Context,
// 	req *pbcbo.GetRedisDetailRequest) (resp *pbcbo.GetRedisDetailResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/GetRedisDetail", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		reqDTO = &dto.GetRedisDetailRequestDTO{}
// 	)

// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	respDTO, err := s.monitoringUsecase.GetRedisDetail(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to redis detail by key, err: %v", req.TransactionID, err)
// 		return &pbcbo.GetRedisDetailResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	redisInfo := &pbcbo.RedisInformation{
// 		Type:       respDTO.Type,
// 		Lenght:     respDTO.Lenght,
// 		SizeData:   respDTO.SizeData,
// 		TimeExpire: respDTO.TimeExpire,
// 		Key:        req.Key,
// 	}
// 	resp = &pbcbo.GetRedisDetailResponse{
// 		StatusCode: common.ACCEPT,
// 		Redis:      redisInfo,
// 	}
// 	return resp, nil
// }

// func (s *api) DeleteRedisByKey(ctx context.Context,
// 	req *pbcbo.DeleteRedisByKeyRequest) (resp *pbcbo.DeleteRedisByKeyResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/DeleteRedisByKey", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		reqDTO = &dto.DeleteRedisByKeyRequestDTO{}
// 	)
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	err = s.monitoringUsecase.DeleteRedisByKey(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to delete redis by key, err: %v", req.TransactionID, err)
// 		return &pbcbo.DeleteRedisByKeyResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pbcbo.DeleteRedisByKeyResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) CheckKeyExists(ctx context.Context,
// 	req *pbcbo.CheckKeyExistsRequest) (resp *pbcbo.CheckKeyExistsResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/CheckKeyExists", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		reqDTO = &dto.CheckKeyExistsRequestDTO{}
// 	)
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	isExists, err := s.monitoringUsecase.CheckKeyExists(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to check exists redis by key, err: %v", req.TransactionID, err)
// 		return &pbcbo.CheckKeyExistsResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pbcbo.CheckKeyExistsResponse{
// 		StatusCode: common.ACCEPT,
// 		IsExists:   isExists,
// 	}
// 	return resp, nil
// }

// func (s *api) AddNewRedis(ctx context.Context,
// 	req *pbcbo.AddNewRedisRequest) (resp *pbcbo.AddNewRedisResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/AddNewRedis", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		reqDTO = &dto.AddNewRedisRequestDTO{}
// 	)
// 	reqDTO.Expiration = req.Expiration
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.Value = req.Value
// 	err = s.monitoringUsecase.AddNewRedis(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to add new redis by key, err: %v", req.TransactionID, err)
// 		return &pbcbo.AddNewRedisResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pbcbo.AddNewRedisResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) SetTimeout(ctx context.Context,
// 	req *pbcbo.SetTimeoutRequest) (resp *pbcbo.SetTimeoutResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/SetTimeout", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		reqDTO = &dto.SetTimeoutRequestDTO{}
// 	)
// 	reqDTO.Expiration = req.Expiration
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	err = s.monitoringUsecase.SetTimeout(ctx, reqDTO)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Failed to set timeout redis by key, err: %v", req.TransactionID, err)
// 		return &pbcbo.SetTimeoutResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pbcbo.SetTimeoutResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) RenameRedisByKey(ctx context.Context,
// 	req *pbcbo.RenameRedisByKeyRequest) (resp *pbcbo.RenameRedisByKeyResponse, err error) {
// 	span := jaeger.Start(ctx, ">api.monitoring/RenameRedisByKey", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	return resp, nil
// }
