package api

import (
	"context"
	pb "shop-back-cms/genproto"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
)

// func (a *api) GetCartItemDetail(ctx context.Context,
// 	req *pb.GetCartItemDetailRequest) (resp *pb.GetCartItemDetailResponse, err error) {
// 	resp = &pb.GetCartItemDetailResponse{}
// 	reqDTO := &dto.GetCartItemDetailRequestDTO{
//		CartItemID:    req.CardItemId,
// 		TransactionID: req.TransactionID,
// 	}
// 	respDTO, err := a.orderUsecase.GetCartItemDetail(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.order/GetCartItemDetail Failed to get cart item detail, err=%v", req.TransactionID, errMessage)
// 		return &pb.GetCartItemDetailResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	a.pbCopier.ToPb(resp, respDTO)
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) CreateOrderItem(ctx context.Context,
// 	req *pb.CreateOrderItemRequest) (resp *pb.CreateOrderItemResponse, err error) {
// 	resp = &pb.CreateOrderItemResponse{}
// 	reqDTO := &dto.OrderItemRequestDTO{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	err = a.orderUsecase.PostNewOrderItem(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.order/CreateOrderItem Failed to add new order item, err=%v", req.TransactionID, errMessage)
// 		return &pb.CreateOrderItemResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, err
// }

// func (a *api) CreateOrder(ctx context.Context,
// 	req *pb.CreateOrderRequest) (resp *pb.CreateOrderResponse, err error) {
// 	resp = &pb.CreateOrderResponse{}
// 	reqDTO := &dto.OrderRequestDTO{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	err = a.orderUsecase.PostNewOrder(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.order/CreateOrder Failed to ad new order, err=%v", req.TransactionID, errMessage)
// 		return &pb.CreateOrderResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, err
// }

// func (a *api) CreateCart(ctx context.Context,
// 	req *pb.CreateCartRequest) (resp *pb.CreateCartResponse, err error) {
// 	resp = &pb.CreateCartResponse{}
// 	reqDTO := &dto.CartRequestDTO{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	err = a.orderUsecase.PostNewCart(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.order/CreateCart Failed to add new card, err=%v", req.TransactionID, errMessage)
// 		return &pb.CreateCartResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, err
// }

// func (a *api) CreateCartItem(ctx context.Context,
// 	req *pb.CreateCartItemRequest) (resp *pb.CreateCartItemResponse, err error) {
// 	resp = &pb.CreateCartItemResponse{}
// 	reqDTO := &dto.CartItemRequestDTO{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	err = a.orderUsecase.PostNewCartItem(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.order/CreateCartItem Failed to add new card item, err=%v", req.TransactionID, errMessage)
// 		return &pb.CreateCartItemResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, err
// }

func (a *api) GetListOrder(ctx context.Context,
	req *pb.GetlistOrderRequest) (res *pb.GetlistOrderResponse, err error) {
	var (
		reqDTO = &dto.GetListOrderRequestDTO{}
	)
	a.pbCopier.FromPb(reqDTO, req)
	resDTO, err := a.orderUsecase.GetListOrder(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		// logger.GlobaLogger.Errorf("%v, api.order/GetListOrder Failed get list order, err=%v", req.TransactionID, errMessage)
		return &pb.GetlistOrderResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: errMessage,
		}, nil
	}
	res = &pb.GetlistOrderResponse{}
	a.pbCopier.ToPb(res, resDTO)
	res.StatusCode = common.ACCEPT
	return res, nil
}

func (a *api) GetListCart(ctx context.Context,
	req *pb.GetListCartRequest) (res *pb.GetListCartResponse, err error) {
	var (
		reqDTO = &dto.GetListProductRequestDTO{}
		resDTO = &dto.GetListProductResponseDTO{}
	)
	// reqDTO.TransactionID = req.TransactionID
	resDTO, err = a.productUsecase.GetListProduct(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		// logger.GlobaLogger.Errorf("%v, api.order/GetListCart Failed get list cart, err=%v", req.TransactionID, errMessage)
		return &pb.GetListCartResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: errMessage,
		}, nil
	}
	res = &pb.GetListCartResponse{}
	a.pbCopier.ToPb(res, resDTO)
	res.StatusCode = common.ACCEPT
	return res, nil
}
