package api

import (
	"context"
	"go-libs/logger"
	pb "shop-back-cms/genproto"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
)

func (a *api) ProductCheckSku(ctx context.Context,
	req *pb.ProductCheckSkuRequest) (resp *pb.ProductCheckSkuResponse, err error) {
	resp = &pb.ProductCheckSkuResponse{}

	reqDTO := &dto.ProductCheckSkuRequestDTO{
		TransactionID: req.TransactionId,
		Sku:           req.Sku,
	}
	err = a.productUsecase.ProductCheckSku(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.product/ProductCheckSku Failed to check sku, err=%v", req.TransactionId, errMessage)
		return &pb.ProductCheckSkuResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: common.ParseError(err).Message(),
		}, nil
	}
	resp.StatusCode = common.ACCEPT
	return resp, err
}

// func (a *api) ProductQuantity(ctx context.Context,
// 	req *pb.ProductQuantityRequest) (resp *pb.ProductQuantityResponse, err error) {
// 	resp = &pb.ProductQuantityResponse{}

// 	reqDTO := &dto.ProductQuantityRequestDTO{
// 		TransactionID: req.TransactionID,
// 		ProductId:     req.ProductId,
// 		Number:        req.Number,
// 		Type:          req.Type,
// 	}
// 	err = a.productUsecase.ProductQuantity(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.product/ProductQuantity Failed to check sku, err=%v", req.TransactionID, errMessage)
// 		return &pb.ProductQuantityResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, err
// }

// func (a *api) SearchProduct(ctx context.Context,
// 	req *pb.SearchProductRequest) (res *pb.SearchProductResponse, err error) {
// 	// todo process
// 	var (
// 		reqDTO = &dto.SearchProductRequestDTO{}
// 		resDTO = &dto.GetListProductResponseDTO{}
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.productUsecase.SearchProduct(ctx, reqDTO)
// 	if err != nil {
// 		res = &pb.SearchProductResponse{
// 			StatusCode: "FAILED",
// 		}
// 		return res, nil
// 	}
// 	res = &pb.SearchProductResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	return res, nil
// }

func (a *api) PostNewProduct(ctx context.Context,
	req *pb.PostNewProductRequest) (res *pb.PostNewProductResponse, err error) {
	var (
		reqDTO = &dto.PostNewProductRequestDTO{}
	)
	a.pbCopier.FromPb(reqDTO, req)
	reqDTO.TransactionID = req.TransactionId
	reqDTO.Tag = req.Tag.String()
	reqDTO.StatusOfProduct = req.StatusOfProduct.String()
	_, err = a.productUsecase.PostNewProduct(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.product/PostNewProduct Failed add new product, err=%v", req.TransactionId, errMessage)
		return &pb.PostNewProductResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: common.ParseError(err).Message(),
		}, nil
	}
	res = &pb.PostNewProductResponse{}
	res.StatusCode = common.ACCEPT
	return res, nil
}

func (a *api) GetListProduct(ctx context.Context,
	req *pb.GetListProductRequest) (res *pb.GetListProductResponse, err error) {
	var (
		reqDTO = &dto.GetListProductRequestDTO{}
		resDTO = &dto.GetListProductResponseDTO{}
	)
	reqDTO.TransactionID = req.TransactionId
	resDTO, err = a.productUsecase.GetListProduct(ctx, reqDTO)
	if err != nil {
		errMessage := common.ParseError(err).Message()
		logger.GlobaLogger.Errorf("%v, api.product/GetListProduct Failed get list product, err=%v", req.TransactionId, errMessage)
		return &pb.GetListProductResponse{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ParseError(err).Code(),
			ReasonMessage: errMessage,
		}, nil
	}
	res = &pb.GetListProductResponse{}
	a.pbCopier.ToPb(res, resDTO)
	res.StatusCode = common.ACCEPT
	return res, nil
}

// func (a *api) GetProductDetail(ctx context.Context,
// 	req *pb.GetProductDetailRequest) (resp *pb.GetProductDetailResponse, err error) {
// 	resp = &pb.GetProductDetailResponse{}
// 	reqDTO := &dto.GetProductDetailRequestDTO{
// 		Id:            uint32(req.ID),
// 		TransactionID: req.TransactionID,
// 	}
// 	product, err := a.productUsecase.GetProductDetail(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("api.product/GetProductDetail Failed to get  detail product: err= %v", errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	productDetail := &pb.Product{
// 		Name:         product.Name,
// 		CreatedAt:    product.CreatedAt,
// 		UpdatedAt:    product.UpdatedAt,
// 		Price:        int64(product.Price),
// 		Vote:         int64(product.Vote),
// 		CategoryName: product.CategoryName,
// 		Description:  product.Description,
// 		UpdatedBy:    product.UpdatedBy,
// 		CreatedBy:    product.CreatedBy,
// 		Quantity:     product.Quantity,
// 		Sku:          product.Sku,
// 		IsActive:     product.IsActive,
// 		CategoryId:   product.CategoryID,
// 		SellPrice:    int64(product.SellPrice),
// 		Id:           product.ID,
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	resp.Product = productDetail
// 	return resp, nil
// }

// func (a *api) PutUpdateProduct(ctx context.Context,
// 	req *pb.PutUpdateProductRequest) (resp *pb.PutUpdateProductResponse, err error) {
// 	var (
// 		reqDTO = &dto.PutUpdateProductRequestDTO{}
// 	)
// 	resp = &pb.PutUpdateProductResponse{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	_, err = a.productUsecase.PutUpdateProduct(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.product/PutUpdateProduct Failed update product: err= %v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) DeleteProduct(ctx context.Context,
// 	req *pb.DeleteProductRequest) (res *pb.DeleteProductResponse, err error) {
// 	var (
// 		reqDTO = &dto.DeleteProductRequestDTO{}
// 	)
// 	reqDTO.ID = req.Id
// 	reqDTO.TransactionID = req.TransactionID
// 	_, err = a.productUsecase.DeleteProduct(ctx, reqDTO)
// 	if err != nil {
// 		errMes := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.product/DeleteProduct Failed delete product,err=%v", req.TransactionID, errMes)
// 		return &pb.DeleteProductResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMes,
// 		}, nil
// 	}
// 	res = &pb.DeleteProductResponse{}
// 	// a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) DoActiveProduct(ctx context.Context,
// 	req *pb.DoActiveProductRequest) (resp *pb.DoActiveProductResponse, err error) {
// 	resp = &pb.DoActiveProductResponse{}
// 	reqDTO := &dto.DoActiveProductRequestDTO{
// 		ProductID: req.ProductId,
// 		IsActive:  req.Active,
// 	}
// 	err = a.productUsecase.DoActiveProduct(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.product/DoActiveProduct Error while active/unactive Product, err=%v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// /*
// 	api intruction
// */

// func (a *api) GetListIntruction(ctx context.Context,
// 	req *pb.GetListIntructionRequest) (res *pb.GetListIntructionResponse, err error) {
// 	span, ctx := opentracing.StartSpanFromContext(ctx, "api.GetListIntruction")
// 	defer span.Finish()
// 	var (
// 		reqDTO = &dto.GetListIntructionRequestDTO{}
// 		resDTO = &dto.GetListIntructionResponseDTO{}
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.productUsecase.GetListIntruction(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.intruction/GetListIntruction Failed get list intruction, err=%v", req.TransactionID, errMessage)
// 		return &pb.GetListIntructionResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	res = &pb.GetListIntructionResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) PostNewIntruction(ctx context.Context,
// 	req *pb.PostNewIntructionRequest) (res *pb.PostNewIntructionResponse, err error) {

// 	_, err = a.productUsecase.PostNewIntruction(ctx, req.Description, req.TransactionID)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.intruction/PostNewIntruction Failed add new intruction, err=%v", req.TransactionID, errMessage)
// 		return &pb.PostNewIntructionResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	res = &pb.PostNewIntructionResponse{}
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) PutUpdateIntruction(ctx context.Context,
// 	req *pb.PutUpdateIntructionRequest) (resp *pb.PutUpdateIntructionResponse, err error) {
// 	isActive := "0"
// 	if req.IsActive {
// 		isActive = "1"

// 	}
// 	var (
// 		reqDTO = &dto.PutUpdateIntructionRequestDTO{
// 			ID:            req.Id,
// 			Description:   req.Description,
// 			TransactionID: req.TransactionID,
// 			IsActive:      isActive,
// 		}
// 	)
// 	resp = &pb.PutUpdateIntructionResponse{}
// 	_, err = a.productUsecase.PutUpdateIntruction(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.intruction/PutUpdateIntruction Failed update intruction: err= %v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) DeleteIntruction(ctx context.Context,
// 	req *pb.DeleteIntructionRequest) (res *pb.DeleteIntructionResponse, err error) {
// 	var (
// 		reqDTO = &dto.DeleteIntructionRequestDTO{}
// 	)
// 	reqDTO.ID = req.Id
// 	reqDTO.TransactionID = req.TransactionID
// 	_, err = a.productUsecase.DeleteIntruction(ctx, reqDTO)
// 	if err != nil {
// 		errMes := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.intruction/DeleteIntruction Failed delete intruction,err=%v", req.TransactionID, errMes)
// 		return &pb.DeleteIntructionResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMes,
// 		}, nil
// 	}
// 	res = &pb.DeleteIntructionResponse{}
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) DoActiveIntruction(ctx context.Context,
// 	req *pb.DoActiveIntructionRequest) (resp *pb.DoActiveIntructionResponse, err error) {
// 	resp = &pb.DoActiveIntructionResponse{}
// 	reqDTO := &dto.DoActiveIntructionRequestDTO{
// 		IsActive:      req.Active,
// 		IntructionID:  req.IntructionID,
// 		TransactionID: req.TransactionID,
// 	}
// 	err = a.productUsecase.DoActiveIntruction(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.intruction/DoActiveIntruction Error while active/unactive intruction, err=%v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }
