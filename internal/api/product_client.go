package api

import (
	"context"
	pb "shop-back-cms/genproto"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	util "shop-back-cms/internal/util"
)

// func (a *api) GetProductPreferentail(ctx context.Context,
// 	req *pb.GetProductPreferentailRequest) (resp *pb.GetProductPreferentailResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetProductPreferentailClientRequestDTO{}
// 	)
// 	resp = &pb.GetProductPreferentailResponse{}
// 	reqDTO.TransactionID = req.TransactionID
// 	resDTO, err := a.productUsecase.GetProductPreferentail(ctx, reqDTO)
// 	if err != nil {
// 		util.SetErrorToResponse(err, resp)
// 		return resp, nil
// 	}
// 	for _, item := range resDTO {
// 		tag := &pb.ProductTagClient{
// 			New:   item.Tag.New,
// 			Promo: item.Tag.Promo,
// 			Best:  item.Tag.Best,
// 		}
// 		amount := &pb.ProductAmount{
// 			Price: item.Amount.Price,
// 			Sale:  item.Amount.Sell,
// 		}
// 		resp.ProductPreferentails = append(resp.ProductPreferentails, &pb.ProductPerferentail{
// 			Name:             item.Name,
// 			Tag:              tag,
// 			Amount:           amount,
// 			ProductID:        item.ID,
// 			Content:          item.Content,
// 			ImagesID:         item.ImageID,
// 			ShortDescription: item.ShortDescription,
// 		})
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

func (a *api) GetProductTrending(ctx context.Context,
	req *pb.GetProductTrendingRequest) (resp *pb.GetProductTrendingResponse, err error) {
	var (
		reqDTO = &dto.GetProductTrendingClientRequestDTO{}
	)
	resp = &pb.GetProductTrendingResponse{}
	a.pbCopier.FromPb(reqDTO, req)
	reqDTO.TransactionID = req.TransactionId
	resDTO, err := a.productUsecase.GetProductTrending(ctx, reqDTO)
	if err != nil {
		util.SetErrorToResponse(err, resp)
		return resp, nil
	}
	a.pbCopier.ToPb(resp, resDTO)
	resp.StatusCode = common.ACCEPT
	return resp, nil
}

// func (a *api) GetProductDetailClient(ctx context.Context,
// 	req *pb.GetProductDetailClientRequest) (resp *pb.GetProductDetailClientResponse, err error) {
// 	resp = &pb.GetProductDetailClientResponse{}
// 	reqDTO := &dto.GetProductDetailClientRequestDTO{}
// 	a.pbCopier.FromPb(reqDTO, req)
// 	product, err := a.productUsecase.GetProductDetailClient(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	a.pbCopier.ToPb(resp, product)
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }
