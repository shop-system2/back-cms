package api

// func (s *api) GetReport(ctx context.Context, req *emptypb.Empty) (res *pb.GetReportResponse, err error) {
// 	var (
// 		resDTO *dto.GetReportResponseDTO
// 	)

// 	resDTO, err = s.reportUsecase.GetReport(ctx)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to get report: ", zap.Error(err))
// 		return &pb.GetReportResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	res = &pb.GetReportResponse{}
// 	s.pbCopier.ToPb(res, resDTO)
// 	return res, nil
// }

// func (s *api) GetReportParameters(ctx context.Context, req *pb.GetReportParametersRequest) (
// 	res *pb.GetReportParametersResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetReportParametersRequestDTO{}
// 		resDTO *dto.GetReportParametersResponseDTO
// 	)

// 	s.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = s.reportUsecase.GetReportParameters(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to get report parameters: ", zap.Error(err))
// 		return &pb.GetReportParametersResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	res = &pb.GetReportParametersResponse{}
// 	s.pbCopier.ToPb(res, resDTO)
// 	return res, nil
// }

// func (s *api) GenerateReport(ctx context.Context,
// 	req *pb.GenerateReportRequest) (res *pb.GenerateReportResponse, err error) {
// 	var (
// 		reqDTO = &dto.GenerateReportRequestDTO{}
// 		resDTO *dto.GenerateReportResponseDTO
// 	)

// 	s.pbCopier.FromPb(reqDTO, req)
// 	// TODO
// 	ssa := true
// 	if ssa {
// 		resDTO, err = s.reportUsecase.GenerateReport(ctx, reqDTO)
// 	}
// 	if !ssa {
// 		resDTO, err = s.reportUsecase.GenerateDataReportDetail(ctx, reqDTO)
// 	}

// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to generate report file: ", zap.Error(err))
// 		return &pb.GenerateReportResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	res = &pb.GenerateReportResponse{
// 		TransactionID: req.TransactionID,
// 	}
// 	s.pbCopier.ToPb(res, resDTO)
// 	return res, nil
// }

// func (s *api) GetReportFile(ctx context.Context,
// 	req *pb.GetReportFileRequest) (res *pb.GetReportFileResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetReportFileRequestDTO{}
// 		resDTO *dto.GetReportFileResponseDTO
// 	)

// 	s.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = s.reportUsecase.GetReportFile(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to get report file: ", zap.Error(err))
// 		return &pb.GetReportFileResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	res = &pb.GetReportFileResponse{}
// 	s.pbCopier.ToPb(res, resDTO)
// 	if !strings.EqualFold(resDTO.FileName, "") {
// 		header := metadata.New(map[string]string{common.CustomHeaderContentType: "application/octet-stream",
// 			common.CustomHeaderContentDisposition:      "attachment; filename=" + resDTO.FileName,
// 			common.CustomHeaderFileName:                resDTO.FileName,
// 			common.CustomHeaderContentTransferEncoding: "binary",
// 			common.CustomHeaderAdditionalInfo:          fmt.Sprintf("%v-%v", req.DomainUser, req.TransactionID)})
// 		if err = grpc.SendHeader(ctx, header); err != nil {
// 			return nil, status.Errorf(codes.Internal, "gRPC unable to send metadata in header")
// 		}
// 	}
// 	return res, nil
// }
