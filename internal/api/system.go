package api

// func (s *api) GetListNotiTemplate(ctx context.Context,
// 	req *pb.GetListNotiTemplateRequest) (resp *pb.GetListNotiTemplateResponse, err error) {

// 	var (
// 		reqDTO = &dto.GetListNotificationTemplateRequestDTO{}
// 	)
// 	reqDTO.TransactionID = req.TransactionID
// 	respDTO, err := s.managementSystemUsecase.GetListNotificationTemplate(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to get list notification template, err: %v", req.TransactionID, err)
// 		return &pb.GetListNotiTemplateResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.GetListNotiTemplateResponse{}
// 	resp.Templates = make([]*pb.NotificationTemplate, len(respDTO))
// 	for i, item := range respDTO {
// 		temp := &pb.NotificationTemplate{
// 			Template:    item.Template,
// 			Id:          uint32(item.ID),
// 			Subject:     item.Subject,
// 			Body:        item.Body,
// 			Pattern:     item.Pattern,
// 			Active:      item.IsActive,
// 			CreatedDate: item.CreatedDate,
// 			CreatedBy:   item.CreatedBy,
// 			UpdatedDate: item.UpdatedDate,
// 			UpdatedBy:   item.UpdatedBy,
// 		}
// 		resp.Templates[i] = temp
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (s *api) GetListNotiTemplateDetail(ctx context.Context,
// 	req *pb.GetListNotiTemplateDetailRequest) (resp *pb.GetListNotiTemplateDetailResponse, err error) {

// 	var (
// 		reqDTO = &dto.GetNotificationTemplateDetailRequestDTO{}
// 	)
// 	reqDTO.ID = req.Id
// 	reqDTO.TransactionID = req.TransactionID
// 	respDTO, err := s.managementSystemUsecase.GetNotificationTemplateDetail(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to get detail notification template detail, err: %v", req.TransactionID, err)
// 		return &pb.GetListNotiTemplateDetailResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.GetListNotiTemplateDetailResponse{
// 		Templates: &pb.NotificationTemplate{
// 			Id:          uint32(respDTO.ID),
// 			Subject:     respDTO.Subject,
// 			Body:        respDTO.Body,
// 			Pattern:     respDTO.Pattern,
// 			Active:      respDTO.IsActive,
// 			CreatedDate: respDTO.CreatedDate,
// 			CreatedBy:   respDTO.CreatedBy,
// 			UpdatedDate: respDTO.UpdatedDate,
// 			UpdatedBy:   respDTO.UpdatedBy,
// 			Template:    respDTO.Template,
// 		},
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) AddNotiTemplate(ctx context.Context,
// 	req *pb.AddNotiTemplateRequest) (resp *pb.AddNotiTemplateResponse, err error) {
// 	var (
// 		reqDTO = &dto.AddNotificationTemplateRequestDTO{}
// 	)
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.Template = req.Template
// 	reqDTO.Body = req.Body
// 	reqDTO.Pattern = req.Pattern
// 	reqDTO.Subject = req.Subject
// 	err = s.managementSystemUsecase.AddNotificationTemplate(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to add new notification template, err: %v", req.TransactionID, err)
// 		return &pb.AddNotiTemplateResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.AddNotiTemplateResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) DeleteNotiTemplate(ctx context.Context,
// 	req *pb.DeleteNotiTemplateRequest) (resp *pb.DeleteNotiTemplateResponse, err error) {

// 	var (
// 		reqDTO = &dto.DeleteNotificationTemplateRequestDTO{}
// 	)
// 	reqDTO.ID = req.Id
// 	reqDTO.TransactionID = req.TransactionID
// 	err = s.managementSystemUsecase.DeleteNotificationTemplate(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to delete notification template, err: %v", req.TransactionID, err)
// 		return &pb.DeleteNotiTemplateResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.DeleteNotiTemplateResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) UpdateNotiTemplate(ctx context.Context,
// 	req *pb.UpdateNotiTemplateRequest) (resp *pb.UpdateNotiTemplateResponse, err error) {

// 	var (
// 		reqDTO = &dto.UpdateNotificationTemplateRequestDTO{}
// 	)
// 	reqDTO.ID = req.Id
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.Template = req.Template
// 	reqDTO.Body = req.Body
// 	reqDTO.Pattern = req.Pattern
// 	reqDTO.Subject = req.Subject
// 	err = s.managementSystemUsecase.UpdateNotificationTemplate(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to update notification template, err: %v", req.TransactionID, err)
// 		return &pb.UpdateNotiTemplateResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.UpdateNotiTemplateResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) ActiveNotiTemplate(ctx context.Context,
// 	req *pb.ActiveNotiTemplateRequest) (resp *pb.ActiveNotiTemplateResponse, err error) {

// 	var (
// 		reqDTO = &dto.ActiveNotificationTemplateRequestDTO{}
// 	)
// 	reqDTO.ID = req.Id
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.IsActive = req.Active
// 	err = s.managementSystemUsecase.ActiveNotificationTemplate(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to active or unactive notification template, err: %v", req.TransactionID, err)
// 		return &pb.ActiveNotiTemplateResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.ActiveNotiTemplateResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// //  management redis
// func (s *api) GetListRedis(ctx context.Context,
// 	req *pb.GetListRedisRequest) (resp *pb.GetListRedisResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetListRedisRequestDTO{}
// 	)
// 	reqDTO.Cursor = req.Cursor
// 	reqDTO.Pattern = req.Pattern
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.Count = req.Count
// 	respDTO, err := s.managementSystemUsecase.GetListRedis(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to get redis, err: %v", req.TransactionID, err)
// 		return &pb.GetListRedisResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	var redisData = make([]*pb.RedisInformation, len(respDTO.Redises))
// 	for i, key := range respDTO.Redises {
// 		redisInfo := &pb.RedisInformation{
// 			Key:    key.Key,
// 			Lenght: key.Lenght,
// 			Type:   key.Type,
// 		}
// 		redisData[i] = redisInfo
// 	}
// 	resp = &pb.GetListRedisResponse{
// 		StatusCode: common.ACCEPT,
// 		Redis: &pb.ManagamentRedis{
// 			Cursor:  respDTO.Cursor,
// 			Redises: redisData,
// 			IsGet:   respDTO.IsGet,
// 			Count:   respDTO.Count,
// 		},
// 	}
// 	return resp, nil
// }

// func (s *api) GetRedisDetail(ctx context.Context,
// 	req *pb.GetRedisDetailRequest) (resp *pb.GetRedisDetailResponse, err error) {

// 	var (
// 		reqDTO = &dto.GetRedisDetailRequestDTO{}
// 	)

// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	respDTO, err := s.managementSystemUsecase.GetRedisDetail(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to redis detail by key, err: %v", req.TransactionID, err)
// 		return &pb.GetRedisDetailResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	redisInfo := &pb.RedisInformation{
// 		Type:       respDTO.Type,
// 		Lenght:     respDTO.Lenght,
// 		SizeData:   respDTO.SizeData,
// 		TimeExpire: respDTO.TimeExpire,
// 		Key:        req.Key,
// 	}
// 	resp = &pb.GetRedisDetailResponse{
// 		StatusCode: common.ACCEPT,
// 		Redis:      redisInfo,
// 	}
// 	return resp, nil
// }

// func (s *api) DeleteRedisByKey(ctx context.Context,
// 	req *pb.DeleteRedisByKeyRequest) (resp *pb.DeleteRedisByKeyResponse, err error) {

// 	var (
// 		reqDTO = &dto.DeleteRedisByKeyRequestDTO{}
// 	)
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	err = s.managementSystemUsecase.DeleteRedisByKey(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to delete redis by key, err: %v", req.TransactionID, err)
// 		return &pb.DeleteRedisByKeyResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.DeleteRedisByKeyResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) CheckKeyExists(ctx context.Context,
// 	req *pb.CheckKeyExistsRequest) (resp *pb.CheckKeyExistsResponse, err error) {

// 	var (
// 		reqDTO = &dto.CheckKeyExistsRequestDTO{}
// 	)
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	isExists, err := s.managementSystemUsecase.CheckKeyExists(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to check exists redis by key, err: %v", req.TransactionID, err)
// 		return &pb.CheckKeyExistsResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.CheckKeyExistsResponse{
// 		StatusCode: common.ACCEPT,
// 		IsExists:   isExists,
// 	}
// 	return resp, nil
// }

// func (s *api) AddNewRedis(ctx context.Context,
// 	req *pb.AddNewRedisRequest) (resp *pb.AddNewRedisResponse, err error) {

// 	var (
// 		reqDTO = &dto.AddNewRedisRequestDTO{}
// 	)
// 	reqDTO.Expiration = req.Expiration
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	reqDTO.Value = req.Value
// 	err = s.managementSystemUsecase.AddNewRedis(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to add new redis by key, err: %v", req.TransactionID, err)
// 		return &pb.AddNewRedisResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.AddNewRedisResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) SetTimeout(ctx context.Context,
// 	req *pb.SetTimeoutRequest) (resp *pb.SetTimeoutResponse, err error) {

// 	var (
// 		reqDTO = &dto.SetTimeoutRequestDTO{}
// 	)
// 	reqDTO.Expiration = req.Expiration
// 	reqDTO.Key = req.Key
// 	reqDTO.TransactionID = req.TransactionID
// 	err = s.managementSystemUsecase.SetTimeout(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to set timeout redis by key, err: %v", req.TransactionID, err)
// 		return &pb.SetTimeoutResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.SetTimeoutResponse{
// 		StatusCode: common.ACCEPT,
// 	}
// 	return resp, nil
// }

// func (s *api) RenameRedisByKey(ctx context.Context,
// 	req *pb.RenameRedisByKeyRequest) (resp *pb.RenameRedisByKeyResponse, err error) {

// 	return resp, nil
// }

// func (s *api) GetFlagPage(ctx context.Context, req *pb.GetFlagPageRequest) (resp *pb.GetFlagPageResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetFlagPagesRequestDTO{}
// 		resDTO []*dto.GetFlagPagesResponseDTO
// 	)

// 	s.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = s.managementSystemUsecase.GetFlagPages(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("%v - Failed to list flag, err: %v", req.TransactionID, zap.Error(err))
// 		return &pb.GetFlagPageResponse{
// 			TransactionID: req.TransactionID,
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	resp = &pb.GetFlagPageResponse{}
// 	resp.Flag = make(map[string]bool, len(resDTO))
// 	for _, item := range resDTO {
// 		resp.Flag[item.Key] = item.Value
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (s *api) GetFlagPageAdmin(ctx context.Context, req *pb.GetFlagPageAdminRequest) (resp *pb.GetFlagPageAdminResponse, err error) {
// 	var (
// 		resDTO []*dto.GetFlagPagesAdminResponseDTO
// 	)

// 	resDTO, err = s.managementSystemUsecase.GetFlagAdminPages(ctx)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to list flag, err: %v", zap.Error(err))
// 		return &pb.GetFlagPageAdminResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}

// 	resp = &pb.GetFlagPageAdminResponse{}
// 	for _, item := range resDTO {
// 		flag := &pb.FlagAdmin{}
// 		s.pbCopier.ToPb(flag, item)
// 		resp.Flags = append(resp.Flags, flag)
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (s *api) PostFlagPage(ctx context.Context, req *pb.PostFlagPageRequest) (resp *pb.PostFlagPageResponse, err error) {

// 	var (
// 		reqDTO = dto.PostFlagPageRequestDTO{}
// 	)
// 	reqDTO.Key = req.Key
// 	reqDTO.Value = req.Value
// 	reqDTO.FromDate = req.FromDate
// 	reqDTO.ToDate = req.ToDate

// 	err = s.managementSystemUsecase.PostFlagPage(ctx, reqDTO)

// 	resp = &pb.PostFlagPageResponse{}
// 	if err != nil {
// 		resp.StatusCode = common.FAILED
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (s *api) DeleteFlagPage(ctx context.Context, req *pb.DeleteFlagPageRequest) (resp *pb.DeleteFlagPageResponse, err error) {

// 	var (
// 		reqDTO = dto.DeleteFlagPageRequestDTO{}
// 	)
// 	resp = &pb.DeleteFlagPageResponse{}
// 	reqDTO.FlagID = req.FlagID
// 	err = s.managementSystemUsecase.DeleteFlagPage(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to delete flag, err: %v", zap.Error(err))
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = common.ParseError(err).Message()
// 		return resp, err
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (s *api) PatchFlagPage(ctx context.Context, req *pb.PatchFlagPageRequest) (resp *pb.PatchFlagPageResponse, err error) {

// 	var (
// 		reqDTO = dto.PatchFlagPageRequestDTO{}
// 	)
// 	resp = &pb.PatchFlagPageResponse{}
// 	s.pbCopier.FromPb(reqDTO, req)
// 	reqDTO.FromDate = req.FromDate
// 	reqDTO.ID = req.FlagID
// 	reqDTO.ToDate = req.ToDate
// 	reqDTO.Key = req.Key
// 	reqDTO.Value = req.Value

// 	err = s.managementSystemUsecase.PatchFlagPage(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to update flag, err: %v", zap.Error(err))
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = common.ParseError(err).Message()
// 		return resp, err
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (s *api) GetFlagDetailPage(ctx context.Context, req *pb.GetFlagDetailPageRequest) (resp *pb.GetFlagDetailResponse, err error) {

// 	var (
// 		reqDTO = dto.GetFlagDetailPageRequestDTO{}
// 	)
// 	resp = &pb.GetFlagDetailResponse{}
// 	reqDTO.FlagID = req.FlagID

// 	respDTO, err := s.managementSystemUsecase.GetFlagDetailPage(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to get detail flag, err: %v", (err))
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = common.ParseError(err).Message()
// 		return resp, err
// 	}
// 	s.pbCopier.ToPb(resp, respDTO)
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (s *api) CheckFlagAccess(ctx context.Context,
// 	req *pb.CheckFlagAccessRequest) (resp *pb.CheckFlagAccessResponse, err error) {
// 	var (
// 		reqDTO = &dto.CheckFlagAccessRequestDTO{}
// 	)
// 	resp = &pb.CheckFlagAccessResponse{}
// 	reqDTO.CurrentDate = req.CurrentDate
// 	reqDTO.Router = req.Router
// 	resDTO, err := s.managementSystemUsecase.CheckFlagAccess(ctx, reqDTO)
// 	if err != nil {
// 		logger.GlobaLogger.Errorf("Failed to check access flag, err: %v", zap.Error(err))
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = common.ParseError(err).Message()
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	resp.IsAccess = resDTO.IsAccess
// 	return resp, nil
// }
