package api

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"

	"go-libs/logger"
	"shop-back-cms/internal/usecase"
	util "shop-back-cms/internal/util"
)

type (
	UploadFileAPI interface {
		http.Handler
	}

	uploadFileAPI struct {
		cfg               *config.Config
		uploadFileUsecase usecase.UploadFileUsecase
	}
	responseObject struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		TransactionID string
		DoneLines     uint32
		FailedLines   uint32
		ActionType    string
		BatchID       string
	}
)

func NewUploadFileAPI(
	cfg *config.Config,
	uploadFileUsecase usecase.UploadFileUsecase,
) UploadFileAPI {
	return &uploadFileAPI{
		cfg:               cfg,
		uploadFileUsecase: uploadFileUsecase,
	}
}
func (a *uploadFileAPI) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// a.logger.Info("Start api upload file")
	ctx := context.Background()
	respObj := &responseObject{}

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		// todo
		return
	}
	// reqObj.Domain = r.Header.Get("domain")
	// reqObj.ActionType = r.FormValue("action_type")
	// batchID := r.FormValue("batch_id")
	// reqObj.BatchID = batchID
	// reqObj.TypeRequest = r.FormValue("type_request")
	// reqObj.RequiredType = r.FormValue("required_type")
	// reqObj.Data = r.FormValue("data")
	// log.Logger.Infow("Request from upload", "request", reqObj)
	file, handler, err := r.FormFile("file")
	actionType := r.FormValue("action_type")
	fileType := r.FormValue("file_type")
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Failed to get file of form data: %v", err)
		// _, _ = w.Write(respBytes)
		return
	}
	defer file.Close()

	newFile := fmt.Sprintf("%v:%v", "file_upload", handler.Filename)
	finalFile, err := os.OpenFile(newFile, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		// todo
		return
	}

	defer os.Remove(newFile)
	defer finalFile.Close()
	_, err = io.Copy(finalFile, file)
	if err != nil {
		// respObj.StatusCode = common.REJECT
		// respObj.ReasonCode = common.ReasonReadFileError.Code()
		// respObj.ReasonMessage = common.ReasonReadFileError.Message()
		// respBytes, _ := json.Marshal(respObj)
		// _, _ = w.Write(respBytes)
		return
	}
	stranID := util.GetID()
	reqDTO := &dto.UploadFileRequestDTO{
		TransactionID: stranID,
		FileName:      newFile,
		ActionType:    actionType,
		FileType:      fileType,
	}
	err = a.uploadFileUsecase.UploadFile(ctx, finalFile, reqDTO)
	if err != nil {
		respObj.StatusCode = common.FAILED
		// respObj.ReasonCode = common.ParseError(err).Code()
		// respObj.ReasonMessage = common.ParseError(err).Message()
		respBytes, _ := json.Marshal(respObj)
		_, _ = w.Write(respBytes)
		return
	}
	respObj.StatusCode = common.ACCEPT
	respBytes, _ := json.Marshal(respObj)
	_, _ = w.Write(respBytes)
	// if reqObj.ActionType == common.ActionTypeIpsIncomingFileData {
	// 	err = a.ipsIncomingUsecase.CreateIPSIncoming(ctx, finalFile, reqObj.BatchID, reqObj.TransactionID, reqObj.Domain)
	// 	respObj.StatusCode = common.ACCEPT
	// 	if err != nil {
	// 		respObj.StatusCode = common.REJECT
	// 		respObj.ReasonCode = common.ParseError(err).Code()
	// 		respObj.ReasonMessage = common.ParseError(err).Message()
	// 	}
	// 	respBytes, _ := json.Marshal(respObj)
	// 	_, _ = w.Write(respBytes)
	// 	return
	// }

	// if reqObj.TypeRequest == common.ImportFileAction {
	// 	err = a.importActionUsecase.ImportFile(ctx, reqObj.ActionType, reqObj.RequiredType, reqObj.Data, reqObj.TransactionID, finalFile)
	// 	if err != nil {
	// 		respObj.StatusCode = common.REJECT
	// 		respBytes, _ := json.Marshal(respObj)
	// 		_, _ = w.Write(respBytes)
	// 		return
	// 	}
	// 	respObj.StatusCode = common.ACCEPT
	// 	respBytes, _ := json.Marshal(respObj)
	// 	_, _ = w.Write(respBytes)
	// 	return
	// }

	// if reqObj.TypeRequest == common.ImportFileType {
	// 	err = a.importActionUsecase.ImportFileByType(ctx, reqObj.ActionType, reqObj.Data, reqObj.TransactionID, finalFile)
	// 	if err != nil {
	// 		respObj.StatusCode = common.REJECT
	// 		respBytes, _ := json.Marshal(respObj)
	// 		_, _ = w.Write(respBytes)
	// 		return
	// 	}
	// 	respObj.StatusCode = common.ACCEPT
	// 	respBytes, _ := json.Marshal(respObj)
	// 	_, _ = w.Write(respBytes)
	// 	return
	// }

	// resDTO, err := a.importActionUsecase.ImportFromFile(ctx,
	// 	reqObj.ActionType,
	// 	reqObj.BatchID,
	// 	reqObj.Domain,
	// 	finalFile,
	// 	reqObj.TransactionID)
	// if err != nil {
	// 	respObj.StatusCode = common.REJECT
	// 	respObj.ReasonCode = common.ParseError(err).Code()
	// 	respObj.ReasonMessage = common.ParseError(err).Message()
	// 	respObj.BatchID = reqObj.BatchID
	// 	respObj.ActionType = reqObj.ActionType
	// 	respBytes, _ := json.Marshal(respObj)
	// 	_, _ = w.Write(respBytes)
	// 	return
	// }

	// if resDTO.StatusCode != common.ACCEPT {
	// 	respObj.StatusCode = resDTO.StatusCode
	// 	respObj.ReasonCode = resDTO.ReasonCode
	// 	respObj.ReasonMessage = resDTO.ReasonMessage
	// 	respObj.BatchID = reqObj.BatchID
	// 	respObj.ActionType = reqObj.ActionType
	// 	respBytes, _ := json.Marshal(respObj)
	// 	_, _ = w.Write(respBytes)
	// 	return
	// }

	// respObj.StatusCode = common.ACCEPT
	// respObj.DoneLines = resDTO.DoneLines
	// respObj.FailedLines = resDTO.FailedLines
	// respBytes, _ := json.Marshal(respObj)
	// _, _ = w.Write(respBytes)
}
