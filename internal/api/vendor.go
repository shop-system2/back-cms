package api

// func (a *api) PostNewVendor(ctx context.Context,
// 	req *pb.PostNewVendorRequest) (res *pb.PostNewVendorResponse, err error) {
// 	var (
// 		reqDTO = &dto.PostNewVendorRequestDTO{}
// 		resDTO *dto.PostNewVendorResponseDTO
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.vendorUsecase.PostNewVendor(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("api.vendor/PostNewVendor Failed add new vendor, err=%v", errMessage)
// 		return &pb.PostNewVendorResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: common.ParseError(err).Message(),
// 		}, nil
// 	}
// 	res = &pb.PostNewVendorResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) GetListVendor(ctx context.Context,
// 	req *pb.GetListVendorRequest) (res *pb.GetListVendorResponse, err error) {
// 	var (
// 		reqDTO = &dto.GetListVendorRequestDTO{}
// 		resDTO = &dto.GetListVendorResponseDTO{}
// 	)
// 	reqDTO.IsActive = req.IsActive
// 	reqDTO.TransactionID = req.TransactionID
// 	resDTO, err = a.vendorUsecase.GetListVendor(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.vendor/GetListVendor Failed get list vendor, err=%v", req.TransactionID, errMessage)
// 		return &pb.GetListVendorResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	res = &pb.GetListVendorResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) SearchVendor(ctx context.Context,
// 	req *pb.SearchVendorRequest) (res *pb.SearchVendorResponse, err error) {
// 	var (
// 		reqDTO = &dto.SearchVendorRequestDTO{}
// 		resDTO = &dto.GetListVendorResponseDTO{}
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.vendorUsecase.SearchVendor(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.vendor/SearchVendor Failed get list vendor, err=%v", req.TransactionID, errMessage)
// 		return &pb.SearchVendorResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMessage,
// 		}, nil
// 	}
// 	res = &pb.SearchVendorResponse{}
// 	a.pbCopier.ToPb(res, resDTO)
// 	res.StatusCode = common.ACCEPT
// 	return res, nil
// }

// func (a *api) GetVendorDetail(ctx context.Context,
// 	req *pb.GetVendorDetailRequest) (resp *pb.GetVendorDetailResponse, err error) {

// 	resp = &pb.GetVendorDetailResponse{}
// 	reqDTO := &dto.GetVendorDetailRequestDTO{
// 		TransactionID: req.TransactionId,
// 		ID:            req.ID,
// 	}
// 	vendor, err := a.vendorUsecase.GetVendorDetail(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("api.vendor/GetVendorDetail Failed to get  detail vendor: err= %v", errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil

// 	}
// 	vendorDetail := &pb.Vendor{
// 		Name:        vendor.Name,
// 		CreatedAt:   vendor.CreatedAt,
// 		UpdatedAt:   vendor.UpdatedAt,
// 		ID:          vendor.ID,
// 		Address:     vendor.Address,
// 		Code:        vendor.Code,
// 		IsActive:    vendor.IsActive,
// 		Description: vendor.Description,
// 		PhoneNumber: vendor.PhoneNumber,
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	resp.Vendor = vendorDetail
// 	return resp, nil
// }

// func (a *api) PutUpdateVendor(ctx context.Context,
// 	req *pb.PutUpdateVendorRequest) (resp *pb.PutUpdateVendorResponse, err error) {
// 	var (
// 		reqDTO = &dto.PutUpdateVendorRequestDTO{}
// 		resDTO *dto.PutUpdateVendorResponseDTO
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.vendorUsecase.PutUpdateVendor(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.vendor/PutUpdateVendor Failed update vendor: err= %v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp = &pb.PutUpdateVendorResponse{}
// 	a.pbCopier.ToPb(resp, resDTO)
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) DeleteVendor(ctx context.Context,
// 	req *pb.DeleteVendorRequest) (resp *pb.DeleteVendorResponse, err error) {
// 	var (
// 		reqDTO = &dto.DeleteVendorRequestDTO{}
// 		resDTO *dto.DeleteVendorResponseDTO
// 	)
// 	a.pbCopier.FromPb(reqDTO, req)
// 	resDTO, err = a.vendorUsecase.DeleteVendor(ctx, reqDTO)
// 	if err != nil {
// 		errMes := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.vendor/DeleteVendor Failed delete vendor,err=%v", req.TransactionId, errMes)
// 		return &pb.DeleteVendorResponse{
// 			StatusCode:    common.FAILED,
// 			ReasonCode:    common.ParseError(err).Code(),
// 			ReasonMessage: errMes,
// 		}, nil
// 	}
// 	resp = &pb.DeleteVendorResponse{}
// 	a.pbCopier.ToPb(resp, resDTO)
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }

// func (a *api) DoActiveVendor(ctx context.Context,
// 	req *pb.DoActiveVendorRequest) (resp *pb.DoActiveVendorResponse, err error) {
// 	resp = &pb.DoActiveVendorResponse{}
// 	reqDTO := &dto.DoActiveVendorRequestDTO{
// 		VendorID: req.VendorId,
// 		IsActive: req.Active,
// 	}
// 	err = a.vendorUsecase.DoActiveVendor(ctx, reqDTO)
// 	if err != nil {
// 		errMessage := common.ParseError(err).Message()
// 		logger.GlobaLogger.Errorf("%v, api.vendor/DoActiveVendor Error while active/unactive Vendor, err=%v", req.TransactionID, errMessage)
// 		resp.StatusCode = common.FAILED
// 		resp.ReasonCode = common.ParseError(err).Code()
// 		resp.ReasonMessage = errMessage
// 		return resp, nil
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }
