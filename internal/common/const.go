package common

const (
	Key                = "Key"
	Direction          = "Direction"
	LocationY          = "LocationY"
	Position           = "Position"
	PositionHorizontal = "horizontal"
	LocationX          = "LocationX"
)
const (
	DomainMDKey = "domain"
	UserIDMDKey = "userID"
	TokenMDKey  = "token"
)
const (
	// PROCESSING represents processing
	PROCESSING = "PROCESSING"
	// ACCEPT represents accept
	ACCEPT = "ACCEPT"
	// FAILED represents failure
	FAILED = "FAILED"
	// LangVi represents language vn
	LangVi = "vi-VN"
	// LangEn represents language us
	LangEn = "en-US"
	// Report download load file type
	FileExcel = "EXCEL"
	FileCsv   = "CSV"
)
const (
	// GRPCMetadata represent grpc metadata
	GRPCMetadata = "grpc-metadata-%v"
	// CustomHeaderContentType represents grpc metadata
	CustomHeaderContentType = "Custom-Header-Content-Type"
	// CustomHeaderContentDisposition represents grpc metadata
	CustomHeaderContentDisposition = "Custom-Header-Content-Disposition"
	// CustomHeaderFileName represents grpc metadata
	CustomHeaderFileName = "Custom-Header-File-Name"
	// CustomHeaderContentTransferEncoding represents grpc metadata
	CustomHeaderContentTransferEncoding = "Custom-Header-Content-Transfer-Encoding"
	// CustomHeaderAdditionalInfo represents grpc metadata
	CustomHeaderAdditionalInfo = "Custom-Header-Additional-Info"
	// CustomFileType represents grpc metadata
	CustomFileType = "Custom-File-Type"
	// CustomFileAdditionInfo represents grpc metadata
	CustomFileAdditionInfo = "Custom-File-Addition-Info"
	// CustomHeaderFileNameRaw  represents for custom file name raw
	CustomHeaderFileNameRaw = "Custom-Header-File-Name-Raw"
)

// status of product
const ()

const (
	CALULATOR_ADD = "ADD"
	CALULATOR_SUB = "SUB"
)

// key cache
const (
	GetCardItemDetail = "back-cms:get-card-item-detail-%v"
	GetListOrder      = "back-cms:get-list-order-%v" // user_id
)
const (
	NORMAL = "NORMAL"
	HIGH   = "HIGH"
	LOW    = "LOW"
)
const (
	SMS          = "SMS"
	MAIL         = "MAIL"
	NOTIFICATION = "NOTIFICATION"
)

//  key redis
const (
	ListIntruction = "back-cms:list-intruction-%v-%v"
)
const (
	KeyCacheGetProductPreferentail = "backclient:product_preferentail"
	KeyCacheGetProductTrending     = "backclient:product_trending"
	KeyCacheGetProductDetail       = "backclient:detail-product:%v"
)
