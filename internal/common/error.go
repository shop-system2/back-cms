package common

// ErrorResponse represent error struct dto
type ErrorCode string

const (
	Reason0 ErrorCode = "0"
	Reason1 ErrorCode = "1"
	Reason2 ErrorCode = "2"
	Reason3 ErrorCode = "3"
)

var errorCodeMessage = map[string]string{
	"0": "Message 0",
	"1": "Message 1",
	"2": "Message 2",
	"3": "Message 3",
}

func (r ErrorCode) Code() string {
	return string(r)
}

func (r ErrorCode) Message() string {
	if value, ok := errorCodeMessage[r.Code()]; ok {
		return value
	}
	return string(r)
}

////

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// CodeFn represent fn error common
func (c *ErrorResponse) CodeFn() string {
	return string(c.Code)
}

// MessageFn represent fn error common
func (c *ErrorResponse) MessageFn() string {
	return string(c.Message)
}
