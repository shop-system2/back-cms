package dto

type (

	// NewCategoryRequestDTO represent new Category dto
	NewCategoryRequestDTO struct {
		TransactionID string

		Name string
		Code string
	}

	// PostNewCategoryRequestDTO represent new Category dto
	PostNewCategoryRequestDTO struct {
		TransactionID string
		Description   string
		Name          string
		Code          string
	}

	//PostNewCategoryResponseDTO represent list Category in dto
	PostNewCategoryResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}

	// CategorysDTO represent list Category in dto
	CategorysDTO struct {
		Name        string `json:"name"`
		Code        string `json:"code"`
		UpdatedAt   string `json:"updated_at"`
		CreatedAt   string `json:"created_at"`
		Id          uint   `json:"id"`
		IsActive    bool
		Description string
	}

	// CategorysResponseDTO represent list Category in dto
	CategorysResponseDTO struct {
		StatusCode string         `json:"status_code"`
		Categorys  []CategorysDTO `json:"categorys"`
	}

	// GetListCategoryRequestDTO represent
	GetListCategoryRequestDTO struct {
		TransactionID string
		IsActive      bool
	}

	// GetListCategoryResponseDTO represent
	GetListCategoryResponseDTO struct {
		StatusCode string         `json:"status_code"`
		Categorys  []CategorysDTO `json:"categorys"`
		Categories []string       `json:"categories"`
	}

	// PutUpdateCategoryRequestDTO represent
	PutUpdateCategoryRequestDTO struct {
		Name string `json:"name"`
		Code string `json:"code"`
		Id   int    `json:"id"`
	}

	// PutUpdateCategoryResponseDTO represent
	PutUpdateCategoryResponseDTO struct {
		StatusCode string `json:"status_code"`
	}

	// DeleteCategoryRequestDTO represent
	DeleteCategoryRequestDTO struct {
		TransactionID string
		Id            uint32 `json:"id"`
	}

	// DeleteCategoryResponseDTO represent
	DeleteCategoryResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}

	GetCategoryDetailRequestDTO struct {
		TransactionID string

		CategoryID uint32
	}

	GetCategoryDetailResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		Category      CategorysDTO
	}

	DoActiveCategoryRequestDTO struct {
		CategoryID    uint32
		IsActive      bool
		TransactionID string
	}

	SearchCategoryRequestDTO struct {
		IsActive      bool
		TransactionID string
		Name          string `json:"name"`
		Code          string `json:"code"`
	}

	SearchCategoryResponseDTO struct {
		StatusCode string         `json:"status_code"`
		Categorys  []CategorysDTO `json:"categorys"`
	}
)
