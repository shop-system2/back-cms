package dto

type (

	// NewClientRequestDTO represent new client dto
	NewClientRequestDTO struct {
		Name  string
		Price uint32
		Vote  int
	}

	// PostNewClientRequestDTO represent new client dto
	PostNewClientRequestDTO struct {
		Name          string
		Email         string
		PhoneNumber   string
		Address       string
		AccountBank   string
		TransactionID string
	}

	//PostNewClientResponseDTO represent
	PostNewClientResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}

	//ClientsDTO represent list client in dto
	ClientsDTO struct {
		ID          uint32
		Name        string `json:"name"`
		Email       string `json:"email"`
		PhoneNumber string `json:"phone_number"`
		Address     string `json:"address"`
		AccountBank string `json:"account_bank"`
		CreatedAt   string `json:"created_at"`
		UpdatedAt   string `json:"updated_at"`
		IsActive    bool
	}

	//GetClientDetailResponseDTO represent list client in dto
	GetClientDetailResponseDTO struct {
		ID          uint32 `json:"id"`
		Name        string `json:"name"`
		Email       string `json:"email"`
		PhoneNumber string `json:"phone_number"`
		AccountBank string `json:"account_bank"`
		Address     string `json:"address"`
		UpdatedAt   string `json:"updated_at"`
		CreatedAt   string `json:"created_at"`
		StatusCode  string `json:"status_code"`
	}

	GetClientDetailRequestDTO struct {
		ID            uint32
		Name          string
		Email         string
		PhoneNumber   string
		TransactionID string
	}

	//ClientsResponseDTO represent list client in dto
	ClientsResponseDTO struct {
		StatusCode string       `json:"status_code"`
		Clients    []ClientsDTO `json:"clients"`
	}

	// GetListClientRequestDTO represent
	GetListClientRequestDTO struct {
		TransactionID string
	}

	// SearchClientRequestDTO represent
	SearchClientRequestDTO struct {
		TransactionID string
		Name          string `json:"name"`
		Email         string `json:"email"`
		PhoneNumber   string `json:"phone_number"`
		AccountBank   string `json:"account_bank"`
	}

	// GetListClientResponseDTO represent
	GetListClientResponseDTO struct {
		StatusCode string       `json:"status_code"`
		Clients    []ClientsDTO `json:"clients"`
	}

	// PutUpdateClientRequestDTO represent
	PutUpdateClientRequestDTO struct {
		Name          string `json:"name"`
		Email         string `json:"email"`
		Address       string `json:"address"`
		PhoneNumber   string `json:"phone_number"`
		AccountBank   string `json:"account_bank"`
		TransactionID string
		Id            uint32
	}

	// PutUpdateClientResponseDTO represent
	PutUpdateClientResponseDTO struct {
		StatusCode string `json:"status_code"`
	}

	// DeleteClientRequestDTO represent
	DeleteClientRequestDTO struct {
		ID            uint32 `json:"id"`
		TransactionID string
	}

	// DeleteClientResponseDTO represent
	DeleteClientResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}
	DoActiveClientRequestDTO struct {
		ClientID      uint32
		IsActive      bool
		TransactionID string
	}
)
