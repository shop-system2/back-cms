package dto

type (

	// NewCommentRequestDTO represent new comment dto
	NewCommentRequestDTO struct {
		Name  string
		Price uint32
		Vote  int
	}

	// PostNewCommentRequestDTO represent new comment dto
	PostNewCommentRequestDTO struct {
		Title         string
		Content       string
		ProductID     int
		UserID        int
		TransactionID string
	}

	//PostNewCommentResponseDTO represent list comment in dto
	PostNewCommentResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}

	// CommentsDTO represent list comment in dto
	CommentsDTO struct {
		Title     string `json:"title"`
		Content   string `json:"content"`
		UpdatedAt string `json:"updated_at"`
		CreatedAt string `json:"created_at"`
		UserID    int    `json:"user_id"`
		ProductID int    `json:"product_id"`
		ID        uint32
		IsActive  bool
	}

	// CommentsResponseDTO represent list comment in dto
	CommentsResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Comments   []CommentsDTO `json:"comments"`
	}

	// GetListCommentRequestDTO represent
	GetListCommentRequestDTO struct {
		TransactionID string
		UserID        uint32
		ProductId     uint32
		CurrentPage   uint32
		Limit         uint32
		IsActive      bool
	}

	// GetListCommentResponseDTO represent
	GetListCommentResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Comments   []CommentsDTO `json:"comments"`
	}

	// PutUpdateCommentRequestDTO represent
	PutUpdateCommentRequestDTO struct {
		Title         string `json:"title"`
		Content       string `json:"content"`
		ProductId     int    `json:"product_id"`
		Id            int    `json:"id"`
		TransactionID string
	}

	// PutUpdateCommentResponseDTO represent
	PutUpdateCommentResponseDTO struct {
		StatusCode string `json:"status_code"`
	}

	// DeleteCommentRequestDTO represent
	DeleteCommentRequestDTO struct {
		Id            int `json:"id"`
		TransactionID string
	}

	// DeleteCommentResponseDTO represent
	DeleteCommentResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}

	DoActiveCommentRequestDTO struct {
		CommentID     uint32
		IsActive      bool
		TransactionID string
	}

	SearchCommentRequestDTO struct {
		TransactionID string
		UserId        uint32
		ProductId     uint32
		FromDate      string
		ToDate        string
	}
	GetCommentDetailRequestDTO struct {
		ID            uint32
		TransactionID string
	}

	GetCommentDetailResponseDTO struct {
		Comments CommentsDTO `json:"comments"`
	}

	GetReviewCommonClientRequestDTO struct {
		TransactionID string
	}

	ReviewCommonClient struct {
		ID          uint32
		Name        string
		Title       string
		IconUrl     string
		ImageUrl    string
		Major       string
		Description string
		Tag         string
		NumberLike  int8
		Comment     int8
		Star        int8
		CreatedAt   string
		UpdatedAt   string
	}
	GetReviewCommonClientResponseDTO struct {
		TransactionID string
		Reviews       []ReviewCommonClient
	}
)
