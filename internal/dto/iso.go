package dto

type (
	GetListPhysicalChannelRequestDTO struct {
		TransactionID string
		ServerID      uint32
	}

	PhysicalChannelDTO struct {
		ID               int32
		ServerName       string
		ServerOID        uint32
		ChannelID        uint32
		LastChanged      string
		LastChangeRecord uint32
		LastConfirmed    string
		TrptStatus       string
		ApplStatus       string
	}

	GetListPhysicalChannelResponseDTO struct {
		StatusCode       string
		ReasonCode       string
		ReasonMessage    string
		PhysicalChannels []*PhysicalChannelDTO
	}

	TransactionsMntoringRequestDTO struct {
		TransactionID string
		ChannelID     uint32
		TimeInterval  uint32
	}

	TransMonitoringFilterDTO struct {
		Text  string
		Value string
	}

	TransMonitoringDTO struct {
		ID              int64
		TransactionDate string
		ChannelName     string
		RespCode        bool
		RespText        string
		Metadata        string
		Amount          string
		RefNumber       string
	}

	GetTransactionMonitoringDTO struct {
		Transaction []*TransMonitoringDTO
		Filters     []*TransMonitoringFilterDTO
		Total       uint32
		Fail        uint32
		Success     uint32
	}

	TransactionsMntoringResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		Transactions  []*TransMonitoringDTO
	}

	GetListRedisRequestDTO struct {
		TransactionID string
		Pattern       string
		Cursor        uint32
		Count         uint32
	}

	RedisInfo struct {
		Key    string
		Lenght int64
		Type   string
	}

	ManagementRedis struct {
		Cursor  uint32
		Count   uint32
		Redises []*RedisInfo
		IsGet   bool
	}

	GetListRedisResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		Redis         ManagementRedis
	}

	DeleteRedisByKeyRequestDTO struct {
		TransactionID string
		Key           string
	}

	GetRedisDetailRequestDTO struct {
		TransactionID string
		Key           string
	}

	GetRedisDetailResponseDTO struct {
		Lenght     int64
		Type       string
		SizeData   int64
		TimeExpire int64
	}

	DeleteRedisByKeyResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}
	RenameRedisByKeyRequestDTO struct {
		TransactionID string
		OldKey        string
		NewKey        string
	}
	RenameRedisByKeyResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}
	CheckKeyExistsRequestDTO struct {
		TransactionID string
		Key           string
	}
	CheckKeyExistsResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}
	AddNewRedisRequestDTO struct {
		TransactionID string
		Key           string
		Value         string
		Expiration    uint32
	}
	AddNewRedisResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}
	SetTimeoutRequestDTO struct {
		TransactionID string
		Key           string
		Expiration    uint32
	}
	SetTimeoutResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}
)
