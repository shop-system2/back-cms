package dto

import "github.com/dgrijalva/jwt-go"

// JwtUser ad
type JwtUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// AuthToken ad
type AuthToken struct {
	TokenType string `json:"token_type"`
	Token     string `json:"access_token"`
	ExpiresIn int64  `json:"expires_in"`
}

// AuthTokenClaim ad
type AuthTokenClaim struct {
	*jwt.StandardClaims
	Username string
}

// ResponseMsg represent message response from server
type ResponseMsg struct {
	StatusCode string `json:"status_code"`
	ReasonCode string `json:"reason_code"`
	Message    string `json:"message"`
}
