package dto

type (
	SendSMSRequestDTO struct {
		PhoneNumber   string
		Messages      string
		TransactionID string
	}

	SendSMSResponseDTO struct {
		ResponseBaseDTO
	}
)
