package dto

type (
	CartItemRequestDTO struct {
		TransactionID string
		ID            uint32
		ProductId     uint32
		CardId        uint32
		Quantity      uint32
		Sku           string
		// Price int64
		// SellPrice int64
		Description string
		CreatedBy   string
		Price       int64
		SellPrice   int64
	}

	GetCartItemDetailRequestDTO struct {
		CartItemID    uint32
		TransactionID string
	}
	GetCartItemDetail struct {
		CartItemId  uint32
		ProductName string
		ProductId   uint32
		CartId      uint32
		Quantity    uint32
		Sku         string
		Description string
		CreatedBy   string
		Price       int64
		SellPrice   int64
		Amount      int64
	}
	GetCartItemDetailResponseDTO struct {
		StatusCode     string
		CartItemDetail GetCartItemDetail
	}
	CartRequestDTO struct {
		TransactionID string

		ID     uint32
		UserId uint32
		Total  uint8
	}

	OrderItemRequestDTO struct {
		TransactionID string
		ID            uint32
		ProductId     uint32
		CardId        uint32
		Quantity      uint32
		Sku           string
		Price         int64
		SellPrice     int64
		Description   string
		CreatedBy     string
	}

	OrderRequestDTO struct {
		TransactionID string
		ID            uint32
		UserId        uint32
		Total         uint8
	}

	Order struct {
		ProductName string
		Price       int64
		SellPrice   int64
		Total       uint32
		TotolPrice  int64
		Quantity    int64
	}

	GetListOrderRequestDTO struct {
		TransactionID string
		Status        string
		ToDate        string
		FromDate      string
		Type          string
	}
	GetListOrderResponseDTO struct {
		ResponseBaseDTO
		Total  uint32
		Price  int64
		Orders []*Order
	}
)
