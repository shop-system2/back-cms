package dto

type (

	// NewProductRequestDTO represent new product dto
	NewProductRequestDTO struct {
		Name  string
		Price uint32
		Vote  int
	}
	// PostNewProductRequestDTO represent new product dto
	PostNewProductRequestDTO struct {
		Name            string
		Price           uint32
		SellPrice       uint32
		Tag             string
		Vote            int
		CategoryId      int
		Quantity        uint32
		Sku             string
		Description     string
		CreatedAt       string
		UpdatedAt       string
		TransactionID   string
		CreatedBy       string
		UpdatedBy       string
		StatusOfProduct string
	}
	//PostNewProductResponseDTO represent
	PostNewProductResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}

	//ProductsDTO represent list product in dto
	ProductsDTO struct {
		Id              uint32
		Quantity        uint32
		Sku             string
		Name            string `json:"name"`
		Price           int64  `json:"price"`
		Vote            int    `json:"vote"`
		CategoryName    string `json:"category_name"`
		CreatedAt       string `json:"created_at"`
		IsActive        bool
		SellPrice       int64
		Description     string
		Tag             string
		StatusOfProduct string
	}

	GetProductDetailRequestDTO struct {
		TransactionID string
		Id            uint32
	}

	//GetProductDetailResponseDTO represent list product in dto
	GetProductDetailResponseDTO struct {
		ID            uint32 `json:"id"`
		Name          string `json:"name"`
		Price         uint32 `json:"price"`
		Vote          int    `json:"vote"`
		CategoryName  string `json:"category_name"`
		UpdatedAt     string `json:"updated_at"`
		CreatedAt     string `json:"created_at"`
		CategoryID    uint32 `json:"category_id"`
		Description   string
		StatusCode    string `json:"status_code"`
		TransactionID string
		CreatedBy     string
		UpdatedBy     string
		Quantity      uint32
		Sku           string
		IsActive      bool
		SellPrice     uint32
	}

	//ProductsResponseDTO represent list product in dto
	ProductsResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Products   []ProductsDTO `json:"products"`
	}
	// GetListProductRequestDTO represent
	GetListProductRequestDTO struct {
		TransactionID string
		CurrentPage   uint32
		Limit         uint32
		IsActive      bool
		UserID        uint32
		Offset        uint32
	}
	SearchProductRequestDTO struct {
		Name          string `json:"name"`
		Code          string `json:"code"`
		TransactionID string
	}

	// GetListProductResponseDTO represent
	GetListProductResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Products   []ProductsDTO `json:"products"`
	}
	// PutUpdateProductRequestDTO represent
	PutUpdateProductRequestDTO struct {
		Name          string `json:"name"`
		Price         uint32 `json:"price"`
		Vote          int    `json:"vote"`
		Id            int    `json:"id"`
		Description   string
		CategoryID    uint32
		UpdatedBy     string
		TransactionID string
	}
	// PutUpdateProductResponseDTO represent
	PutUpdateProductResponseDTO struct {
		StatusCode string `json:"status_code"`
	}
	// DeleteProductRequestDTO represent
	DeleteProductRequestDTO struct {
		ID            uint32 `json:"id"`
		TransactionID string
	}
	// DeleteProductResponseDTO represent
	DeleteProductResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}

	DoActiveProductRequestDTO struct {
		ProductID     uint32
		IsActive      bool
		TransactionID string
	}
	ProductCheckSkuRequestDTO struct {
		TransactionID string
		Sku           string
	}
	ProductQuantityRequestDTO struct {
		TransactionID string
		Sku           string
		ProductId     uint32
		Type          string
		Number        uint32
	}

	PostNewIntructionRequestDTO struct {
		Description string
	}
	GetListIntructionRequestDTO struct {
		TransactionID string
		CurrentPage   uint32
		Limit         uint32
		IsActive      bool
		UserID        uint32
		Offset        uint32
	}

	PutUpdateIntructionRequestDTO struct {
		ID            uint32
		Description   string
		TransactionID string
		IsActive      string
		Type          string
	}
	IntructionsDTO struct {
		ID          uint32
		IsActive    bool
		Description string
		Type        string
	}
	GetListIntructionResponseDTO struct {
		StatusCode  string
		Intructions []IntructionsDTO
	}
	PostNewIntructionResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}
	DeleteIntructionRequestDTO struct {
		ID            uint32 `json:"id"`
		TransactionID string
	}
	DoActiveIntructionRequestDTO struct {
		IntructionID  uint32
		IsActive      bool
		TransactionID string
	}
	PutUpdateIntructionResponseDTO struct {
		TransactionID string

		StatusCode string `json:"status_code"`
	}
	DeleteIntructionResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}
)
