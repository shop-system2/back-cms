package dto

type (
	GetProductDetailClientRequestDTO struct {
		TransactionID string
		ID            uint32
	}

	ProductDetail struct {
		ID               uint32 `json:"id"`
		Name             string `json:"name"`
		Vote             string `json:"vote"`
		CategoryName     string `json:"category_name"`
		UpdatedAt        string `json:"updated_at"`
		CreatedAt        string `json:"created_at"`
		CategoryID       uint32 `json:"category_id"`
		Description      string
		TransactionID    string
		CreatedBy        string
		UpdatedBy        string
		Quantity         uint32
		Sku              string
		IsActive         bool
		SellPrice        int64
		Price            int64 `json:"price"`
		ShortDescription string
		StatusOfProduct  string
	}
	GetProductDetailClientResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		ProductDetail ProductDetail
	}

	GetProductClientRequestDTO struct {
		TransactionID string
		CategoryID    uint32
		Sex           string
	}
	GetProductPreferentailClientRequestDTO struct {
		TransactionID string
	}
	ProductPreferentailAmount struct {
		Sell  int64
		Price int64
	}
	ProductPreferentailTag struct {
		New   string
		Promo string
		Best  string
	}
	ProductPreferentailClientResponseDTO struct {
		ID               uint32
		Content          string
		Name             string
		Amount           ProductPreferentailAmount
		Tag              ProductPreferentailTag
		ImageID          string
		ShortDescription string
	}

	ProductAmount struct {
		Sell  int64
		Price int64
	}
	ProductTag struct {
		New   string
		Promo string
		Best  string
	}

	ProductTrendingClientResponseDTO struct {
		ResponseBaseDTO
		ProductTrending []*ProductClientDTO
	}

	ProductClientDTO struct {
		Id           uint
		Content      string
		Name         string
		Amount       ProductAmount
		Tag          ProductTag
		ImageID      string
		CategoryName string
	}

	// *********** old code

	// NewProductClientRequestDTO represent new product dto
	NewProductClientRequestDTO struct {
		Name  string
		Price int64
		Vote  int
	}
	// PostNewProductClientRequestDTO represent new product dto
	PostNewProductClientRequestDTO struct {
		Name          string
		Price         int64
		Vote          int
		CategoryID    int
		Description   string
		CreatedAt     string
		UpdatedAt     string
		TransactionID string
		CreatedBy     string
		UpdatedBy     string
	}
	//PostNewProductClientResponseDTO represent
	PostNewProductClientResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}

	//ProductsDTO represent list product in dto
	ProductsClientDTO struct {
		Name         string `json:"name"`
		Price        int64  `json:"price"`
		Vote         int    `json:"vote"`
		CategoryName string `json:"category_name"`
		CreatedAt    string `json:"created_at"`
	}

	//ProductsClientResponseDTO represent list product in dto
	ProductsClientResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Products   []ProductsDTO `json:"products"`
	}
	// GetListProductClientRequestDTO represent
	GetListProductClientRequestDTO struct {
		TransactionID string
		CurrentPage   uint32
		Limit         uint32
		IsActive      bool
		UserID        uint32
		Offset        uint32
	}
	SearchProductClientRequestDTO struct {
		Name          string `json:"name"`
		Code          string `json:"code"`
		TransactionID string
	}

	// GetListProductClientResponseDTO represent
	GetListProductClientResponseDTO struct {
		StatusCode string        `json:"status_code"`
		Products   []ProductsDTO `json:"products"`
	}
	// PutUpdateProductClientRequestDTO represent
	PutUpdateProductClientRequestDTO struct {
		Name          string `json:"name"`
		Price         int64  `json:"price"`
		Vote          int    `json:"vote"`
		ID            int    `json:"id"`
		Description   string
		CategoryID    uint32
		UpdatedBy     string
		TransactionID string
	}
	// PutUpdateProductClientResponseDTO represent
	PutUpdateProductClientResponseDTO struct {
		StatusCode string `json:"status_code"`
	}
	// DeleteProductClientRequestDTO represent
	DeleteProductClientRequestDTO struct {
		ID            uint32 `json:"id"`
		TransactionID string
	}
	// DeleteProductClientResponseDTO represent
	DeleteProductClientResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}

	DoActiveProductClientRequestDTO struct {
		ProductID     uint32
		IsActive      bool
		TransactionID string
	}

	GetProductTrendingClientRequestDTO struct {
		TransactionID string
		CategoryID    uint32
		Sex           string
		ID            uint32
	}
)
