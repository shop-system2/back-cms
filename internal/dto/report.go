package dto

import "github.com/360EntSecGroup-Skylar/excelize/v2"

type (
	// ReportDTO represent dto
	ReportDTO struct {
		ID   uint32
		Name string
	}
	// GetListReportRequestDTO represent getlistreprot dto
	GetListReportRequestDTO struct {
	}
	// GetListReportResponsetDTO represent getlistreprot dto
	GetListReportResponsetDTO struct {
		StatusCode string      `json:"status_code"`
		Reports    []ReportDTO `json:"reports"`
	}
	// GetParametersByReportIDRequestDTO represent GetParametersByReportIDRequest dto
	GetParametersByReportIDRequestDTO struct {
		ID uint32
	}
	// ParameterDTO represent Parameter dto
	ParameterDTO struct {
		ID           uint32 `json:"id"`
		Type         string `json:"type"`
		Name         string `json:"name"`
		DefaultValue string `json:"default_name"`
		IsMultiple   uint32 `json:"is_multiple"`
		Value        string `json:"value"`
	}

	// GetParametersByReportIDResponseDTO represent GetParametersByReportIDResponse dto
	GetParametersByReportIDResponseDTO struct {
		StatusCode string         `json:"status_code"`
		Parameters []ParameterDTO `json:"reports"`
	}
)

type (
	// GetReportParametersRequestDTO represents GetReportParametersRequest DTO
	GetReportParametersRequestDTO struct {
		ID int64
	}

	// GetReportParametersResponseDTO represents GetReportParametersResponse DTO
	GetReportParametersResponseDTO struct {
		ResponseBaseDTO
		Parameters []*Parameter
	}

	// GetReportResponseDTO represents list report
	GetReportResponseDTO struct {
		ResponseBaseDTO
		Reports []*Report
	}

	// Report represents report value
	Report struct {
		Name string
		ID   int64
	}

	// Parameter represents parameter report
	Parameter struct {
		Name         string
		Type         string
		DefaultValue string
		Values       string
		Label        string
		IsMultiple   int64
		Rules        []*Rule
	}

	// Rule represents control rule
	Rule struct {
		Enum            string
		Len             int64
		Max             int64
		Message         string
		Min             int64
		Pattern         string
		Required        string
		Transform       string
		Type            string
		Validator       string
		Whitespace      string
		Validatetrigger string
		Disabled        string
	}

	// GenerateReportRequestDTO represents GenerateReportRequest DTO
	GenerateReportRequestDTO struct {
		ID            int64
		DomainUser    string
		TransactionID string
		FileType      string
		Parameters    map[string]string
	}

	// GenerateReportResponseDTO represents GenerateReportResponse DTO
	GenerateReportResponseDTO struct {
		ResponseBaseDTO
	}

	// GetReportFileRequestDTO represents GetReportFileRequest DTO
	GetReportFileRequestDTO struct {
		DomainUser    string
		TransactionID string
	}

	// GetReportFileResponseDTO represents GetReportFileResponse DTO
	GetReportFileResponseDTO struct {
		ResponseBaseDTO
		FileName  string
		FileExcel *excelize.File
	}

	ReportResponseChannel struct {
		ProcessErr         chan error
		ProcessAllDone     chan bool
		ProcessDoneChannel chan bool
		DateDetailItem     chan []interface{}
	}

	FileDTO struct {
		TitleName     string
		FileName      string
		Base64File    string
		Status        string
		FileChunk     int
		TransactionID string
		FileType      string
		DomainUser    string
		HeaderFile    []string
		Style         int
	}

	GetFileDTO struct {
		FileName string
		File     *excelize.File
		ResponseBaseDTO
		CSVBytes []byte
		FileType string
	}
)
