package dto

// import (
// 	"context"
// 	"fmt"
// 	cimClientPB "gateway/genproto"
// 	common "shop-back-cms/internal/common"
// 	"net/http"
// 	"regexp"
// )

// type MiddlewareInterceptor func(http.ResponseWriter, *http.Request, http.HandlerFunc)

// type MiddlewareHandlerFunc http.HandlerFunc

// type MiddlewareChain []MiddlewareInterceptor

// const (
// 	pattern = "^Bearer (\\S*)"
// )

// func NewRequestIdInterceptor() MiddlewareInterceptor {
// 	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
// 		fmt.Println("Intercept 000")

// 		next(w, r)
// 	}
// }
// func (cont MiddlewareHandlerFunc) Intercept(mwInter MiddlewareInterceptor) MiddlewareHandlerFunc {
// 	return func(writer http.ResponseWriter, request *http.Request) {
// 		fmt.Println("Intercept 2222222222")
// 		mwInter(writer, request, http.HandlerFunc(cont))
// 	}
// }

// func HomeRouter(w http.ResponseWriter, r *http.Request) {
// 	// Handle your request
// 	fmt.Println("HomeRouter 11111111")
// }
// func (chainMD MiddlewareChain) Handler(handler http.HandlerFunc) http.Handler {
// 	fmt.Println("Handler 3333")
// 	curr := MiddlewareHandlerFunc(handler)
// 	for i := len(chainMD) - 1; i >= 0; i-- {
// 		middleware := chainMD[i]
// 		curr = curr.Intercept(middleware)
// 	}

// 	return http.HandlerFunc(curr)
// }
// func RouterHttp(oldRouter *http.ServeMux, cimClient cimClientPB.CIMAPIClient) {
// 	// interceptorChain := MiddlewareHandlerFunc(HomeRouter).
// 	// 	Intercept(JWTInterceptor(cimClient))
// 	abc := MiddlewareChain{
// 		// JWTInterceptor(cimClient),
// 		NewRequestIdInterceptor(),
// 		// mutilple middleware here
// 	}
// 	fmt.Println("router http 00000")
// 	oldRouter.Handle("/cbo/download/report", abc.Handler(HomeRouter))
// }

// func JWTInterceptor(cimClient cimClientPB.CIMAPIClient) MiddlewareInterceptor {
// 	pattern, _ := regexp.Compile(pattern)

// 	return func(w http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
// 		authorizationString := req.Header.Get("Authorization")
// 		if authorizationString == "" {
// 			w.WriteHeader(http.StatusUnauthorized)
// 			_, _ = w.Write([]byte("Unauthorized"))
// 			return
// 		}
// 		if !pattern.Match([]byte(authorizationString)) {
// 			w.WriteHeader(http.StatusUnauthorized)
// 			_, _ = w.Write([]byte("Unauthorized"))
// 			return
// 		}
// 		tokens := pattern.FindSubmatch([]byte(authorizationString))
// 		ctx := context.Background()
// 		if len(tokens) == 2 && string(tokens[1]) != "" {
// 			jwt := string(tokens[1])
// 			domain, refreshTokenStr, statusCode, errorCode, err := checkJWT(ctx, cimClient, jwt)
// 			if err != nil {
// 				w.WriteHeader(http.StatusUnauthorized)
// 				_, _ = w.Write([]byte("Unauthorized"))
// 				return
// 			}
// 			if domain != "" {
// 				req.Header.Set("domain", domain)
// 			}
// 			switch statusCode {
// 			case common.FAILED:
// 				if errorCode == common.ReasonJWTExpired.Code() {
// 					// do refresh token
// 					newToken, err := refreshToken(ctx, cimClient, domain, refreshTokenStr)
// 					if err != nil {
// 						w.WriteHeader(http.StatusUnauthorized)
// 						_, _ = w.Write([]byte("Unauthorized"))
// 						return
// 					}
// 					if newToken != "" {
// 						w.Header().Add("authorization", newToken)
// 						next.ServeHTTP(w, req)
// 						return
// 					}
// 					w.WriteHeader(http.StatusUnauthorized)
// 					_, _ = w.Write([]byte("Unauthorized"))
// 					return
// 				}
// 				if errorCode == common.ReasonJWTInvalid.Code() {
// 					w.WriteHeader(http.StatusUnauthorized)
// 					_, _ = w.Write([]byte("Unauthorized"))
// 					return
// 				}
// 			case common.DONE:
// 				next.ServeHTTP(w, req)
// 				return
// 			}
// 			next.ServeHTTP(w, req)
// 			return
// 		}
// 		w.WriteHeader(http.StatusUnauthorized)
// 		_, _ = w.Write([]byte("Unauthorized"))
// 	}
// }
// func checkJWT(ctx context.Context,
// 	cimClient cimClientPB.CIMAPIClient,
// 	jwt string) (domain, refreshToken, statusCode, errorCode string, err error) {
// 	verifyTokenReq := &cimClientPB.VerifyTokenRequest{
// 		Jwt:         jwt,
// 		Uid:         "cbo",
// 		ClientToken: "C0F69A5549CDF331D8CCB0BEDEFC26D209811CBD7F35FEBE99084AD36E0469C6",
// 	}
// 	verifyTokenResp, err := cimClient.DoVerifyJWTToken(ctx, verifyTokenReq)
// 	if err != nil {
// 		return domain, refreshToken, statusCode, errorCode, err
// 	}
// 	domain = verifyTokenResp.Domain
// 	errorCode = verifyTokenResp.ReasonCode
// 	statusCode = verifyTokenResp.StatusCode
// 	refreshToken = verifyTokenResp.RefreshToken
// 	return domain, refreshToken, statusCode, errorCode, nil
// }

// func refreshToken(ctx context.Context, cimClient cimClientPB.CIMAPIClient, domain, refreshToken string) (string, error) {
// 	var (
// 		err      error
// 		newToken string
// 	)
// 	refreshTokenReq := &cimClientPB.UserRefreshTokenRequest{
// 		Domain:       domain,
// 		RefreshToken: refreshToken,
// 	}
// 	refreshTokenResp, err := cimClient.DoRefreshToken(ctx, refreshTokenReq)
// 	if err != nil {
// 		// Log here
// 		return newToken, err
// 	}
// 	if refreshTokenResp.StatusCode == common.ACCEPT {
// 		newToken = refreshTokenResp.JwtStr
// 	}
// 	return newToken, nil
// }
