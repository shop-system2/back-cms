package dto

type (
	GetListNotificationTemplateRequestDTO struct {
		TransactionID string
	}
	NotificationTemplateResponseDTO struct {
		ID          int32
		Template    string
		Subject     string
		Body        string
		Pattern     string
		IsActive    bool
		CreatedBy   string
		CreatedDate string
		UpdatedBy   string
		UpdatedDate string
	}

	GetNotificationTemplateDetailRequestDTO struct {
		TransactionID string
		ID            uint32
	}
	GetNotificationTemplateDetailResponseDTO struct {
		TransactionID string
	}

	AddNotificationTemplateRequestDTO struct {
		TransactionID string
		Template      string
		Subject       string
		Body          string
		Pattern       string
	}
	AddNotificationTemplateResponseDTO struct {
		TransactionID string
	}

	DeleteNotificationTemplateRequestDTO struct {
		TransactionID string
		ID            uint32
	}
	DeleteNotificationTemplateResponseDTO struct {
		TransactionID string
	}

	UpdateNotificationTemplateRequestDTO struct {
		TransactionID string
		ID            uint32
		Template      string
		Subject       string
		Body          string
		Pattern       string
	}
	UpdateNotificationTemplateResponseDTO struct {
		TransactionID string
	}

	ActiveNotificationTemplateRequestDTO struct {
		TransactionID string
		ID            uint32
		IsActive      bool
	}
	ActiveNotificationTemplateResponseDTO struct {
		TransactionID string
	}

	// feature flag
	GetFlagPagesRequestDTO struct {
		TransactionID string
	}

	GetFlagPagesResponseDTO struct {
		Key   string
		Value bool
	}

	GetFlagDetailPageRequestDTO struct {
		FlagID uint32
	}

	GetFlagDetailPageResponseDTO struct {
		ID       uint32
		Key      string
		Value    string
		FromDate string
		ToDate   string
	}

	GetFlagPagesAdminResponseDTO struct {
		Key      string
		Value    string
		FromDate string
		ToDate   string
		IsActive string
	}

	GetFlagPagesAdminRequestDTO struct{}

	PostFlagPageResponseDTO struct {
	}

	PostFlagPageRequestDTO struct {
		Key      string
		Value    bool
		FromDate string
		ToDate   string
	}

	DeleteFlagPageResponseDTO struct{}

	DeleteFlagPageRequestDTO struct {
		FlagID uint32
	}

	PatchFlagPageResponseDTO struct{}

	PatchFlagPageRequestDTO struct {
		ID       uint32
		Key      string
		Value    bool
		FromDate string
		ToDate   string
	}
	CheckFlagAccessResponseDTO struct {
		IsAccess bool
	}

	CheckFlagAccessRequestDTO struct {
		Router      string
		CurrentDate string
	}
)
