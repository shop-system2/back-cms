package dto

type (
	UploadFileRequestDTO struct {
		TransactionID string
		FileName      string
		ActionType    string
		FileType      string
	}
)
