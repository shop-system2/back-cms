package dto

type (
	// UploadFileRequest abc
	UploadFileRequest struct {
		TransactionID string
		ActionType    string
		BatchID       string
		Domain        string
	}

	// UploadFileResponse abc
	UploadFileResponse struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
		TransactionID string
		DoneLines     uint32
		FailedLines   uint32
	}
)
