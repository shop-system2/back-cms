package dto

type (
	// User reresent to insert db
	User struct {
		Domain     string `json:"domain"`
		Password   string `json:"password"`
		Email      string `json:"email"`
		Phone      string `json:"phone"`
		EmployeeID string `json:"employee_id"`
	}

	// ListUserDTOResponse represent
	ListUserDTOResponse struct {
		Domain     string `json:"domain"`
		Password   string `json:"password"`
		Email      string `json:"email"`
		Phone      string `json:"phone"`
		EmployeeID string `json:"employee_id"`
	}
	// ListUserDTORequest represent
	ListUserDTORequest struct {
	}
	// UserLoginRequestDTO reresent to check record in db
	UserLoginRequestDTO struct {
		Domain   string `json:"domain"`
		Password string `json:"password"`
	}
)
