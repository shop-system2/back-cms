package dto

type (

	// NewVendorRequestDTO represent new product dto
	NewVendorRequestDTO struct {
		Name  string
		Price uint32
		Vote  int
	}
	DoActiveVendorRequestDTO struct {
		TransactionID string
		VendorID      uint32
		IsActive      bool
	}

	// PostNewVendorRequestDTO represent new product dto
	PostNewVendorRequestDTO struct {
		Name          string
		Code          string
		Address       string
		PhoneNumber   string
		TransactionID string
		Description   string
	}

	//PostNewVendorResponseDTO represent
	PostNewVendorResponseDTO struct {
		StatusCode    string
		ReasonCode    string
		ReasonMessage string
	}

	//VendorsDTO represent list product in dto
	VendorsDTO struct {
		Name        string `json:"name"`
		Code        string `json:"code"`
		Address     string `json:"address"`
		PhoneNumber string `json:"phone_number"`
		CreatedAt   string `json:"created_at"`
		UpdatedAt   string `json:"updated_at"`
		IsActive    bool
		Description string
		ID          uint32
	}

	//GetVendorDetailResponseDTO represent list product in dto
	GetVendorDetailResponseDTO struct {
		ID          uint32 `json:"id"`
		Name        string `json:"name"`
		Code        string `json:"code"`
		Address     string `json:"address"`
		PhoneNumber string `json:"phone_number"`
		UpdatedAt   string `json:"updated_at"`
		CreatedAt   string `json:"created_at"`
		StatusCode  string `json:"status_code"`
		IsActive    bool
		Description string
	}

	//VendorsResponseDTO represent list product in dto
	VendorsResponseDTO struct {
		StatusCode string       `json:"status_code"`
		Vendors    []VendorsDTO `json:"vendors"`
	}
	// GetListVendorRequestDTO represent
	GetListVendorRequestDTO struct {
		TransactionID string
		IsActive      bool
	}

	GetVendorDetailRequestDTO struct {
		ID            uint32
		TransactionID string
	}

	// SearchVendorRequestDTO represent
	SearchVendorRequestDTO struct {
		Name        string `json:"name"`
		Code        string `json:"code"`
		IsActive    bool
		PhoneNumber string `json:"phone_number"`
	}

	// GetListVendorResponseDTO represent
	GetListVendorResponseDTO struct {
		StatusCode string       `json:"status_code"`
		Vendors    []VendorsDTO `json:"vendors"`
	}
	// PutUpdateVendorRequestDTO represent
	PutUpdateVendorRequestDTO struct {
		ID            uint32
		Name          string `json:"name"`
		Code          string `json:"code"`
		Address       string `json:"address"`
		PhoneNumber   string `json:"phone_number"`
		IsActive      bool
		Description   string
		TransactionID string
	}
	// PutUpdateVendorResponseDTO represent
	PutUpdateVendorResponseDTO struct {
		StatusCode string `json:"status_code"`
	}
	// DeleteVendorRequestDTO represent
	DeleteVendorRequestDTO struct {
		Id            int `json:"id"`
		TransactionID string
	}
	// DeleteVendorResponseDTO represent
	DeleteVendorResponseDTO struct {
		StatusCode    string `json:"status_code"`
		ReasonCode    string `json:"reason_code"`
		ReasonMessage string `json:"reason_message"`
	}
)
