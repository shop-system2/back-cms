package facade

import (
	"shop-back-cms/config"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
)

type (
	ProcessFileFacade interface {
	}
	processFileFacade struct {
		cfg            *config.Config
		cacheHelper    cache.CacheHelper
		fileRepository repository.FileRepository
	}
)

func NewProcessFileFacade(
	cfg *config.Config,
	cacheHelper cache.CacheHelper,
	fileRepository repository.FileRepository,
) ProcessFileFacade {
	return &processFileFacade{
		cfg:            cfg,
		cacheHelper:    cacheHelper,
		fileRepository: fileRepository,
	}
}

// type (
// 	UploadFileUsecase interface {
// 		UploadFile(ctx context.Context, file *os.File, req *dto.UploadFileRequestDTO) error
// 	}

// 	uploadFileUsecase struct {
// 		cfg         *config.Config
// 		cacheHelper cache.CacheHelper

// 	}
// )

// func NewUploadFileUsecase(
// 	cfg *config.Config,
// 	cacheHelper cache.CacheHelper,
// ) UploadFileUsecase {
// 	return &uploadFileUsecase{
// 		cfg:         cfg,
// 		cacheHelper: cacheHelper,
// 	}
// }

// func (f *actionFacade) ImportFile(ctx context.Context,
// 	actionType, batchID, importBy string,
// 	file *os.File,
// 	transactionID string, moreInfo ...string) (outFile *excelize.File, err error) {
// 	span := jaeger.Start(ctx, ">repository.actionFacade/ImportFile", ext.SpanKindRPCClient)

// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	defer file.Close()
// 	// get data def
// 	// get import parser
// 	// index of failed file
// 	importedDataLines, err := f.importParser.Parser(ctx, actionType, importBy, batchID, file)

// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while import file: %v", transactionID, err)
// 		return nil, errors.New(common.ReasonImportFileFailed.Code())
// 	}

// 	// make batch to multi go routine
// 	// batch size for 500
// 	batchSize := f.cfg.MultiThreadParams.BatchSize
// 	maxRoutine := f.cfg.MultiThreadParams.MaxMultiThread
// 	maxSize, err := strconv.ParseInt(fmt.Sprintf("%.0f",
// 		math.Ceil(float64(len(*importedDataLines))/float64(batchSize))), 10, 32)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Can not make multiple batching import: %v", transactionID, err)
// 		return nil, errors.New(common.ReasonImportFileFailed.Code())
// 	}

// 	// create import action first
// 	importModel := &model.ImportAction{
// 		BatchID:           batchID,
// 		ActionType:        actionType,
// 		IsActive:          sql.NullBool{Bool: true},
// 		RequiredApproval:  sql.NullBool{Bool: true},
// 		ImportBy:          importBy,
// 		UpdatedBy:         importBy,
// 		ImportRows:        uint32(len(*importedDataLines)),
// 		ImportSuccessRows: uint32(len(*importedDataLines)),
// 		ImportFailedRows:  0,
// 	}

// 	importActionID, err := f.importActionRepository.ImportAction(ctx, importModel)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while creating import action: %v, data: %v",
// 			transactionID,
// 			err,
// 			importModel)
// 		return nil, errors.New(common.ReasonDBError.Code())
// 	}

// 	if importedDataLines == nil || len(*importedDataLines) == 0 {
// 		log.Logger.Errorf("%v - Error while import file: %v", transactionID, common.ReasonFileEmpty.Message())
// 		return nil, errors.New(common.ReasonFileEmpty.Code())
// 	}

// 	// import record and data
// 	tx, err := f.baseRepository.BuildTransactions(ctx, "")
// 	dataParser, err := f.baseDataParser.GetParserData(actionType)
// 	if dataParser == nil || err != nil {
// 		log.Logger.Warnf("Not found data parser in base parser")
// 		return nil, errors.New(common.ReasonGeneralError.Code())
// 	}
// 	dataModel, _ := model.GetModelFromActionType(actionType)
// 	modelDef, err := model.GetStructureFromObject(dataModel)
// 	if err != nil {
// 		return nil, err
// 	}
// 	dbModelDef, err := f.baseRepository.GetDataPropertyDefinition(ctx, modelDef)
// 	if err != nil {
// 		return nil, err
// 	}

// 	var wg sync.WaitGroup
// 	wg.Add(int(maxSize))
// 	workingChan := make(chan bool, maxRoutine)
// 	errChan := make(chan error, maxSize)
// 	doneChan := make(chan bool, maxSize)
// 	allChanDone := make(chan bool, 1)
// 	waitChan := make(chan bool, 1)
// 	currentIndex := 0
// 	isPassedWithoutError := true
// 	for i := 0; i < maxRoutine; i++ {
// 		workingChan <- true
// 	}

// 	// set import summary info  cbo:import:action_type:batch_id:info {total batch: 150, total_lines: 300000}
// 	err = f.cacheHelper.Set(ctx, fmt.Sprintf(common.ImportBatchProcessingInfo, actionType, batchID),
// 		&dto.ImportBatchProcessingInfo{TotalBatch: uint32(maxSize), TotalLine: uint32(len(*importedDataLines))},
// 		time.Duration(f.cfg.TimeDuration.TimeWaitingImportBatch)*time.Minute)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while setting batch info to redis: %v", transactionID, err)
// 	}
// 	log.Logger.Debugf("%v - Max Batch: %d", transactionID, maxSize)
// 	log.Logger.Debugf("%v - Max Go routine: %d", transactionID, maxRoutine)
// 	go func() {
// 		for {
// 			select {
// 			case processErr := <-errChan:
// 				if processErr != nil {
// 					isPassedWithoutError = false
// 				}
// 			case <-allChanDone:
// 				waitChan <- true
// 				return
// 			}
// 		}
// 	}()
// 	go func() {
// 		for i := 0; i < int(maxSize); i++ {
// 			<-doneChan
// 			// Say that another goroutine can now start.
// 			log.Logger.Debugf("%v - Start doing batch: %d", transactionID, i)
// 			err = f.cacheHelper.Set(ctx, fmt.Sprintf(common.ImportBatchProcessingCurrent, actionType, batchID),
// 				&dto.ImportBatchProcessingCurrent{CurrentIndex: uint32(i)},
// 				time.Duration(f.cfg.TimeDuration.TimeWaitingImportBatch)*time.Minute)
// 			if err != nil {
// 				log.Logger.Errorf("%v - Error while setting batch current to redis: %v", transactionID, err)
// 			}
// 			workingChan <- true
// 		}
// 		log.Logger.Debugf("%v - All Done", transactionID)
// 		wg.Wait()
// 		allChanDone <- true
// 	}()
// 	for i := 0; i < int(maxSize); i++ {
// 		var arr []interface{}
// 		if currentIndex+batchSize > len(*importedDataLines) {
// 			arr = (*importedDataLines)[currentIndex:len(*importedDataLines)]
// 		}
// 		if len(arr) == 0 {
// 			arr = (*importedDataLines)[currentIndex : currentIndex+batchSize]
// 		}
// 		currentIndex += batchSize
// 		// call redis to increase - cbo:import:action_type:batch_id:count
// 		<-workingChan
// 		// process here
// 		go func(wg *sync.WaitGroup) {
// 			defer wg.Done()
// 			defer func() {
// 				doneChan <- true
// 			}()
// 			isFailed := f.importFileBatching(ctx,
// 				&arr,
// 				tx,
// 				dataParser,
// 				importActionID,
// 				importBy,
// 				actionType,
// 				dbModelDef,
// 				transactionID)
// 			if isFailed {
// 				errChan <- errors.New("process with error for batch")
// 				log.Logger.Errorf("%v - Error while import record: %v", transactionID, err)
// 				return
// 			}
// 			errChan <- nil
// 		}(&wg)

// 	}
// 	<-waitChan
// 	if !isPassedWithoutError {
// 		outFile, err = f.BuildReturnFile(ctx, batchID, actionType, importedDataLines, transactionID)
// 		if err != nil {
// 			return nil, errors.New(common.ReasonImportFileFailed.Code())
// 		}
// 		return outFile, errors.New(common.ReasonImportFileFailed.Code())
// 	}
// 	err = tx.CommitAll()
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while committing transaction: %v", transactionID, err)
// 		return nil, errors.New(common.ReasonImportFileFailed.Code())
// 	}
// 	log.Logger.Debugf("%v - Action %v - done read file, batch name %v", transactionID, actionType, batchID)
// 	// Could use file to append the error
// 	// currently, build a new file instead
// 	isSucess, err := f.CheckBatchRequireApprove(ctx, actionType, batchID, importBy, transactionID, moreInfo...)
// 	if err != nil || !isSucess {
// 		return nil, errors.New(common.ReasonImportFileFailed.Code())
// 	}
// 	return nil, nil
// }
