package helper

import (
	"context"
	"fmt"

	"cloud.google.com/go/bigquery"
	"google.golang.org/api/iterator"
)

func NewClientBigQuery(ctx context.Context, projectID string) (*bigquery.Client, error) {
	// client, err := bigquery.NewClient(ctx, projectID)
	// if err != nil {
	// 	return client, err
	// }
	// return client, nil

	client, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		// return fmt.Errorf("bigquery.NewClient: %v", err)
	}
	defer client.Close()
	q := client.Query(
		"SELECT name FROM `bigquery-public-data.usa_names.usa_1910_2013` " +
			"WHERE state = \"TX\" " +
			"LIMIT 100")
	// Location must match that of the dataset(s) referenced in the query.
	q.Location = "US"
	// Run the query and print results when the query job is completed.
	job, err := q.Run(ctx)
	if err != nil {
		// return err
	}
	status, err := job.Wait(ctx)
	if err != nil {
		// return err
	}
	if err := status.Err(); err != nil {
		// return err
	}
	it, err := job.Read(ctx)
	for {
		var row []bigquery.Value
		err := it.Next(&row)
		if err == iterator.Done {
			break
		}
		if err != nil {
			// return err
		}
		// fmt.Fprintln(w, row)
		fmt.Println(row)
	}
	return nil, nil
}
