package cache

import (
	"context"
	"go-libs/logger"
	"time"

	"github.com/go-redis/redis"
)

type CacheHelper interface {
	Set(ctx context.Context, key string, value interface{}, time time.Duration) error
	SetInterface(ctx context.Context, key string, value interface{}, time time.Duration) error
	GetInterface(ctx context.Context, key string, inputValue interface{}) (value interface{}, err error)
	GetString(ctx context.Context, key string, value string) (string, error)
	SetString(ctx context.Context, key string, value string, time time.Duration) error
	Del(ctx context.Context, key string) error
	Expire(ctx context.Context, key string, time time.Duration) error
	Exists(ctx context.Context, key string) error

	GetMulti(ctx context.Context, data interface{}, keys ...string) ([]interface{}, error)
	SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) (bool, error)
	DelMulti(ctx context.Context, keys ...string) error
	GetKeysByPattern(ctx context.Context, pattern string, cursor uint64, limit int64) ([]string, uint64, error)
	RenameKey(ctx context.Context, oldKey, newKey string) error
	GetStrLenght(ctx context.Context, key string) (int64, error)
	GetType(ctx context.Context, key string) (string, error)
	DebugObjectByKey(ctx context.Context, key string) (string, error)
	TimeExpire(ctx context.Context, key string) (time.Duration, error) // return seconds
}

// NewRedisInstance create instance redis
func NewRedisInstance(addrs []string, password string, database int) CacheHelper {
	client := redis.NewClient(&redis.Options{
		Addr:     addrs[0],
		Password: password, // no password set
		DB:       database, // use default DB
	})
	_, err := client.Ping().Result()
	if err != nil {
		logger.GlobaLogger.Panicf("Init redis errror %v", err)
		return &redisHelper{
			client: client,
		}
	}
	logger.GlobaLogger.Infof("Init redis successfully")
	return &redisHelper{
		client: client,
	}
}
