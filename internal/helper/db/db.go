package db

import (
	"database/sql"

	"go.mongodb.org/mongo-driver/mongo"
)

type DBHelper interface {
	Open() *sql.DB
	Close() error
	Begin() (*sql.Tx, error)
	Commit(tx *sql.Tx) error
	Rollback(tx *sql.Tx) error
}

type (
	MongoHelper interface {
		Open() *mongo.Client
	}
	mongoHelper struct {
		db *mongo.Client
	}
)
