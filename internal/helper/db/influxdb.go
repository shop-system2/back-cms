package db

import (
	"context"
	"go-libs/logger"
	"net/http"
	"sync"

	"github.com/influxdata/influxdb-client-go/domain"
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

type client struct {
	serverUrl     string
	authorization string
	// options       *Options
	// writeApis     []WriteApi
	httpClient *http.Client
	lock       sync.Mutex
}

// InfluxDBClient provides API to communicate with InfluxDBServer
// There two APIs for writing, WriteApi and WriteApiBlocking.
// WriteApi provides asynchronous, non-blocking, methods for writing time series data.
// WriteApiBlocking provides blocking methods for writing time series data
type InfluxDBClient interface {
	// WriteApi returns the asynchronous, non-blocking, Write client.
	WriteApi(org, bucket string) // WriteApi
	// WriteApi returns the synchronous, blocking, Write client.
	WriteApiBlocking(org, bucket string) // WriteApiBlocking
	// QueryApi returns Query client
	QueryApi(org string) // QueryApi
	// Close ensures all ongoing asynchronous write clients finish
	Close()
	// Options returns the options associated with client
	// Options() *Options
	// ServerUrl returns the url of the server url client talks to
	ServerUrl() string
	// Setup sends request to initialise new InfluxDB server with user, org and bucket, and data retention period
	// Retention period of zero will result to infinite retention
	// and returns details about newly created entities along with the authorization object
	Setup(ctx context.Context, username, password, org, bucket string, retentionPeriodHours int) (*domain.OnboardingResponse, error)
	// Ready checks InfluxDB server is running
	Ready(ctx context.Context) (bool, error)
	// Internal  method for handling posts
	// postRequest(ctx context.Context, url string, body io.Reader, requestCallback RequestCallback, responseCallback ResponseCallback) *Error
}

// func NewClient(serverUrl string, authToken string) InfluxDBClient {
// 	return NewClientWithOptions(serverUrl, authToken, DefaultOptions())
// }
// func NewClientWithOptions(serverUrl string, authToken string, options *Options) InfluxDBClient {
// 	client := &client{
// 		serverUrl:     serverUrl,
// 		authorization: "Token " + authToken,
// 		httpClient: &http.Client{
// 			Timeout: time.Second * 20,
// 			Transport: &http.Transport{
// 				DialContext: (&net.Dialer{
// 					Timeout: 5 * time.Second,
// 				}).DialContext,
// 				TLSHandshakeTimeout: 5 * time.Second,
// 				TLSClientConfig:     options.TlsConfig(),
// 			},
// 		},
// 		options:   options,
// 		writeApis: make([]WriteApi, 0, 5),
// 	}
// 	return client
// }

type influxDBMy struct {
	client *influxdb2.Client
}

type InfluxDBMy interface {
	Test() error
}

func (in *influxDBMy) Test() error {
	return nil
}

func NewClientInfluxDB() InfluxDBMy {
	// create new client with default option for server url authenticate by token
	client := influxdb2.NewClient("http://172.29.164.163:8086", "")

	_, err := client.Ready(context.Background())
	if err != nil {
		logger.GlobaLogger.Warnf("influxdb not ready")
	}

	return &influxDBMy{
		client: &client,
	}
	// user blocking write client for writes to desired bucket
	// writeAPI := client.WriteAPIBlocking("my-org", "my-bucket")
	// // create point using full params constructor
	// p := influxdb2.NewPoint("stat",
	// 	map[string]string{"unit": "temperature"},
	// 	map[string]interface{}{"avg": 24.5, "max": 45},
	// 	time.Now())
	// // write point immediately
	// writeAPI.WritePoint(context.Background(), p)
	// // create point using fluent style
	// p = influxdb2.NewPointWithMeasurement("stat").
	// 	AddTag("unit", "temperature").
	// 	AddField("avg", 23.2).
	// 	AddField("max", 45).
	// 	SetTime(time.Now())
	// writeAPI.WritePoint(context.Background(), p)

	// // Or write directly line protocol
	// line := fmt.Sprintf("stat,unit=temperature avg=%f,max=%f", 23.5, 45.0)
	// writeAPI.WriteRecord(context.Background(), line)
	// // Ensures background processes finish
	// client.Close()
}
