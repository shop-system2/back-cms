package db

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func NewMongoDB(str string) MongoHelper {
	mongoClient, err := InitMongoDB(str)
	if err != nil {
		panic("")
	}
	return &mongoHelper{
		db: mongoClient,
	}
}
func InitMongoDB(strConnect string) (*mongo.Client, error) {
	ctx := context.Background()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(strConnect))
	if err != nil {
		return client, err
	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return client, err
	}
	// database := client.Database("logger")
	// podcastsCollection := database.Collection("podcasts")
	// episodesCollection := database.Collection("episodes")
	// var episodes []Episode
	// cursor, err := episodesCollection.Find(ctx, bson.M{"duration": bson.D{{"$gt", 25}}})
	// if err != nil {
	// 	panic(err)
	// }
	// if err = cursor.All(ctx, &episodes); err != nil {
	// 	panic(err)
	// }
	// fmt.Println(episodes)
	// podcast := Podcast{
	// 	Title:  "The Polyglot Developer",
	// 	Author: "Nic Raboy",
	// 	Tags:   []string{"development", "programming", "coding"},
	// }
	// insertResult, err := podcastsCollection.InsertOne(ctx, podcast)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println(insertResult.InsertedID)
	return client, nil

}

func (m *mongoHelper) Open() *mongo.Client {
	return m.db
}
