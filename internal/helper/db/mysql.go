package db

import (
	"database/sql"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/sql/migration"

	_ "github.com/go-sql-driver/mysql"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type dbHelper struct {
	db   *sql.DB
	gorm *gorm.DB
}

// dsn connect to db
func dsn(dbName, username, password, hostname string) string {
	return fmt.Sprintf("%s:%s@tcp(%s)/%s", username, password, hostname, dbName)
}

func InitMysqlDB(host string, port int, username, passsword, database string) (*sql.DB, error) {
	hostPort := fmt.Sprintf("%v:%v", host, port)
	connectString := dsn(database, username, passsword, hostPort)
	db, err := sql.Open("mysql", connectString)
	if err != nil {
		logger.GlobaLogger.Errorf("Init mysql errror %v", err)
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		logger.GlobaLogger.Errorf("Init mysql errror %v", err)
		return nil, err
	}
	logger.GlobaLogger.Infof("Init mysql successfully")
	return db, nil
}

func NewMysqlDBHelper(host string, port int, username, password, database string, isMigration bool) DBHelper {
	db, err := InitMysqlDB(host, port, username, password, database)
	if err != nil {
		fmt.Println("failed to init Mysql")
		// TODO pannic
	}
	connectString := dsn(database, username, password, fmt.Sprintf("%v:%v", host, port))
	gorm, err := gorm.Open(mysql.Open(connectString), &gorm.Config{})
	if err != nil {
		fmt.Println("failed to init Mysql")
		// TODO pannic
	}
	if isMigration {
		migration.MigrationDb(gorm)
	}
	return &dbHelper{
		db:   db,
		gorm: gorm,
	}
}

func NewMysqlGorm(host string, port int, username, password, database string, isMigration bool) *gorm.DB {
	connectString := dsn(database, username, password, fmt.Sprintf("%v:%v", host, port))
	// "host=localhost user=postgres password=postgres dbname=postgres port=5432 sslmode=disable"
	connectString = fmt.Sprintf("%s%s", connectString, "?parseTime=true")

	gorm, err := gorm.Open(mysql.Open(connectString), &gorm.Config{})
	if err != nil {
		//		panic(err)
		fmt.Println(err)
	}

	return gorm
}

func (h *dbHelper) Open() *sql.DB {
	return h.db
}

func (h *dbHelper) Close() error {
	return h.db.Close()
}

func (h *dbHelper) Begin() (*sql.Tx, error) {
	return h.db.Begin()
}

func (h *dbHelper) Commit(tx *sql.Tx) error {
	return tx.Commit()
}

func (h *dbHelper) Rollback(tx *sql.Tx) error {
	return tx.Rollback()
}
