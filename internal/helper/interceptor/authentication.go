package helper_grpc

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/util"
	gatewayClientPB "shop-gateway/genproto"
	"strings"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	patternStr = "^Bearer (\\S*)"
)

func checkMethodExcludeInterceptor(arr []string, method string) bool {
	for _, item := range arr {
		if strings.Contains(method, item) {
			return true
		}
	}
	return false
}
func JWPInterceptorAuthentication(gateway gatewayClientPB.ServerAPIClient,
	arrShipAuthor []string) grpc.UnaryServerInterceptor {
	fmt.Println("sssss")
	pattern, _ := regexp.Compile(patternStr)
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {

		isChekc := true
		if isChekc {
			return handler(ctx, req)
		}

		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return handler(ctx, req)
		}

		if checkMethodExcludeInterceptor(arrShipAuthor, info.FullMethod) {
			return handler(ctx, req)
		}

		authorizations, ok := md["authorization"]
		if ok && len(authorizations) != 0 {
			firstItem := authorizations[0]
			if firstItem != "" && pattern.Match([]byte(firstItem)) {
				tokens := pattern.FindSubmatch([]byte(firstItem))
				if len(tokens) == 2 && string(tokens[1]) != "" {
					jwt := string(tokens[1])
					clientRequest := &gatewayClientPB.VerifyTokenRequest{
						Jwt:   jwt,
						Uid:   "back_cms",
						Token: "ducnp_test", // TODO test token
					}
					claim, err := gateway.DoVerifyToken(ctx, clientRequest)
					if err != nil {
						return nil, status.Error(codes.Unauthenticated, codes.Unauthenticated.String())
					}
					if claim.Domain != "" {
						md[common.DomainMDKey] = []string{claim.Domain}
						userIDStr := util.ConvertUint32ToString(claim.UserId)
						md[common.UserIDMDKey] = []string{userIDStr}
						md[common.TokenMDKey] = []string{firstItem}
					}
					switch claim.StatusCode {
					case common.FAILED:
						if claim.RefreshToken == "" {
							return nil, status.Error(codes.Unauthenticated, codes.Unauthenticated.String())
						}
						userRefresh := &gatewayClientPB.UserRefreshTokenRequest{
							RefreshToken: claim.RefreshToken,
							Domain:       claim.Domain,
							UserId:       claim.UserId,
						}
						refreshResponse, err := gateway.DoUserRefreshToken(ctx, userRefresh)
						if err != nil || refreshResponse.JwtStr == "" || refreshResponse.StatusCode != common.ACCEPT {
							return nil, status.Error(codes.Unauthenticated, codes.Unauthenticated.String())
						}
						newMD := map[string][]string{
							"authorization": {refreshResponse.JwtStr},
						}
						_ = grpc.SetHeader(ctx, newMD)
						md[common.DomainMDKey] = []string{claim.Domain}
						return handler(ctx, req)
					case common.ACCEPT:
						ctx = metadata.NewIncomingContext(ctx, md)
						return handler(ctx, req)
					}
					return handler(ctx, req)

				}
			}
		}
		return nil, status.Error(codes.Unauthenticated, codes.Unauthenticated.String())
	}
}

func JWTHttpInterceptor( /*cimClient cimClientPB.CIMAPIClient,*/ excludePaths []string, next http.Handler) http.Handler {
	// pattern, _ := regexp.Compile(pattern)
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		next.ServeHTTP(w, req)
		// authorizationString := req.Header.Get("Authorization")
		// if authorizationString == "" {
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	_, _ = w.Write([]byte("Unauthorized"))
		// 	return
		// }
		// if !pattern.Match([]byte(authorizationString)) {
		// 	w.WriteHeader(http.StatusUnauthorized)
		// 	_, _ = w.Write([]byte("Unauthorized"))
		// 	return
		// }
		// tokens := pattern.FindSubmatch([]byte(authorizationString))
		// ctx := context.Background()
		// if len(tokens) == 2 && string(tokens[1]) != "" {
		// 	jwt := string(tokens[1])
		// 	domain, refreshTokenStr, statusCode, errorCode, err := checkJWT(ctx, cimClient, jwt)
		// 	if err != nil {
		// 		w.WriteHeader(http.StatusUnauthorized)
		// 		_, _ = w.Write([]byte("Unauthorized"))
		// 		return
		// 	}
		// 	if domain != "" {
		// 		req.Header.Set("domain", domain)
		// 	}
		// 	switch statusCode {
		// 	case common.FAILED:
		// 		if errorCode == common.ReasonJWTExpired.Code() {
		// 			// do refresh token
		// 			newToken, err := refreshToken(ctx, cimClient, domain, refreshTokenStr)
		// 			if err != nil {
		// 				w.WriteHeader(http.StatusUnauthorized)
		// 				_, _ = w.Write([]byte("Unauthorized"))
		// 				return
		// 			}
		// 			if newToken != "" {
		// 				w.Header().Add("authorization", newToken)
		// 				next.ServeHTTP(w, req)
		// 				return
		// 			}
		// 			w.WriteHeader(http.StatusUnauthorized)
		// 			_, _ = w.Write([]byte("Unauthorized"))
		// 			return
		// 		}
		// 		if errorCode == common.ReasonJWTInvalid.Code() {
		// 			w.WriteHeader(http.StatusUnauthorized)
		// 			_, _ = w.Write([]byte("Unauthorized"))
		// 			return
		// 		}
		// 	case common.DONE:
		// 		next.ServeHTTP(w, req)
		// 		return
		// 	}
		// 	next.ServeHTTP(w, req)
		// 	return
		// }
		// w.WriteHeader(http.StatusUnauthorized)
		// _, _ = w.Write([]byte("Unauthorized"))
	})
}

// func GetRequestMetadata(ctx context.Context) map[string]string {
// auth := strings.ToLower(u.cfg.Base.CISGRPC.Username) + ":" + u.cfg.Base.CISGRPC.Password
// enc := base64.StdEncoding.EncodeToString([]byte(auth))
// return map[string]string{
// 	"authorization": "Basic " + enc,
// }
// }
