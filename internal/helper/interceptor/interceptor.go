package helper_grpc

import (
	"context"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"math/rand"
	"reflect"
	"strings"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randString(n int) string {
	if n < 0 {
		return ""
	}
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
func nowAsUnixSecond() int64 {
	return time.Now().UnixNano() / 1e9
}

func getID() string {
	dest, _ := hex.DecodeString(fmt.Sprintf("%d", nowAsUnixSecond()))
	var id strings.Builder
	encode := base64.StdEncoding.EncodeToString(dest)
	rand.Seed(time.Now().UnixNano())
	id.WriteString(encode)
	id.WriteString(randString(4))
	return strings.Replace(id.String(), "=", randString(1), 1)
}

func SetUniqueIDUnaryInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		valueOf := reflect.ValueOf(req)
		typeOf := reflect.TypeOf(req)
		if typeOf.Kind() != reflect.Ptr {
			return handler(ctx, req)
		}
		transID := valueOf.Elem().FieldByName("TransactionID")
		if transID.Kind() == reflect.Invalid {
			transID = valueOf.Elem().FieldByName("TransactionId")
		}
		if transID.Kind() != reflect.String {
			return handler(ctx, req)
		}
		if transID.String() != "" {
			return handler(ctx, req)
		}
		id := getID()
		if transID.CanSet() {
			transID.SetString(id)
		}
		return handler(ctx, req)
	}
}
func UnpanicGRPC() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		defer func() {
			if errInf := recover(); errInf != nil {
				err = status.Errorf(codes.Internal, "Panic from server")
			}
		}()

		return handler(ctx, req)
	}
}
