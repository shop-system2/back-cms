package kafka

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	// log "github.com/sirupsen/log"
	"go-libs/logger"

	"github.com/Shopify/sarama"
)

type (
	KafkaConsumerGroup struct {
		ready           chan bool
		groups          sarama.ConsumerGroup
		singleConsumber sarama.Consumer
		MessageCh       chan *sarama.ConsumerMessage
		ErrorCh         chan *sarama.ConsumerError
	}
	PropertyKafkaConsumer struct {
		Broker   []string
		Topic    []string
		Group    string
		Version  string
		IsOldest bool
		Strategy string
	}
)

func InitKafkaConsumer(ctx context.Context, property PropertyKafkaConsumer) (*KafkaConsumerGroup, error) {
	kafkaVersion, err := sarama.ParseKafkaVersion(property.Version)
	if err != nil {
		logger.GlobaLogger.Panicf("Error while parsing kafka version", (err))
	}
	config := sarama.NewConfig()
	config.Version = kafkaVersion
	config.Consumer.Return.Errors = true
	config.Consumer.MaxWaitTime = 3 * time.Second

	kafkaConsumer := &KafkaConsumerGroup{
		ready:     make(chan bool),
		MessageCh: make(chan *sarama.ConsumerMessage),
		ErrorCh:   make(chan *sarama.ConsumerError),
	}
	config.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
	sarama.NewConsumer(property.Broker, config)
	consumer, err := sarama.NewConsumerGroup(property.Broker, property.Group, config)
	if err != nil {
		logger.GlobaLogger.Panicf(fmt.Sprintf("Error while creating consumer group: %v", (err)))
	}
	kafkaConsumer.groups = consumer
	// kafkaConsumer.singleConsumber =
	go func() {
		for {
			if err := consumer.Consume(ctx, property.Topic, kafkaConsumer); err != nil {
				if err == sarama.ErrClosedConsumerGroup {
					logger.GlobaLogger.Info("Consumer Group is closed")
					break
				}
				logger.GlobaLogger.Errorf(fmt.Sprintf("Consumer Group: Failed to consume: %v", (err)))
				time.Sleep(2 * time.Second)
			}
			if ctx.Err() != nil {
				return
			}
			kafkaConsumer.ready = make(chan bool)
		}
	}()
	<-kafkaConsumer.ready
	return kafkaConsumer, nil

}
func (kfg *KafkaConsumerGroup) HandleCloseConsumeGroup() {
	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)
	go func(sigterm chan os.Signal) {
		<-sigterm
		close(kfg.MessageCh)
		close(kfg.ErrorCh)
		logger.GlobaLogger.Info("*******STOP KAFKA CONSUMER **********")
		if err := kfg.groups.Close(); err != nil {
			logger.GlobaLogger.Errorf(fmt.Sprintf("Error closing client: %v", err))
		}
	}(sigterm)
}
func (kfg *KafkaConsumerGroup) Setup(sarama.ConsumerGroupSession) error {
	close(kfg.ready)
	return nil
}

func (kfg *KafkaConsumerGroup) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}
func (kfg *KafkaConsumerGroup) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for message := range claim.Messages() {
		logger.GlobaLogger.Info(fmt.Sprintf("%s - Message claimed", string(message.Key)))
		session.MarkMessage(message, "")
		kfg.MessageCh <- message
	}
	return nil
}
