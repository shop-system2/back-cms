package kafka

import (
	"encoding/json"
	"errors"
	"fmt"

	"go-libs/logger"

	"golang.org/x/net/context"

	"github.com/Shopify/sarama"
)

type (
	KafkaProducer interface {
		Send(ctx context.Context, producerName, topic, key string, value interface{}) error
	}
	kafkaProducer struct {
		ProducerName string
		IsReady      bool
		Topic        []string
	}
	syncKafkaProducer struct {
		kafkaProducer
		producerInstance sarama.SyncProducer
	}
	asyncKafkaProducer struct {
		kafkaProducer
		producerInstance sarama.AsyncProducer
	}
)

func NewKafkaProducer(async bool, producerName string, topics, brokers []string, version string) KafkaProducer {
	if async {
		asyncKafka, err := initAsyncKafkaProducer(producerName, topics, brokers, version)
		if err != nil {
			// logger.GlobaLogger.Panic("Failed to init async Kafka producer", (err))
			// TODO fix
			return asyncKafka
		}
		return asyncKafka
	}
	asyncKafka, err := initSyncKafkaProducer(producerName, topics, brokers, version)
	if err != nil {
		// logger.GlobaLogger.Panic("Failed to init sync Kafka producer", (err))
		// TODO fix
		return asyncKafka

	}
	return asyncKafka
}

func initAsyncKafkaProducer(producerName string, topics, brokers []string, version string) (kafka *asyncKafkaProducer, err error) {
	versionKafka, err := sarama.ParseKafkaVersion(version)
	if err != nil {
		logger.GlobaLogger.Errorf("Can't parse kafka version, err %v", (err))
		return nil, err
	}
	config := sarama.NewConfig()
	config.Version = versionKafka
	config.Producer.RequiredAcks = sarama.WaitForLocal
	config.Producer.Return.Errors = true
	config.Producer.Return.Successes = true

	kafkaPro, err := sarama.NewAsyncProducer(brokers, config)
	if err != nil {
		logger.GlobaLogger.Errorf("Can't init kafka producer with err %v", (err))
		return nil, err
	}
	logger.GlobaLogger.Infof("Init Async Kafka Producer successfully")

	go func() {
		for {
			select {
			case err := <-kafkaPro.Errors():
				fmt.Println("kafka sending err, ", err.Msg.Key)
				kafkaPro.Input() <- err.Msg
			case sucess := <-kafkaPro.Successes():
				fmt.Println("success:", sucess)
			}
		}
	}()

	kafka = &asyncKafkaProducer{
		producerInstance: kafkaPro,
		kafkaProducer: kafkaProducer{
			Topic:        topics,
			IsReady:      true,
			ProducerName: producerName,
		},
	}
	return kafka, nil
}

func initSyncKafkaProducer(producerName string, topics, brokers []string, version string) (kafka *syncKafkaProducer, err error) {
	config := sarama.NewConfig()
	kafkaPro, err := sarama.NewSyncProducer([]string{""}, config)
	if err != nil {
		logger.GlobaLogger.Errorf("Can't init kafka producer with err %v", (err))
		return nil, err
	}
	logger.GlobaLogger.Infof("Init sync Kafka Producer successfully")
	kafka = &syncKafkaProducer{
		producerInstance: kafkaPro,
		kafkaProducer: kafkaProducer{
			Topic:        topics,
			IsReady:      true,
			ProducerName: producerName,
		},
	}
	return kafka, nil
}

func (h *asyncKafkaProducer) Send(ctx context.Context, producerName, topic, key string, value interface{}) error {
	if !h.IsReady {
		return errors.New("kafkaf producer not is ready")
	}

	if h.ProducerName != producerName {
		return errors.New("wrong producer name")
	}

	existTopic := false
	for _, value := range h.Topic {
		if value == topic {
			existTopic = true
			break
		}
	}
	if !existTopic {
		return errors.New("wrong topic")
	}

	buffer, err := json.Marshal(value)
	if err != nil {
		return errors.New("can't marshal object")
	}
	message := &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(key),
		Value: sarama.ByteEncoder(buffer),
	}
	logger.GlobaLogger.Infof("Send to queue")
	h.producerInstance.Input() <- message
	return nil
}
func (h *syncKafkaProducer) Send(ctx context.Context, producerName, topic, key string, value interface{}) error {
	return nil
}
