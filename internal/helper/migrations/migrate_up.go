package migrations

import "fmt"

func (m *Migrator) MigrateUp(stepUp int) error {
	// fmt.Println("run func migraion Up")
	tx, err := m.Database.Begin()
	if err != nil {
		return err
	}
	count := 0
	for _, v := range m.Versions {
		if stepUp > 0 && count == stepUp {
			break

		}
		mg := m.Migrations[v]
		if mg.Done {
			continue
		}
		err := mg.Up(tx)
		if err != nil {
			tx.Rollback()
			return err
		}
		_, err = tx.Exec("INSERT INTO `schema_migrations` VALUES(?)", mg.Version)
		if err != nil {
			tx.Rollback()
			return err
		}
		fmt.Println("Finished running migration", mg.Version)
		count++
	}

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}
