package migrations

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"text/template"
	"time"
)

type (
	Migration struct {
		Version string
		Done    bool
		Up      func(*sql.Tx) error
		Down    func(*sql.Tx) error
	}

	// Migrator will have a collection of migration, store the list of migration version in array
	Migrator struct {
		Database   *sql.DB
		Versions   []string
		Migrations map[string]*Migration
	}
)

// migrator create list
var migrator = &Migrator{
	Versions:   []string{},
	Migrations: map[string]*Migration{},
}

func (m *Migrator) AddMigration(mg *Migration) {
	m.Migrations[mg.Version] = mg
	index := 0
	for index < len(m.Versions) {

		if m.Versions[index] > mg.Version {
			break
		}
		index++
	}

	m.Versions = append(m.Versions, mg.Version)
	copy(m.Versions[index+1:], m.Versions[index:])
	m.Versions[index] = mg.Version
}

func Create(name string) error {
	version := time.Now().Format("20060102150405")
	in := struct {
		Version string
		Name    string
	}{
		Version: version,
		Name:    name,
	}
	_, errCreate := os.Create(fmt.Sprintf("internal/helper/migrations/%s_%s.go", version, name))
	if errCreate != nil {
		fmt.Println("create failed")
		return errCreate
	}

	var out bytes.Buffer
	file, errFile := template.ParseFiles("internal/helper/migrations/template.txt")
	if errFile != nil {
		fmt.Println("failed open file")
		return errFile
	}
	tem := template.Must(file, nil)

	err := tem.Execute(&out, in)
	if err != nil {
		return errors.New("Unable to execute template:" + err.Error())
	}
	f, err := os.Create(fmt.Sprintf("internal/helper/migrations/%s_%s.go", version, name))
	if err != nil {
		return errors.New("Unable to create migration file:" + err.Error())
	}
	defer f.Close()
	if _, err := f.WriteString(out.String()); err != nil {
		return errors.New("Unable to write to migration file:" + err.Error())
	}

	// fmt.Println("create file OK.")
	return nil

}

func InitDB(db *sql.DB) (*Migrator, error) {
	migrator.Database = db
	_, errE := db.Exec(`CREATE TABLE IF NOT EXISTS schema_migrations (version varchar(255));`)
	if errE != nil {
		fmt.Println("Unable to create `schema_migrations` table", errE)
		return nil, errE
	}
	rows, err := db.Query("SELECT version FROM `schema_migrations`;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var version string
		err := rows.Scan(&version)
		if err != nil {
			return nil, err
		}

		if migrator.Migrations[version] != nil {
			migrator.Migrations[version].Done = true
		}
	}
	return migrator, nil
}

func (m *Migrator) MigrationStatus() error {
	for _, v := range m.Versions {
		mg := m.Migrations[v]
		if mg.Done {
			vDone := fmt.Sprintf("Migraion done: %s", v)
			fmt.Println(vDone)
		} else {
			vDone := fmt.Sprintf("Migration pending: %s", v)
			fmt.Println(vDone)
		}
	}
	return nil
}

func init() {
	migrateCreateCmd.Flags().StringP("name", "n", "table", "Name for the migration")
	migrateUpCmd.Flags().IntP("step", "s", 0, "Number of migrations to execute")
	migrateDownCmd.Flags().IntP("step", "s", 0, "Number of migrations to execute")
	migrateCmd.AddCommand(migrateCreateCmd, migrateUpCmd, migrateDownCmd)
	rootCmd.AddCommand(migrateCmd)
}

// Execute ..
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		log.Fatalln(err.Error())
	}
}
