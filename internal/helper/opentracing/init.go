package opentracing

import (
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-client-go/config"
)

func InitJeager(service string) error {
	cfg := &config.Configuration{
		ServiceName: service,
		Reporter: &config.ReporterConfig{
			LogSpans: true,
		},
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
	}
	_, err := cfg.InitGlobalTracer(
		service,
		config.Logger(jaeger.StdLogger),
	)
	if err != nil {
		return err
	}
	return nil
}
