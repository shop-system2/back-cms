package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type LoggerCommon struct {
	ID              primitive.ObjectID `bson:"_id,omitempty"`
	TransactionID   string             `bson:"transactionid,omitempty"`
	AppName         string             `bson:"appname,omitempty"`
	Namespace       string             `bson:"namespace,omitempty"`
	Pod             string             `bson:"pod,omitempty"`
	Host            string             `bson:"host,omitempty"`
	Level           string             `bson:"level,omitempty"`
	Message         string             `bson:"message,omitempty"`
	Time            string             `bson:"time,omitempty"`
	ResponseContent string             `bson:"responsecontent,omitempty"`
	RequestContent  string             `bson:"requestcontent,omitempty"`
	Method          string             `bson:"method,omitempty"`
	FileName        string             `bson:"filename,omitempty"`
	Line            int64              `bson:"line,omitempty"`
}
