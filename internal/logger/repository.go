package loggersystem

import (
	"context"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/logger/model"
)

type (
	// LoggerRepository declare all func in comment repository
	LoggerRepository interface {
		InsertRecord(ctx context.Context, log model.LoggerCommon) error
	}
	loggerRepository struct {
		dbmongo db.MongoHelper
	}
)

const databaseName string = "logger"

// NewLoggerRepository func new instance comment repository
func NewLoggerRepository(dbmongo db.MongoHelper) LoggerRepository {
	return &loggerRepository{
		dbmongo: dbmongo,
	}
}

func (r *loggerRepository) InsertRecord(ctx context.Context, log model.LoggerCommon) error {
	client := r.dbmongo.Open()
	database := client.Database(databaseName)
	_, _ = database.Collection(log.AppName).InsertOne(ctx, log)
	return nil
}
