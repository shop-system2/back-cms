package loggersystem

import (
	"context"
	"shop-back-cms/internal/helper/kafka"
	"shop-back-cms/internal/logger/model"
	"time"
)

var FuncLogger *globalLogger = &globalLogger{}

type (
	GlobalLogger interface {
		WriteToKafka(ctx context.Context) error
		ReadKafka(log model.LoggerCommon) error
	}

	globalLogger struct {
		topic         string
		producerName  string
		kafkaProducer kafka.KafkaProducer
		repository    LoggerRepository
	}
)

func NewGlobalLogger(topic, producername string, kafka kafka.KafkaProducer, repository LoggerRepository) GlobalLogger {
	global := &globalLogger{
		topic:         topic,
		producerName:  producername,
		kafkaProducer: kafka,
		repository:    repository,
	}
	FuncLogger = global
	return global
}

func (g *globalLogger) WriteToKafka(ctx context.Context) error {
	value := model.LoggerCommon{
		AppName:         "BackCms",
		Namespace:       "uat",
		Pod:             "card-back-office-74669dfdbb-f7pwz",
		Time:            time.Now().Format("15-04-05-02-01-2006"),
		ResponseContent: "ResponseContent",
		Method:          "ducnp-method",
		Line:            123,
		FileName:        "log.go",
		Level:           "INFO",
		Message:         "tesst message queue",
		RequestContent:  "RequestContent",
		Host:            "127.0.0.1",
		TransactionID:   "123123123123",
	}
	err := g.kafkaProducer.Send(ctx, g.producerName, g.topic, "LOGGER", value)
	if err != nil {
		return err
	}
	return nil
}
func (g *globalLogger) ReadKafka(log model.LoggerCommon) error {
	g.repository.InsertRecord(context.Background(), log)
	return nil

}
