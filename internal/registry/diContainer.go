package registry

import (
	"fmt"
	"go-libs/logger"
	"shop-back-cms/config"
	"shop-back-cms/internal/adapter"
	"shop-back-cms/internal/api"
	"shop-back-cms/internal/facade"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/helper/kafka"

	// "shop-back-cms/internal/helper/logger"
	loggersystem "shop-back-cms/internal/logger"
	"shop-back-cms/internal/repository"

	"shop-back-cms/internal/usecase"

	"sync"

	gatewayClient "shop-gateway/cmd/client"

	"github.com/sarulabs/di"
	"gorm.io/gorm"
)

var (
	buildOnce sync.Once
	builder   *di.Builder
	container di.Container
)

const (

	// service gateway
	ServiceIdentityManagement string = "ServiceIdentityManagement"

	// api
	APIDIName          string = "API"
	ConsumberAPIDIName string = "ConsumberAPI"
	UploadAPIDIName    string = "UploadAPI"

	// External
	RedisCacheHelper      string = "RedisCacheHelper"
	ExternalDBMysqlHelper string = "ExternalDBMysqlHelper"
	MongodbExternal       string = "ExternalDBMongo"
	// ConfigApp a
	ConfigApp string = "ConfigApp"

	// UserUsecase
	UserUsecase     string = "UserUsecase"
	UserRespository string = "UserRespository"

	// CommentUsecase
	CommentUsecase     string = "CommentUsecase"
	CommentRespository string = "CommentRespository"

	// HelperPbCopier
	HelperPbCopier      string = "HelperPbCopier"
	HelperModelCopier   string = "HelperModelCopier"
	HelperDB            string = "HelperDB"
	RedisCacheHelperss  string = "RedisCacheHelper"
	kafkaProducerIDName string = "KafkaProducer"

	loggerHelperIDName string = "LoggerHelper"

	// usecase
	ProductUsecase          string = "ProductUsecase"
	ReportUsecase           string = "ReportUsecase"
	ClientUsecase           string = "ClientUsecase"
	CategoryUsecase         string = "CategoryUsecase"
	VendorUsecase           string = "VendorUsecase"
	ManagementSystemUsecase string = "ManagementSystemUsecase"
	NotificationUsecase     string = "NotificationUsecase"
	OrderUsecase            string = "OrderUsecase"
	UploadFileUsecase       string = "UploadFileUsecase"
	LoggerSystemUsecase     string = "LoggerSystemUsecase"
	// repository
	CategoryRespository        string = "CategoryRespositorys"
	VendorRespository          string = "VendorRespositorys"
	ClientRespository          string = "ClientRespositorys"
	ReportRespository          string = "ReportRespositorys"
	ProductRespository         string = "ProductRespository"
	ManagementSystemRepository string = "ManagementSystemRepository"
	OrderRespository           string = "OrderRespository"
	FileRespository            string = "FileRespository"
	RepositoryLogsystem        string = "RepositoryLogsystem"
	// adapter
	NotificationAdapterNameDI string = "NotificationAdapter"

	// external client
	GatewayClientNameDI string = "GatewayClient"

	// facade
	ProcessFileFacadeNameDI string = "ProcessFileFacade"
)

// BuildDiContainer represent DiContainer
func BuildDiContainer() {
	buildOnce.Do(func() {

		builder, _ = di.NewBuilder()

		if err := buildRepository(); err != nil {
			panic(err)
		}
		if err := buildUsecase(); err != nil {
			panic(err)
		}
		if err := buildAPI(); err != nil {
			panic(err)
		}
		if err := buildAdapter(); err != nil {
			panic(err)
		}
		if err := buildAPIUsing(); err != nil {
			panic(err)
		}
		if err := buildHelper(); err != nil {
			panic(err)
		}
		if err := ExternalHelperBuild(); err != nil {
			panic(err)
		}

		// if err := clientExternalBuild(); err != nil {
		// 	panic(err)
		// }
		if err := buildFacade(); err != nil {
			panic(err)
		}

		container = builder.Build()
	})

}

// buildAPIUsing create instance for api
func buildAPIUsing() error {
	defs := []di.Def{}
	APIDef := di.Def{
		Name:  APIDIName,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			// productUsecase := ctn.Get(ProductUsecase).(usecase.ProductUsecase)
			// orderUsecase := ctn.Get(OrderUsecase).(usecase.OrderUsecase)
			// commentUsecase := ctn.Get(CommentUsecase).(usecase.CommentUsecase)
			// categoryUsecase := ctn.Get(CategoryUsecase).(usecase.CategoryUsecase)
			// helperPbCopier := ctn.Get(HelperPbCopier).(helper.PbCopier)
			// vendorUsecase := ctn.Get(VendorUsecase).(usecase.VendorUsecase)
			// clientUsecase := ctn.Get(ClientUsecase).(usecase.ClientUsecase)
			// reportUsecase := ctn.Get(ReportUsecase).(usecase.ReportUsecase)
			// managementSystemUsecase := ctn.Get(ManagementSystemUsecase).(usecase.ManagementSystemUsecase)
			// notificationUsecase := ctn.Get(NotificationUsecase).(usecase.NotificationUsecase)
			// return api.NewAPI(helperPbCopier, productUsecase, orderUsecase, commentUsecase,
			// 	categoryUsecase, vendorUsecase, clientUsecase, reportUsecase, managementSystemUsecase,
			// 	notificationUsecase), nil

			productUsecase := ctn.Get(ProductUsecase).(usecase.ProductUsecase)
			// orderUsecase := ctn.Get(OrderUsecase).(usecase.OrderUsecase)
			// commentUsecase := ctn.Get(CommentUsecase).(usecase.CommentUsecase)
			categoryUsecase := ctn.Get(CategoryUsecase).(usecase.CategoryUsecase)
			helperPbCopier := ctn.Get(HelperPbCopier).(helper.PbCopier)
			// vendorUsecase := ctn.Get(VendorUsecase).(usecase.VendorUsecase)
			// clientUsecase := ctn.Get(ClientUsecase).(usecase.ClientUsecase)
			// reportUsecase := ctn.Get(ReportUsecase).(usecase.ReportUsecase)
			// managementSystemUsecase := ctn.Get(ManagementSystemUsecase).(usecase.ManagementSystemUsecase)
			// notificationUsecase := ctn.Get(NotificationUsecase).(usecase.NotificationUsecase)
			return api.NewAPI(helperPbCopier, productUsecase, nil, nil,
				categoryUsecase, nil, nil, nil, nil,
				nil), nil

		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, APIDef)

	consumerAPIDef := di.Def{
		Name:  ConsumberAPIDIName,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			return api.NewKafkaConsumerAPI(cfg), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, consumerAPIDef)

	uploadAPIDef := di.Def{
		Name:  UploadAPIDIName,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			uploadFileUsecase := ctn.Get(UploadFileUsecase).(usecase.UploadFileUsecase)
			return api.NewUploadFileAPI(cfg, uploadFileUsecase), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, uploadAPIDef)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

// buildAPI create instance for api
func buildAPI() error {
	defs := []di.Def{}

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func ExternalHelperBuild() error {
	defs := []di.Def{}
	cacheHelperExternal := di.Def{
		Name:  RedisCacheHelper,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get("ConfigApp").(*config.Config)
			return cache.NewRedisInstance(cfg.Redis.Addrs, cfg.Redis.Password, cfg.Redis.Database), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, cacheHelperExternal)

	dbMysqlHelperExternal := di.Def{
		Name:  ExternalDBMysqlHelper,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			mysqlDB := db.NewMysqlDBHelper(cfg.Mysql.Host, cfg.Mysql.Port, cfg.Mysql.Username, cfg.Mysql.Password, cfg.Mysql.Database, cfg.Mysql.IsMigration)
			return mysqlDB, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, dbMysqlHelperExternal)

	gormDatabase := di.Def{
		Name:  "GormDatabase",
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			mysqlDB := db.NewMysqlGorm(cfg.Mysql.Host, cfg.Mysql.Port, cfg.Mysql.Username, cfg.Mysql.Password, cfg.Mysql.Database, cfg.Mysql.IsMigration)
			return mysqlDB, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, gormDatabase)

	mongodbExternal := di.Def{
		Name:  MongodbExternal,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			mysqlDB := db.NewMongoDB(cfg.Mongo.StringConnect)
			return mysqlDB, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, mongodbExternal)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildUsecase() error {
	defs := []di.Def{}

	productUsecaseDef := di.Def{
		Name:  ProductUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			productRepository := ctn.Get(ProductRespository).(repository.ProductRepository)
			helperPbCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			return usecase.NewProductUsecase(cfg, productRepository, helperRedis, helperPbCopier), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, productUsecaseDef)

	orderUsecaseDef := di.Def{
		Name:  OrderUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			orderRepository := ctn.Get(OrderRespository).(repository.OrderRepository)
			helperPbCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			return usecase.NewOrderUsecase(cfg, orderRepository, helperRedis, helperPbCopier), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, orderUsecaseDef)

	categoryUsecaseDef := di.Def{
		Name:  CategoryUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			categoryRepository := ctn.Get(CategoryRespository).(repository.CategoryRepository)
			helperModelCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			cfg := ctn.Get(ConfigApp).(*config.Config)
			return usecase.NewCategoryUsecase(cfg, helperModelCopier, categoryRepository, helperRedis), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, categoryUsecaseDef)

	managementSystemUsecaseDef := di.Def{
		Name:  ManagementSystemUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			managementSystemRepository := ctn.Get(ManagementSystemRepository).(repository.ManagementSystemRepository)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)

			return usecase.NewManagementSystemUsecase(cfg, helperRedis, managementSystemRepository), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, managementSystemUsecaseDef)

	vendorUsecaseDef := di.Def{
		Name:  VendorUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			vendorRepository := ctn.Get(VendorRespository).(repository.VendorRepository)
			helperModelCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			return usecase.NewVendorUsecase(cfg, helperModelCopier, vendorRepository, helperRedis), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, vendorUsecaseDef)

	clientUsecaseDef := di.Def{
		Name:  ClientUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			clientRepository := ctn.Get(ClientRespository).(repository.ClientRepository)
			helperModelCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			cfg := ctn.Get(ConfigApp).(*config.Config)
			return usecase.NewClientUsecase(cfg, clientRepository, helperModelCopier, helperRedis), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, clientUsecaseDef)

	reportUsecaseDef := di.Def{
		Name:  ReportUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			reportRepository := ctn.Get(ReportRespository).(repository.ReportRepository)
			helperModelCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			cacheHelper := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			return usecase.NewReportUsecase(helperModelCopier, reportRepository, cacheHelper), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, reportUsecaseDef)

	commentUsecaseDef := di.Def{
		Name:  CommentUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			commentRepository := ctn.Get(CommentRespository).(repository.CommentRepository)
			helperModelCopier := ctn.Get(HelperModelCopier).(helper.ModelCopier)
			cacheHelper := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			return usecase.NewCommentUsecase(cfg, commentRepository, helperModelCopier, cacheHelper), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, commentUsecaseDef)

	notificationUsecaseDef := di.Def{
		Name:  NotificationUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			notificationAdapter := ctn.Get(NotificationAdapterNameDI).(adapter.NotificationAdapter)
			return usecase.NewNotificationUsecase(cfg, notificationAdapter), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, notificationUsecaseDef)

	processFilesecaseDef := di.Def{
		Name:  UploadFileUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			cacheHelper := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			fileFacade := ctn.Get(ProcessFileFacadeNameDI).(facade.ProcessFileFacade)
			fileRespository := ctn.Get(FileRespository).(repository.FileRepository)
			return usecase.NewUploadFileUsecase(cfg, cacheHelper, fileFacade, fileRespository), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, processFilesecaseDef)

	loggerUesecaseDef := di.Def{
		Name:  LoggerSystemUsecase,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			kafkaProducer := ctn.Get(kafkaProducerIDName).(kafka.KafkaProducer)
			logsystem := ctn.Get(RepositoryLogsystem).(loggersystem.LoggerRepository)
			return loggersystem.NewGlobalLogger(cfg.Kafka.Topics[0], cfg.Kafka.ProducerName, kafkaProducer, logsystem), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, loggerUesecaseDef)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildAdapter() error {
	defs := []di.Def{}

	jwtAdapter := di.Def{
		Name:  "JWTAdapter",
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			// return repository.NewProductRepository(), nil
			return adapter.NewJWTAdapter(), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, jwtAdapter)

	notificationAdapter := di.Def{
		Name:  NotificationAdapterNameDI,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			kafkaProducer := ctn.Get(kafkaProducerIDName).(kafka.KafkaProducer)
			return adapter.NewNotificationAdapter(cfg, kafkaProducer), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, notificationAdapter)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildHelper() error {
	defs := []di.Def{}

	kafkaProducer := di.Def{
		Name:  kafkaProducerIDName,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			return kafka.NewKafkaProducer(cfg.Kafka.Async, cfg.Kafka.ProducerName, cfg.Kafka.Topics,
				cfg.Kafka.Brokers, cfg.Kafka.Version), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, kafkaProducer)

	configPbCopier := di.Def{
		Name:  ConfigApp,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			config, err := config.LoadConfig()
			if err != nil {
				return nil, err
			}
			return config, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, configPbCopier)

	helperPbCopier := di.Def{
		Name:  HelperPbCopier,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			return helper.NewPbCopier(), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, helperPbCopier)

	helperModelCopier := di.Def{
		Name:  HelperModelCopier,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			return helper.NewModelCopier(), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, helperModelCopier)

	loggerDIName := di.Def{
		Name:  loggerHelperIDName,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			// cfg := ctn.Get(ConfigApp).(*config.Config)
			// apiLogger := logger.NewAPILogger(cfg)
			// apiLogger.InitLogger()
			// return apiLogger, nil

			cfg := ctn.Get(ConfigApp).(*config.Config)
			apiLogger := logger.Newlogger(cfg.Logger.Mode, cfg.Logger.Level, cfg.Logger.Encoding)
			return apiLogger, nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, loggerDIName)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildRepository() error {
	defs := []di.Def{}

	commentRepository := di.Def{
		Name:  CommentRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewcommentRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, commentRepository)

	productRepository := di.Def{
		Name:  ProductRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			dbGorm := ctn.Get("GormDatabase").(*gorm.DB)
			return repository.NewProductRepository(db, dbGorm), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, productRepository)

	logysytemRepository := di.Def{
		Name:  RepositoryLogsystem,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(MongodbExternal).(db.MongoHelper)
			return loggersystem.NewLoggerRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, logysytemRepository)

	orderRepository := di.Def{
		Name:  OrderRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewOrderRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, orderRepository)

	managementSystemRepository := di.Def{
		Name:  ManagementSystemRepository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewManagementSystemRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, managementSystemRepository)

	categoryRepository := di.Def{
		Name:  CategoryRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			dbGorm := ctn.Get("GormDatabase").(*gorm.DB)
			return repository.NewCategoryRepository(db, dbGorm), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, categoryRepository)

	vendorRepository := di.Def{
		Name:  VendorRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewVendorRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, vendorRepository)

	clientRepository := di.Def{
		Name:  ClientRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			db := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewClientRepository(db), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, clientRepository)

	reportRepository := di.Def{
		Name:  ReportRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			mysqlHelper := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewReportRepository(mysqlHelper), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, reportRepository)

	fileRepository := di.Def{
		Name:  FileRespository,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			mysqlHelper := ctn.Get(ExternalDBMysqlHelper).(db.DBHelper)
			return repository.NewFileRepository(mysqlHelper), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, fileRepository)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func clientExternalBuild() error {
	defs := []di.Def{}
	gateway := di.Def{
		Name:  GatewayClientNameDI,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			addr := fmt.Sprintf("%s:%d", cfg.GatewayClient.Host, cfg.GatewayClient.Port)
			return gatewayClient.NewIMClient(addr)
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, gateway)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

func buildFacade() error {
	defs := []di.Def{}
	processFileFacedeNameDI := di.Def{
		Name:  ProcessFileFacadeNameDI,
		Scope: di.App,
		Build: func(ctn di.Container) (interface{}, error) {
			cfg := ctn.Get(ConfigApp).(*config.Config)
			helperRedis := ctn.Get(RedisCacheHelper).(cache.CacheHelper)
			fileRespository := ctn.Get(FileRespository).(repository.FileRepository)
			return facade.NewProcessFileFacade(cfg, helperRedis, fileRespository), nil
		},
		Close: func(obj interface{}) error {
			return nil
		},
	}
	defs = append(defs, processFileFacedeNameDI)

	err := builder.Add(defs...)
	if err != nil {
		return err
	}
	return nil
}

// GetDependency represent container
func GetDependency(depen string) interface{} {
	return container.Get(depen)
}
