package repository

import "gorm.io/gorm"

const (
	// status of query, get all, get record deleted, get record not delete
	GetActive = iota
	GetDeActive
	GetAll
)

func AppendSql(db *gorm.DB, debug bool, actionRecord int) *gorm.DB {
	if debug {
		db = db.Debug()
	}
	if actionRecord == GetActive {
		return db
	}
	if actionRecord == GetDeActive {
		return db.Unscoped().Where("deleted_at IS NOT NULL")
	}
	return db.Unscoped()
}
