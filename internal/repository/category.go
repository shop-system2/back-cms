package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
	"strings"

	"gorm.io/gorm"
)

/*
* category
 */
type (
	// CategoryRepository declare all func in comment repository
	CategoryRepository interface {
		GetListCategory(ctx context.Context, req *dto.GetListCategoryRequestDTO) ([]model.Category, error)
		PostNewCategory(ctx context.Context, req *dto.PostNewCategoryRequestDTO) (int64, error)
		PutUpdateCategory(ctx context.Context, req *dto.PutUpdateCategoryRequestDTO) error
		DeleteCategory(ctx context.Context, req *dto.DeleteCategoryRequestDTO) error
		DoActiveCategory(ctx context.Context, categoryID, isActive uint32) error
		GetCategoryDetail(ctx context.Context, categoryID uint32) (*model.Category, error)
		SearchCategory(ctx context.Context, req *dto.SearchCategoryRequestDTO) ([]*model.Category, error)
	}
	categoryRepository struct {
		dbHelper db.DBHelper
		gorm     *gorm.DB
	}
)

// NewCategoryRepository func new instance category repository
func NewCategoryRepository(dbHelper db.DBHelper, gorm *gorm.DB) CategoryRepository {
	return &categoryRepository{
		dbHelper: dbHelper,
		gorm:     gorm,
	}
}

func (r *categoryRepository) SearchCategory(ctx context.Context,
	req *dto.SearchCategoryRequestDTO) (res []*model.Category, err error) {
	var statement strings.Builder
	var (
		category model.Category
		args     []interface{}
	)
	stmt := `SELECT id, name, code , is_active, description , created_at, updated_at FROM categories WHERE 1=1 `

	statement.WriteString(stmt)
	if req.Name != "" {
		statement.WriteString(` and name like`)
		statement.WriteString(` '%`)
		statement.WriteString(fmt.Sprint(req.Name))
		statement.WriteString(`%' `)
		// args = append(args, req.Name)
	}
	if req.Code != "" {
		statement.WriteString(` and code = ?`)
		args = append(args, req.Code)
	}
	if req.IsActive {
		statement.WriteString(` and is_active = 1`)
	}
	statement.WriteString(` ORDER BY id DESC`)
	fmt.Println(statement.String())
	rows, err := r.dbHelper.Open().Query(statement.String(), args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		// err = rows.Scan(&category.ID, &category.Name, &category.Email, &category.Address, &category.PhoneNumber, &category.AccountBank, &category.CreatedAt, &category.UpdatedAt)
		// if err != nil {
		// 	return nil, errors.New((common.ReasonDBError.Code()))
		// }
		// res = append(res, category)
		err = rows.Scan(&category.ID,
			&category.Name,
			&category.Code,
			&category.IsActive,
			&category.Description,
			&category.CreatedAt,
			&category.UpdatedAt)
		if err != nil {
			return nil, err
		}
		res = append(res, &category)
	}
	if len(res) == 0 {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	return res, nil
}

func (r *categoryRepository) GetListCategory(ctx context.Context,
	req *dto.GetListCategoryRequestDTO) (res []model.Category, err error) {
	var (
		category = []model.Category{}
	)
	db := r.gorm
	db = AppendSql(db, true, GetAll)
	err = db.Table(model.Category{}.Table()).Find(&category).Error

	if err != nil {
		return category, err
	}
	return category, nil

	// stmt := `SELECT id, name, code , is_active, description , created_at, updated_at FROM categories WHERE 1=1 `
	// builder := strings.Builder{}
	// builder.WriteString(stmt)
	// if req.IsActive {
	// 	builder.WriteString(` and is_active = 1`)
	// }
	// builder.WriteString(` ORDER BY id DESC`)
	// rows, err := r.dbHelper.Open().Query(builder.String())
	// if err != nil {
	// 	return nil, err
	// }
	// defer rows.Close()
	// for rows.Next() {
	// 	err = rows.Scan(&category.ID,
	// 		&category.Name,
	// 		&category.Code,
	// 		&category.IsActive,
	// 		&category.Description,
	// 		&category.CreatedAt,
	// 		&category.UpdatedAt)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	res = append(res, category)
	// }
	// return res, nil
}

func (r *categoryRepository) PostNewCategory(ctx context.Context,
	req *dto.PostNewCategoryRequestDTO) (rowAffeted int64, err error) {
	stmt := "insert into categories (name, code, description) values (?, ?, ?)"
	result, err := r.dbHelper.Open().Exec(stmt, req.Name, req.Code, req.Description)
	if err != nil {
		return rowAffeted, err
	}
	rowAffeted, err = result.RowsAffected()
	return rowAffeted, err
}

func (r *categoryRepository) PutUpdateCategory(ctx context.Context, req *dto.PutUpdateCategoryRequestDTO) (err error) {

	smtp := "update categories set name = ?, code = ? where id = ?"
	result, err := r.dbHelper.Open().Exec(smtp, req.Name, req.Code, req.Id)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonNotFound.Code()))
	}
	return nil
}

func (r *categoryRepository) DeleteCategory(ctx context.Context, req *dto.DeleteCategoryRequestDTO) (err error) {
	smtp := `delete from categories where id = ?`
	result, err := r.dbHelper.Open().Exec(smtp, req.Id)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonNotFound.Code()))
	}
	return nil
}

func (r *categoryRepository) DoActiveCategory(ctx context.Context, roleID, isActive uint32) error {
	stmt := `update categories set is_active = ? where id = ?`
	result, err := r.dbHelper.Open().Exec(stmt, isActive, roleID)
	if err != nil {
		return err
	}
	numEffect, _ := result.RowsAffected()
	if numEffect == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}
func (r *categoryRepository) GetCategoryDetail(ctx context.Context, categoryID uint32) (category *model.Category, err error) {
	stmt := `select id, name, code,is_active, description, created_at, created_at from categories where id = ?`
	row := r.dbHelper.Open().QueryRow(stmt, categoryID)
	category = &model.Category{}
	err = row.Scan(&category.ID, &category.Name, &category.Code, &category.IsActive,
		&category.Description, &category.CreatedAt, &category.UpdatedAt)
	if err != nil {
		return nil, err
	}
	if err == sql.ErrNoRows {
		return nil, err
	}
	return category, nil
}
