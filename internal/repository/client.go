package repository

import (
	"context"
	"database/sql"
	"errors"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
	"strings"
)

type (
	// ClientRepository declare all func in comment repository
	ClientRepository interface {
		GetListClient(ctx context.Context, req *dto.GetListClientRequestDTO) ([]model.ClientModel, error)
		SearchClient(ctx context.Context, req *dto.SearchClientRequestDTO) ([]model.ClientModel, error)
		GetClientDetail(ctx context.Context, clientId uint32) (*model.ClientModel, error)
		PostNewClient(ctx context.Context, req *dto.PostNewClientRequestDTO) error
		PutUpdateClient(ctx context.Context, req *dto.PutUpdateClientRequestDTO) error
		DeleteClient(ctx context.Context, clientId uint32) error
		DoActiveClient(ctx context.Context, categoryID, isActive uint32) error
	}
	clientRepository struct {
		dbHelper db.DBHelper
	}
)

// NewClientRepository func new instance comment repository
func NewClientRepository(dbHelper db.DBHelper) ClientRepository {
	return &clientRepository{
		dbHelper: dbHelper,
	}
}

func (r *clientRepository) DoActiveClient(ctx context.Context, clientID, isActive uint32) error {
	stmt := `update client set is_active = ? where id = ?`
	result, err := r.dbHelper.Open().Exec(stmt, isActive, clientID)
	if err != nil {
		return err
	}
	numEffect, _ := result.RowsAffected()
	if numEffect == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}

func (r *clientRepository) GetListClient(ctx context.Context,
	req *dto.GetListClientRequestDTO) (res []model.ClientModel, err error) {
	var (
		client model.ClientModel
	)
	stmt := `	select id, name, email, address, phone_number, account_bank, created_at, updated_at ,is_active 
				from client 
				 order by id desc`
	rows, err := r.dbHelper.Open().Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&client.ID, &client.Name, &client.Email, &client.Address, &client.PhoneNumber, &client.AccountBank,
			&client.CreatedAt, &client.UpdatedAt, &client.IsActive)
		if err != nil {
			return nil, err
		}
		res = append(res, client)
	}
	return res, nil
}

func (r *clientRepository) SearchClient(ctx context.Context, req *dto.SearchClientRequestDTO) (res []model.ClientModel, err error) {
	var statement strings.Builder
	var (
		client model.ClientModel
		args   []interface{}
	)
	stmt := `SELECT id, 
					name, 
					email, 
					address, 
					phone_number, 
					account_bank, 
					created_at, 
					updated_at
					FROM v_client 
					WHERE 1=1 `
	statement.WriteString(stmt)
	if req.Name != "" {
		statement.WriteString(` and name = ?`)
		args = append(args, req.Name)
	}
	if req.Email != "" {
		statement.WriteString(` and email = ?`)
		args = append(args, req.Email)
	}
	if req.PhoneNumber != "" {
		statement.WriteString(` and phone_number = ?`)
		args = append(args, req.PhoneNumber)
	}
	if req.AccountBank != "" {
		statement.WriteString(` and account_bank = ?`)
		args = append(args, req.AccountBank)
	}
	rows, err := r.dbHelper.Open().Query(statement.String(), args...)
	if err != nil {
		return nil, errors.New((common.ReasonDBError.Code()))
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&client.ID, &client.Name, &client.Email, &client.Address, &client.PhoneNumber, &client.AccountBank, &client.CreatedAt, &client.UpdatedAt)
		if err != nil {
			return nil, errors.New((common.ReasonDBError.Code()))
		}
		res = append(res, client)
	}
	if len(res) == 0 {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	return res, nil
}

func (r *clientRepository) GetClientDetail(ctx context.Context,
	clientId uint32) (client *model.ClientModel, err error) {
	client = &model.ClientModel{}
	stmt := `select id, name, email, address, phone_number, account_bank, created_at, updated_at from client where id = ? order by id desc;`
	row := r.dbHelper.Open().QueryRow(stmt, clientId)
	err = row.Scan(&client.ID, &client.Name, &client.Email, &client.Address, &client.PhoneNumber, &client.AccountBank, &client.CreatedAt, &client.UpdatedAt)
	if err != nil {
		return nil, errors.New((common.ReasonDBError.Code()))
	}
	if err == sql.ErrNoRows {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	return client, nil
}

func (r *clientRepository) PostNewClient(ctx context.Context,
	req *dto.PostNewClientRequestDTO) (err error) {
	stmt := "insert into client (name, email, address, phone_number, account_bank ) values (?, ?, ?, ?, ?)"
	result, err := r.dbHelper.Open().Exec(stmt, req.Name, req.Email, req.Address, req.PhoneNumber, req.AccountBank)
	if err != nil {
		return err
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonDBError.Code()))
	}
	return nil
}

func (r *clientRepository) PutUpdateClient(ctx context.Context, req *dto.PutUpdateClientRequestDTO) (err error) {
	smtp := "update client set name = ?, email = ?, address = ?, phone_number = ?, account_bank = ? where id = ?"
	result, err := r.dbHelper.Open().Exec(smtp, req.Name, req.Email, req.Address, req.PhoneNumber, req.AccountBank, req.Id)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonNotFound.Code()))
	}
	return nil
}

func (r *clientRepository) DeleteClient(ctx context.Context,
	clientId uint32) (err error) {
	smtp := `DELETE FROM client WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(smtp, clientId)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonNotFound.Code()))
	}
	return nil
}

// SELECT id, name, 			email, 		address, phone_number, account_bank, 				created_at, 				updated_at					FROM v_client WHERE 1=1 and ("ducnp" is null or name = "ducnp") and ("" is null or email = "") and ("" is null or phone_number = "") and ("" is null or account_bank = "");
