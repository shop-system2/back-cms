package repository

import (
	"context"
	"database/sql"
	"errors"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
)

type (
	// commentRepository declare all func in comment repository
	CommentRepository interface {
		GetListComment(ctx context.Context, req *dto.GetListCommentRequestDTO) ([]*model.CommentModel, error)
		SearchComment(ctx context.Context, req *dto.SearchCommentRequestDTO) ([]model.CommentModel, error)
		GetCommentDetail(ctx context.Context, id uint32) (*model.CommentModel, error)
		PostNewComment(ctx context.Context, req *dto.PostNewCommentRequestDTO) error
		PutUpdateComment(ctx context.Context, req *dto.PutUpdateCommentRequestDTO) error
		DeleteComment(ctx context.Context, commentID uint32) error
		DoActiveComment(ctx context.Context, commentID, isActive uint32) error
		// review
		GetReviewCommonClient(ctx context.Context) ([]model.ReviewCommonClient, error)
	}
	commentRepository struct {
		dbHelper db.DBHelper
	}
)

func NewcommentRepository(dbHelper db.DBHelper) CommentRepository {
	return &commentRepository{
		dbHelper: dbHelper,
	}
}

func (r *commentRepository) DoActiveComment(ctx context.Context, commentID, isActive uint32) error {
	stmt := `update comment set is_active = ? where id = ?`
	result, err := r.dbHelper.Open().Exec(stmt, isActive, commentID)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	numEffect, _ := result.RowsAffected()
	if numEffect == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}

func (r *commentRepository) GetListComment(ctx context.Context,
	req *dto.GetListCommentRequestDTO) (res []*model.CommentModel, err error) {
	stmt := `select comment.name, price, vote, comment.created_at, category.name as category_name 
			FROM comment as comment, category as category WHERE comment.category_id = category.id  order by comment.id desc`
	rows, err := r.dbHelper.Open().Query(stmt)
	if err != nil {
		return nil, errors.New((common.ReasonDBError.Code()))
	}
	defer rows.Close()
	for rows.Next() {
		modelComment := &model.CommentModel{}
		err = rows.Scan(&modelComment.Content, &modelComment.Title, &modelComment.Content, &modelComment.CreatedAt, &modelComment.CreatedAt)
		if err != nil {
			return nil, errors.New((common.ReasonDBError.Code()))
		}
		res = append(res, modelComment)
	}
	return res, err
}

func (r *commentRepository) SearchComment(ctx context.Context, req *dto.SearchCommentRequestDTO) (res []model.CommentModel, err error) {
	// todo handle
	var (
		pro model.CommentModel
	)
	stmt := `select comment.name, price, vote, comment.created_at, category.name as category_name 
	from comment as comment, category as category 
	where comment.category_id = category.id and comment.name like ? order by comment.id desc`
	rows, err := r.dbHelper.Open().Query(stmt, "%"+req.TransactionID+"%")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&pro.Content, &pro.Content, &pro.Content, &pro.CreatedAt, &pro.Content)
		if err != nil {
			return nil, err
		}
		res = append(res, pro)
	}
	return res, err
}

func (r *commentRepository) GetCommentDetail(ctx context.Context,
	id uint32) (res *model.CommentModel, err error) {
	// TODO handle
	var (
		pro model.CommentModel
	)
	stmt := `select id, name, price, vote, created_at, updated_at, category_id from comment where id = ? order by id desc;`
	row := r.dbHelper.Open().QueryRow(stmt, id)

	err = row.Scan(&pro.ID, &pro.Content, &pro.Content, &pro.Content, &pro.CreatedAt, &pro.UpdatedAt, &pro.Content)
	if err != nil || err == sql.ErrNoRows {
		return nil, err
	}
	res = &pro
	return res, nil
}

func (r *commentRepository) PostNewComment(ctx context.Context,
	req *dto.PostNewCommentRequestDTO) (err error) {
	stmt := "INSERT INTO comment (title, content, user_id, product_id ) VALUES (?, ?, ?, ?)"
	result, err := r.dbHelper.Open().Exec(stmt, req.Title, req.Content, req.UserID, req.ProductID)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonDBError.Code()))
	}
	return nil
}

func (r *commentRepository) PutUpdateComment(ctx context.Context,
	req *dto.PutUpdateCommentRequestDTO) (err error) {
	smtp := "UPDATE comment SET title = ?, content = ? WHERE id = ?"
	result, err := r.dbHelper.Open().Exec(smtp, req.Title, req.Content, req.Id)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonDBError.Code()))
	}
	return nil
}

func (r *commentRepository) DeleteComment(ctx context.Context, commentID uint32) (err error) {
	smtp := `DELETE FROM comment where id = ?`
	result, err := r.dbHelper.Open().Exec(smtp, commentID)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonDBError.Code()))
	}
	return nil
}

// review

func (r *commentRepository) GetReviewCommonClient(ctx context.Context) (reviews []model.ReviewCommonClient, err error) {
	stmt := `
	SELECT 
		id, 
		name, 
		title,
		icon_url,
		image_url,
		major,
		description,
		tag,
		number_like,
		comment,
		star,
		created_at,
		updated_at
	FROM review_common WHERE 1=1 AND is_active = '1'`
	rows, err := r.dbHelper.Open().Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	review := model.ReviewCommonClient{}
	for rows.Next() {
		err = rows.Scan(&review.ID,
			&review.Name,
			&review.Title,
			&review.IconUrl,
			&review.ImageUrl,
			&review.Major,
			&review.Description,
			&review.Tag,
			&review.NumberLike,
			&review.Comment,
			&review.Star,
			&review.CreatedAt,
			&review.UpdatedAt)
		if err != nil {
			return nil, err
		}
		reviews = append(reviews, review)
	}
	return reviews, err
}
