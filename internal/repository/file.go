package repository

import "shop-back-cms/internal/helper/db"

type (
	FileRepository interface {
	}
	fileRepository struct {
		dbHelper db.DBHelper
	}
)

func NewFileRepository(dbHelper db.DBHelper) FileRepository {
	return &fileRepository{
		dbHelper: dbHelper,
	}
}
