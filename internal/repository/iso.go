package repository

// import (
// 	"context"
// 	"errors"
// 	"shop-back-cms/internal/common"
// 	"shop-back-cms/internal/helper/db"
// 	"shop-back-cms/internal/repository/model"
// 	"strings"
// 	"time"

// 	"github.com/opentracing/opentracing-go/ext"
// )

// type (
// 	MonitoringRepository interface {
// 		GetListPhysicalChannel(ctx context.Context, serverID uint32) ([]*model.PhysicalChannel, error)
// 		GetListChannel(ctx context.Context) ([]*model.Channel, error) // delete
// 		GetTransactionsMonitoring(ctx context.Context, serverID uint32) ([]*model.TransactionMonitoring, error)
// 	}

// 	monitoringRepository struct {
// 		dbHelper db.DBHelper
// 	}
// )

// // NewMonitoringRepository creates an instance
// func NewMonitoringRepository(dbHelper db.DBHelper) MonitoringRepository {
// 	return &monitoringRepository{
// 		dbHelper: dbHelper,
// 	}
// }

// func (r *monitoringRepository) GetTransactionsMonitoring(ctx context.Context,
// 	channelID uint32) (transactions []*model.TransactionMonitoring, err error) {
// 	span := jaeger.Start(ctx, ">repository.monitoringRepository/GetTransactionsMonitoring", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	stmt := `SELECT id,
// 			TO_CHAR(transaction_date, 'yyyy-mm-dd hh24:mi:ss') as transaction_date,
// 			channel_name, resp_code,resp_text, metadata
// 			FROM oms.oms_cbo_v_monitoring_transaction
// 			WHERE channel_id = :1`
// 	ctxTimeout, cancel := context.WithTimeout(context.Background(), time.Duration(100)*time.Second)
// 	defer cancel()
// 	builder := strings.Builder{}
// 	builder.WriteString(stmt)
// 	var args []interface{}
// 	args = append(args, channelID)
// 	rows, err := r.dbHelper.Open().QueryContext(ctxTimeout, builder.String(), args...)
// 	if err != nil {
// 		return nil, errors.New(common.ReasonDBError.Code())
// 	}
// 	for rows.Next() {
// 		transaction := &model.TransactionMonitoring{}
// 		err = rows.Scan(
// 			&transaction.ID,
// 			&transaction.TransactionDate,
// 			&transaction.ChannelName,
// 			&transaction.RespCode,
// 			&transaction.RespText,
// 			&transaction.Metadata,
// 		)
// 		if err != nil {
// 			return nil, errors.New(common.ReasonDBError.Code())
// 		}
// 		transactions = append(transactions, transaction)
// 	}
// 	if len(transactions) == 0 {
// 		return nil, errors.New(common.ReasonNotFound.Code())
// 	}
// 	return transactions, nil
// }
// func (r *monitoringRepository) GetListChannel(ctx context.Context) (channels []*model.Channel, err error) {
// 	span := jaeger.Start(ctx, ">repository.monitoringRepository/GetListChannel", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	stmt := `select id, name, code from ows.mess_channel`
// 	rows, err := r.dbHelper.Open().Query(stmt)
// 	if err != nil {
// 		return nil, errors.New(common.ReasonDBError.Code())
// 	}
// 	for rows.Next() {
// 		itemChannel := &model.Channel{}
// 		err = rows.Scan(
// 			&itemChannel.ID,
// 			&itemChannel.Name,
// 			&itemChannel.Code,
// 		)
// 		if err != nil {
// 			return nil, errors.New(common.ReasonDBError.Code())
// 		}
// 		channels = append(channels, itemChannel)
// 	}
// 	if len(channels) == 0 {
// 		return nil, errors.New(common.ReasonNotFound.Code())
// 	}
// 	return channels, nil
// }

// func (r *monitoringRepository) GetListPhysicalChannel(ctx context.Context,
// 	serverID uint32) (phyChannels []*model.PhysicalChannel, err error) {
// 	span := jaeger.Start(ctx, ">repository.monitoringRepository/GetListPhysicalChannel", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	queryBuilder := strings.Builder{}
// 	stmt := `SELECT id, name_in_server,
// 		ol_server_oid,
// 		ol_logical_channel_id,
// 		last_changed,
// 		last_change_record,
// 		 last_confirmed,
// 		trpt_status,
// 		appl_status
// 		FROM oms.oms_cbo_v_monitoring_connection`
// 	queryBuilder.WriteString(stmt)
// 	rows, err := r.dbHelper.Open().Query(queryBuilder.String())
// 	if err != nil {
// 		return nil, err
// 	}
// 	for rows.Next() {
// 		itemChannel := &model.PhysicalChannel{}
// 		err = rows.Scan(&itemChannel.ID,
// 			&itemChannel.ServerName,
// 			&itemChannel.ServerOID,
// 			&itemChannel.ChannelID,
// 			&itemChannel.LastChanged,
// 			&itemChannel.LastChangeRecord,
// 			&itemChannel.LastConfirmed,
// 			&itemChannel.TrptStatus,
// 			&itemChannel.ApplStatus,
// 		)
// 		if err != nil {
// 			return nil, errors.New(common.ReasonDBError.Code())
// 		}
// 		phyChannels = append(phyChannels, itemChannel)
// 	}
// 	if len(phyChannels) == 0 {
// 		return nil, errors.New(common.ReasonNotFound.Code())
// 	}
// 	return phyChannels, nil

// }
