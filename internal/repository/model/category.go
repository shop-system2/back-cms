package model

import (
	"database/sql"

	"gorm.io/gorm"
)

type (
	// CategoryModel represent category model
	Category struct {
		gorm.Model
		Name        string         `gorm:"column:name" bson:"name"`
		Code        string         `gorm:"column:code" bson:"code"`
		Description sql.NullString `gorm:"column:description" bson:"description"`
		IsActive    string         `gorm:"column:is_active" bson:"is_active"`
		// ID        uint32
		// CreatedAt string
		// UpdatedAt string
	}
)

func (u Category) Table() string {
	return "categories"
}
