package model

type (
	// ClientModel represent product model
	ClientModel struct {
		ID          uint32
		Name        string
		Email       string
		Address     string
		PhoneNumber string
		AccountBank string
		UpdatedAt   string
		CreatedAt   string
		IsActive    string
	}
	GetClientDetailModel struct {
		ID          uint32
		Email       string
		PhoneNumber string
		Name        string
	}
)
