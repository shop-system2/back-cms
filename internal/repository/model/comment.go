package model

import "database/sql"

type (
	// CommentModel represent comment model
	CommentModel struct {
		ID        uint32
		Title     string
		Content   string
		IsActive  uint32
		UserID    sql.NullInt32
		ProductID uint32
		CreatedAt string
		UpdatedAt string
	}

	ReviewCommonClient struct {
		ID          uint32
		Name        string
		Title       string
		IconUrl     string
		ImageUrl    string
		Major       sql.NullString
		Description sql.NullString
		Tag         string
		NumberLike  int8
		Comment     int8
		Star        int8
		CreatedAt   string
		UpdatedAt   string
	}
)
