package model

import "database/sql"

type (
	PhysicalChannel struct {
		ID               int32
		ServerName       string
		ServerOID        uint32
		ChannelID        uint32
		LastChanged      string
		LastChangeRecord uint32
		LastConfirmed    string
		TrptStatus       sql.NullString
		ApplStatus       sql.NullString
	}

	Channel struct {
		ID   uint32
		Name string
		Code string
	}

	TransactionMonitoring struct {
		ID              int64
		TransactionDate string
		ChannelName     string
		RespCode        sql.NullString
		RespText        sql.NullString
		Metadata        string
		ChannelID       uint32
	}
)
