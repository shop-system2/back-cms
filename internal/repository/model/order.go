package model

type (
	CartItemModel struct {
		CartItemId  uint32
		ProductName string
		ProductId   uint32
		CartId      uint32
		Quantity    uint32
		Sku         string
		Description string
		CreatedBy   string
		Price       int64
		SellPrice   int64
	}

	CartModel struct {
		ID     uint32
		UserId uint32
		Total  uint8
	}

	OrderItemModel struct {
		ID        uint32
		ProductId uint32
		OrderId   uint32
		Quantity  uint32
		Sku       string
		// Price int64
		// SellPrice int64
		Description string
		CreatedBy   string
		Price       int64
		SellPrice   int64
	}

	OrderModel struct {
		ID     uint32
		UserId uint32
		Total  uint8
	}

	GetListOrderRequest struct {
		Status   string
		ToDate   string
		FromDate string
		Type     string
		UserID   uint32
	}
	Order struct {
		ProductName string
		Quantity    int64
		Price       int64
		SellPrice   int64
	}
)
