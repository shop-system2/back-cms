package model

import (
	"database/sql"

	"gorm.io/gorm"
)

type (
	// ProductsModel represent product model
	Product struct {
		gorm.Model
		Name            string `gorm:"column:name" bson:"name"`
		Price           int64  `gorm:"column:price" bson:"price"`
		SellPrice       int64  `gorm:"column:sell_price" bson:"sell_price"`
		Tag             string `gorm:"column:tag" bson:"tag"`
		Vote            int    `gorm:"column:vote" bson:"vote"`
		CategoryName    string `gorm:"column:category_name" bson:"category_name"`
		UpdatedBy       string `gorm:"column:updated_by" bson:"updated_by"`
		CreatedBy       string `gorm:"column:created_by" bson:"created_by"`
		CategoryID      uint32 `gorm:"column:category_id" bson:"category_id"`
		IsActive        string `gorm:"column:is_active" bson:"is_active"`
		Description     string `gorm:"column:description" bson:"description"`
		Quantity        uint32 `gorm:"column:quantity" bson:"quantity"`
		Sku             string `gorm:"column:sku" bson:"sku"`
		StatusOfProduct string `gorm:"column:status_of_product" bson:"status_of_product"`
		ImageID         string `gorm:"column:image_id" bson:"image_id"`
		// ID              uint32         `gorm:"column:id" bson:"id"`
		// UpdatedAt       string         `gorm:"column:id" bson:"id"`
		// CreatedAt       string         `gorm:"column:id" bson:"id"`
	}

	IntructionModel struct {
		ID          uint32
		Description string
		IsActive    string
		Type        sql.NullString
	}
)

func (u Product) Table() string {
	return "products"
}
