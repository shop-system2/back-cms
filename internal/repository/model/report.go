package model

type (
	// ReportModel represent report model
	ReportModel struct {
		ID   uint32
		Name string
	}

	// ParamatersByReportIDModel represent ParamatersByReportID model
	ParamatersByReportIDModel struct {
		ID           uint32
		Type         string
		Name         string
		DefaultValue string
		IsMultiple   uint32
		Value        string
	}
)

// Rule represent control rule
type Rule struct {
	Enum            string
	Len             int64
	Max             int64
	Message         string
	Min             int64
	Pattern         string
	Required        string
	Transform       string
	Type            string
	Validator       string
	Whitespace      string
	Validatetrigger string
	Disabled        string
}

// ReportParameter represents report parameter
type ReportParameter struct {
	ReportID     int64
	Name         string
	Type         string
	DefaultValue string
	Values       string
	Label        string
	IsMultiple   int64
	Rules        []*Rule
}

// Report represents list report
type Report struct {
	Name          string
	ID            int64
	SQLType       string
	SQLValue      string
	SQLParameters string
	CreatedDate   string
	CreatedBy     string
	UpdatedDate   string
	UpdatedBy     string
	Header        []string
}

// ReportGDTongEcomNapas represents report giao dich tong ECOM Napas
type ReportGDTongEcomNapas struct {
	YearMonth      string
	CardType       string
	CardScheme     string
	TransCondition string
	TransLocation  string
	TotalTrans     int64
	TotalAmount    float64
}

// ReportDetail represents result of each report
type ReportDetail struct {
	Args []interface{}
}

// ReportFile represent report file data
type ReportFile struct {
	FileName   string
	Base64File string
	Status     string
}
