package model

import (
	"database/sql"
)

// Locale type struct table
type (
	NotificationTemplate struct {
		ID          int32
		Template    string
		Subject     sql.NullString
		Body        sql.NullString
		Pattern     sql.NullString
		IsActive    string
		CreatedBy   sql.NullString
		CreatedDate sql.NullString
		UpdatedBy   sql.NullString
		UpdatedDate sql.NullString
	}

	AddNotificationTemplate struct {
		ID       int32
		Template string
		Subject  string
		Body     string
		Pattern  string
	}

	FeatureFlag struct {
		ID       uint32
		Key      string
		Value    string
		FromDate sql.NullString
		ToDate   sql.NullString
		IsActive string
	}
)
