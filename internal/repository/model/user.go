package model

type (
	// UserModel represent user model
	ListUserModel struct {
		Name      string
		Price     uint32
		Vote      int
		CreatedAt string
	}
)
