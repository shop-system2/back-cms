package model

import "database/sql"

type (
	// VendorModel represent product model
	VendorModel struct {
		ID          uint32
		Name        string
		Code        string
		Address     string
		PhoneNumber string
		UpdatedAt   string
		CreatedAt   string
		Description sql.NullString
		IsActive    string
	}
)
