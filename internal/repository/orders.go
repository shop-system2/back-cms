package repository

import (
	"context"
	"database/sql"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
	"strings"
)

type (
	OrderRepository interface {
		PostNewCart(ctx context.Context, req *model.CartModel) (uint32, error)
		PostNewCartItem(ctx context.Context, req *model.CartItemModel) error
		GetCartItemDetail(ctx context.Context, cardItemId uint32) (*model.CartItemModel, error)
		PostNewOrder(ctx context.Context, req *model.OrderModel) (uint32, error)
		PostNewOrderItem(ctx context.Context, req *model.OrderItemModel) error
		GetCartIDByUserID(ctx context.Context, userID uint32) (uint32, bool, error)
		GetOrderIDByUserID(ctx context.Context, userID uint32) (uint32, bool, error)
		GetListOrder(ctx context.Context, req *model.GetListOrderRequest) ([]model.Order, error)
	}
	orderRepository struct {
		dbHelper db.DBHelper
	}
)

// NewOrderRepository func new instance comment repository
func NewOrderRepository(dbHelper db.DBHelper) OrderRepository {
	return &orderRepository{
		dbHelper: dbHelper,
	}
}

func (r *orderRepository) GetListOrder(ctx context.Context,
	req *model.GetListOrderRequest) (order []model.Order, err error) {
	var (
		queryBuild strings.Builder
		agrs       []interface{}
	)
	item := model.Order{}
	queryBuild.WriteString(`
		SELECT product.name, order_item.price, order_item.sell_price,
		order_item.quantity
		FROM orders
		JOIN order_item ON orders.id = order_item.order_id
		JOIN product ON order_item.product_id = product.id WHERE 1=1 
		AND orders.user_id = ?
	`)
	agrs = append(agrs, req.UserID)
	db := r.dbHelper.Open()
	rows, err := db.Query(queryBuild.String(), agrs...)
	if err != nil {
		return order, err
	}
	for rows.Next() {
		rows.Scan(
			&item.ProductName,
			&item.Price,
			&item.SellPrice,
			&item.Quantity,
		)
		if err != nil {
			return order, err
		}
		order = append(order, item)
	}
	return order, nil
}

func (r *orderRepository) GetCartItemDetail(ctx context.Context,
	cardItemId uint32) (item *model.CartItemModel, err error) {
	stmt := `
	select product.name, product.id, cart_item.id, cart_item.quantity, cart_item.sku, 
	product.price, product.sell_price, cart_item.cart_id
	from cart_item 
	join product on cart_item.product_id = product.id
	where cart_item.id = ?;
	`
	db := r.dbHelper.Open()
	row := db.QueryRow(stmt, cardItemId)
	item = &model.CartItemModel{}
	err = row.Scan(
		&item.ProductName,
		&item.ProductId,
		&item.CartItemId,
		&item.Quantity,
		&item.Sku,
		&item.Price,
		&item.SellPrice,
		&item.CartId,
	)
	if err != nil {
		return nil, err
	}
	return item, nil
}

func (r *orderRepository) GetCartIDByUserID(ctx context.Context,
	userID uint32) (cardID uint32, isFound bool, err error) {
	stmt := `select id from carts where user_id = ?`
	db := r.dbHelper.Open()
	err = db.QueryRow(stmt, userID).Scan(&cardID)
	if err != nil && err == sql.ErrNoRows {
		return 0, false, nil
	}
	if err != nil {
		return 0, false, err
	}
	if cardID == 0 {
		return 0, false, nil
	}
	return cardID, true, err
}
func (r *orderRepository) GetOrderIDByUserID(ctx context.Context,
	userID uint32) (cardID uint32, isFound bool, err error) {
	stmt := `select id from orders where user_id = ?`
	db := r.dbHelper.Open()
	err = db.QueryRow(stmt, userID).Scan(&cardID)
	if err != nil && err == sql.ErrNoRows {
		return 0, false, nil
	}
	if err != nil {
		return 0, false, err
	}
	if cardID == 0 {
		return 0, false, nil
	}
	return cardID, true, err
}

func (r *orderRepository) PostNewCart(ctx context.Context,
	req *model.CartModel) (lastID uint32, err error) {
	var (
		agrs       []interface{}
		queryBuild strings.Builder
	)
	queryBuild.WriteString(`INSERT INTO carts (
		user_id, 
		total) VALUES (?, ?)`)
	agrs = append(agrs, req.UserId, req.Total)
	result, err := r.dbHelper.Open().Exec(queryBuild.String(), agrs...)
	if err != nil {
		return 0, err
	}
	lastIDs, err := result.LastInsertId()
	return uint32(lastIDs), err
}

func (r *orderRepository) PostNewCartItem(ctx context.Context,
	req *model.CartItemModel) (err error) {
	var (
		agrs       []interface{}
		queryBuild strings.Builder
	)
	queryBuild.WriteString(`INSERT INTO cart_item (
		product_id, cart_id , quantity, sku, created_by,description) 
		VALUES (?, ?, ?, ?, ?, ?)`)
	agrs = append(agrs, req.ProductId, req.CartId, req.Quantity,
		req.Sku, req.CreatedBy, req.Description)
	_, err = r.dbHelper.Open().Exec(queryBuild.String(), agrs...)
	if err != nil {
		return err
	}
	// if num, err := result.RowsAffected(); err != nil || num == 0 {
	// 	return errors.New(common.ReasonNotFound.Code())
	// }
	return nil
}

func (r *orderRepository) PostNewOrder(ctx context.Context,
	req *model.OrderModel) (lastID uint32, err error) {
	var (
		agrs       []interface{}
		queryBuild strings.Builder
	)
	queryBuild.WriteString(`INSERT INTO orders (
		user_id, 
		total) VALUES (?, ?)`)
	agrs = append(agrs, req.UserId, req.Total)
	result, err := r.dbHelper.Open().Exec(queryBuild.String(), agrs...)
	if err != nil {
		return 0, err
	}
	lastIDs, err := result.LastInsertId()
	return uint32(lastIDs), err
}

func (r *orderRepository) PostNewOrderItem(ctx context.Context,
	req *model.OrderItemModel) (err error) {
	var (
		agrs       []interface{}
		queryBuild strings.Builder
	)
	queryBuild.WriteString(`INSERT INTO order_item (
		product_id, order_id , quantity, sku, created_by,description, price, sell_price ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`)
	agrs = append(agrs, req.ProductId, req.OrderId, req.Quantity,
		req.Sku, req.CreatedBy, req.Description, req.Price, req.SellPrice)
	_, err = r.dbHelper.Open().Exec(queryBuild.String(), agrs...)
	if err != nil {
		return err
	}
	// if num, err := result.RowsAffected(); err != nil || num == 0 {
	// 	return errors.New(common.ReasonNotFound.Code())
	// }
	return nil
}
