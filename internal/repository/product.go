package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
	"strings"

	"gorm.io/gorm"
)

type (
	// ProductRepository declare all func in comment repository
	ProductRepository interface {
		GetListProduct(ctx context.Context, req *dto.GetListProductRequestDTO, isPaging bool) ([]model.Product, error)
		SearchProduct(ctx context.Context, req *dto.SearchProductRequestDTO) ([]*model.Product, error)
		GetProductDetail(ctx context.Context, id uint32) (*model.Product, error)
		PostNewProduct(ctx context.Context, req *dto.PostNewProductRequestDTO, pro model.Product) error
		PutUpdateProduct(ctx context.Context, req *dto.PutUpdateProductRequestDTO) error
		DeleteProduct(ctx context.Context, id uint32) error
		DoActiveProduct(ctx context.Context, productID, isActive uint32) error
		CheckSku(ctx context.Context, sku string) (bool, error)
		ProductQuantity(ctx context.Context, number, productid uint32, typeCal string) error
		// intruction
		PostNewIntruction(ctx context.Context, req []string) error
		GetListIntruction(ctx context.Context, req *dto.GetListIntructionRequestDTO, isPaging bool) ([]*model.IntructionModel, error)
		DeleteIntruction(ctx context.Context, id uint32) error
		DoActiveIntruction(ctx context.Context, productID, isActive uint32) error
		PutUpdateIntruction(ctx context.Context, req *dto.PutUpdateIntructionRequestDTO) error

		// product client
		GetProductPreferentail(ctx context.Context,
			req *dto.GetProductPreferentailClientRequestDTO) ([]*model.ProductPreferentailModel, error)
		GetProductTrending(ctx context.Context,
			req *model.ProductModelRequest) ([]model.Product, error)
		GetProductDetailClient(ctx context.Context, id uint32) (*model.ProductClientModel, error)
	}

	productRepository struct {
		dbHelper db.DBHelper
		gorm     *gorm.DB
	}
)

// NewProductRepository func new instance comment repository
func NewProductRepository(dbHelper db.DBHelper, gorm *gorm.DB) ProductRepository {
	return &productRepository{
		dbHelper: dbHelper,
		gorm:     gorm,
	}
}

func (r *productRepository) CheckSku(ctx context.Context, sku string) (bool, error) {
	var (
		agrs         []interface{}
		queryBuilder strings.Builder
		number       int
	)
	queryBuilder.WriteString(`select count(id) from products where sku = ?`)
	agrs = append(agrs, sku)
	db := r.dbHelper.Open()
	err := db.QueryRow(queryBuilder.String(), agrs...).Scan(&number)
	if err != nil && err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	if number == 0 {
		return false, nil
	}
	return true, nil
}
func (r *productRepository) ProductQuantity(ctx context.Context, number, productid uint32, typeCal string) error {
	var (
		queryBuilder strings.Builder
	)
	queryBuilder.WriteString(`update product `)
	switch typeCal {
	case common.CALULATOR_ADD:
		queryBuilder.WriteString(fmt.Sprintf("quantity = quantity + %v", number))
	case common.CALULATOR_SUB:
		queryBuilder.WriteString(fmt.Sprintf("quantity = quantity - %v", number))
	}
	queryBuilder.WriteString(`where id = ?`)
	db := r.dbHelper.Open()
	_, err := db.Exec(queryBuilder.String(), productid)
	if err != nil {
		return err
	}
	return nil
}
func (r *productRepository) SearchProduct(ctx context.Context,
	req *dto.SearchProductRequestDTO) (res []*model.Product, err error) {
	// todo HANDLER
	stmt := `select product.name, price, vote, product.created_at, category.name as category_name 
	from product as product, category as category 
	where product.category_id = category.id and product.name like ? order by product.id desc`
	rows, err := r.dbHelper.Open().Query(stmt, "%"+req.Name+"%")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		pro := &model.Product{}
		err = rows.Scan(&pro.Name, &pro.Price, &pro.Vote, &pro.CreatedAt, &pro.CategoryName)
		if err != nil {
			return nil, err
		}
		res = append(res, pro)
	}
	return res, err
}

func (r *productRepository) DoActiveProduct(ctx context.Context, clientID, isActive uint32) error {
	stmt := `UPDATE product SET is_active = ? WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(stmt, isActive, clientID)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	numEffect, _ := result.RowsAffected()
	if numEffect == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}

func (r *productRepository) GetListProduct(ctx context.Context,
	req *dto.GetListProductRequestDTO, isPaging bool) ([]model.Product, error) {
	var (
		product = []model.Product{}
		err     error
	)
	err = r.gorm.Table(model.Product{}.Table()).Find(&product).Error
	return product, err

	// var (
	// 	statement strings.Builder
	// 	args      []interface{}
	// )
	// stmt := `SELECT id, name, price, sell_price, tag, description, is_active,
	// 		quantity, sku,status_of_product,
	//  		created_at, updated_at, created_by, updated_by
	// 		FROM products WHERE 1 = 1 `
	// statement.WriteString(stmt)
	// if isPaging {
	// 	statement.WriteString(" LIMIT ? offset ?")
	// 	args = append(args, req.Limit, req.Offset)
	// }
	// statement.WriteString(" ORDER BY id DESC")
	// rows, err := r.dbHelper.Open().Query(statement.String(), args...)
	// if err != nil {
	// 	return nil, err
	// }
	// defer rows.Close()
	// for rows.Next() {
	// 	pro := &model.Product{}
	// 	err = rows.Scan(&pro.ID, &pro.Name, &pro.Price, &pro.SellPrice, &pro.Tag, &pro.Description,
	// 		&pro.IsActive, &pro.Quantity, &pro.Sku, &pro.StatusOfProduct,
	// 		&pro.CreatedAt, &pro.UpdatedAt, &pro.CreatedBy, &pro.UpdatedBy)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	res = append(res, pro)
	// }

	// return res, err
}

func (r *productRepository) GetProductDetail(ctx context.Context,
	id uint32) (res *model.Product, err error) {
	stmt := `SELECT id, name, price, description, 
				is_active, created_at, updated_at, created_by, updated_by,
				quantity, sku
				FROM product WHERE id = ?`
	row := r.dbHelper.Open().QueryRow(stmt, id)
	res = &model.Product{}
	err = row.Scan(&res.ID, &res.Name, &res.Price, &res.Description,
		&res.Vote, &res.CreatedAt, &res.UpdatedAt, &res.CreatedBy, &res.UpdatedBy,
		&res.Quantity, &res.Sku)
	if err != nil || err == sql.ErrNoRows {
		return nil, errors.New(common.ReasonDBError.Code())
	}
	return res, nil
}

func (r *productRepository) PostNewProduct(ctx context.Context,
	req *dto.PostNewProductRequestDTO, product model.Product) (err error) {
	err = r.gorm.Create(&product).Error
	return err

	// stmt := `INSERT INTO product (name, price, sell_price, description, tag, created_by, category_id, quantity, sku , status_of_product)
	// VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
	// result, err := r.dbHelper.Open().Exec(stmt, req.Name, req.Price, req.SellPrice,
	// 	req.Description, req.Tag, req.CreatedBy, req.CategoryId, req.Quantity, req.Sku, req.StatusOfProduct)
	// if err != nil {
	// 	return err
	// }
	// if num, err := result.RowsAffected(); err != nil || num == 0 {
	// 	return errors.New(common.ReasonNotFound.Code())
	// }
	// return nil
}

func (r *productRepository) PutUpdateProduct(ctx context.Context,
	req *dto.PutUpdateProductRequestDTO) (err error) {
	var (
		builder strings.Builder
		args    []interface{}
	)
	stmt := `UPDATE product SET name = ?,
				 price = ?, description = ?, category_id = ?, updated_by = ? where id = ?`
	builder.WriteString(stmt)
	args = append(args, req.Name, req.Price, req.Description, req.CategoryID, req.UpdatedBy, req.Id)
	result, err := r.dbHelper.Open().Exec(builder.String(), args...)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}

func (r *productRepository) DeleteProduct(ctx context.Context, id uint32) (err error) {
	smtp := `DELETE FROM product WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(smtp, id)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}

/*
	Intruction
*/
func (r *productRepository) PostNewIntruction(ctx context.Context,
	req []string) (err error) {
	dbTrans, err := r.dbHelper.Open().Begin()
	if err != nil {
		return err
	}
	for i := 0; i < len(req); i++ {
		var (
			agrs         []interface{}
			queryBuilder strings.Builder
		)
		queryBuilder.WriteString(`INSERT INTO intruction (description) VALUES (?) `)
		agrs = append(agrs, req[i])
		_, err = dbTrans.Exec(queryBuilder.String(), agrs...)
		if err != nil {
			return err
		}
	}
	err = dbTrans.Commit()
	if err != nil {
		return err
	}
	return nil
}

func (r *productRepository) GetListIntruction(ctx context.Context,
	req *dto.GetListIntructionRequestDTO, isPaging bool) (res []*model.IntructionModel, err error) {
	var (
		statement strings.Builder
		args      []interface{}
	)
	statement.WriteString(`SELECT id, description, type, is_active
	FROM intruction WHERE 1 = 1 `)

	if req.IsActive {
		statement.WriteString("  and is_active = 1")
	}

	statement.WriteString(" ORDER BY id DESC")
	if isPaging {
		statement.WriteString(" LIMIT ? offset ?")
		args = append(args, req.Limit, req.Offset)
	}
	rows, err := r.dbHelper.Open().Query(statement.String(), args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		intruc := &model.IntructionModel{}
		err = rows.Scan(&intruc.ID, &intruc.Description, &intruc.Type, &intruc.IsActive)
		if err != nil {
			return nil, err
		}
		res = append(res, intruc)
	}
	return res, err
}

func (r *productRepository) DeleteIntruction(ctx context.Context, id uint32) (err error) {
	smtp := `DELETE FROM intruction WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(smtp, id)
	if err != nil {
		return err
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (r *productRepository) DoActiveIntruction(ctx context.Context, intructionID, isActive uint32) error {
	stmt := `UPDATE intruction SET is_active = ? WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(stmt, isActive, intructionID)
	if err != nil {
		return err
	}
	numEffect, _ := result.RowsAffected()
	if numEffect == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (r *productRepository) PutUpdateIntruction(ctx context.Context,
	req *dto.PutUpdateIntructionRequestDTO) (err error) {
	var (
		builder strings.Builder
		args    []interface{}
	)
	stmt := `UPDATE intruction SET description = ?,
		type = ?, is_active = ? where id = ?`
	builder.WriteString(stmt)
	args = append(args, req.Description, req.Type, req.IsActive, req.ID)
	result, err := r.dbHelper.Open().Exec(builder.String(), args...)
	if err != nil {
		return err
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return sql.ErrNoRows
	}
	return nil
}
