package repository

import (
	"context"
	"database/sql"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/repository/model"
)

func (r *productRepository) GetProductTrending(ctx context.Context,
	req *model.ProductModelRequest) ([]model.Product, error) {
	var (
		product = []model.Product{}
		err     error
	)
	result := map[string]interface{}{
		"status_of_product": "TRENDING",
	}

	err = r.gorm.Table(model.Product{}.Table()).Where(result).Find(&product).Error
	return product, err

	// var (
	// 	queryBuilder strings.Builder
	// )
	// stmt := `SELECT id, description, name, price, sell_price, tag
	// 		FROM sbl_v_product_trending WHERE 1=1 `
	// queryBuilder.WriteString(stmt)
	// rows, err := r.dbHelper.Open().Query(stmt)
	// if err != nil {
	// 	return nil, err
	// }
	// defer rows.Close()
	// for rows.Next() {
	// 	pro := &model.ProductClientModel{}
	// 	err = rows.Scan(&pro.ID, &pro.Description, &pro.Name, &pro.Price, &pro.SellPrice, &pro.Tag)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	res = append(res, pro)
	// }
	// return res, err
}

func (r *productRepository) GetProductPreferentail(ctx context.Context,
	req *dto.GetProductPreferentailClientRequestDTO) (res []*model.ProductPreferentailModel, err error) {
	stmt := `SELECT id, description, name, price, sell_price, tag,short_description
			FROM sbl_v_product_perferentail`
	rows, err := r.dbHelper.Open().Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		pro := &model.ProductPreferentailModel{}
		err = rows.Scan(
			&pro.ID,
			&pro.Description,
			&pro.Name,
			&pro.Price,
			&pro.SellPrice,
			&pro.Tag,
			&pro.ShortDescription,
		)
		if err != nil {
			return nil, err
		}
		res = append(res, pro)
	}
	return res, err
}

func (r *productRepository) GetProductDetailClient(ctx context.Context,
	id uint32) (res *model.ProductClientModel, err error) {
	stmt := `SELECT
				id, 
				name, 
				price,
				sell_price,
				tag,
				vote,
				description, 
				category_id,
				quantity,
				sku,
				created_at, 
				updated_at, 
				created_by,
				updated_by,
				is_active,
				status_of_product,
				short_description
			FROM product WHERE id = ?`
	row := r.dbHelper.Open().QueryRow(stmt, id)
	res = &model.ProductClientModel{}
	err = row.Scan(
		&res.ID,
		&res.Name,
		&res.Price,
		&res.SellPrice,
		&res.Tag,
		&res.Vote,
		&res.Description,
		&res.CategoryID,
		&res.Quantity,
		&res.Sku,
		&res.CreatedAt,
		&res.UpdatedAt,
		&res.CreatedBy,
		&res.UpdatedBy,
		&res.IsActive,
		&res.StatusOfProduct,
		&res.ShortDescription,
	)
	if err == sql.ErrNoRows {
		return nil, sql.ErrNoRows
	}
	if err != nil {
		return nil, err
	}
	return res, nil
}
