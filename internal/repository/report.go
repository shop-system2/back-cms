package repository

import (
	"context"
	"database/sql"
	"reflect"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
	"strings"
	"time"
)

type (
	// ReportRepository declare all func in comment repository
	ReportRepository interface {
		GetListReport(ctx context.Context, req *dto.GetListReportRequestDTO) ([]model.ReportModel, error)
		GetParametersByReportID(ctx context.Context, req *dto.GetParametersByReportIDRequestDTO) ([]model.ParamatersByReportIDModel, error)

		GetReport(ctx context.Context) ([]*model.Report, error)
		GetReportParameters(ctx context.Context, id int64) ([]*model.ReportParameter, error)
		GetReportDetail(ctx context.Context, statement string, args []interface{}) ([]interface{}, error)
		// refactoring to process partial
		LoadDataReportDetail(ctx context.Context, statement string, args []interface{},
			req *dto.ReportResponseChannel) error
	}
	reportRepository struct {
		dbHelper db.DBHelper
	}
)

// NewReportRepository creates an instance
func NewReportRepository(
	dbHelper db.DBHelper,

) ReportRepository {
	return &reportRepository{
		dbHelper: dbHelper,
	}
}

func (r *reportRepository) GetListReport(ctx context.Context, req *dto.GetListReportRequestDTO) (res []model.ReportModel, err error) {
	db, err := helper.OpenConnectDB()
	if err != nil {
		return nil, err
	}
	var (
		report model.ReportModel
	)
	stmt := `select id, name from report order by id desc`
	rows, err := db.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&report.ID, &report.Name)
		if err != nil {
			return nil, err
		}
		res = append(res, report)
	}
	return res, err
}

func (r *reportRepository) GetParametersByReportID(ctx context.Context,
	req *dto.GetParametersByReportIDRequestDTO) (res []model.ParamatersByReportIDModel, err error) {
	db, err := helper.OpenConnectDB()
	if err != nil {
		return nil, err
	}
	var (
		parameters model.ParamatersByReportIDModel
	)
	stmt := `select id, type, name, default_value, is_multiple, value from report_parameters where report_id = ? order by id desc`
	rows, err := db.Query(stmt, req.ID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&parameters.ID,
			&parameters.Type,
			&parameters.Name,
			&parameters.DefaultValue,
			&parameters.IsMultiple,
			&parameters.Value)

		if err != nil {
			return nil, err
		}
		res = append(res, parameters)
	}

	return res, nil
}

func (r *reportRepository) LoadDataReportDetail(ctx context.Context, statement string, args []interface{},
	reqRep *dto.ReportResponseChannel) error {
	var (
		err error
	)
	// TODO movement 100 config
	ctxTimeout, cancel := context.WithTimeout(context.Background(),
		time.Duration(100)*time.Second)
	defer cancel()
	database := r.dbHelper.Open()
	rows, err := database.QueryContext(ctxTimeout, statement, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var arg []interface{}
		columns, err := rows.ColumnTypes()
		if err != nil {
			return err
		}
		arg = make([]interface{}, len(columns))
		for index, column := range columns {
			switch column.ScanType().Kind() {
			case reflect.String:
				arg[index] = new(*string)
			case reflect.Int64:
				arg[index] = new(*int64)
			case reflect.Float64:
				arg[index] = new(*float64)
			default:
				arg[index] = new(*string)
			}
		}
		err = rows.Scan(arg...)
		if err != nil {
			return err
		}
		reqRep.DateDetailItem <- arg
	}
	time.Sleep(1 * time.Second)
	reqRep.ProcessAllDone <- true
	return nil
}

func (r *reportRepository) GetReport(ctx context.Context) ([]*model.Report, error) {
	var err error
	var (
		statement = `SELECT
				id
				, name
				, sql_type
				, sql_value
				, sql_parameters
				, created_date
				, NVL(created_by, ' ') created_by
				, updated_date
				, NVL(updated_by, ' ') updated_by
				, NVL(header, ' ') header
			FROM OMS_REPORT
			ORDER BY name`
	)
	var (
		reports []*model.Report
		header  string
	)
	// TODO movement 100 config
	ctxTimeout, cancel := context.WithTimeout(context.Background(),
		time.Duration(100)*time.Second)
	defer cancel()

	database := r.dbHelper.Open()
	rows, err := database.QueryContext(ctxTimeout, statement)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		report := &model.Report{}
		err = rows.Scan(
			&report.ID,
			&report.Name,
			&report.SQLType,
			&report.SQLValue,
			&report.SQLParameters,
			&report.CreatedDate,
			&report.CreatedBy,
			&report.UpdatedDate,
			&report.UpdatedBy,
			&header)
		if err != nil {
			return nil, err
		}
		report.Header = strings.Split(header, ",")
		reports = append(reports, report)
	}

	if len(reports) == 0 {
		return nil, sql.ErrNoRows
	}

	return reports, nil
}

func (r *reportRepository) GetReportParameters(ctx context.Context, id int64) ([]*model.ReportParameter, error) {
	var err error
	var (
		statement = `SELECT
			report_id
			, name
			, param_type
			, default_value
			, value
			, label
			, is_multiple
			, enum
			, len
			, max
			, message
			, min
			, pattern
			, required
			, transform
			, type
			, validator
			, whitespace
			, validatetrigger
			, disabled
		FROM
			OMS_V_REPORT_PARAMETERS
		WHERE report_id=:1`
	)
	var (
		reportParameters []*model.ReportParameter
	)
	// TODO movement 100 config

	ctxTimeout, cancel := context.WithTimeout(context.Background(),
		time.Duration(100)*time.Second)
	defer cancel()

	database := r.dbHelper.Open()
	rows, err := database.QueryContext(ctxTimeout, statement, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		parameter := &model.ReportParameter{}
		rule := &model.Rule{}
		err = rows.Scan(
			&parameter.ReportID,
			&parameter.Name,
			&parameter.Type,
			&parameter.DefaultValue,
			&parameter.Values,
			&parameter.Label,
			&parameter.IsMultiple,
			&rule.Enum,
			&rule.Len,
			&rule.Max,
			&rule.Message,
			&rule.Min,
			&rule.Pattern,
			&rule.Required,
			&rule.Transform,
			&rule.Type,
			&rule.Validator,
			&rule.Whitespace,
			&rule.Validatetrigger,
			&rule.Disabled)
		if err != nil {
			return nil, err
		}
		parameter.Rules = append(parameter.Rules, rule)

		isContain := false
		for _, reportParameter := range reportParameters {
			if strings.EqualFold(reportParameter.Name, parameter.Name) {
				reportParameter.Rules = append(reportParameter.Rules, rule)
				isContain = true
				break
			}
		}
		if !isContain {
			reportParameters = append(reportParameters, parameter)
		}
	}

	if len(reportParameters) == 0 {
		return nil, sql.ErrNoRows
	}

	return reportParameters, nil
}

func (r *reportRepository) GetReportDetail(ctx context.Context, statement string, args []interface{}) ([]interface{}, error) {
	var err error
	var (
		reportDetail []interface{}
	)
	// TODO movement 100 config

	ctxTimeout, cancel := context.WithTimeout(context.Background(),
		time.Duration(100)*time.Second)
	defer cancel()
	rows, err := r.dbHelper.Open().QueryContext(ctxTimeout, statement, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		var arg []interface{}
		columns, err := rows.ColumnTypes()

		if err != nil {
			return nil, err
		}

		for _, column := range columns {
			switch column.ScanType().Kind() {
			case reflect.String:
				arg = append(arg, new(*string))
			case reflect.Int64:
				arg = append(arg, new(*int64))
			case reflect.Float64:
				arg = append(arg, new(*float64))
			default:
				arg = append(arg, new(*string))
			}
		}

		err = rows.Scan(
			arg...)
		if err != nil {
			return reportDetail, err
		}
		reportDetail = append(reportDetail, arg)
	}

	if len(reportDetail) == 0 {
		return nil, sql.ErrNoRows
	}

	return reportDetail, nil
}
