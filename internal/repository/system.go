package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
	"strings"
)

type (
	// ManagementSystemRepository initialization
	ManagementSystemRepository interface {
		GetListNotificationTemplate(ctx context.Context) ([]*model.NotificationTemplate, error)
		GetNotificationTemplateDetail(ctx context.Context, id uint32) (*model.NotificationTemplate, error)
		AddNotificationTemplate(ctx context.Context, req *model.AddNotificationTemplate) error
		DeleteNotificationTemplate(ctx context.Context, id uint32) error
		UpdateNotificationTemplate(ctx context.Context, req *model.AddNotificationTemplate) error
		ActiveNotificationTemplate(ctx context.Context, id uint32, isActive string) error

		// *** feature flag ***
		GetFlagPages(ctx context.Context) ([]*model.FeatureFlag, error)
		PatchFlagPage(ctx context.Context, reqDTO dto.PatchFlagPageRequestDTO) error
		DeleteFlagPage(ctx context.Context, flagID uint32) error
		PostFlagPage(ctx context.Context, reqDTO dto.PostFlagPageRequestDTO) error
		GetAdminFlagPages(ctx context.Context) ([]model.FeatureFlag, error)
		GetDetailFlagPage(ctx context.Context, flagID uint32) (*model.FeatureFlag, error)
		CheckFlagAccess(ctx context.Context, router string) (model.FeatureFlag, error)
	}
	managementSystemRepository struct {
		dbHelper db.DBHelper
	}
)

// NewManagementSystemRepository creates an instance
func NewManagementSystemRepository(dbHelper db.DBHelper) ManagementSystemRepository {
	return &managementSystemRepository{
		dbHelper: dbHelper,
	}
}

func (r *managementSystemRepository) ActiveNotificationTemplate(ctx context.Context,
	id uint32, isActive string) (err error) {
	stmt := `UPDATE notification_template SET is_active = :1 WHERE id = :2`
	row, err := r.dbHelper.Open().Exec(stmt, isActive, id)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	eff, _ := row.RowsAffected()
	if eff == 0 {
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (r *managementSystemRepository) UpdateNotificationTemplate(ctx context.Context,
	req *model.AddNotificationTemplate) (err error) {
	stmt := `UPDATE notification_template SET template = :1, subject = :2, body = :3, pattern = :4 WHERE id = :5`
	var agrs []interface{}
	agrs = append(agrs, req.Template, req.Subject, req.Body, req.Pattern, req.ID)
	row, err := r.dbHelper.Open().Exec(stmt, agrs...)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	eff, _ := row.RowsAffected()
	if eff == 0 {
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (r *managementSystemRepository) DeleteNotificationTemplate(ctx context.Context,
	id uint32) (err error) {
	stmt := `DELETE FROM notification_template WHERE id = :1`
	row, err := r.dbHelper.Open().Exec(stmt, id)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	eff, _ := row.RowsAffected()
	if eff == 0 {
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (r *managementSystemRepository) AddNotificationTemplate(ctx context.Context,
	req *model.AddNotificationTemplate) (err error) {

	stmt := `INSERT INTO notification_template (template, subject, body, pattern, is_active)
			VALUES (:1, :2, :3, :4, 'Y')`
	var agrs []interface{}
	agrs = append(agrs, req.Template, req.Subject, req.Body, req.Pattern)
	row, err := r.dbHelper.Open().Exec(stmt, agrs...)
	if err != nil {
		return errors.New(common.ReasonDBError.Code())
	}
	eff, _ := row.RowsAffected()
	if eff == 0 {
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (r *managementSystemRepository) GetNotificationTemplateDetail(ctx context.Context,
	id uint32) (templateDetail *model.NotificationTemplate, err error) {

	stmt := `SELECT id, template, subject, body, pattern, is_active, created_by, created_date, updated_by , updated_date 
				FROM notification_template WHERE id = :1`
	database := r.dbHelper.Open()
	row := database.QueryRow(stmt, id)
	templateDetail = &model.NotificationTemplate{}
	err = row.Scan(&templateDetail.ID,
		&templateDetail.Template,
		&templateDetail.Subject,
		&templateDetail.Body,
		&templateDetail.Pattern,
		&templateDetail.IsActive,
		&templateDetail.CreatedBy,
		&templateDetail.CreatedDate,
		&templateDetail.UpdatedBy,
		&templateDetail.UpdatedDate)
	if err == sql.ErrNoRows {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	if err != nil {
		logger.GlobaLogger.Errorf("Error while parsing row from result: %v", err.Error())
		return nil, errors.New(common.ReasonDBError.Code())
	}
	if templateDetail == nil {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	return templateDetail, nil
}

func (r *managementSystemRepository) GetListNotificationTemplate(ctx context.Context) (resp []*model.NotificationTemplate, err error) {

	stmt := `SELECT id, template, subject, body, pattern, is_active, created_by, created_date, updated_by , updated_date 
				FROM notification_template`
	rows, err := r.dbHelper.Open().Query(stmt)
	if err != nil {
		logger.GlobaLogger.Errorf(" Error while get list notification template: %v", err.Error())
		return nil, errors.New(common.ReasonDBError.Code())
	}
	defer rows.Close()
	var managementSystem *model.NotificationTemplate
	for rows.Next() {
		managementSystem = &model.NotificationTemplate{}
		err = rows.Scan(&managementSystem.ID,
			&managementSystem.Template,
			&managementSystem.Subject,
			&managementSystem.Body,
			&managementSystem.Pattern,
			&managementSystem.IsActive,
			&managementSystem.CreatedBy,
			&managementSystem.CreatedDate,
			&managementSystem.UpdatedBy,
			&managementSystem.UpdatedDate)
		if err != nil {
			logger.GlobaLogger.Errorf("Error while parsing row from result: %v", err.Error())
			return nil, errors.New(common.ReasonDBError.Code())
		}
		resp = append(resp, managementSystem)
	}
	if len(resp) == 0 {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	return resp, nil
}

func (r *managementSystemRepository) GetFlagPages(ctx context.Context) (res []*model.FeatureFlag, err error) {

	var (
		stmt = `
			SELECT PATH, VALUE, FROM_DATE, TO_DATE
			FROM FEATURE_FLAG WHERE IS_ACTIVE = 1
		`
	)

	database := r.dbHelper.Open()
	rows, err := database.Query(stmt)
	if err != nil {
		logger.GlobaLogger.Errorf("repository.managementSystemRepository/GetFlagPages Error while get all flags: %v", err.Error())
		return nil, errors.New(common.ReasonDBError.Code())
	}
	defer rows.Close()

	for rows.Next() {
		flag := &model.FeatureFlag{}
		err = rows.Scan(&flag.Key, &flag.Value, &flag.FromDate, &flag.ToDate)
		if err != nil {
			logger.GlobaLogger.Errorf("repository.managementSystemRepository/GetFlagPages Error while scan record db: %v", err.Error())
			return nil, err
		}
		res = append(res, flag)
	}

	return res, nil
}

func (r *managementSystemRepository) GetAdminFlagPages(ctx context.Context) (res []model.FeatureFlag, err error) {
	var (
		stmt = `
			SELECT PATH, VALUE, FROM_DATE, TO_DATE, IS_ACTIVE
			FROM FEATURE_FLAG
		`
	)
	rows, err := r.dbHelper.Open().Query(stmt)
	if err != nil {
		logger.GlobaLogger.Errorf("repository.managementSystemRepository/GetAdminFlagPages Error while get all flags: %v", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		flag := model.FeatureFlag{}
		err = rows.Scan(&flag.Key, &flag.Value, &flag.FromDate, &flag.ToDate, &flag.IsActive)
		if err != nil {
			logger.GlobaLogger.Errorf("repository.managementSystemRepository/GetFlagPages Error while scan record db: %v", err.Error())
			return nil, errors.New(common.ReasonDBError.Code())
		}
		res = append(res, flag)
	}

	return res, nil
}

func (r *managementSystemRepository) GetDetailFlagPage(ctx context.Context, flagID uint32) (res *model.FeatureFlag, err error) {
	var (
		stmt = `SELECT PATH, VALUE, FROM_DATE, TO_DATE
				FROM FEATURE_FLAG WHERE ID = :1`
	)
	rows, err := r.dbHelper.Open().Query(stmt, flagID)
	if err != nil {
		logger.GlobaLogger.Errorf("repository.managementSystemRepository/GetDetailFlagPage Error while get flag detail flags: %v", err.Error())
		return nil, errors.New(common.ReasonDBError.Code())
	}
	defer rows.Close()
	flag := model.FeatureFlag{}

	for rows.Next() {
		err = rows.Scan(&flag.Key, &flag.Value, &flag.FromDate, &flag.ToDate)
		if err != nil {
			logger.GlobaLogger.Errorf("repository.managementSystemRepository/GetDetailFlagPage Error while scan record db: %v", err.Error())
			return nil, errors.New(common.ReasonDBError.Code())
		}
	}
	if flag.Key == "" {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	return &flag, nil
}

func (r *managementSystemRepository) PatchFlagPage(ctx context.Context, reqDTO dto.PatchFlagPageRequestDTO) (err error) {
	sqlStatement := `UPDATE FEATURE_FLAG SET PATH = :1,
					VALUE = :2,
					FROM_DATE = :3,
					TO_DATE = :4
					WHERE ID = :5`
	conn := r.dbHelper.Open()
	_, err = conn.Exec(sqlStatement, reqDTO.Key, reqDTO.Value, reqDTO.FromDate, reqDTO.ToDate, reqDTO.ID)
	if err != nil {
		return err
	}
	return nil
}

func (r *managementSystemRepository) PostFlagPage(ctx context.Context, reqDTO dto.PostFlagPageRequestDTO) (err error) {
	tx := r.dbHelper.Open()
	stmt := `INSERT INTO FEATURE_FLAG
			(PATH, VALUE, FROM_DATE, TO_DATE) VALUES
			(:1, :2, :3, :4)`

	value := "0"
	if reqDTO.Value {
		value = "1"
	}
	_, err = tx.Exec(stmt, reqDTO.Key, value, reqDTO.FromDate, reqDTO.ToDate)
	if err != nil {
		logger.GlobaLogger.Errorf("repository.managementSystemRepository/PostFlagPage Error while post new Id: %v", err.Error())
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (r *managementSystemRepository) DeleteFlagPage(ctx context.Context, flagID uint32) (err error) {
	stmt := `DELETE FROM FEATURE_FLAG WHERE id = :1 `
	conn := r.dbHelper.Open()
	result, err := conn.Exec(stmt, flagID)
	if err != nil {
		logger.GlobaLogger.Errorf("repository.managementSystemRepository/DeleteFlagPage Error while delete Id: %v", err.Error())
		return errors.New(common.ReasonDBError.Code())
	}
	if num, err := result.RowsAffected(); num == 0 || err != nil {
		if num == 0 {
			return nil
		}
		if err != nil {
			return errors.New(common.ReasonDBError.Code())
		}
	}
	return nil
}

func (r *managementSystemRepository) CheckFlagAccess(ctx context.Context,
	router string) (flag model.FeatureFlag, err error) {
	builder := strings.Builder{}
	stmt := `SELECT id, value, from_date, to_date FROM feature_flag WHERE is_active = '1' and path = ?`
	builder.WriteString(stmt)
	var args []interface{}
	args = append(args, router)
	fmt.Println(builder.String())
	row := r.dbHelper.Open().QueryRow(builder.String(), args...)
	err = row.Scan(&flag.ID, &flag.Value, &flag.FromDate, &flag.ToDate)
	if err != nil {
		return flag, errors.New(common.ReasonDBError.Code())
	}
	if err != nil && err == sql.ErrNoRows {
		return flag, errors.New(common.ReasonNotFound.Code())
	}
	return flag, nil
}

// SELECT id, value, from_date, to_date FROM feature_flag WHERE is_active = '1' and path = '/system/redis';
