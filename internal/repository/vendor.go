package repository

import (
	"context"
	"database/sql"
	"errors"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper/db"
	"shop-back-cms/internal/repository/model"
	"strings"
)

type (
	// VendorRepository declare all func in comment repository
	VendorRepository interface {
		GetListVendor(ctx context.Context, req *dto.GetListVendorRequestDTO) ([]*model.VendorModel, error)
		SearchVendor(ctx context.Context, req *dto.SearchVendorRequestDTO) ([]model.VendorModel, error)
		GetVendorDetail(ctx context.Context, id uint32) (*model.VendorModel, error)
		PostNewVendor(ctx context.Context, req *dto.PostNewVendorRequestDTO) error
		PutUpdateVendor(ctx context.Context, req *dto.PutUpdateVendorRequestDTO) error
		DeleteVendor(ctx context.Context, vendorID uint32) error
		DoActiveVendor(ctx context.Context, vendorID, isActive uint32) error
	}
	vendorRepository struct {
		dbHelper db.DBHelper
	}
)

// NewVendorRepository func new instance comment repository
func NewVendorRepository(dbHelper db.DBHelper) VendorRepository {
	return &vendorRepository{
		dbHelper: dbHelper,
	}
}
func (r *vendorRepository) GetListVendor(ctx context.Context,
	req *dto.GetListVendorRequestDTO) (vendor []*model.VendorModel, err error) {
	stmt := `SELECT id, name, code, address, phone_number, description, created_at, updated_at , is_active
	FROM vendor ORDER BY id DESC`
	rows, err := r.dbHelper.Open().Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		vendorI := &model.VendorModel{}
		err = rows.Scan(&vendorI.ID, &vendorI.Name,
			&vendorI.Code, &vendorI.Address,
			&vendorI.PhoneNumber, &vendorI.Description, &vendorI.CreatedAt,
			&vendorI.UpdatedAt,
			&vendorI.IsActive,
		)
		if err != nil {
			return nil, err
		}
		vendor = append(vendor, vendorI)
	}
	return vendor, nil
}

func (r *vendorRepository) SearchVendor(ctx context.Context,
	req *dto.SearchVendorRequestDTO) (res []model.VendorModel, err error) {
	// TODO fix
	var (
		vendor model.VendorModel
	)
	stmt := `select id, name, code, address, phone_number, created_at, updated_at from vendor 
	where name like ? order by id desc`
	rows, err := r.dbHelper.Open().Query(stmt, "%"+req.Name+"%")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&vendor.ID, &vendor.Name,
			&vendor.Code, &vendor.Address, &vendor.PhoneNumber, &vendor.CreatedAt, &vendor.UpdatedAt)
		if err != nil {
			return nil, err
		}
		res = append(res, vendor)
	}
	return res, err
}

func (r *vendorRepository) GetVendorDetail(ctx context.Context,
	id uint32) (vendor *model.VendorModel, err error) {
	vendor = &model.VendorModel{}
	stmt := `SELECT id, name, code, address, phone_number, description, is_active,
	created_at, updated_at FROM vendor WHERE id = ? ORDER BY id DESC`
	row := r.dbHelper.Open().QueryRow(stmt, id)
	err = row.Scan(&vendor.ID, &vendor.Name, &vendor.Code,
		&vendor.Address, &vendor.PhoneNumber, &vendor.Description, &vendor.IsActive,
		&vendor.CreatedAt, &vendor.UpdatedAt)
	if err != nil {
		return nil, err
	}
	if err == sql.ErrNoRows {
		return nil, err
	}
	return vendor, nil
}

func (r *vendorRepository) PostNewVendor(ctx context.Context,
	req *dto.PostNewVendorRequestDTO) (err error) {
	var (
		statement strings.Builder
		args      []interface{}
	)
	stmt := `INSERT INTO vendor (name, code, address, phone_number, description ) 
	VALUES (?, ?, ?, ?, ?)`
	statement.WriteString(stmt)
	args = append(args, req.Name, req.Code, req.Address, req.PhoneNumber, req.Description)
	result, err := r.dbHelper.Open().Exec(statement.String(), args...)
	if err != nil {
		return err
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonDBError.Code()))
	}
	return nil
}

func (r *vendorRepository) PutUpdateVendor(ctx context.Context,
	req *dto.PutUpdateVendorRequestDTO) (err error) {
	smtp := `UPDATE vendor SET name = ?, 
	code = ?, address = ?, phone_number = ? WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(smtp, req.Name, req.Code, req.Address, req.PhoneNumber, req.ID)
	if err != nil {
		return err
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonNotFound.Code()))
	}
	return nil
}

func (r *vendorRepository) DeleteVendor(ctx context.Context,
	vendorID uint32) (err error) {
	smtp := `DELETE FROM vendor WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(smtp, vendorID)
	if err != nil {
		return errors.New((common.ReasonDBError.Code()))
	}
	if num, err := result.RowsAffected(); err != nil || num == 0 {
		return errors.New((common.ReasonNotFound.Code()))
	}
	return nil
}

func (r *vendorRepository) DoActiveVendor(ctx context.Context, clientID, isActive uint32) error {
	stmt := `UPDATE vendor SET is_active = ? WHERE id = ?`
	result, err := r.dbHelper.Open().Exec(stmt, isActive, clientID)
	if err != nil {
		return err
	}
	numEffect, _ := result.RowsAffected()
	if numEffect == 0 {
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}
