package usecase

import (
	"golang.org/x/crypto/bcrypt"
)

// ValidatePhoneNumber check phone valid
func ValidatePhoneNumber(user string) bool {
	return true
}

// ValidatePassword check phone valid
func ValidatePassword(pass string) bool {
	return true
}

// ValidateUsername check phone valid
func ValidateUsername(num string) bool {
	return true
}

// HashPassword passwoird
func HashPassword(pass string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(pass), 14)
	return string(bytes), err
}

// CheckPasswordHash passwoird
func CheckPasswordHash(pass, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(pass))
	if err != nil {
		return false
	}
	return true
}
