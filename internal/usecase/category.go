package usecase

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go-libs/logger"

	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
)

type (
	// CategoryUsecase represent all func in comment usecase
	CategoryUsecase interface {
		GetListCategory(ctx context.Context, req *dto.GetListCategoryRequestDTO) (res *dto.GetListCategoryResponseDTO, err error)
		PostNewCategory(ctx context.Context, req *dto.PostNewCategoryRequestDTO) (res *dto.PostNewCategoryResponseDTO, err error)
		PutUpdateCategory(ctx context.Context, req *dto.PutUpdateCategoryRequestDTO) (res *dto.PutUpdateCategoryResponseDTO, err error)
		DeleteCategory(ctx context.Context, req *dto.DeleteCategoryRequestDTO) (res *dto.DeleteCategoryResponseDTO, err error)
		DoActiveCategory(ctx context.Context, req *dto.DoActiveCategoryRequestDTO) error
		GetCategoryDetail(ctx context.Context, req *dto.GetCategoryDetailRequestDTO) (*dto.CategorysDTO, error)
		SearchCategory(ctx context.Context, req *dto.SearchCategoryRequestDTO) (res *dto.SearchCategoryResponseDTO, err error)
	}

	categoryUsecase struct {
		cfg                *config.Config
		modelCopier        helper.ModelCopier
		categoryRepository repository.CategoryRepository
		helperRedis        cache.CacheHelper
	}
)

// NewCategoryUsecase new instance for commentDI
func NewCategoryUsecase(
	cfg *config.Config,
	modelCopier helper.ModelCopier,
	categoryRepository repository.CategoryRepository,
	helperRedis cache.CacheHelper,
) CategoryUsecase {
	return &categoryUsecase{
		cfg:                cfg,
		categoryRepository: categoryRepository,
		modelCopier:        modelCopier,
		helperRedis:        helperRedis,
	}
}

func (u *categoryUsecase) SearchCategory(ctx context.Context,
	req *dto.SearchCategoryRequestDTO) (res *dto.SearchCategoryResponseDTO, err error) {
	var (
		categoryModel = []*model.Category{}
	)
	res = &dto.SearchCategoryResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Category.CacheKeySearchCategory, req.Name, req.Code, req.IsActive)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, categoryModel)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Search category in redis.", req.TransactionID)
		categoryModel = valueInterface.([]*model.Category)
		// u.modelCopier.CopyFromModel(res, categoryModel)
		// return res, nil
	} else {
		categoryModel, err = u.categoryRepository.SearchCategory(ctx, req)
		if err != nil {
			logger.GlobaLogger.Errorf("%v: Error while search category: err = %v", req.TransactionID, err)
			return nil, errors.New((common.ReasonDBError.Code()))
		}
		u.helperRedis.SetInterface(ctx, keyCache, categoryModel, time.Duration(u.cfg.Client.CacheTimeSearchClient)*time.Second)
	}

	res.Categorys = make([]dto.CategorysDTO, len(categoryModel))
	for i, category := range categoryModel {
		item := dto.CategorysDTO{
			// ID:          (category.ID),
			Name:        category.Name,
			Code:        category.Code,
			Description: category.Description.String,
			// CreatedAt:   category.CreatedAt,
			// UpdatedAt:   category.UpdatedAt,
			// IsActive: category.IsActive == "1",
		}
		res.Categorys[i] = item
	}
	return res, nil

	// u.modelCopier.CopyFromModel(res, categoryModel)
	// return res, nil
}

func (u *categoryUsecase) GetCategoryDetail(ctx context.Context,
	req *dto.GetCategoryDetailRequestDTO) (category *dto.CategorysDTO, err error) {
	var (
		moClient *model.Category
	)
	category = &dto.CategorysDTO{}
	keyCache := fmt.Sprintf(u.cfg.Category.CacheKeyDetailCategory, req.CategoryID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, category)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get detail category in redis.", req.TransactionID)
		category = resInter.(*dto.CategorysDTO)
		return category, nil
	}
	moClient, err = u.categoryRepository.GetCategoryDetail(ctx, req.CategoryID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: usecase.categoryUsecase/GetCategoryDetail: Error while get detail category: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.helperRedis.Set(ctx, keyCache, category, time.Duration(u.cfg.Client.CacheTimeDetailClient)*time.Second)
	category = &dto.CategorysDTO{}
	// category.ID = (moClient.ID)
	category.Name = moClient.Name
	category.Code = moClient.Code
	// category.CreatedAt = moClient.CreatedAt
	// category.UpdatedAt = moClient.UpdatedAt
	// category.IsActive = moClient.IsActive == "1"
	category.Description = moClient.Description.String
	return category, nil
}

func (u *categoryUsecase) DoActiveCategory(ctx context.Context,
	req *dto.DoActiveCategoryRequestDTO) (err error) {
	isActive := uint32(1)
	if !req.IsActive {
		isActive = uint32(0)
	}
	err = u.categoryRepository.DoActiveCategory(ctx, req.CategoryID, isActive)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.categoryUsecase/DoActiveCategory: Error while active category, %v", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (u *categoryUsecase) PostNewCategory(ctx context.Context,
	req *dto.PostNewCategoryRequestDTO) (res *dto.PostNewCategoryResponseDTO, err error) {
	_, err = u.categoryRepository.PostNewCategory(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v,usecase.categoryUsecase/PostNewCategory: Error while add new category, err = %v", req.TransactionID, err)
		return nil, errors.New((common.ReasonDBError.Code()))
	}
	res = &dto.PostNewCategoryResponseDTO{
		StatusCode: "ACCEPT",
	}
	return res, nil
}

func (u *categoryUsecase) GetListCategory(ctx context.Context,
	req *dto.GetListCategoryRequestDTO) (res *dto.GetListCategoryResponseDTO, err error) {
	var (
		listCategory = []model.Category{}
	)
	res = &dto.GetListCategoryResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Category.CacheKeyListCategory)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, listCategory)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v: Get list category in redis.", req.TransactionID)
		listCategory = valueInterface.([]model.Category)
	} else {
		listCategory, err = u.categoryRepository.GetListCategory(ctx, req)
		if err != nil {
			logger.GlobaLogger.Errorf("%v: Error while get list category: err = %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		u.helperRedis.SetInterface(ctx, keyCache, listCategory, time.Duration(u.cfg.Category.CacheTimeListCategory)*time.Second)
	}
	res.Categorys = make([]dto.CategorysDTO, len(listCategory))
	res.Categories = make([]string, len(listCategory))
	for i, category := range listCategory {

		item := dto.CategorysDTO{
			Id:          category.ID,
			Name:        category.Name,
			Code:        category.Code,
			Description: category.Description.String,
			// CreatedAt:   category.CreatedAt,
			// UpdatedAt:   category.UpdatedAt,
			// IsActive: category.IsActive == "1",
		}
		res.Categorys[i] = item
		res.Categories[i] = category.Name
	}
	return res, nil

}

func (u *categoryUsecase) PutUpdateCategory(ctx context.Context, req *dto.PutUpdateCategoryRequestDTO) (res *dto.PutUpdateCategoryResponseDTO, err error) {
	err = u.categoryRepository.PutUpdateCategory(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("usecase.categoryUsecase/PutUpdateCategory: Error while update category")
		return nil, err
	}
	res = &dto.PutUpdateCategoryResponseDTO{
		StatusCode: "ACCEPT",
	}
	return res, nil
}

func (u *categoryUsecase) DeleteCategory(ctx context.Context, req *dto.DeleteCategoryRequestDTO) (res *dto.DeleteCategoryResponseDTO, err error) {
	err = u.categoryRepository.DeleteCategory(ctx, req)
	if err != nil {
		return nil, err
	}
	res = &dto.DeleteCategoryResponseDTO{
		StatusCode: "ACCEPT",
	}
	return res, nil
}
