package usecase

import (
	"context"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
	"time"
)

type (
	// ClientUsecase declare all func in comment usecase
	ClientUsecase interface {
		GetListClient(ctx context.Context, req *dto.GetListClientRequestDTO) (res *dto.GetListClientResponseDTO, err error)
		SearchClient(ctx context.Context, req *dto.SearchClientRequestDTO) (res *dto.GetListClientResponseDTO, err error)
		GetClientDetail(ctx context.Context, req *dto.GetClientDetailRequestDTO) (res *dto.GetClientDetailResponseDTO, err error)
		PostNewClient(ctx context.Context, req *dto.PostNewClientRequestDTO) (res *dto.PostNewClientResponseDTO, err error)
		PutUpdateClient(ctx context.Context, req *dto.PutUpdateClientRequestDTO) (res *dto.PutUpdateClientResponseDTO, err error)
		DeleteClient(ctx context.Context, req *dto.DeleteClientRequestDTO) (res *dto.DeleteClientResponseDTO, err error)
		DoActiveClient(ctx context.Context, req *dto.DoActiveClientRequestDTO) error
		// TODO write func check phone_number, email exists
		// TODO write func get detail by email, phone,
	}
	clientUsecase struct {
		cfg              *config.Config
		clientRepository repository.ClientRepository
		modelCopier      helper.ModelCopier
		helperRedis      cache.CacheHelper
	}
)

// NewClientUsecase create instance Comment Usecase
func NewClientUsecase(
	cfg *config.Config,
	clientRepository repository.ClientRepository,
	modelCopier helper.ModelCopier,
	helperRedis cache.CacheHelper,
) ClientUsecase {
	return &clientUsecase{
		cfg:              cfg,
		clientRepository: clientRepository,
		modelCopier:      modelCopier,
		helperRedis:      helperRedis,
	}
}

func (u *clientUsecase) DoActiveClient(ctx context.Context,
	req *dto.DoActiveClientRequestDTO) (err error) {
	isActive := uint32(1)
	if !req.IsActive {
		isActive = uint32(0)
	}
	err = u.clientRepository.DoActiveClient(ctx, req.ClientID, isActive)
	if err != nil {
		logger.GlobaLogger.Errorf("%v,usecase.clientUsecase/DoActiveClient: Error while active client, %v", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (u *clientUsecase) PostNewClient(ctx context.Context,
	req *dto.PostNewClientRequestDTO) (res *dto.PostNewClientResponseDTO, err error) {
	// TODO validation phone_number, email, account_brank
	if req.Name == "" {
		logger.GlobaLogger.Errorf("usecase.client/PostNewClient: Filed name not empty")
		return nil, errors.New(common.ReasonClientFieldNameNotEmpty.Code())
	}

	err = u.clientRepository.PostNewClient(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.comment/PostNewClient:  Error while add new client", req.TransactionID, err)
		return nil, errors.New((common.ReasonDBError.Code()))
	}
	return res, err
}

func (u *clientUsecase) GetListClient(ctx context.Context,
	req *dto.GetListClientRequestDTO) (res *dto.GetListClientResponseDTO, err error) {
	var (
		listClient = []model.ClientModel{}
	)
	res = &dto.GetListClientResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Client.CacheKeyListClient)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, listClient)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get list client in redis.", req.TransactionID)
		listClient = valueInterface.([]model.ClientModel)
		// u.modelCopier.CopyFromModel(&res.Clients, listClient)
		// return res, nil
	} else {
		listClient, err = u.clientRepository.GetListClient(ctx, req)
		if err != nil {
			logger.GlobaLogger.Errorf("%v: Error while get list client: err = %v", req.TransactionID, err)
			return nil, errors.New((common.ReasonDBError.Code()))
		}
		u.helperRedis.SetInterface(ctx, keyCache, listClient, time.Duration(u.cfg.Client.CacheTimeListClient)*time.Second)
	}
	res.Clients = make([]dto.ClientsDTO, len(listClient))
	for i, client := range listClient {
		item := &dto.ClientsDTO{
			ID:          client.ID,
			Name:        client.Name,
			Email:       client.Email,
			PhoneNumber: client.PhoneNumber,
			Address:     client.Address,
			AccountBank: client.AccountBank,
			CreatedAt:   client.CreatedAt,
			UpdatedAt:   client.UpdatedAt,
			IsActive:    client.IsActive == "1",
		}
		res.Clients[i] = *item
	}
	// u.modelCopier.CopyFromModel(&res.Clients, listClient)
	return res, nil
}

func (u *clientUsecase) SearchClient(ctx context.Context, req *dto.SearchClientRequestDTO) (res *dto.GetListClientResponseDTO, err error) {
	var (
		clientModel = []model.ClientModel{}
	)
	res = &dto.GetListClientResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Client.CacheKeySearchClient, req.Name, req.Email, req.PhoneNumber, req.AccountBank)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, clientModel)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Search client in redis.", req.TransactionID)
		clientModel = valueInterface.([]model.ClientModel)
		u.modelCopier.CopyFromModel(&res.Clients, clientModel)
		return res, nil
	}
	clientModel, err = u.clientRepository.SearchClient(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while search client: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.helperRedis.SetInterface(ctx, keyCache, clientModel, time.Duration(u.cfg.Client.CacheTimeSearchClient)*time.Second)
	u.modelCopier.CopyFromModel(&res.Clients, clientModel)
	return res, nil
}

func (u *clientUsecase) GetClientDetail(ctx context.Context,
	req *dto.GetClientDetailRequestDTO) (res *dto.GetClientDetailResponseDTO, err error) {
	var (
		moClient *model.ClientModel
	)
	res = &dto.GetClientDetailResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Client.CacheKeyDetailClient, req.ID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, res)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get detail client in redis.", req.TransactionID)
		res = resInter.(*dto.GetClientDetailResponseDTO)
		return res, nil
	}
	moClient, err = u.clientRepository.GetClientDetail(ctx, req.ID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get detail client: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.modelCopier.CopyFromModel(&res, moClient)
	u.helperRedis.Set(ctx, keyCache, res, time.Duration(u.cfg.Client.CacheTimeDetailClient)*time.Second)
	return res, nil
}

func (u *clientUsecase) PutUpdateClient(ctx context.Context, req *dto.PutUpdateClientRequestDTO) (res *dto.PutUpdateClientResponseDTO, err error) {
	err = u.clientRepository.PutUpdateClient(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while update client: err = %v", req.TransactionID, err)
		return nil, err
	}
	return res, nil
}

func (u *clientUsecase) DeleteClient(ctx context.Context, req *dto.DeleteClientRequestDTO) (res *dto.DeleteClientResponseDTO, err error) {
	if req.ID == 0 {
		logger.GlobaLogger.Errorf("%v,usecase.client/DeleteClient: Filed ID = 0 invalid", req.TransactionID)
		return nil, errors.New(common.ReasonClientFieldIDInvalid.Code())
	}
	err = u.clientRepository.DeleteClient(ctx, req.ID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.client/DeleteClient: Error while add new client, err=%v", req.TransactionID, err)
		return nil, err
	}
	res = &dto.DeleteClientResponseDTO{}
	return res, nil
}
