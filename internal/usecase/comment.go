package usecase

import (
	"context"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
	"time"
)

type (
	// CommentUsecase declare all func in comment usecase
	CommentUsecase interface {
		GetListComment(ctx context.Context, req *dto.GetListCommentRequestDTO) (res *dto.GetListCommentResponseDTO, err error)
		SearchComment(ctx context.Context, req *dto.SearchCommentRequestDTO) (res *dto.GetListCommentResponseDTO, err error)
		GetCommentDetail(ctx context.Context, req *dto.GetCommentDetailRequestDTO) (res *dto.CommentsDTO, err error)
		PostNewComment(ctx context.Context, req *dto.PostNewCommentRequestDTO) (res *dto.PostNewCommentResponseDTO, err error)
		PutUpdateComment(ctx context.Context, req *dto.PutUpdateCommentRequestDTO) (res *dto.PutUpdateCommentResponseDTO, err error)
		DeleteComment(ctx context.Context, req *dto.DeleteCommentRequestDTO) (res *dto.DeleteCommentResponseDTO, err error)
		DoActiveComment(ctx context.Context, req *dto.DoActiveCommentRequestDTO) error
		// review
		GetReviewCommonClient(ctx context.Context, req *dto.GetReviewCommonClientRequestDTO) (res *dto.GetReviewCommonClientResponseDTO, err error)
	}
	commentUsecase struct {
		cfg               *config.Config
		commentRepository repository.CommentRepository
		modelCopier       helper.ModelCopier
		helperRedis       cache.CacheHelper
	}
)

// NewCommentUsecase create instance Comment Usecase
func NewCommentUsecase(
	cfg *config.Config,
	commentRepository repository.CommentRepository,
	modelCopier helper.ModelCopier,
	helperRedis cache.CacheHelper,
) CommentUsecase {
	return &commentUsecase{
		cfg:               cfg,
		commentRepository: commentRepository,
		modelCopier:       modelCopier,
		helperRedis:       helperRedis,
	}
}

func (u *commentUsecase) DoActiveComment(ctx context.Context,
	req *dto.DoActiveCommentRequestDTO) (err error) {
	isActive := uint32(1)
	if !req.IsActive {
		isActive = uint32(0)
	}
	err = u.commentRepository.DoActiveComment(ctx, req.CommentID, isActive)
	if err != nil {
		logger.GlobaLogger.Errorf("%v,usecase.commentUsecase/DoActiveComment: Error while active comment, %v", req.TransactionID, err)
		return err
	}
	return nil
}

func (u *commentUsecase) PostNewComment(ctx context.Context,
	req *dto.PostNewCommentRequestDTO) (res *dto.PostNewCommentResponseDTO, err error) {
	err = u.commentRepository.PostNewComment(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.comment/PostNewComment: Error while add new comment", req.TransactionID, err)
		return nil, err
	}
	return res, err
}

func (u *commentUsecase) GetListComment(ctx context.Context,
	req *dto.GetListCommentRequestDTO) (res *dto.GetListCommentResponseDTO, err error) {
	var (
		modelComment = []*model.CommentModel{}
	)
	res = &dto.GetListCommentResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Comment.CacheKeyListComment, req.CurrentPage, req.IsActive)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelComment)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get list comment in redis.", req.TransactionID)
		modelComment = valueInterface.([]*model.CommentModel)
		u.modelCopier.CopyFromModel(&res.Comments, modelComment)
		return res, nil
	}
	modelComment, err = u.commentRepository.GetListComment(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get list comment: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.helperRedis.SetInterface(ctx, keyCache, modelComment, time.Duration(u.cfg.Comment.CacheTimeListComment)*time.Second)
	u.modelCopier.CopyFromModel(&res.Comments, modelComment)
	return res, nil
}

func (u *commentUsecase) SearchComment(ctx context.Context,
	req *dto.SearchCommentRequestDTO) (res *dto.GetListCommentResponseDTO, err error) {
	var (
		commentModel = []model.CommentModel{}
	)
	res = &dto.GetListCommentResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Comment.CacheKeySearchComment, req.UserId, req.ProductId, req.FromDate, req.ToDate)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, commentModel)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Search comment in redis.", req.TransactionID)
		commentModel = valueInterface.([]model.CommentModel)
		u.modelCopier.CopyFromModel(&res.Comments, commentModel)
		return res, nil
	}
	commentModel, err = u.commentRepository.SearchComment(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while search comment: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.helperRedis.SetInterface(ctx, keyCache, commentModel, time.Duration(u.cfg.Comment.CacheTimeSearchComment)*time.Second)
	u.modelCopier.CopyFromModel(&res.Comments, commentModel)
	return res, nil
}

func (u *commentUsecase) GetCommentDetail(ctx context.Context,
	req *dto.GetCommentDetailRequestDTO) (res *dto.CommentsDTO, err error) {
	var (
		moComment *model.CommentModel
	)
	res = &dto.CommentsDTO{}
	keyCache := fmt.Sprintf(u.cfg.Comment.CacheKeyDetailComment, req.ID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, res)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get detail comment in redis.", req.TransactionID)
		res = resInter.(*dto.CommentsDTO)
		return res, nil
	}
	moComment, err = u.commentRepository.GetCommentDetail(ctx, req.ID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get detail comment: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.modelCopier.CopyFromModel(&res, moComment)
	u.helperRedis.Set(ctx, keyCache, res, time.Duration(u.cfg.Comment.CacheTimeDetailComment)*time.Second)
	return res, nil
}

func (u *commentUsecase) PutUpdateComment(ctx context.Context,
	req *dto.PutUpdateCommentRequestDTO) (res *dto.PutUpdateCommentResponseDTO, err error) {
	err = u.commentRepository.PutUpdateComment(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while update comment: err = %v", req.TransactionID, err)
		return nil, err
	}
	return res, nil
}

func (u *commentUsecase) DeleteComment(ctx context.Context,
	req *dto.DeleteCommentRequestDTO) (res *dto.DeleteCommentResponseDTO, err error) {
	if req.Id == 0 {
		logger.GlobaLogger.Errorf("%v,usecase.comment/DeleteComment: Filed ID = 0 invalid", req.TransactionID)
		return nil, errors.New(common.ReasonClientFieldIDInvalid.Code())
	}
	err = u.commentRepository.DeleteComment(ctx, uint32(req.Id))
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.comment/DeleteComment: Error while add new comment, err=%v", req.TransactionID, err)
		return nil, err
	}
	res = &dto.DeleteCommentResponseDTO{}
	return res, nil
}

func (u *commentUsecase) GetReviewCommonClient(ctx context.Context,
	req *dto.GetReviewCommonClientRequestDTO) (res *dto.GetReviewCommonClientResponseDTO, err error) {
	var (
		modelComment = []model.ReviewCommonClient{}
	)
	res = &dto.GetReviewCommonClientResponseDTO{}
	keyCache := "back-cms:review-common"
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelComment)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v: Get list review common in redis.", req.TransactionID)
		modelComment = valueInterface.([]model.ReviewCommonClient)
		u.modelCopier.CopyFromModel(&res.Reviews, modelComment)
		return res, nil
	}
	modelComment, err = u.commentRepository.GetReviewCommonClient(ctx)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get list comment: err = %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	u.helperRedis.SetInterface(ctx, keyCache, modelComment, time.Duration(66)*time.Second)
	u.modelCopier.CopyFromModel(&res.Reviews, modelComment)
	return res, nil
}
