package usecase

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"os"
	"shop-back-cms/config"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/facade"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
)

// type (
// 	UploadFileRequestDTO struct {
// 		TransactionID string
// 		FileName      string

// 	}
// )

type (
	UploadFileUsecase interface {
		UploadFile(ctx context.Context, file *os.File, req *dto.UploadFileRequestDTO) error
	}

	uploadFileUsecase struct {
		cfg               *config.Config
		cacheHelper       cache.CacheHelper
		processFileFacade facade.ProcessFileFacade
		fileRepository    repository.FileRepository
	}
)

func NewUploadFileUsecase(
	cfg *config.Config,
	cacheHelper cache.CacheHelper,
	processFileFacade facade.ProcessFileFacade,
	fileRepository repository.FileRepository,
) UploadFileUsecase {
	return &uploadFileUsecase{
		cfg:               cfg,
		cacheHelper:       cacheHelper,
		processFileFacade: processFileFacade,
		fileRepository:    fileRepository,
	}
}

func (u *uploadFileUsecase) readFileCSV() error {
	csvFile, err := os.Open("emp.csv")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened CSV file")
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println(err)
	}
	for _, line := range csvLines {
		fmt.Println(line)
	}
	return nil
}

func (u *uploadFileUsecase) UploadFile(ctx context.Context, file *os.File, req *dto.UploadFileRequestDTO) error {
	var (
		err error
	)
	file.Name()
	switch req.FileType {
	case "csv":
		// u.readFileCSV()
		fmt.Println(file.Name())
		csvFile, err := os.Open(file.Name())
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("Successfully Opened CSV file")
		defer csvFile.Close()

		csvLines, err := csv.NewReader(csvFile).ReadAll()
		if err != nil {
			fmt.Println(err)
		}
		itemp := 0
		for _, line := range csvLines {
			itemp++
			fmt.Println(line)
			if itemp == 100 {
				break
			}
		}
		return nil
	case "xlsx":
		// TODO
	default:
		return errors.New("sdf")
	}
	return err
}

// func getFromMD(ctx context.Context, key string) string {
// 	if md, ok := metadata.FromIncomingContext(ctx); ok {
// 		if domain, ok := md[key]; ok && len(domain) != 0 {
// 			return domain[0]
// 		}
// 	}
// 	return ""
// }

// func getCreatedByFromMD(ctx context.Context) string {
// 	return getFromMD(ctx, common.DomainMDKey)
// }

// func (u *uploadFileUsecase) GetListUploadFile(ctx context.Context,
// 	req *dto.GetListUploadFileRequestDTO) (resp []*dto.UploadFileDTO, err error) {
// 	span := jaeger.Start(ctx, ">usecase.uploadFileUsecase/GetListUploadFile", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		listIPS []*model.GetUploadFile
// 	)
// 	listIPS, err = u.iPSIncomingRepository.GetListUploadFile(ctx, req)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while get list ips: %v", req.TransactionID, err)
// 		return nil, errors.New(common.ReasonDBError.Code())
// 	}
// 	if len(listIPS) == 0 {
// 		return nil, errors.New(common.ReasonNotFound.Code())
// 	}
// 	resp = make([]*dto.UploadFileDTO, len(listIPS))
// 	for index, ips := range listIPS {
// 		ipsDTO := &dto.UploadFileDTO{}
// 		ipsDTO.FileName = ips.FileName
// 		ipsDTO.NumberRecord = ips.NumberRecord
// 		ipsDTO.ProcessDate = ips.ProcessDate.String
// 		ipsDTO.ProcessStatus = ips.ProcessStatus
// 		ipsDTO.UserImport = ips.UserImport.String
// 		ipsDTO.UserProcess = ips.UserProcess.String
// 		resp[index] = ipsDTO
// 	}
// 	return resp, nil
// }

// func (u *uploadFileUsecase) readFileDataUploadFile(
// 	inputFile *os.File, fileName string) (outDataIPS []*dto.UploadFileData, err error) {
// 	file, err := os.Open(inputFile.Name())
// 	if err != nil {
// 		return nil, err
// 	}
// 	scanner := bufio.NewScanner(file)
// 	for scanner.Scan() {
// 		line := scanner.Text()
// 		ItemData := &dto.UploadFileData{
// 			FileName: fileName,
// 			LineData: line,
// 		}
// 		outDataIPS = append(outDataIPS, ItemData)
// 	}
// 	return outDataIPS, nil
// }

// func (u *uploadFileUsecase) checkUploadFileType(ctx context.Context, fileName, transactionID string) bool {
// 	log.Logger.Infof("%v - Get list file type", transactionID)
// 	fileTypes, err := u.iPSIncomingRepository.ListFileType(ctx)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while get list file type: %v", transactionID, err)
// 		return false
// 	}
// 	for _, typeVal := range fileTypes {
// 		if strings.Contains(fileName, typeVal.FileType) {
// 			return true
// 		}
// 	}
// 	log.Logger.Errorf("%v - Not found file name in table file type", transactionID)
// 	return false
// }

// func (u *uploadFileUsecase) CreateUploadFile(ctx context.Context,
// 	inputFile *os.File, fileName string, transactionID, domain string) error {
// 	span := jaeger.Start(ctx, ">repository.uploadFileUsecase/CreateUploadFile", ext.SpanKindRPCClient)
// 	var err error
// 	defer jaeger.Finish(span, err)
// 	tx, err := u.baseRepository.BuildTransactions(ctx, "")
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while init transaction: %v", transactionID, err)
// 		return errors.New(common.ReasonFailedInitTransaction.Code())
// 	}
// 	transactionDB := tx.GetTransactions()
// 	isExists, err := u.iPSIncomingRepository.CheckUploadFileExists(ctx, fileName)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while check file is process: %v", transactionID, err)
// 		return errors.New(common.ReasonDBError.Code())
// 	}
// 	if isExists {
// 		log.Logger.Errorf("%v - File exists: %v", transactionID, err)
// 		return errors.New(common.ReasonDuplicateImportBatch.Code())
// 	}

// 	isType := u.checkUploadFileType(ctx, fileName, transactionID)
// 	if !isType {
// 		log.Logger.Errorf("%v - Error while check file type: %v", transactionID, err)
// 		return errors.New(common.ReasonFileNameIncorrect.Code())
// 	}

// 	outData, err := u.readFileDataUploadFile(inputFile, fileName)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while read file import: %v", transactionID, err)
// 		return errors.New(common.ReasonReadFileError.Code())
// 	}
// 	maxMultipleThread := common.MaxMultipleThread
// 	currentIndex := 0
// 	maxLineHandle := common.MaxLineHandle
// 	maxSize, err := strconv.ParseInt(fmt.Sprintf("%.0f",
// 		math.Ceil(float64(len(outData))/float64(maxLineHandle))), 10, 32)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Can not make multiple batching import file ips: %v", transactionID, err)
// 		return errors.New(common.ReasonImportFileFailed.Code())
// 	}
// 	log.Logger.Infof("%v - Max size in create ips: %v", transactionID, maxSize)
// 	var wg sync.WaitGroup
// 	wg.Add(int(maxSize))
// 	errChannel := make(chan error, maxSize)
// 	doneChannel := make(chan bool, maxSize)
// 	workingChannel := make(chan bool, maxMultipleThread)
// 	allDoneChannel := make(chan bool, 1)
// 	WaitContinueProcess := make(chan bool, 1)
// 	isCommitData := true
// 	for i := 0; i < maxMultipleThread; i++ {
// 		workingChannel <- true
// 	}

// 	go func() {
// 		for {
// 			select {
// 			case isErr := <-errChannel:
// 				if isErr != nil {
// 					isCommitData = false
// 				}
// 			case <-allDoneChannel:
// 				WaitContinueProcess <- true
// 				return
// 			}

// 		}
// 	}()

// 	go func() {
// 		for i := 0; i < int(maxSize); i++ {
// 			<-doneChannel
// 			workingChannel <- true
// 		}
// 		log.Logger.Debugf("%v - All Done", transactionID)
// 		wg.Wait()
// 		allDoneChannel <- true
// 	}()

// 	go func() {

// 		for i := 0; i < int(maxSize); i++ {
// 			var splitData []*dto.UploadFileData
// 			indexCurrent := i * maxLineHandle
// 			lenoutData := len(outData)
// 			if currentIndex+maxLineHandle > lenoutData {
// 				splitData = (outData)[indexCurrent:lenoutData]
// 			}
// 			if len(splitData) == 0 {
// 				splitData = (outData)[currentIndex : currentIndex+maxLineHandle]
// 			}
// 			currentIndex += maxLineHandle
// 			<-workingChannel
// 			go func(wg *sync.WaitGroup) {
// 				defer wg.Done()
// 				defer func() {
// 					doneChannel <- true
// 				}()
// 				log.Logger.Infof("%v - Goroutine create ips incoming", transactionID)
// 				err = u.iPSIncomingRepository.CreateUploadFile(ctx, transactionDB, splitData, domain)
// 				if err != nil {
// 					errChannel <- errors.New("err import file")
// 					log.Logger.Errorf("%v - Error while import ips file: %v", transactionID, err)
// 					return
// 				}
// 				errChannel <- nil
// 			}(&wg)
// 		}
// 	}()
// 	<-WaitContinueProcess
// 	if !isCommitData {
// 		return errors.New(common.ReasonImportFileFailed.Code())
// 	}
// 	err = tx.CommitAll()
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while committing data ips transaction: %v", transactionID, err)
// 		return errors.New(common.ReasonImportFileFailed.Code())
// 	}
// 	log.Logger.Infof("%v - Commit all data successfull create ips incoming", transactionID)
// 	return nil
// }

// func (u *uploadFileUsecase) ProcessUploadFile(ctx context.Context,
// 	fileName, transactionID string) (resp *dto.ProcessUploadFileResponseDTO, err error) {
// 	span := jaeger.Start(ctx, ">repository.uploadFileUsecase/ProcessUploadFile", ext.SpanKindRPCClient)
// 	defer jaeger.Finish(span, err)
// 	resp = &dto.ProcessUploadFileResponseDTO{}
// 	err = u.iPSIncomingRepository.CheckUploadFileProcess(ctx, fileName)
// 	if err != nil {
// 		log.Logger.Errorf("%v -  Errordb while process ips file: %v", transactionID, err)
// 		return nil, errors.New(common.ReasonDBError.Code())
// 	}

// 	isType := u.checkUploadFileType(ctx, fileName, transactionID)
// 	if !isType {
// 		log.Logger.Errorf("%v - Error while check file type: %v", transactionID, err)
// 		return nil, errors.New(common.ReasonFileNameIncorrect.Code())
// 	}
// 	var userProcess string
// 	if user, ok := metadata.FromIncomingContext(ctx); len(user["domain"]) > 0 && ok {
// 		userProcess = user["domain"][0]
// 	}
// 	code, _, err := u.iPSIncomingRepository.ProcessUploadFile(ctx, fileName, userProcess)
// 	resp.StatusCode = common.REJECT
// 	if err != nil {
// 		log.Logger.Errorf("%v -  Errordb while process ips file: %v", transactionID, err)
// 		return nil, errors.New(common.ReasonDBError.Code())
// 	}
// 	if code != "00" {
// 		log.Logger.Errorf("%v - Error while process ips file with code: %v", transactionID, err)
// 		return nil, errors.New(common.ReasonProcessFailed.Code())
// 	}
// 	resp.StatusCode = common.ACCEPT
// 	return resp, nil
// }
