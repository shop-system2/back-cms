package usecase

// import (
// 	"context"
// 	"errors"
// 	"fmt"
// 	"log"
// 	"shop-back-cms/config"
// 	"shop-back-cms/internal/common"
// 	"shop-back-cms/internal/dto"
// 	"shop-back-cms/internal/helper/cache"
// 	"shop-back-cms/internal/repository"
// 	"shop-back-cms/internal/repository/model"
// 	"shop-back-cms/internal/util"
// 	"strconv"
// 	"strings"
// 	"time"

// 	"github.com/go-redis/redis"
// 	"github.com/opentracing/opentracing-go/ext"
// )

// type (
// 	MonitoringUsecase interface {
// 		GetListPhysicalChannel(ctx context.Context,
// 			req *dto.GetListPhysicalChannelRequestDTO) ([]*dto.PhysicalChannelDTO, error)
// 		GetTransactionsMonitoring(ctx context.Context,
// 			req *dto.TransactionsMntoringRequestDTO) (*dto.GetTransactionMonitoringDTO, error)
// 		GetListRedis(ctx context.Context, req *dto.GetListRedisRequestDTO) (*dto.ManagementRedis, error)
// 		CheckKeyExists(ctx context.Context, req *dto.CheckKeyExistsRequestDTO) (bool, error)
// 		AddNewRedis(ctx context.Context, req *dto.AddNewRedisRequestDTO) error
// 		DeleteRedisByKey(ctx context.Context, req *dto.DeleteRedisByKeyRequestDTO) error
// 		SetTimeout(ctx context.Context, req *dto.SetTimeoutRequestDTO) error
// 		GetRedisDetail(ctx context.Context, req *dto.GetRedisDetailRequestDTO) (dto.GetRedisDetailResponseDTO, error)
// 	}

// 	monitoringUsecase struct {
// 		cfg                  *config.Config
// 		cacheHelper          cache.CacheHelper
// 		monitoringRepository repository.MonitoringRepository
// 	}
// )

// func NewMonitoringUsecase(
// 	cfg *config.Config,
// 	cacheHelper cache.CacheHelper,
// 	monitoringRepository repository.MonitoringRepository,
// ) MonitoringUsecase {
// 	return &monitoringUsecase{
// 		cfg:                  cfg,
// 		cacheHelper:          cacheHelper,
// 		monitoringRepository: monitoringRepository,
// 	}
// }

// func (u *monitoringUsecase) parserRespcode(in string) bool {
// 	return in == "00"
// }

// func (u *monitoringUsecase) parserISOFromStringToMap(mata string) map[string]string {
// 	defer func() {
// 		ok := recover()
// 		if ok != nil {
// 			log.Logger.Errorf("Process is pannic.")
// 		}
// 	}()
// 	isoMap := make(map[string]string)
// 	isoElements := strings.Split(mata, "F")
// 	for i := 0; i < len(isoElements); i++ {
// 		itemISO := strings.Split(isoElements[i], "=")
// 		if len(itemISO) == 2 {
// 			isoMap[itemISO[0]] = itemISO[1]
// 		}
// 	}
// 	return isoMap
// }

// func (u *monitoringUsecase) parserISOToAmount(isoMap map[string]string, index, length string) string {
// 	val, ok := isoMap[index]
// 	if !ok {
// 		return ""
// 	}
// 	i, _ := strconv.ParseInt(length, 10, 64)
// 	amount := val[:i]
// 	return amount
// }

// func (u *monitoringUsecase) parserISOToReferentNumber(isoMap map[string]string, index, length string) string {
// 	val, ok := isoMap[index]
// 	if !ok {
// 		return ""
// 	}
// 	i, _ := strconv.ParseInt(length, 10, 64)
// 	number := val[:i]
// 	return number
// }

// func (u *monitoringUsecase) isAppendValueFilter(arrInput []*dto.TransMonitoringFilterDTO, input string) bool {
// 	for _, item := range arrInput {
// 		if item.Value == input {
// 			return false
// 		}
// 	}
// 	return true
// }

// func (u *monitoringUsecase) GetTransactionsMonitoring(ctx context.Context,
// 	req *dto.TransactionsMntoringRequestDTO) (resp *dto.GetTransactionMonitoringDTO, err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/GetTransactionsMonitoring", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 		if recovered := recover(); recovered != nil {
// 			log.Logger.Errorf("%v - Recovered from error:%v", req.TransactionID, recovered)
// 			err = errors.New(common.ReasonGeneralError.Code())
// 		}
// 	}()
// 	var (
// 		modelTransaction []*model.TransactionMonitoring
// 	)
// 	cacheListTrans := fmt.Sprintf("list-transaction-monitoring-%v", req.ChannelID)
// 	valueInterface, err := u.cacheHelper.GetInterface(ctx, cacheListTrans, modelTransaction)
// 	if err != nil {
// 		modelTransaction, err = u.monitoringRepository.GetTransactionsMonitoring(ctx, req.ChannelID)
// 		if err != nil {
// 			log.Logger.Errorf("%v - Error while get list transaction monitoring: %v", req.TransactionID, err)
// 			return nil, err
// 		}
// 		timeStoreRedis := req.TimeInterval
// 		if timeStoreRedis == 0 {
// 			timeStoreRedis = uint32(u.cfg.Monitoring.CacheTimeListChannel)
// 		}
// 		err = u.cacheHelper.Set(ctx, cacheListTrans, modelTransaction,
// 			time.Duration(timeStoreRedis)*time.Second)
// 		if err != nil {
// 			log.Logger.Warnf("%v - Error while set cache list physical channel, err: %v", req.TransactionID, err)
// 		}
// 	} else {
// 		modelTransaction = valueInterface.([]*model.TransactionMonitoring)
// 	}
// 	isoAmount := strings.Split(u.cfg.Monitoring.ISO.Amount, ",")
// 	amountIndex := isoAmount[0]
// 	amountLength := isoAmount[1]
// 	isoRefNumber := strings.Split(u.cfg.Monitoring.ISO.RetrievalReferentNumber, ",")
// 	refNumberIndex := isoRefNumber[0]
// 	refNumberLength := isoRefNumber[1]
// 	totalTransaction := len(modelTransaction)
// 	transtions := make([]*dto.TransMonitoringDTO, totalTransaction)
// 	filters := []*dto.TransMonitoringFilterDTO{}
// 	numberSuccess := 0
// 	numberFail := 0

// 	for index, item := range modelTransaction {
// 		itemDTO := &dto.TransMonitoringDTO{}
// 		iso := u.parserISOFromStringToMap(item.Metadata)
// 		itemDTO.ID = item.ID
// 		itemDTO.ChannelName = item.ChannelName
// 		amount := u.parserISOToAmount(iso, amountIndex, amountLength)
// 		itemDTO.Amount = amount
// 		itemDTO.RespCode = u.parserRespcode(item.RespCode.String)
// 		if itemDTO.RespCode {
// 			numberSuccess++
// 		} else {
// 			numberFail++
// 		}
// 		itemDTO.RespText = item.RespText.String
// 		itemDTO.TransactionDate = item.TransactionDate
// 		itemDTO.RefNumber = u.parserISOToReferentNumber(iso, refNumberIndex, refNumberLength)
// 		transtions[index] = itemDTO
// 		isAppend := u.isAppendValueFilter(filters, itemDTO.RespText)
// 		if isAppend {
// 			itemFilter := &dto.TransMonitoringFilterDTO{
// 				Text:  itemDTO.RespText,
// 				Value: itemDTO.RespText,
// 			}
// 			filters = append(filters, itemFilter)
// 		}
// 	}
// 	resp = &dto.GetTransactionMonitoringDTO{}
// 	resp.Transaction = transtions
// 	resp.Filters = filters
// 	resp.Fail = uint32(numberFail)
// 	resp.Success = uint32(numberSuccess)
// 	resp.Total = uint32(totalTransaction)
// 	return resp, nil
// }

// func (u *monitoringUsecase) GetListPhysicalChannel(ctx context.Context,
// 	req *dto.GetListPhysicalChannelRequestDTO) (listPhyChannel []*dto.PhysicalChannelDTO, err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/GetListPhysicalChannel", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		modelListPhyChannel []*model.PhysicalChannel
// 	)
// 	cacheListChannelKey := fmt.Sprintf("list-physical-channel-%v", req.ServerID)
// 	valueInterface, err := u.cacheHelper.GetInterface(ctx, cacheListChannelKey, modelListPhyChannel)
// 	if err != nil {
// 		modelListPhyChannel, err = u.monitoringRepository.GetListPhysicalChannel(ctx, req.ServerID)
// 		if err != nil {
// 			log.Logger.Errorf("%v - Error while get list physical channel: %v", req.TransactionID, err)
// 			return nil, err
// 		}
// 		err = u.cacheHelper.Set(ctx, cacheListChannelKey, modelListPhyChannel,
// 			time.Duration(u.cfg.Monitoring.CacheTimeListChannel)*time.Second)
// 		if err != nil {
// 			log.Logger.Warnf("%v - Error while set cache list physical channel, err: %v", req.TransactionID, err)
// 		}
// 	} else {
// 		modelListPhyChannel = valueInterface.([]*model.PhysicalChannel)
// 	}

// 	listPhyChannel = make([]*dto.PhysicalChannelDTO, len(modelListPhyChannel))
// 	for index, item := range modelListPhyChannel {
// 		itemDTO := &dto.PhysicalChannelDTO{}
// 		itemDTO.ID = item.ID
// 		itemDTO.ServerName = item.ServerName
// 		itemDTO.ServerOID = item.ServerOID
// 		itemDTO.ChannelID = item.ChannelID
// 		itemDTO.LastChanged = item.LastChanged
// 		itemDTO.LastChangeRecord = item.LastChangeRecord
// 		itemDTO.LastConfirmed = item.LastConfirmed
// 		itemDTO.ApplStatus = item.ApplStatus.String
// 		itemDTO.TrptStatus = item.TrptStatus.String
// 		listPhyChannel[index] = itemDTO
// 	}
// 	return listPhyChannel, nil
// }

// func (u *monitoringUsecase) GetListRedis(ctx context.Context,
// 	req *dto.GetListRedisRequestDTO) (respDTO *dto.ManagementRedis, err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/GetListRedis", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	var (
// 		keysTemp []string
// 		isGet    bool // continue action get list redis by pattern
// 		countR   uint32
// 	)
// 	respDTO = &dto.ManagementRedis{}
// 	countR = req.Count
// 	if countR > u.cfg.ManagementRedis.MaxCount || countR == 0 {
// 		log.Logger.Infof("%v - Set count default value", req.TransactionID)
// 		countR = u.cfg.ManagementRedis.DefaultCount
// 	}

// 	pattern := req.Pattern
// 	if len(pattern) == 1 && pattern == "*" {
// 		log.Logger.Errorf("%v - Pattern need more, not *: %v", req.TransactionID, err)
// 		return nil, errors.New(common.ReasonRedisPatternIncorrect.Code())
// 	}

// 	keysTemp, cursor, err := u.cacheHelper.GetKeysByPattern(ctx, pattern, uint64(req.Cursor), int64(countR))
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while get list redis: %v", req.TransactionID, err)
// 		return nil, errors.New(common.ReasonCacheError.Code())
// 	}
// 	isGet = true
// 	if cursor == 0 {
// 		log.Logger.Infof("%v - Cursor = 0 so that cannot be request", req.TransactionID)
// 		isGet = false
// 	}
// 	if len(keysTemp) == 0 && !isGet {
// 		log.Logger.Infof("%v - Get list redis not found", req.TransactionID)
// 		return nil, errors.New(common.ReasonNotFound.Code())
// 	}
// 	respDTO.Redises = make([]*dto.RedisInfo, len(keysTemp))
// 	for i, key := range keysTemp {
// 		itemRedis := &dto.RedisInfo{}
// 		itemRedis.Key = key
// 		respDTO.Redises[i] = itemRedis
// 	}
// 	respDTO.IsGet = isGet
// 	respDTO.Cursor = uint32(cursor)
// 	respDTO.Count = (countR)
// 	return respDTO, nil
// }

// func (u *monitoringUsecase) getSizeOfKey(in, transactionID string) int64 {
// 	log.Logger.Infof("%v - Split size in debugObject", transactionID)
// 	slitDObject := strings.SplitAfter(in, "serializedlength:")
// 	if len(slitDObject) < 2 {
// 		log.Logger.Errorf("%v - Split size in debugObject failure", transactionID)
// 		return 0
// 	}
// 	sizeKey := strings.Split(slitDObject[1], " ")
// 	if len(sizeKey) == 0 {
// 		log.Logger.Errorf("%v - Split size in debugObject failure", transactionID)
// 		return 0
// 	}
// 	strSize := sizeKey[0]
// 	endsize := util.ConvertStringToInt64(strSize)
// 	return endsize
// }

// func (u *monitoringUsecase) GetRedisDetail(ctx context.Context,
// 	req *dto.GetRedisDetailRequestDTO) (resp dto.GetRedisDetailResponseDTO, err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/GetRedisDetail", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	if req.Key == "" {
// 		log.Logger.Errorf("%v - Key redis not empty", req.TransactionID)
// 		return resp, errors.New("key invalid")
// 	}
// 	timeSecond, err := u.cacheHelper.TimeExpire(ctx, req.Key)
// 	typeOfKey, err := u.cacheHelper.GetType(ctx, req.Key)
// 	if typeOfKey == "string" {
// 		lengthOfKey, _ := u.cacheHelper.GetStrLenght(ctx, req.Key)
// 		resp.Lenght = lengthOfKey
// 	}
// 	property, err := u.cacheHelper.DebugObjectByKey(ctx, req.Key)
// 	resp.SizeData = u.getSizeOfKey(property, req.TransactionID)
// 	resp.Type = typeOfKey
// 	resp.TimeExpire = int64(timeSecond)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while get detail", req.TransactionID)
// 		return resp, errors.New(common.ReasonRedisNil.Code())
// 	}
// 	return resp, nil
// }

// func (u *monitoringUsecase) CheckKeyExists(ctx context.Context, req *dto.CheckKeyExistsRequestDTO) (isExists bool, err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/CheckKeyExists", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	isExists = false
// 	err = u.cacheHelper.Exists(ctx, req.Key)
// 	if err == redis.Nil {
// 		return isExists, nil
// 	}
// 	isExists = true
// 	return isExists, nil
// }

// func (u *monitoringUsecase) AddNewRedis(ctx context.Context, req *dto.AddNewRedisRequestDTO) (err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/AddNewRedis", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	expiration := req.Expiration
// 	if expiration == 0 {
// 		expiration = u.cfg.ManagementRedis.DefaultExpiration
// 	}
// 	err = u.cacheHelper.Set(ctx, req.Key, req.Value, time.Duration(expiration)*time.Second)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while add new redis by key", req.TransactionID)
// 		return errors.New(common.ReasonCacheError.Code())
// 	}
// 	return nil
// }

// func (u *monitoringUsecase) SetTimeout(ctx context.Context, req *dto.SetTimeoutRequestDTO) (err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/SetTimeout", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	expiration := req.Expiration
// 	if expiration == 0 {
// 		expiration = u.cfg.ManagementRedis.DefaultExpiration
// 	}
// 	err = u.cacheHelper.Expire(ctx, req.Key, time.Duration(expiration)*time.Second)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while set timeout redis by key", req.TransactionID)
// 		return errors.New(common.ReasonCacheError.Code())
// 	}
// 	return nil
// }

// func (u *monitoringUsecase) DeleteRedisByKey(ctx context.Context, req *dto.DeleteRedisByKeyRequestDTO) (err error) {
// 	span := jaeger.Start(ctx, ">usecase.monitoringUsecase/DeleteRedisByKey", ext.SpanKindRPCClient)
// 	defer func() {
// 		jaeger.Finish(span, err)
// 	}()
// 	err = u.cacheHelper.Del(ctx, req.Key)
// 	if err != nil {
// 		log.Logger.Errorf("%v - Error while delete redis by key", req.TransactionID)
// 		return errors.New(common.ReasonCacheError.Code())
// 	}
// 	return nil
// }
