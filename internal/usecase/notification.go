package usecase

import (
	"context"
	"fmt"
	"shop-back-cms/config"
	"shop-back-cms/internal/adapter"
	"shop-back-cms/internal/dto"
	funcLogger "shop-back-cms/internal/logger"
)

type (
	NotificationUsecase interface {
		SendSMS(ctx context.Context, req *dto.SendSMSRequestDTO) (*dto.SendSMSResponseDTO, error)
	}

	notificationUsecase struct {
		cfg                 *config.Config
		notificationAdapter adapter.NotificationAdapter
	}
)

func NewNotificationUsecase(
	cfg *config.Config,
	notificationAdapter adapter.NotificationAdapter,
) NotificationUsecase {
	return &notificationUsecase{
		cfg:                 cfg,
		notificationAdapter: notificationAdapter,
	}
}

func (u *notificationUsecase) SendSMS(ctx context.Context,
	req *dto.SendSMSRequestDTO) (resp *dto.SendSMSResponseDTO, err error) {
	for i := 0; i < 100; i++ {
		funcLogger.FuncLogger.WriteToKafka(ctx)
	}
	_ = u.notificationAdapter.SendEmail(ctx, fmt.Sprintf("%v-%v", 11, req.Messages))
	resp = &dto.SendSMSResponseDTO{}
	return resp, nil
}
