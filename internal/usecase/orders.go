package usecase

import (
	"context"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
	"shop-back-cms/internal/util"
	"time"
)

type (
	// OrderUsecase declare all func in order usecase
	OrderUsecase interface {
		GetListOrder(ctx context.Context, req *dto.GetListOrderRequestDTO) (*dto.GetListOrderResponseDTO, error)
		PostNewCart(ctx context.Context, req *dto.CartRequestDTO) error
		PostNewCartItem(ctx context.Context, req *dto.CartItemRequestDTO) error
		PostNewOrder(ctx context.Context, req *dto.OrderRequestDTO) error
		PostNewOrderItem(ctx context.Context, req *dto.OrderItemRequestDTO) error
		GetCartItemDetail(ctx context.Context, req *dto.GetCartItemDetailRequestDTO) (*dto.GetCartItemDetailResponseDTO, error)
	}
	orderUsecase struct {
		cfg             *config.Config
		orderRepository repository.OrderRepository
		helperRedis     cache.CacheHelper
		modelCopier     helper.ModelCopier
	}
)

// NewOrderUsecase create instance Comment Usecase
func NewOrderUsecase(
	cfg *config.Config,
	orderRepository repository.OrderRepository,
	helperRedis cache.CacheHelper,
	modelCopier helper.ModelCopier,
) OrderUsecase {
	return &orderUsecase{
		cfg:             cfg,
		orderRepository: orderRepository,
		helperRedis:     helperRedis,
		modelCopier:     modelCopier,
	}
}

func (u *orderUsecase) GetListOrder(ctx context.Context,
	req *dto.GetListOrderRequestDTO) (resp *dto.GetListOrderResponseDTO, err error) {
	var (
		modelOrder []model.Order
		total      int64
		price      int64
	)
	userID := util.GetUserIDFromCtx(ctx)
	if userID == 0 {
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	resp = &dto.GetListOrderResponseDTO{}
	keyCache := fmt.Sprintf(common.GetListOrder, userID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, resp)
	if err == nil && resInter != nil {
		logger.GlobaLogger.Infof("%v: Get list order data in redis.", req.TransactionID)
		resp = resInter.(*dto.GetListOrderResponseDTO)
		return resp, nil
	}
	reqModel := &model.GetListOrderRequest{}
	u.modelCopier.CopyFromModel(reqModel, req)
	reqModel.UserID = userID
	modelOrder, err = u.orderRepository.GetListOrder(ctx, reqModel)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while  Get list order data: err = %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	resp.Orders = make([]*dto.Order, len(modelOrder))
	for i, item := range modelOrder {
		calItem := &dto.Order{
			ProductName: item.ProductName,
			Price:       item.Price,
			SellPrice:   item.SellPrice,
			Quantity:    int64(item.Quantity),
		}
		resp.Orders[i] = calItem

		total += item.Quantity
		tempPrice := item.Price
		if item.SellPrice != 0 {
			tempPrice = item.SellPrice
		}
		price += item.Quantity * tempPrice
	}
	resp.Price = price
	resp.Total = uint32(total)
	u.helperRedis.Set(ctx, keyCache, resp, time.Duration(u.cfg.RedisTimeDuration.GetListOrder)*time.Second)
	return resp, err
}

func (u *orderUsecase) GetCartItemDetail(ctx context.Context,
	req *dto.GetCartItemDetailRequestDTO) (resp *dto.GetCartItemDetailResponseDTO, err error) {
	var (
		modelCardItem *model.CartItemModel
	)
	resp = &dto.GetCartItemDetailResponseDTO{}
	keyCache := fmt.Sprintf(common.GetCardItemDetail, req.CartItemID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, resp)
	if err == nil && resInter != nil {
		logger.GlobaLogger.Infof("%v: Get card item detail in redis.", req.TransactionID)
		resp = resInter.(*dto.GetCartItemDetailResponseDTO)
		return resp, nil
	}
	modelCardItem, err = u.orderRepository.GetCartItemDetail(ctx, req.CartItemID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get card item detail: err = %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	u.modelCopier.CopyFromModel(&resp.CartItemDetail, modelCardItem)
	price := modelCardItem.Price
	if modelCardItem.SellPrice != 0 {
		price = modelCardItem.SellPrice
	}
	amount := int64(modelCardItem.Quantity) * price
	resp.CartItemDetail.Amount = amount
	u.helperRedis.Set(ctx, keyCache, resp, time.Duration(u.cfg.RedisTimeDuration.GetCardItemDetail)*time.Second)
	return resp, err
}

func (u *orderUsecase) getCartId(ctx context.Context, transaction string) (uint32, error) {
	userID := util.GetUserIDFromCtx(ctx)
	if userID == 0 {
		return 0, errors.New(common.ReasonNotFound.Code())
	}
	cardID, isFound, err := u.orderRepository.GetCartIDByUserID(ctx, userID)
	if err != nil {
		return 0, errors.New(common.ReasonDBError.Code())
	}
	if !isFound {
		reqModel := &model.CartModel{
			UserId: userID,
		}
		cardID, err = u.orderRepository.PostNewCart(ctx, reqModel)
		if err != nil {
			logger.GlobaLogger.Errorf("%v, usecase.order/getCardId:  Error while add new card item", transaction, err)
			return 0, errors.New(common.ReasonDBError.Code())
		}
	}
	return cardID, nil
}
func (u *orderUsecase) PostNewCartItem(ctx context.Context,
	req *dto.CartItemRequestDTO) (err error) {
	cartID, err := u.getCartId(ctx, req.TransactionID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.order/PostNewCartItem:  Error while get card id", req.TransactionID, err)
	}
	domain := util.GetUDomainFromCtx(ctx)
	reqModel := &model.CartItemModel{
		CartId:      cartID,
		ProductId:   req.ProductId,
		Quantity:    req.Quantity,
		Sku:         req.Sku,
		Description: req.Description,
		CreatedBy:   domain,
		Price:       req.Price,
		SellPrice:   req.SellPrice,
	}
	err = u.orderRepository.PostNewCartItem(ctx, reqModel)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.order/PostNewCartItem:  Error while add new card item", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return err
}

func (u *orderUsecase) PostNewCart(ctx context.Context,
	req *dto.CartRequestDTO) (err error) {
	reqModel := &model.CartModel{}
	_, err = u.orderRepository.PostNewCart(ctx, reqModel)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.order/PostNewCart:  Error while add new cart", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return err
}

func (u *orderUsecase) PostNewOrder(ctx context.Context,
	req *dto.OrderRequestDTO) (err error) {
	reqModel := &model.OrderModel{}
	_, err = u.orderRepository.PostNewOrder(ctx, reqModel)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.order/PostNewOrder:  Error while add new order", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return err
}

func (u *orderUsecase) getOrderId(ctx context.Context, transaction string) (uint32, error) {
	userID := util.GetUserIDFromCtx(ctx)
	if userID == 0 {
		userID = 7
		// return 0, errors.New(common.ReasonNotFound.Code())
	}
	orderID, isFound, err := u.orderRepository.GetOrderIDByUserID(ctx, userID)
	if err != nil {
		return 0, errors.New(common.ReasonDBError.Code())
	}
	if !isFound {
		reqModel := &model.OrderModel{
			UserId: userID,
		}
		orderID, err = u.orderRepository.PostNewOrder(ctx, reqModel)
		if err != nil {
			logger.GlobaLogger.Errorf("%v, usecase.order:  Error while add new card item or order id", transaction, err)
			return 0, errors.New(common.ReasonDBError.Code())
		}
	}
	return orderID, nil
}

func (u *orderUsecase) PostNewOrderItem(ctx context.Context,
	req *dto.OrderItemRequestDTO) (err error) {
	reqModel := &model.OrderItemModel{}
	orderID, err := u.getOrderId(ctx, req.TransactionID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.order/PostNewCartItem:  Error while get card id", req.TransactionID, err)
		return err
	}
	domain := util.GetUDomainFromCtx(ctx)
	if domain == "" {
		domain = "ducnp"
		// logger.GlobaLogger.Errorf("%v, usecase.order/PostNewOrderItem:  Error while get domain into context", req.TransactionID, err)
		// return errors.New(common.ReasonNotFound.Code())
	}

	u.modelCopier.CopyFromModel(reqModel, req)
	reqModel.CreatedBy = domain
	reqModel.OrderId = orderID
	err = u.orderRepository.PostNewOrderItem(ctx, reqModel)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.order/PostNewOrderItem:  Error while add new order item", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	// push socket

	return err
}
