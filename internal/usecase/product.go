package usecase

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
	"shop-back-cms/internal/util"
	"strings"
	"time"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

type (
	// ProductUsecase declare all func in product usecase
	ProductUsecase interface {
		GetListProduct(ctx context.Context, req *dto.GetListProductRequestDTO) (res *dto.GetListProductResponseDTO, err error)
		SearchProduct(ctx context.Context, req *dto.SearchProductRequestDTO) (res *dto.GetListProductResponseDTO, err error)
		GetProductDetail(ctx context.Context, req *dto.GetProductDetailRequestDTO) (res *dto.GetProductDetailResponseDTO, err error)
		PostNewProduct(ctx context.Context, req *dto.PostNewProductRequestDTO) (res *dto.PostNewProductResponseDTO, err error)
		PutUpdateProduct(ctx context.Context, req *dto.PutUpdateProductRequestDTO) (res *dto.PutUpdateProductResponseDTO, err error)
		DeleteProduct(ctx context.Context, req *dto.DeleteProductRequestDTO) (res *dto.DeleteProductResponseDTO, err error)
		DoActiveProduct(ctx context.Context, req *dto.DoActiveProductRequestDTO) error
		ProductCheckSku(ctx context.Context, req *dto.ProductCheckSkuRequestDTO) error
		ProductQuantity(ctx context.Context, req *dto.ProductQuantityRequestDTO) error
		// intruction
		GetListIntruction(ctx context.Context, req *dto.GetListIntructionRequestDTO) (res *dto.GetListIntructionResponseDTO, err error)
		PostNewIntruction(ctx context.Context, req []string, transactionID string) (res *dto.PostNewIntructionResponseDTO, err error)
		DeleteIntruction(ctx context.Context, req *dto.DeleteIntructionRequestDTO) (res *dto.DeleteIntructionResponseDTO, err error)
		DoActiveIntruction(ctx context.Context, req *dto.DoActiveIntructionRequestDTO) error
		PutUpdateIntruction(ctx context.Context, req *dto.PutUpdateIntructionRequestDTO) (res *dto.PutUpdateIntructionResponseDTO, err error)

		// product client
		GetProductPreferentail(ctx context.Context,
			req *dto.GetProductPreferentailClientRequestDTO) (res []*dto.ProductPreferentailClientResponseDTO, err error)
		GetProductTrending(ctx context.Context,
			req *dto.GetProductTrendingClientRequestDTO) (res *dto.ProductTrendingClientResponseDTO, err error)
		GetProductDetailClient(ctx context.Context, req *dto.GetProductDetailClientRequestDTO) (res *dto.GetProductDetailClientResponseDTO, err error)
	}
	productUsecase struct {
		cfg               *config.Config
		productRepository repository.ProductRepository
		helperRedis       cache.CacheHelper
		modelCopier       helper.ModelCopier
	}
)

// NewProductUsecase create instance Comment Usecase
func NewProductUsecase(
	cfg *config.Config,
	productRepository repository.ProductRepository,
	helperRedis cache.CacheHelper,
	modelCopier helper.ModelCopier,
) ProductUsecase {
	return &productUsecase{
		cfg:               cfg,
		productRepository: productRepository,
		helperRedis:       helperRedis,
		modelCopier:       modelCopier,
	}
}
func (u *productUsecase) genProductSku(ctx context.Context) string {
	for {
		isCheck := true
		sku := util.GenProductSKU(16)
		isFound, err := u.productRepository.CheckSku(ctx, sku)
		if err != nil {
			isCheck = false
		}
		if isFound {
			isCheck = false
		}
		if isCheck {
			return sku
		}
	}
}
func (u *productUsecase) ProductQuantity(ctx context.Context, req *dto.ProductQuantityRequestDTO) error {
	err := u.productRepository.ProductQuantity(ctx, req.Number, req.ProductId, req.Type)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.product/ProductQuantity: error check product sku, err = %v", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (u *productUsecase) ProductCheckSku(ctx context.Context, req *dto.ProductCheckSkuRequestDTO) error {
	isFound, err := u.productRepository.CheckSku(ctx, req.Sku)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.product/ProductCheckSku: error check product sku, err = %v", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	if !isFound {
		logger.GlobaLogger.Errorf("%v, usecase.product/ProductCheckSku: not found sku", req.TransactionID)
		return errors.New(common.ReasonNotFound.Code())
	}
	return nil
}

func (u *productUsecase) PostNewProduct(ctx context.Context,
	req *dto.PostNewProductRequestDTO) (res *dto.PostNewProductResponseDTO, err error) {
	productModel := model.Product{}
	//	 todo get user action in context
	if req.Name == "" {
		logger.GlobaLogger.Errorf("%v, usecase.product/PostNewProduct: Filed name not empty", req.TransactionID)
		return nil, errors.New(common.ReasonGeneralError.Code())
	}
	sku := strings.TrimSpace(req.Sku)
	if sku != "" {
		isFound, err := u.productRepository.CheckSku(ctx, sku)
		if err != nil {
			logger.GlobaLogger.Errorf("%v, usecase.product/PostNewProduct: error check product sku, err = %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		if isFound {
			logger.GlobaLogger.Errorf("%v, usecase.product/PostNewProduct: sku exists", req.TransactionID)
			return nil, errors.New(common.ReasonNotFound.Code())
		}
	}
	if sku == "" {
		logger.GlobaLogger.Infof("%v, usecase.product/PostNewProduct: gen sku", req.TransactionID)
		sku = u.genProductSku(ctx)
	}
	req.Sku = sku

	u.modelCopier.CopyToModel(&productModel, &req)

	err = u.productRepository.PostNewProduct(ctx, req, productModel)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.product/PostNewProduct:  Error while add new product", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	return res, err
}

func (u *productUsecase) GetListProduct(ctx context.Context,
	req *dto.GetListProductRequestDTO) (res *dto.GetListProductResponseDTO, err error) {
	var (
		modelProduct = []model.Product{}
	)
	res = &dto.GetListProductResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Product.CacheKeyListProduct, req.CurrentPage)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelProduct)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v: Get list Product in redis.", req.TransactionID)
		modelProduct = valueInterface.([]model.Product)
	} else {
		isPaging := false
		if req.CurrentPage > 0 {
			logger.GlobaLogger.Infof("%v: Get list with page = %v", req.TransactionID, req.CurrentPage)
			isPaging = true
			req.Offset = (req.CurrentPage - 1) * req.Limit
		}
		modelProduct, err = u.productRepository.GetListProduct(ctx, req, isPaging)
		if err != nil {
			logger.GlobaLogger.Errorf("%v: Error while get list product: err = %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		if len(modelProduct) == 0 {
			logger.GlobaLogger.Infof("%v: Get list not found.", req.TransactionID)
			return nil, errors.New(common.ReasonNotFound.Code())
		}
		u.helperRedis.SetInterface(ctx, keyCache, modelProduct, time.Duration(u.cfg.Product.CacheTimeListProduct)*time.Second)
	}
	res.Products = make([]dto.ProductsDTO, len(modelProduct))
	for i, product := range modelProduct {
		item := &dto.ProductsDTO{
			Name:        product.Name,
			Description: product.Description,
			Tag:         product.Tag,
			// CreatedAt:       product.CreatedAt,
			Price:     product.Price,
			SellPrice: product.SellPrice,
			IsActive:  product.IsActive == "1",
			// Id:              product.ID,
			Quantity:        product.Quantity,
			Sku:             product.Sku,
			Vote:            product.Vote,
			StatusOfProduct: product.StatusOfProduct,
		}
		res.Products[i] = *item
	}
	return res, nil
}

func (u *productUsecase) SearchProduct(ctx context.Context,
	req *dto.SearchProductRequestDTO) (res *dto.GetListProductResponseDTO, err error) {
	var (
		modelProduct = []*model.Product{}
	)
	res = &dto.GetListProductResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Product.CacheKeySearchProduct, req.Name)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelProduct)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Search Product in redis.", req.TransactionID)
		modelProduct = valueInterface.([]*model.Product)
		u.modelCopier.CopyFromModel(&res.Products, modelProduct)
		return res, nil
	}
	modelProduct, err = u.productRepository.SearchProduct(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get list product: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.helperRedis.SetInterface(ctx, keyCache, modelProduct, time.Duration(u.cfg.Product.CacheTimeSearchProduct)*time.Second)
	u.modelCopier.CopyFromModel(&res.Products, modelProduct)
	return res, nil
}

func (u *productUsecase) GetProductDetail(ctx context.Context,
	req *dto.GetProductDetailRequestDTO) (res *dto.GetProductDetailResponseDTO, err error) {
	var (
		moProduct *model.Product
	)
	res = &dto.GetProductDetailResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Product.CacheKeyDetailProduct, req.Id)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, res)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get detail product in redis.", req.TransactionID)
		res = resInter.(*dto.GetProductDetailResponseDTO)
		return res, nil
	}
	moProduct, err = u.productRepository.GetProductDetail(ctx, req.Id)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get detail product: err = %v", req.TransactionID, err)
		return nil, err
	}
	u.modelCopier.CopyFromModel(&res, moProduct)
	u.helperRedis.Set(ctx, keyCache, res, time.Duration(u.cfg.Product.CacheTimeDetailProduct)*time.Second)
	return res, nil
}

func (u *productUsecase) PutUpdateProduct(ctx context.Context,
	req *dto.PutUpdateProductRequestDTO) (res *dto.PutUpdateProductResponseDTO, err error) {
	err = u.productRepository.PutUpdateProduct(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while update product: err = %v", req.TransactionID, err)
		return nil, err
	}
	return res, nil
}

func (u *productUsecase) DeleteProduct(ctx context.Context,
	req *dto.DeleteProductRequestDTO) (res *dto.DeleteProductResponseDTO, err error) {
	if req.ID == 0 {
		logger.GlobaLogger.Errorf("%v,usecase.product/DeleteProduct: Filed ID = 0 invalid", req.TransactionID)
		// TODO reason
		return nil, errors.New("common.ReasonProductFieldIDInvalid.Code()")
	}
	err = u.productRepository.DeleteProduct(ctx, req.ID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.product/DeleteProduct: Error while add new product, err=%v", req.TransactionID, err)
		return nil, err
	}
	res = &dto.DeleteProductResponseDTO{}
	return res, nil
}

func (u *productUsecase) DoActiveProduct(ctx context.Context,
	req *dto.DoActiveProductRequestDTO) (err error) {
	isActive := uint32(1)
	if !req.IsActive {
		isActive = uint32(0)
	}
	err = u.productRepository.DoActiveProduct(ctx, req.ProductID, isActive)
	if err != nil {
		logger.GlobaLogger.Errorf("%v,usecase.productUsecase/DoActiveProduct: Error while active product, %v", req.TransactionID, err)
		return err
	}
	return nil
}

/*
	intruction
*/

func (u *productUsecase) PostNewIntruction(ctx context.Context,
	arrdescription []string, transactionID string) (res *dto.PostNewIntructionResponseDTO, err error) {
	if len(arrdescription) == 0 {
		logger.GlobaLogger.Errorf("%v,usecase.product/PostNewIntruction: list empty", transactionID)
		return nil, errors.New(common.ReasonInvalidArgument.Code())
	}
	err = u.productRepository.PostNewIntruction(ctx, arrdescription)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.product/PostNewIntruction:  Error while add new intruction", transactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	return res, err
}

func (u *productUsecase) GetListIntruction(ctx context.Context,
	req *dto.GetListIntructionRequestDTO) (res *dto.GetListIntructionResponseDTO, err error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "productUsecase.GetListIntruction", ext.SpanKindRPCClient)
	defer span.Finish()

	var (
		modelIntruction = []*model.IntructionModel{}
	)
	res = &dto.GetListIntructionResponseDTO{}
	keyCache := fmt.Sprintf(common.ListIntruction, req.CurrentPage, req.Limit)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelIntruction)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v: Get list Intruction in redis.", req.TransactionID)
		modelIntruction = valueInterface.([]*model.IntructionModel)
	} else {
		isPaging := false
		if req.CurrentPage > 0 {
			logger.GlobaLogger.Infof("%v: Get list with page = %v", req.TransactionID, req.CurrentPage)
			isPaging = true
			req.Offset = (req.CurrentPage - 1) * req.Limit
		}
		modelIntruction, err = u.productRepository.GetListIntruction(ctx, req, isPaging)
		if err != nil {
			logger.GlobaLogger.Errorf("%v: Error while get list intruction: err = %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		if len(modelIntruction) == 0 {
			logger.GlobaLogger.Infof("%v: Get list intruction not found.", req.TransactionID)
			return nil, errors.New(common.ReasonNotFound.Code())
		}
		u.helperRedis.SetInterface(ctx, keyCache, modelIntruction, time.Duration(120)*time.Second)
	}
	res.Intructions = make([]dto.IntructionsDTO, len(modelIntruction))
	for i, item := range modelIntruction {
		item := &dto.IntructionsDTO{
			Description: item.Description,
			Type:        item.Type.String,
			IsActive:    item.IsActive == "1",
			ID:          item.ID,
		}
		res.Intructions[i] = *item
	}
	return res, nil
}

func (u *productUsecase) DeleteIntruction(ctx context.Context,
	req *dto.DeleteIntructionRequestDTO) (res *dto.DeleteIntructionResponseDTO, err error) {
	if req.ID == 0 {
		logger.GlobaLogger.Errorf("%v,usecase.product/DeleteIntruction: Filed ID = 0 invalid", req.TransactionID)
		return nil, errors.New(common.ReasonInvalidArgument.Code())
	}
	err = u.productRepository.DeleteIntruction(ctx, req.ID)
	if err != nil || err == sql.ErrNoRows {
		logger.GlobaLogger.Errorf("%v,usecase.productUsecase/DoActiveIntruction: Error while delele intruction, %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	res = &dto.DeleteIntructionResponseDTO{
		StatusCode: common.ACCEPT,
	}
	return res, nil
}

func (u *productUsecase) DoActiveIntruction(ctx context.Context,
	req *dto.DoActiveIntructionRequestDTO) (err error) {
	isActive := uint32(1)
	if !req.IsActive {
		isActive = uint32(0)
	}
	err = u.productRepository.DoActiveIntruction(ctx, req.IntructionID, isActive)
	if err != nil || err == sql.ErrNoRows {
		logger.GlobaLogger.Errorf("%v,usecase.productUsecase/DoActiveIntruction: Error while active intruction, %v", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (u *productUsecase) PutUpdateIntruction(ctx context.Context,
	req *dto.PutUpdateIntructionRequestDTO) (res *dto.PutUpdateIntructionResponseDTO, err error) {
	if req.ID == 0 {
		logger.GlobaLogger.Errorf("%v,usecase.product/DeleteIntruction: Filed ID = 0 invalid", req.TransactionID)
		return nil, errors.New(common.ReasonInvalidArgument.Code())
	}
	err = u.productRepository.PutUpdateIntruction(ctx, req)
	if err != nil || err == sql.ErrNoRows {
		logger.GlobaLogger.Errorf("%v,usecase.productUsecase/DoActiveIntruction:Error while update intruction, %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	return res, nil
}
