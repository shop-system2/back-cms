package usecase

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/repository/model"
	"time"
)

// type (
// 	// ProductUsecase declare all func in product usecase
// 	ProductUsecase interface {
// 		GetProductPreferentail(ctx context.Context,
// 			req *dto.GetProductPreferentailClientRequestDTO) (res []*dto.ProductPreferentailClientResponseDTO, err error)
// 		GetProductTrending(ctx context.Context,
// 			req *dto.GetProductTrendingClientRequestDTO) (res *dto.ProductTrendingClientResponseDTO, err error)
// 		GetProductDetail(ctx context.Context, req *dto.GetProductDetailClientRequestDTO) (res *dto.GetProductDetailClientResponseDTO, err error)
// 	}
// 	productUsecase struct {
// 		cfg               *config.Config
// 		productRepository repository.ProductRepository
// 		helperRedis       cache.CacheHelper
// 		modelCopier       helper.ModelCopier
// 	}
// )

// // NewProductUsecase create instance Comment Usecase
// func NewProductUsecase(
// 	cfg *config.Config,
// 	productRepository repository.ProductRepository,
// 	helperRedis cache.CacheHelper,
// 	modelCopier helper.ModelCopier,
// ) ProductUsecase {
// 	return &productUsecase{
// 		cfg:               cfg,
// 		productRepository: productRepository,
// 		helperRedis:       helperRedis,
// 		modelCopier:       modelCopier,
// 	}
// }

func (u *productUsecase) GetProductTrending(ctx context.Context,
	req *dto.GetProductTrendingClientRequestDTO) (resp *dto.ProductTrendingClientResponseDTO, err error) {
	var (
		modelProduct = []model.Product{}
	)
	keyCache := fmt.Sprintf(common.KeyCacheGetProductTrending)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelProduct)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v,usecase.ProductUsecase client/GetProductTrending get in Redis", req.TransactionID)
		modelProduct = valueInterface.([]model.Product)
	} else {
		reqModel := &model.ProductModelRequest{}
		u.modelCopier.CopyFromModel(reqModel, req)
		modelProduct, err = u.productRepository.GetProductTrending(ctx, reqModel)
		if err != nil {
			logger.GlobaLogger.Errorf("%v,usecase.ProductUsecase client/GetProductTrending: Error, %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		if len(modelProduct) == 0 {
			logger.GlobaLogger.Errorf("%v - Get product trending not found", req.TransactionID)
			return nil, errors.New(common.ReasonNotFound.Code())
		}
		u.helperRedis.SetInterface(ctx, keyCache, modelProduct,
			time.Duration(66)*time.Second)
	}
	resp = &dto.ProductTrendingClientResponseDTO{}
	resp.ProductTrending = make([]*dto.ProductClientDTO, len(modelProduct))
	for i, item := range modelProduct {
		tag := dto.ProductTag{
			New: item.Tag,
		}
		amount := dto.ProductAmount{
			Sell:  item.SellPrice,
			Price: item.Price,
		}
		resp.ProductTrending[i] = &dto.ProductClientDTO{
			Id:           item.ID,
			Content:      item.Description,
			Name:         item.Name,
			Tag:          tag,
			ImageID:      item.ImageID,
			Amount:       amount,
			CategoryName: item.CategoryName,
		}
	}
	return resp, nil
}

func (u *productUsecase) GetProductPreferentail(ctx context.Context,
	req *dto.GetProductPreferentailClientRequestDTO) (product []*dto.ProductPreferentailClientResponseDTO, err error) {
	var (
		modelProduct = []*model.ProductPreferentailModel{}
	)
	keyCache := fmt.Sprintf(common.KeyCacheGetProductPreferentail)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, modelProduct)
	if err == nil && valueInterface != nil {
		logger.GlobaLogger.Infof("%v,usecase.ProductUsecase client/GetProductPreferentail get in Redis", req.TransactionID)
		modelProduct = valueInterface.([]*model.ProductPreferentailModel)
	} else {
		modelProduct, err = u.productRepository.GetProductPreferentail(ctx, req)
		if err != nil {
			logger.GlobaLogger.Errorf("%v,usecase.ProductUsecase client/GetProductPreferentail: Error, %v", req.TransactionID, err)
			return nil, errors.New(common.ReasonDBError.Code())
		}
		if len(modelProduct) == 0 {
			logger.GlobaLogger.Errorf("%v - Get product Preferentail: not found", req.TransactionID)
			return nil, errors.New(common.ReasonNotFound.Code())
		}
		u.helperRedis.SetInterface(ctx, keyCache, modelProduct,
			time.Duration(66)*time.Second)
	}
	product = make([]*dto.ProductPreferentailClientResponseDTO, len(modelProduct))
	for i, item := range modelProduct {
		tag := dto.ProductPreferentailTag{
			New: item.Tag,
		}
		amount := dto.ProductPreferentailAmount{
			Sell:  item.SellPrice,
			Price: item.Price,
		}
		product[i] = &dto.ProductPreferentailClientResponseDTO{
			ID:               item.ID,
			Content:          item.Description,
			Name:             item.Name,
			Tag:              tag,
			ImageID:          item.ImageID,
			Amount:           amount,
			ShortDescription: item.ShortDescription.String,
		}
	}
	return product, nil
}

func (u *productUsecase) GetProductDetailClient(ctx context.Context,
	req *dto.GetProductDetailClientRequestDTO) (res *dto.GetProductDetailClientResponseDTO, err error) {
	var (
		moProduct *model.ProductClientModel
	)
	res = &dto.GetProductDetailClientResponseDTO{}
	keyCache := fmt.Sprintf(common.KeyCacheGetProductDetail, req.ID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, res)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get detail product in redis.", req.TransactionID)
		res = resInter.(*dto.GetProductDetailClientResponseDTO)
		return res, nil
	}
	moProduct, err = u.productRepository.GetProductDetailClient(ctx, req.ID)
	if err == sql.ErrNoRows {
		logger.GlobaLogger.Errorf("%v: Error while get detail product not found", req.TransactionID)
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while get detail product: err = %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	res = &dto.GetProductDetailClientResponseDTO{
		ProductDetail: dto.ProductDetail{
			ID:               moProduct.ID,
			Name:             moProduct.Name,
			Price:            moProduct.Price,
			Vote:             moProduct.Vote.String,
			CategoryName:     moProduct.CategoryName,
			Sku:              moProduct.Sku,
			SellPrice:        moProduct.SellPrice,
			ShortDescription: moProduct.ShortDescription.String,
			Quantity:         moProduct.Quantity,
			UpdatedAt:        moProduct.UpdatedAt,
			IsActive:         moProduct.IsActive == "1",
			CreatedAt:        moProduct.CreatedAt,
			CategoryID:       moProduct.CategoryID,
			Description:      moProduct.Description,
			CreatedBy:        moProduct.CreatedBy.String,
			UpdatedBy:        moProduct.UpdatedBy.String,
			StatusOfProduct:  moProduct.StatusOfProduct,
		},
	}
	u.helperRedis.Set(ctx, keyCache, res, time.Duration(120)*time.Second)
	return res, nil
}
