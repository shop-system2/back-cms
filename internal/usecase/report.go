package usecase

import (
	"bytes"

	"context"
	"encoding/base64"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"go-libs/logger"
	"reflect"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
	"shop-back-cms/internal/util"
	"strconv"
	"strings"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/go-redis/redis"
	"go.uber.org/zap"
)

const SizeProcess = 1000

type (
	// ReportUsecase declare all func in comment usecase
	ReportUsecase interface {
		GetListReport(ctx context.Context, req *dto.GetListReportRequestDTO) (res *dto.GetListReportResponsetDTO, err error)
		GetParametersByReportID(ctx context.Context, req *dto.GetParametersByReportIDRequestDTO) (res *dto.GetParametersByReportIDResponseDTO, err error)
		GetReport(ctx context.Context) (*dto.GetReportResponseDTO, error)
		GetReportParameters(ctx context.Context, req *dto.GetReportParametersRequestDTO) (*dto.GetReportParametersResponseDTO, error)
		GenerateReport(ctx context.Context, req *dto.GenerateReportRequestDTO) (*dto.GenerateReportResponseDTO, error)
		GetReportFile(ctx context.Context, req *dto.GetReportFileRequestDTO) (*dto.GetReportFileResponseDTO, error)
		GetReportFileFull(ctx context.Context, domain, transactionID string) (*dto.GetFileDTO, error)
		GenerateDataReportDetail(ctx context.Context,
			req *dto.GenerateReportRequestDTO) (*dto.GenerateReportResponseDTO, error)
	}
	reportUsecase struct {
		modelCopier      helper.ModelCopier
		reportRepository repository.ReportRepository
		helperRedis      cache.CacheHelper
		golibscache      cache.CacheHelper
	}
)

// NewReportUsecase create instance Comment Usecase
func NewReportUsecase(
	modelCopier helper.ModelCopier,
	reportRepository repository.ReportRepository,
	golibscache cache.CacheHelper,

) ReportUsecase {
	return &reportUsecase{
		reportRepository: reportRepository,
		modelCopier:      modelCopier,
		golibscache:      golibscache,
	}
}

func (u *reportUsecase) GetListReport(ctx context.Context, req *dto.GetListReportRequestDTO) (res *dto.GetListReportResponsetDTO, err error) {

	var (
		reports = []model.ReportModel{}
	)
	res = &dto.GetListReportResponsetDTO{}
	valueInterface, err := u.helperRedis.GetInterface(ctx, "get_list_report", reports)
	if err == nil {
		reports = valueInterface.([]model.ReportModel)
		res.StatusCode = "ACCEPT"
		u.modelCopier.CopyFromModel(&res.Reports, reports)
		return res, nil
	}
	reports, err = u.reportRepository.GetListReport(ctx, req)
	if err != nil {
		return nil, err
	}
	res.StatusCode = "ACCEPT"

	u.helperRedis.SetInterface(ctx, "get_list_report", reports, time.Duration(SizeProcess)*time.Second)
	u.golibscache.SetInterface(ctx, "testListReport", reports, time.Duration(SizeProcess)*time.Second)
	u.modelCopier.CopyFromModel(&res.Reports, reports)
	return res, nil
}

func (u *reportUsecase) GetParametersByReportID(ctx context.Context,
	req *dto.GetParametersByReportIDRequestDTO) (res *dto.GetParametersByReportIDResponseDTO, err error) {
	var (
		parameters = []model.ParamatersByReportIDModel{}
	)
	res = &dto.GetParametersByReportIDResponseDTO{}
	valueInterface, err := u.helperRedis.GetInterface(ctx, fmt.Sprintf("%v-%v", "get_parameters_by_report_id", req.ID), parameters)
	val1, errs := u.golibscache.GetInterface(ctx, "testListReport", parameters)
	if errs != nil {
		fmt.Println("value Test err:", errs)
	}
	if errs == nil {
		fmt.Println("value Test val1:", val1)
	}
	if err == nil {
		parameters = valueInterface.([]model.ParamatersByReportIDModel)
		res.StatusCode = "ACCEPT"
		u.modelCopier.CopyFromModel(&res.Parameters, parameters)
		return res, nil
	}
	parameters, err = u.reportRepository.GetParametersByReportID(ctx, req)
	if err != nil {
		return nil, err
	}
	res.StatusCode = "ACCEPT"
	u.helperRedis.SetInterface(ctx, fmt.Sprintf("%v-%v", "get_parameters_by_report_id", req.ID), parameters, time.Duration(SizeProcess)*time.Second)
	u.modelCopier.CopyFromModel(&res.Parameters, parameters)
	u.golibscache.SetInterface(ctx, "11111111111", parameters, time.Duration(SizeProcess)*time.Second)

	return res, nil
}

type ReportObjParamRequest struct {
	FileType      string
	FileName      string
	HeaderFile    []string
	DateDetail    interface{}
	Args          []interface{}
	Statement     string
	DomainUser    string
	TransactionID string
	ErrChan       chan error
	DoneChan      chan bool
}

func (u *reportUsecase) processLoadReportData(ctx context.Context, req *ReportObjParamRequest) {
	var reportFileData dto.FileDTO
	fileName := fmt.Sprintf("report-%v-%v", req.DomainUser, time.Now().Format("20060102-150405"))

	reqRes := dto.ReportResponseChannel{}
	reqRes.ProcessDoneChannel = make(chan bool)
	reqRes.ProcessAllDone = make(chan bool)
	// TODO SizeProcess -> config
	reqRes.DateDetailItem = make(chan []interface{}, SizeProcess)
	writeFileDone := make(chan bool, 1)
	go func() {
		errDB := u.reportRepository.LoadDataReportDetail(ctx, req.Statement, req.Args, &reqRes)
		if errDB != nil {
			reportFileData.Status = common.FAILED
			logger.GlobaLogger.Errorf("%v - Failed to get report detail from DB: %v", req.TransactionID, errDB)
			// todo config
			keyCacheFileReport := fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser, req.TransactionID)
			errSetCacheReport := u.helperRedis.Set(ctx, keyCacheFileReport, reportFileData,
				time.Duration(SizeProcess)*time.Second)
			if errSetCacheReport != nil {
				logger.GlobaLogger.Errorf("Error while set data in cache of report 11")
			}
			return
		}
	}()
	go func() {
		chunkCount := 0
		// TODO SizeProcess
		itemArr := make([]interface{}, SizeProcess)
		index := 0
		//todo
		commonKeyCache := fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser, req.TransactionID)
		for {
			select {
			case item, ok := <-reqRes.DateDetailItem:
				if ok && item != nil {
					itemArr[index] = item
					index++
				}
				if index == SizeProcess {
					chunkCount++
					logger.GlobaLogger.Debugf("%v - Full size of chunk %v with length %v", req.TransactionID, chunkCount, 1000)
					bytesOut, _ := json.Marshal(itemArr)
					chunkKeyCache := fmt.Sprintf("%v:%v", commonKeyCache, chunkCount)
					_ = u.helperRedis.Set(ctx, chunkKeyCache, bytesOut, time.Duration(3600)*time.Second)
					index = 0
				}
			case <-reqRes.ProcessAllDone:
				if index != 0 {
					itemArr = itemArr[:index]
					bytesOut, _ := json.Marshal(itemArr)
					chunkCount++
					logger.GlobaLogger.Debugf("%v - Last chunk %v with size: %v", req.TransactionID, chunkCount, len(itemArr))
					chunkKeyCache := fmt.Sprintf("%v:%v", commonKeyCache, chunkCount)
					_ = u.helperRedis.Set(ctx, chunkKeyCache, bytesOut, time.Duration(10)*time.Minute)
				}
				reportFileData.FileChunk = chunkCount
				writeFileDone <- true
				return
			}
		}
	}()
	<-writeFileDone
	reportFileData.FileName = fileName
	reportFileData.DomainUser = req.DomainUser
	reportFileData.TransactionID = req.TransactionID
	reportFileData.HeaderFile = req.HeaderFile
	reportFileData.TitleName = req.FileName
	reportFileData.Status = common.ACCEPT
	reportFileData.FileType = req.FileType
	logger.GlobaLogger.Debugf("%v - Cooldown process")
	time.Sleep(1 * time.Second)
	// TODO 1000
	errSetCacheReport := u.helperRedis.Set(ctx, fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser, req.TransactionID),
		reportFileData, time.Duration(1000)*time.Second)
	if errSetCacheReport != nil {
		logger.GlobaLogger.Errorf("Error while set data in cache of report")
	}
}

func (u *reportUsecase) handlerParam(sqlParams []string, parameters map[string]string) (args []interface{}) {
	if len(sqlParams) == 0 {
		return args
	}
	for _, param := range sqlParams {
		if parameters == nil || strings.EqualFold(parameters[param], "") {
			continue
		}
		if strings.Contains(parameters[param], ",") {
			args = append(args, strings.Split(parameters[param], ","))
			continue
		}
		args = append(args, parameters[param])
	}
	return args
}

func (u *reportUsecase) GenerateDataReportDetail(ctx context.Context, req *dto.GenerateReportRequestDTO) (
	res *dto.GenerateReportResponseDTO, err error) {
	var (
		reports    []*model.Report
		sqlParams  []string
		header     []string
		reportName string
		errDB      error
		statement  string
		args       []interface{}
	)

	res, err = u.checkGenerateReportAction(ctx, req)
	if err != nil {
		logger.GlobaLogger.Warnf("%v - Error while checking permission:%v", req.TransactionID, err)
		return res, nil
	}

	valueInterface, err := u.helperRedis.GetInterface(ctx, "back-cms:report", reports)
	if err != nil {
		reports, errDB = u.reportRepository.GetReport(ctx)
		if errDB != nil {
			logger.GlobaLogger.Errorf("Failed to get report from DB: ", zap.Error(errDB))
			return nil, errors.New(common.Reason0.Code())
		}
		_ = u.helperRedis.Set(ctx, "back-cms:report", reports,
			time.Duration(1000)*time.Second)
	} else {
		reports = valueInterface.([]*model.Report)
	}
	for _, report := range reports {
		if req.ID != report.ID {
			continue
		}
		reportName = report.Name
		statement = report.SQLValue
		sqlParams = strings.Split(report.SQLParameters, ",")
		header = report.Header
		break
	}

	args = u.handlerParam(sqlParams, req.Parameters)
	paramRequest := ReportObjParamRequest{}
	paramRequest.FileName = reportName
	paramRequest.HeaderFile = header
	paramRequest.Args = args
	paramRequest.Statement = statement
	paramRequest.DomainUser = req.DomainUser
	paramRequest.TransactionID = req.TransactionID
	paramRequest.FileType = req.FileType
	go u.processLoadReportData(ctx, &paramRequest)

	// return value after process
	reportFileData := model.ReportFile{Status: common.PROCESSING}
	_ = u.helperRedis.Set(ctx, fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser, req.TransactionID),
		reportFileData, time.Duration(1000)*time.Second)
	res = &dto.GenerateReportResponseDTO{
		ResponseBaseDTO: dto.ResponseBaseDTO{
			StatusCode:    common.PROCESSING,
			TransactionID: req.TransactionID,
		},
	}
	return res, nil
}

func (u *reportUsecase) getdataInRedis(ctx context.Context, req *dto.GetReportFileRequestDTO) (
	res *dto.GetReportFileResponseDTO) {
	var reportFileData model.ReportFile
	keyCacheFileReport := fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser, req.TransactionID)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCacheFileReport, reportFileData)
	if err != nil {
		res = &dto.GetReportFileResponseDTO{
			ResponseBaseDTO: dto.ResponseBaseDTO{
				StatusCode:    common.FAILED,
				ReasonCode:    common.ReasonNotFound.Code(),
				ReasonMessage: common.ReasonNotFound.Message(),
			},
		}
		return res
	}

	reportFileData = valueInterface.(model.ReportFile)
	if strings.EqualFold(reportFileData.Status, common.ACCEPT) {
		res = &dto.GetReportFileResponseDTO{
			ResponseBaseDTO: dto.ResponseBaseDTO{
				StatusCode: common.ACCEPT,
			},
			FileName: reportFileData.FileName,
		}
		return res
	}
	if strings.EqualFold(reportFileData.Status, common.FAILED) {
		res = &dto.GetReportFileResponseDTO{
			ResponseBaseDTO: dto.ResponseBaseDTO{
				StatusCode: common.FAILED,
			},
		}
		return res
	}
	res = &dto.GetReportFileResponseDTO{
		ResponseBaseDTO: dto.ResponseBaseDTO{
			StatusCode: common.PROCESSING,
		},
	}
	return res
}

func (u *reportUsecase) saveParticalFileExcel(ctx context.Context, req *dto.FileDTO,
	commonKeyCache, fileName string) (res *dto.GetFileDTO, err error) {
	res = &dto.GetFileDTO{}
	fileExcel := excelize.NewFile()
	style, errStyle := fileExcel.NewStyle(`{"font":{"bold":true,"size":15}}`)
	if errStyle != nil {
		logger.GlobaLogger.Warnf("%v - Error while set style: %v", req.TransactionID, errStyle)
	}
	req.Style = style
	streamWriter, errNewStreamWriter := fileExcel.NewStreamWriter("Sheet1")
	if errNewStreamWriter != nil {
		logger.GlobaLogger.Errorf("%v - Error while new stream writer: %v", req.TransactionID, errNewStreamWriter)
		return nil, errors.New(common.ReasonRedisNil.Code())
	}
	rowIndex := util.WriteTitleXlsxExcelize(streamWriter, req)
	rowIndex = util.WriteHeaderXlsxExcelize(streamWriter, req, rowIndex)

	var writeFileAllDoneChan = make(chan bool, 1)
	var writeFileErrChan = make(chan error, 1)
	var errs error
	go func() {
		var arrModel []interface{}
		for i := 1; i <= req.FileChunk; i++ {
			logger.GlobaLogger.Debugf("%v - Process %v chunk of total:%v", req.TransactionID, i, req.FileChunk)
			commonKeyCacheItme := fmt.Sprintf("%v:%v", commonKeyCache, i)
			valueItem, errRedisItem := u.helperRedis.GetInterface(ctx, commonKeyCacheItme, []byte{})
			if errRedisItem != nil {
				logger.GlobaLogger.Errorf("%v - Error while get data in redis by chunk: %v", req.TransactionID, errRedisItem)
				writeFileErrChan <- errors.New(common.ReasonRedisNil.Code())
			}
			rawData := valueItem.([]byte)
			errUnmarshal := json.Unmarshal(rawData, &arrModel)
			if errUnmarshal != nil {
				logger.GlobaLogger.Errorf("%v - Error while unmarshal data in redis by chunk: %v", req.TransactionID, errUnmarshal)
				err = errors.New(common.ReasonProcessErr.Code())
				writeFileErrChan <- err
			}
			rowIndexCurrent := rowIndex
			_, err = util.WriteValueXlsxExcelize(streamWriter, &arrModel, rowIndexCurrent)
			if err != nil {
				logger.GlobaLogger.Errorf("%v - Error while write data to file excel in loop chunk: %v", req.TransactionID, err)
				writeFileErrChan <- err
			}
			rowIndex += len(arrModel)
		}
		logger.GlobaLogger.Debugf("%v - CoolDown process before flush file", req.TransactionID)
		time.Sleep(10 * time.Second)
		logger.GlobaLogger.Debugf("%v - Start Flushing data to file", req.TransactionID)
		errFlushStream := streamWriter.Flush()
		if errFlushStream != nil {
			logger.GlobaLogger.Warnf("%v - Error while flush stream: %v", req.TransactionID, errFlushStream)
		}
		logger.GlobaLogger.Debugf("%v - End flushed data to file", req.TransactionID)
		writeFileAllDoneChan <- true
	}()
	go func() {
		err := <-writeFileErrChan
		if err != nil {
			errs = errors.New(common.ReasonProcessErr.Code())
			writeFileAllDoneChan <- true
		}
	}()
	<-writeFileAllDoneChan
	logger.GlobaLogger.Infof("%v - Done of exporting excel file", req.TransactionID)
	if errs != nil {
		res.StatusCode = common.FAILED
		return res, errors.New("err")
	}
	res.StatusCode = common.ACCEPT
	res.File = fileExcel
	res.FileName = fmt.Sprintf("%v.xlsx", fileName)
	return res, nil
}

func (u *reportUsecase) formatData(value reflect.Value) string {
	if !value.IsValid() {
		return ""
	}
	switch value.Interface().(type) {
	case string:
		return value.String()
	case float64:
		return strconv.FormatFloat(value.Float(), 'f', 3, 64)
	case int64:
		return fmt.Sprint(value.Int())
	default:
		return value.String()
	}
}

func (u *reportUsecase) saveParticalFileCsv(ctx context.Context, req *dto.FileDTO,
	commonKeyCache, fileName string) (res *dto.GetFileDTO, err error) {

	res = &dto.GetFileDTO{}
	csvBytes := &bytes.Buffer{}
	csvWrite := csv.NewWriter(csvBytes)
	_ = csvWrite.Write([]string{req.TitleName})
	_ = csvWrite.Write([]string{req.DomainUser})
	_ = csvWrite.Write(req.HeaderFile)
	_ = csvWrite.Write([]string{})

	var arrModel []interface{}
	for i := 1; i <= req.FileChunk; i++ {
		commonKeyCacheItme := fmt.Sprintf("%v:%v", commonKeyCache, i)
		valueItem, errRedisItem := u.helperRedis.GetInterface(ctx, commonKeyCacheItme, []byte{})
		if errRedisItem != nil {
			logger.GlobaLogger.Errorf("%v - Error while get data in redis by chunk: %v", req.TransactionID, errRedisItem)
			return nil, errors.New(common.ReasonRedisNil.Code())
		}
		rawData := valueItem.([]byte)
		errUnmarshal := json.Unmarshal(rawData, &arrModel)
		if errUnmarshal != nil {
			logger.GlobaLogger.Errorf("%v - Error while unmarshal data in redis by chunk: %v", req.TransactionID, errUnmarshal)
			return nil, errors.New(common.ReasonProcessErr.Code())
		}
		for _, v := range arrModel {
			valueOf := reflect.ValueOf(v)
			num := valueOf.Len()
			itemRecord := make([]string, num)
			for j := 0; j < num; j++ {
				field := valueOf.Index(j)
				eValue := field.Elem()
				formatValue := u.formatData(eValue)
				itemRecord[j] = formatValue
			}
			_ = csvWrite.Write(itemRecord)
		}
	}
	csvWrite.Flush()

	res.StatusCode = common.ACCEPT
	res.CSVBytes = csvBytes.Bytes()
	res.FileName = fmt.Sprintf("%v.csv", fileName)
	res.FileType = req.FileType
	return res, nil
}

func (u *reportUsecase) saveParticalFile(ctx context.Context, domain,
	transactionID string) (res *dto.GetFileDTO, err error) {

	res = &dto.GetFileDTO{}
	keyCache := fmt.Sprintf("cbo:report:build:%v", transactionID)
	defer func() {
		if inf := recover(); inf != nil {
			logger.GlobaLogger.Infof("%v - recover from panic, err: %v", transactionID, inf)
		}
	}()
	var (
		valueInterface interface{}
		dataFile       dto.FileDTO
		fileResponse   = &dto.GetReportFileResponseDTO{}
	)
	if fileResponse.FileName == "" {
		fileResponse.FileName = "report"
	}
	res.FileName = fileResponse.FileName

	fileResponse = u.getdataInRedis(ctx, &dto.GetReportFileRequestDTO{
		DomainUser:    domain,
		TransactionID: transactionID,
	})

	if fileResponse.ResponseBaseDTO.StatusCode == common.PROCESSING {
		logger.GlobaLogger.Warnf("%v - Data processing", transactionID)
		res.StatusCode = fileResponse.StatusCode
		return res, nil
	}

	if fileResponse.ResponseBaseDTO.StatusCode == common.FAILED {
		res.StatusCode = common.FAILED
		logger.GlobaLogger.Warnf("%v - Error while getting report file", transactionID)
		return res, nil
	}
	logger.GlobaLogger.Infof("%v - Start locking to process file", transactionID)
	isSet, err := u.helperRedis.SetNX(ctx, keyCache, "", time.Duration(100)*time.Minute)
	if err != nil || !isSet {
		res.File = nil
		res.StatusCode = common.PROCESSING
		return res, nil
	}

	commonKeyCache := fmt.Sprintf("back-cms:report-detail-%v:%v", domain, transactionID)
	valueInterface, err = u.helperRedis.GetInterface(ctx, commonKeyCache, dataFile)
	if err != nil {
		logger.GlobaLogger.Warnf("%v - Error while getting file report from redis: %v", transactionID, err)
		return nil, err
	}
	dataFile = valueInterface.(dto.FileDTO)
	if dataFile.FileType == "" {
		// TODO csv
		dataFile.FileType = "CSV"
	}

	if dataFile.FileType == common.FileCsv {
		return u.saveParticalFileCsv(ctx, &dataFile, commonKeyCache, fileResponse.FileName)
	}
	logger.GlobaLogger.Debugf("%v - Jumpping to creating file", transactionID)
	res, err = u.saveParticalFileExcel(ctx, &dataFile, commonKeyCache, fileResponse.FileName)
	if err != nil {
		logger.GlobaLogger.Warnf("%v - Error while saving file: %v", transactionID, err)
	}
	return res, err
}

// Process all
func (u *reportUsecase) GetReport(ctx context.Context) (
	res *dto.GetReportResponseDTO, err error) {
	var (
		reports []*model.Report
		errDB   error
	)
	valueInterface, err := u.helperRedis.GetInterface(ctx, "back-cms:report", reports)
	if err != nil {
		reports, errDB = u.reportRepository.GetReport(ctx)
		if errDB != nil {
			logger.GlobaLogger.Errorf("Failed to get report from DB: ", zap.Error(errDB))
			return nil, err
		}
		if err == redis.Nil {
			_ = u.helperRedis.Set(ctx, "back-cms:report", reports, time.Duration(1000)*time.Second)
		}
	} else {
		logger.GlobaLogger.Info("Get list report from cache")
		reports = valueInterface.([]*model.Report)
	}
	// list resources from cim
	result := make([]*model.Report, 0)

	// prepare request to cim to get user reports
	// cimCtx := util.AddCIMAuth(ctx)
	// cimReq := &cimClientPB.GetResourceByUserRequest{
	// 	ResourceSource: cimClientPB.ResourceSource_CBO,
	// 	ResourceType:   cimClientPB.ResourceType_REPORT,
	// }
	// cimResp, err := u.cimClient.GetResourcesByUser(cimCtx, cimReq)
	// if err != nil {
	// 	logger.GlobaLogger.Errorf("Error while getting report from cim: %v", err)
	// 	return nil, err
	// }

	// if cimResp.IsAdminDomain {
	// 	result = append(result, reports...)
	// 	res = &dto.GetReportResponseDTO{
	// 		ResponseBaseDTO: dto.ResponseBaseDTO{
	// 			StatusCode: common.ACCEPT,
	// 		},
	// 	}
	// 	u.modelConverter.FromModel(&res.Reports, result)
	// 	return res, nil
	// }

	// if cimResp.StatusCode != common.DONE {
	// 	logger.GlobaLogger.Warnf("Not found any report from cim: %v", errors.New(common.ReasonNotFound.Code()))
	// 	return nil, errors.New(common.ReasonNotFound.Code())
	// }

	// mapCIMReport := make(map[string]string)
	// for _, cimReport := range cimResp.Resources {
	// 	if _, ok := mapCIMReport[cimReport.Value]; !ok {
	// 		mapCIMReport[cimReport.Value] = cimReport.Value
	// 	}
	// }

	for _, currentReport := range reports {
		// if _, ok := mapCIMReport[strconv.FormatInt(currentReport.ID, 10)]; ok {
		result = append(result, currentReport)
		// }
	}

	res = &dto.GetReportResponseDTO{
		ResponseBaseDTO: dto.ResponseBaseDTO{
			StatusCode: common.ACCEPT,
		},
	}
	// u.modelConverter.FromModel(&res.Reports, result)
	return res, nil
}

func (u *reportUsecase) GetReportParameters(ctx context.Context, req *dto.GetReportParametersRequestDTO) (
	res *dto.GetReportParametersResponseDTO, err error) {
	var (
		reportParameters []*model.ReportParameter
		errDB            error
	)

	valueInterface, err := u.helperRedis.GetInterface(ctx, fmt.Sprintf("back-cms:report-parameters-%v", req.ID), reportParameters)
	if err != nil {
		reportParameters, errDB = u.reportRepository.GetReportParameters(ctx, req.ID)
		if errDB != nil {
			logger.GlobaLogger.Errorf("Failed to get report parameters from DB: ", zap.Error(errDB))
			return nil, errDB
		}
		if err == redis.Nil {
			_ = u.helperRedis.Set(ctx, fmt.Sprintf("back-cms:report-parameters-%v", req.ID), reportParameters,
				time.Duration(1000)*time.Second)
		}
	} else {
		reportParameters = valueInterface.([]*model.ReportParameter)
	}

	res = &dto.GetReportParametersResponseDTO{
		ResponseBaseDTO: dto.ResponseBaseDTO{
			StatusCode: common.ACCEPT,
		},
	}
	// u.modelConverter.FromModel(&res.Parameters, reportParameters)
	return res, nil
}

func (u *reportUsecase) checkGenerateReportAction(ctx context.Context,
	req *dto.GenerateReportRequestDTO) (res *dto.GenerateReportResponseDTO, err error) {
	// prepare request to cim to get user reports
	res = &dto.GenerateReportResponseDTO{
		ResponseBaseDTO: dto.ResponseBaseDTO{
			StatusCode:    common.PROCESSING,
			TransactionID: req.TransactionID,
		},
	}

	// if !u.cfg.ReportSetting.EnableCheckingPermission {
	// 	return res, nil
	// }

	// callCIMCtx := util.AddCIMAuth(ctx)
	// cimResp, err := u.cimClient.GetResourcesByUser(callCIMCtx, &cimClientPB.GetResourceByUserRequest{
	// 	ResourceSource: cimClientPB.ResourceSource_CBO,
	// 	ResourceType:   cimClientPB.ResourceType_REPORT,
	// })
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while getting report from cim: %v", req.TransactionID, err)
		res.StatusCode = common.FAILED
		res.ReasonCode = common.ParseError(err).Code()
		res.ReasonMessage = common.ParseError(err).Message()
		return res, err
	}
	// if cimResp.StatusCode != common.DONE {
	// 	logger.GlobaLogger.Warnf("%v - Not found any report from cim: %v", req.TransactionID, errors.New(common.ReasonNotFound.Code()))
	// 	res.StatusCode = common.FAILED
	// 	res.ReasonCode = cimResp.ReasonCode
	// 	res.ReasonMessage = cimResp.ReasonMessage
	// 	return res, errors.New(common.ReasonNotFound.Code())
	// }

	// list resources from cim
	// mapCIMReport := make(map[string]string)
	// for _, cimReport := range cimResp.Resources {
	// 	if _, ok := mapCIMReport[cimReport.Value]; !ok {
	// 		mapCIMReport[cimReport.Value] = cimReport.Value
	// 	}
	// }

	// if _, ok := mapCIMReport[strconv.FormatInt(req.ID, 10)]; !ok {
	// 	logger.GlobaLogger.Warnf("%v - Not found any report from cim: %v", req.TransactionID, errors.New(common.ReasonNotFound.Code()))
	// 	res.StatusCode = common.FAILED
	// 	res.ReasonCode = common.ReasonNotFound.Code()
	// 	res.ReasonMessage = common.ReasonNotFound.Message()
	// 	return res, errors.New(common.ReasonNotFound.Code())
	// }
	return res, nil
}

func (u *reportUsecase) GenerateReport(ctx context.Context, req *dto.GenerateReportRequestDTO) (
	res *dto.GenerateReportResponseDTO, err error) {
	var (
		args []interface{}
	)

	res, err = u.checkGenerateReportAction(ctx, req)
	if err != nil {
		logger.GlobaLogger.Warnf("%v - Error while checking permission:%v", req.TransactionID, err)
		return res, nil
	}

	go func() {
		var (
			reports      []*model.Report
			reportDetail interface{}
			sqlParams    []string
			// cardType     []string
			header     []string
			reportName string
			errDB      error
			statement  string
		)
		valueInterface, err := u.helperRedis.GetInterface(ctx, "back-cms:report", reports)
		if err != nil {
			reports, errDB = u.reportRepository.GetReport(ctx)
			if errDB != nil {
				logger.GlobaLogger.Errorf("Failed to get report from DB: ", zap.Error(errDB))
				return
			}
			if err == redis.Nil {
				_ = u.helperRedis.Set(ctx, "back-cms:report", reports, time.Duration(1000)*time.Second)
			}
		} else {
			reports = valueInterface.([]*model.Report)
		}

		for _, report := range reports {
			if req.ID != report.ID {
				continue
			}
			reportName = report.Name
			statement = report.SQLValue
			sqlParams = strings.Split(report.SQLParameters, ",")
			header = report.Header
			break

		}

		for _, param := range sqlParams {
			if req.Parameters == nil || strings.EqualFold(req.Parameters[param], "") {
				continue
			}

			if strings.Contains(req.Parameters[param], ",") {
				args = append(args, strings.Split(req.Parameters[param], ","))
			} else {
				args = append(args, req.Parameters[param])
			}
		}
		reportDetail, errDB = u.reportRepository.GetReportDetail(ctx, statement, args)
		if errDB != nil {
			logger.GlobaLogger.Errorf("Failed to get report detail from DB: ", zap.Error(errDB))
		}
		fileName := fmt.Sprintf("report-%v-%v.xlsx", req.DomainUser, time.Now().Format("20060102-150405"))
		base64File := util.ExportToXLSX(ctx, reportName, req.DomainUser, header, reportDetail)

		if strings.EqualFold(base64File, "") {
			return
		}

		var reportFileData dto.FileDTO
		reportFileData.FileName = fileName
		reportFileData.Base64File = base64File
		reportFileData.Status = common.ACCEPT

		_ = u.helperRedis.Set(ctx, fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser, req.TransactionID),
			reportFileData, time.Duration(1000)*time.Second)
	}()

	reportFileData := model.ReportFile{Status: common.PROCESSING}
	_ = u.helperRedis.Set(ctx, fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser, req.TransactionID),
		reportFileData, time.Duration(1000)*time.Second)

	return res, nil
}

func (u *reportUsecase) GetReportFile(ctx context.Context, req *dto.GetReportFileRequestDTO) (
	res *dto.GetReportFileResponseDTO, err error) {

	var reportFileData model.ReportFile
	valueInterface, err := u.helperRedis.GetInterface(ctx, fmt.Sprintf("back-cms:report-detail-%v:%v", req.DomainUser,
		req.TransactionID), reportFileData)
	if err == nil {
		reportFileData = valueInterface.(model.ReportFile)
		if strings.EqualFold(reportFileData.Status, common.ACCEPT) {
			res = &dto.GetReportFileResponseDTO{
				ResponseBaseDTO: dto.ResponseBaseDTO{
					StatusCode: common.ACCEPT,
				},
				FileName: reportFileData.FileName,
			}
			return res, nil
		}

		res = &dto.GetReportFileResponseDTO{
			ResponseBaseDTO: dto.ResponseBaseDTO{
				StatusCode: common.PROCESSING,
			},
		}
		return res, nil

	}
	res = &dto.GetReportFileResponseDTO{
		ResponseBaseDTO: dto.ResponseBaseDTO{
			StatusCode:    common.FAILED,
			ReasonCode:    common.ReasonNotFound.Code(),
			ReasonMessage: common.ReasonNotFound.Message(),
		},
	}
	return res, nil
}

func (u *reportUsecase) GetReportFileFull(ctx context.Context, domain, transactionID string) (res *dto.GetFileDTO, err error) {

	var (
		valueInterface interface{}
		dataFile       dto.FileDTO
		file           *excelize.File
	)

	res = &dto.GetFileDTO{}
	// TODO
	aasdf := true
	if !aasdf {
		res, err = u.saveParticalFile(ctx, domain, transactionID)
		if err != nil {
			return nil, errors.New(common.ReasonNotFound.Code())
		}
		return res, nil
	}
	res.StatusCode = common.FAILED
	fileResponse, err := u.GetReportFile(ctx, &dto.GetReportFileRequestDTO{
		DomainUser:    domain,
		TransactionID: transactionID,
	})

	if err != nil {
		logger.GlobaLogger.Warnf("%v - Error while getting file report: %v", transactionID, err)
		return res, err
	}

	if fileResponse.ResponseBaseDTO.StatusCode == common.PROCESSING {
		res.StatusCode = common.PROCESSING
		logger.GlobaLogger.Warnf("%v - File is still processing to be a file: %v", transactionID)
		return res, nil
	}

	if fileResponse.ResponseBaseDTO.StatusCode != common.ACCEPT {
		logger.GlobaLogger.Warnf("%v - Error while getting report file: %v",
			transactionID,
			fmt.Sprintf("%v-%v", fileResponse.ReasonCode, fileResponse.ReasonMessage))
		err = errors.New(fileResponse.ReasonCode)
		return nil, err
	}
	res.StatusCode = common.ACCEPT

	if fileResponse.FileName == "" {
		logger.GlobaLogger.Warnf("%v - Error while getting file report: %v", transactionID, err)
		return nil, err
	}

	valueInterface, err = u.helperRedis.GetInterface(ctx,
		fmt.Sprintf("back-cms:report-detail-%v:%v", domain, transactionID), dataFile)
	if err != nil {
		logger.GlobaLogger.Warnf("%v - Error while getting file report from redis: %v", transactionID, err)
		return nil, err
	}
	dataFile = valueInterface.(dto.FileDTO)
	bytesData, errDecode := base64.RawStdEncoding.DecodeString(dataFile.Base64File)
	if errDecode != nil {
		logger.GlobaLogger.Errorf(fmt.Sprintf("%v - Error when decode base64 string with err: ", transactionID), zap.Error(errDecode))
		err = errors.New(common.ReasonNotFound.Code())
		return nil, err
	}
	byteReader := bytes.NewReader(bytesData)
	file, err = excelize.OpenReader(byteReader)
	if err != nil {
		return nil, err
	}

	res.File = file
	res.FileName = fileResponse.FileName
	return res, nil
}
