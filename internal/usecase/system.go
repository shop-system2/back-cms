package usecase

import (
	"context"
	"errors"
	"fmt"
	"go-libs/logger"
	"html"
	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
	"shop-back-cms/internal/util"
	"strings"
	"time"

	"github.com/go-redis/redis"
)

type (
	// ManagementSystemUsecase interface for managementSystem usecase
	ManagementSystemUsecase interface {
		GetListNotificationTemplate(ctx context.Context,
			req *dto.GetListNotificationTemplateRequestDTO) ([]*dto.NotificationTemplateResponseDTO, error)
		GetNotificationTemplateDetail(ctx context.Context,
			req *dto.GetNotificationTemplateDetailRequestDTO) (*dto.NotificationTemplateResponseDTO, error)
		AddNotificationTemplate(ctx context.Context, req *dto.AddNotificationTemplateRequestDTO) error
		DeleteNotificationTemplate(ctx context.Context, req *dto.DeleteNotificationTemplateRequestDTO) error
		UpdateNotificationTemplate(ctx context.Context, req *dto.UpdateNotificationTemplateRequestDTO) error
		ActiveNotificationTemplate(ctx context.Context, req *dto.ActiveNotificationTemplateRequestDTO) error

		// redis
		GetListRedis(ctx context.Context, req *dto.GetListRedisRequestDTO) (*dto.ManagementRedis, error)
		CheckKeyExists(ctx context.Context, req *dto.CheckKeyExistsRequestDTO) (bool, error)
		AddNewRedis(ctx context.Context, req *dto.AddNewRedisRequestDTO) error
		DeleteRedisByKey(ctx context.Context, req *dto.DeleteRedisByKeyRequestDTO) error
		SetTimeout(ctx context.Context, req *dto.SetTimeoutRequestDTO) error
		GetRedisDetail(ctx context.Context, req *dto.GetRedisDetailRequestDTO) (dto.GetRedisDetailResponseDTO, error)

		// *** feature flag ***
		GetFlagPages(ctx context.Context, reqDTO *dto.GetFlagPagesRequestDTO) ([]*dto.GetFlagPagesResponseDTO, error)
		GetFlagDetailPage(ctx context.Context, reqDTO dto.GetFlagDetailPageRequestDTO) (dto.GetFlagDetailPageResponseDTO, error)
		GetFlagAdminPages(ctx context.Context) ([]*dto.GetFlagPagesAdminResponseDTO, error)
		PostFlagPage(ctx context.Context, reqDTO dto.PostFlagPageRequestDTO) error
		DeleteFlagPage(ctx context.Context, reqDTO dto.DeleteFlagPageRequestDTO) error
		PatchFlagPage(ctx context.Context, reqDTO dto.PatchFlagPageRequestDTO) error
		CheckFlagAccess(ctx context.Context, req *dto.CheckFlagAccessRequestDTO) (*dto.CheckFlagAccessResponseDTO, error)
	}
	managementSystemUsecase struct {
		cfg                        *config.Config
		cacheHelper                cache.CacheHelper
		managementSystemRepository repository.ManagementSystemRepository
	}
)

// NewManagementSystemUsecase new instance for managementSystem usecase
func NewManagementSystemUsecase(
	cfg *config.Config,
	cacheHelper cache.CacheHelper,
	managementSystemRepository repository.ManagementSystemRepository) ManagementSystemUsecase {
	return &managementSystemUsecase{
		cfg:                        cfg,
		cacheHelper:                cacheHelper,
		managementSystemRepository: managementSystemRepository,
	}
}

func (u *managementSystemUsecase) GetListNotificationTemplate(ctx context.Context,
	req *dto.GetListNotificationTemplateRequestDTO) (templates []*dto.NotificationTemplateResponseDTO,
	err error) {

	var (
		notificationTemplates []*model.NotificationTemplate
	)
	cacheNotiTemplateList := fmt.Sprintf(u.cfg.ManagementNotiTemplate.KeyCacheNotiTemplate)
	valueInterface, err := u.cacheHelper.GetInterface(ctx, cacheNotiTemplateList, notificationTemplates)
	if err != nil {
		notificationTemplates, err = u.managementSystemRepository.GetListNotificationTemplate(ctx)
		if err != nil {
			logger.GlobaLogger.Errorf("%v - Error while get list notification template", req.TransactionID)
			return nil, err
		}
		err = u.cacheHelper.Set(ctx, cacheNotiTemplateList, notificationTemplates,
			time.Duration(u.cfg.ManagementNotiTemplate.CacheTimeNotiTemplate)*time.Second)
		if err != nil {
			logger.GlobaLogger.Warnf("%v - Error while set list cache notification, err: %v", req.TransactionID, err)
		}
	} else {
		notificationTemplates = valueInterface.([]*model.NotificationTemplate)
	}
	templates = make([]*dto.NotificationTemplateResponseDTO, len(notificationTemplates))
	for index, item := range notificationTemplates {
		itemDTO := &dto.NotificationTemplateResponseDTO{
			ID:          item.ID,
			Subject:     item.Subject.String,
			Body:        item.Body.String,
			Template:    item.Template,
			Pattern:     item.Pattern.String,
			IsActive:    item.IsActive == "Y",
			CreatedBy:   item.CreatedBy.String,
			CreatedDate: item.CreatedDate.String,
			UpdatedBy:   item.UpdatedBy.String,
			UpdatedDate: item.UpdatedDate.String,
		}
		templates[index] = itemDTO
	}
	return templates, nil
}

func (u *managementSystemUsecase) GetNotificationTemplateDetail(ctx context.Context,
	req *dto.GetNotificationTemplateDetailRequestDTO) (resp *dto.NotificationTemplateResponseDTO, err error) {

	resp = &dto.NotificationTemplateResponseDTO{}
	var (
		notificationTemplate *model.NotificationTemplate
	)
	cacheNotiTemplateDetail := fmt.Sprintf(u.cfg.ManagementNotiTemplate.KeyCacheNotiTemplateDetail, req.ID)
	valueInterface, err := u.cacheHelper.GetInterface(ctx, cacheNotiTemplateDetail, notificationTemplate)
	if err != nil {
		notificationTemplate, err = u.managementSystemRepository.GetNotificationTemplateDetail(ctx, req.ID)
		if err != nil {
			logger.GlobaLogger.Errorf("%v - Error while get detail notification template", req.TransactionID)
			return nil, err
		}
		err = u.cacheHelper.Set(ctx, cacheNotiTemplateDetail, notificationTemplate,
			time.Duration(u.cfg.ManagementNotiTemplate.CacheTimeNotiTemplateDetail)*time.Second)
		if err != nil {
			logger.GlobaLogger.Warnf("%v - Error while set cache notification detail, err: %v", req.TransactionID, err)
		}
	} else {
		notificationTemplate = valueInterface.(*model.NotificationTemplate)
	}
	resp.ID = notificationTemplate.ID
	resp.Template = notificationTemplate.Template
	resp.Subject = notificationTemplate.Subject.String
	resp.Pattern = notificationTemplate.Pattern.String
	resp.Body = notificationTemplate.Body.String
	resp.CreatedBy = notificationTemplate.CreatedBy.String
	resp.UpdatedBy = notificationTemplate.UpdatedBy.String
	resp.CreatedDate = notificationTemplate.CreatedDate.String
	resp.UpdatedDate = notificationTemplate.UpdatedDate.String
	resp.IsActive = notificationTemplate.IsActive == "Y"
	return resp, nil
}

func (u *managementSystemUsecase) AddNotificationTemplate(ctx context.Context,
	req *dto.AddNotificationTemplateRequestDTO) (err error) {

	bodyEscape := html.EscapeString(req.Body)

	modelAdd := &model.AddNotificationTemplate{
		Subject:  req.Subject,
		Template: req.Template,
		Pattern:  req.Pattern,
		Body:     bodyEscape,
	}
	err = u.managementSystemRepository.AddNotificationTemplate(ctx, modelAdd)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while add new notification template", req.TransactionID)
		return err
	}
	cacheNotiTemplate := fmt.Sprintf(u.cfg.ManagementNotiTemplate.KeyCacheNotiTemplate)
	_ = u.cacheHelper.Del(ctx, cacheNotiTemplate)
	return nil
}
func (u *managementSystemUsecase) DeleteNotificationTemplate(ctx context.Context,
	req *dto.DeleteNotificationTemplateRequestDTO) (err error) {

	err = u.managementSystemRepository.DeleteNotificationTemplate(ctx, req.ID)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while delete notification template, id: %v", req.TransactionID, req.ID)
		return err
	}
	cacheNotiTemplate := fmt.Sprintf(u.cfg.ManagementNotiTemplate.KeyCacheNotiTemplate)
	_ = u.cacheHelper.Del(ctx, cacheNotiTemplate)
	return nil
}

func (u *managementSystemUsecase) UpdateNotificationTemplate(ctx context.Context,
	req *dto.UpdateNotificationTemplateRequestDTO) (err error) {

	bodyEscape := html.EscapeString(req.Body)
	templateID := int32(req.ID)
	modelAdd := &model.AddNotificationTemplate{
		Subject:  req.Subject,
		Template: req.Template,
		Pattern:  req.Pattern,
		Body:     bodyEscape,
		ID:       templateID,
	}
	err = u.managementSystemRepository.UpdateNotificationTemplate(ctx, modelAdd)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while update notification template, id: %v", req.TransactionID, req.ID)
		return err
	}
	cacheNotiTemplateDetail := fmt.Sprintf(u.cfg.ManagementNotiTemplate.KeyCacheNotiTemplateDetail, templateID)
	cacheNotiTemplate := fmt.Sprintf(u.cfg.ManagementNotiTemplate.KeyCacheNotiTemplate)
	_ = u.cacheHelper.DelMulti(ctx, cacheNotiTemplate, cacheNotiTemplateDetail)
	return nil
}

func (u *managementSystemUsecase) ActiveNotificationTemplate(ctx context.Context,
	req *dto.ActiveNotificationTemplateRequestDTO) (err error) {

	isActive := "Y"
	if !req.IsActive {
		logger.GlobaLogger.Infof("%v - Unactive notification template: %v", req.TransactionID, req.ID)
		isActive = "N"
	}
	err = u.managementSystemRepository.ActiveNotificationTemplate(ctx, req.ID, isActive)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while active or unactive notification template, id: %v", req.TransactionID, req.ID)
		return err
	}
	cacheNotiTemplate := fmt.Sprintf(u.cfg.ManagementNotiTemplate.KeyCacheNotiTemplate)
	_ = u.cacheHelper.Del(ctx, cacheNotiTemplate)
	return nil
}

func (u *managementSystemUsecase) GetListRedis(ctx context.Context,
	req *dto.GetListRedisRequestDTO) (respDTO *dto.ManagementRedis, err error) {

	var (
		keysTemp []string
		isGet    bool // continue action get list redis by pattern
		countR   uint32
	)
	respDTO = &dto.ManagementRedis{}
	countR = req.Count
	if countR > u.cfg.ManagementRedis.MaxCount || countR == 0 {
		logger.GlobaLogger.Infof("%v - Set count default value", req.TransactionID)
		countR = u.cfg.ManagementRedis.DefaultCount
	}

	pattern := req.Pattern
	if len(pattern) == 1 && pattern == "*" {
		logger.GlobaLogger.Errorf("%v - Pattern need more, not *: %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonRedisPatternIncorrect.Code())
	}

	keysTemp, cursor, err := u.cacheHelper.GetKeysByPattern(ctx, pattern, uint64(req.Cursor), int64(countR))
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while get list redis: %v", req.TransactionID, err)
		return nil, errors.New(common.ReasonCacheError.Code())
	}
	isGet = true
	if cursor == 0 {
		logger.GlobaLogger.Infof("%v - Cursor = 0 so that cannot be request", req.TransactionID)
		isGet = false
	}
	if len(keysTemp) == 0 && !isGet {
		logger.GlobaLogger.Infof("%v - Get list redis not found", req.TransactionID)
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	respDTO.Redises = make([]*dto.RedisInfo, len(keysTemp))
	for i, key := range keysTemp {
		itemRedis := &dto.RedisInfo{}
		itemRedis.Key = key
		respDTO.Redises[i] = itemRedis
	}
	respDTO.IsGet = isGet
	respDTO.Cursor = uint32(cursor)
	respDTO.Count = (countR)
	return respDTO, nil
}

func (u *managementSystemUsecase) getSizeOfKey(in, transactionID string) int64 {
	logger.GlobaLogger.Infof("%v - Split size in debugObject", transactionID)
	slitDObject := strings.SplitAfter(in, "serializedlength:")
	if len(slitDObject) < 2 {
		logger.GlobaLogger.Errorf("%v - Split size in debugObject failure", transactionID)
		return 0
	}
	sizeKey := strings.Split(slitDObject[1], " ")
	if len(sizeKey) == 0 {
		logger.GlobaLogger.Errorf("%v - Split size in debugObject failure", transactionID)
		return 0
	}
	strSize := sizeKey[0]
	endsize := util.ConvertStringToInt64(strSize)
	return endsize
}

func (u *managementSystemUsecase) GetRedisDetail(ctx context.Context,
	req *dto.GetRedisDetailRequestDTO) (resp dto.GetRedisDetailResponseDTO, err error) {

	if req.Key == "" {
		logger.GlobaLogger.Errorf("%v - Key redis not empty", req.TransactionID)
		return resp, errors.New("key invalid")
	}
	timeSecond, err := u.cacheHelper.TimeExpire(ctx, req.Key)
	typeOfKey, err := u.cacheHelper.GetType(ctx, req.Key)
	if typeOfKey == "string" {
		lengthOfKey, _ := u.cacheHelper.GetStrLenght(ctx, req.Key)
		resp.Lenght = lengthOfKey
	}
	property, err := u.cacheHelper.DebugObjectByKey(ctx, req.Key)
	resp.SizeData = u.getSizeOfKey(property, req.TransactionID)
	resp.Type = typeOfKey
	resp.TimeExpire = int64(timeSecond)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while get detail", req.TransactionID)
		return resp, errors.New(common.ReasonRedisNil.Code())
	}
	return resp, nil
}

func (u *managementSystemUsecase) CheckKeyExists(ctx context.Context, req *dto.CheckKeyExistsRequestDTO) (isExists bool, err error) {

	isExists = false
	err = u.cacheHelper.Exists(ctx, req.Key)
	if err == redis.Nil {
		return isExists, nil
	}
	isExists = true
	return isExists, nil
}

func (u *managementSystemUsecase) AddNewRedis(ctx context.Context, req *dto.AddNewRedisRequestDTO) (err error) {

	expiration := req.Expiration
	if expiration == 0 {
		expiration = u.cfg.ManagementRedis.DefaultExpiration
	}
	err = u.cacheHelper.Set(ctx, req.Key, req.Value, time.Duration(expiration)*time.Second)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while add new redis by key", req.TransactionID)
		return errors.New(common.ReasonCacheError.Code())
	}
	return nil
}

func (u *managementSystemUsecase) SetTimeout(ctx context.Context, req *dto.SetTimeoutRequestDTO) (err error) {

	expiration := req.Expiration
	if expiration == 0 {
		expiration = u.cfg.ManagementRedis.DefaultExpiration
	}
	err = u.cacheHelper.Expire(ctx, req.Key, time.Duration(expiration)*time.Second)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while set timeout redis by key", req.TransactionID)
		return errors.New(common.ReasonCacheError.Code())
	}
	return nil
}

func (u *managementSystemUsecase) DeleteRedisByKey(ctx context.Context, req *dto.DeleteRedisByKeyRequestDTO) (err error) {

	err = u.cacheHelper.Del(ctx, req.Key)
	if err != nil {
		logger.GlobaLogger.Errorf("%v - Error while delete redis by key", req.TransactionID)
		return errors.New(common.ReasonCacheError.Code())
	}
	return nil
}

//  feature flag
func (u *managementSystemUsecase) PostFlagPage(ctx context.Context, reqDTO dto.PostFlagPageRequestDTO) (err error) {
	err = u.managementSystemRepository.PostFlagPage(ctx, reqDTO)
	if err != nil {
		logger.GlobaLogger.Errorf("usecase.managementSystemUsecase/PostFlagPage Error while post new flag feature: %v", err.Error())
		return err
	}
	return nil
}

func (u *managementSystemUsecase) DeleteFlagPage(ctx context.Context, reqDTO dto.DeleteFlagPageRequestDTO) (err error) {

	id := reqDTO.FlagID
	err = u.managementSystemRepository.DeleteFlagPage(ctx, id)
	if err != nil {
		logger.GlobaLogger.Errorf("usecase.managementSystemUsecase/DeleteFlagPage Error while delete flag feature: %v", err.Error())
		return err
	}
	return nil
}

func (u *managementSystemUsecase) PatchFlagPage(ctx context.Context, reqDTO dto.PatchFlagPageRequestDTO) (err error) {

	err = u.managementSystemRepository.PatchFlagPage(ctx, reqDTO)
	if err != nil {
		logger.GlobaLogger.Errorf("usecase.managementSystemUsecase/PatchFlagPage Error while patch update flag feature: %v", err.Error())
		return err
	}
	return nil
}

func (u *managementSystemUsecase) GetFlagDetailPage(ctx context.Context,
	reqDTO dto.GetFlagDetailPageRequestDTO) (respDTO dto.GetFlagDetailPageResponseDTO, err error) {

	response, err := u.managementSystemRepository.GetDetailFlagPage(ctx, reqDTO.FlagID)
	if err != nil {
		logger.GlobaLogger.Errorf("usecase.managementSystemUsecase/PatchFlagPage Error while patch update flag feature: %v", err.Error())
		return respDTO, err
	}
	respDTO.Key = response.Key
	respDTO.Value = response.Value
	respDTO.FromDate = response.FromDate.String
	respDTO.ToDate = response.ToDate.String
	return respDTO, nil
}

func (u *managementSystemUsecase) GetFlagAdminPages(ctx context.Context) (respDTO []*dto.GetFlagPagesAdminResponseDTO, err error) {

	var (
		flags []model.FeatureFlag
	)

	flags, err = u.managementSystemRepository.GetAdminFlagPages(ctx)
	if err != nil {
		logger.GlobaLogger.Warnf("Error while getting all feature flag from db: %v", err)
		return respDTO, errors.New(common.ReasonDBError.Code())
	}
	respDTO = []*dto.GetFlagPagesAdminResponseDTO{}
	for _, flag := range flags {
		flagDTO := dto.GetFlagPagesAdminResponseDTO{}
		flagDTO.Key = flag.Key
		flagDTO.Value = flag.Value
		flagDTO.FromDate = flag.FromDate.String
		flagDTO.ToDate = flag.ToDate.String
		flagDTO.IsActive = flag.IsActive
		respDTO = append(respDTO, &flagDTO)
	}

	return respDTO, nil
}

func (u *managementSystemUsecase) GetFlagPages(ctx context.Context,
	reqDTO *dto.GetFlagPagesRequestDTO) (respDTO []*dto.GetFlagPagesResponseDTO, err error) {

	var (
		flags []*model.FeatureFlag
	)

	flags, err = u.managementSystemRepository.GetFlagPages(ctx)
	if err != nil {
		logger.GlobaLogger.Warnf("Error while getting all feature flag from db: %v", err)
		return respDTO, err
	}
	respDTO = []*dto.GetFlagPagesResponseDTO{}
	for _, flag := range flags {
		flagDTO := dto.GetFlagPagesResponseDTO{}
		flagDTO.Key = flag.Key
		isActive := false
		if flag.Value == "1" {
			isActive = true
		}
		if !isActive {
			isActive = haveActivePageByTime(flag.FromDate.String, flag.ToDate.String)
		}
		flagDTO.Value = isActive
		respDTO = append(respDTO, &flagDTO)
	}

	return respDTO, nil
}

func (u *managementSystemUsecase) CheckFlagAccess(ctx context.Context,
	req *dto.CheckFlagAccessRequestDTO) (reqDTO *dto.CheckFlagAccessResponseDTO, err error) {

	flag, err := u.managementSystemRepository.CheckFlagAccess(ctx, req.Router)
	if err != nil {
		logger.GlobaLogger.Warnf("Error while getting all feature flag from db: %v", err)
		return nil, errors.New(common.ReasonNotFound.Code())
	}
	reqDTO = &dto.CheckFlagAccessResponseDTO{}

	isAccessTemp := false
	if flag.Value == "1" {
		isAccessTemp = true
	}
	if !isAccessTemp {
		isAccessTemp = haveActivePageByTime(flag.FromDate.String, flag.ToDate.String)
	}

	reqDTO.IsAccess = isAccessTemp
	return reqDTO, nil
}

func haveActivePageByTime(from, to string) bool {
	if to == "" || from == "" {
		return false
	}
	newFrom := fmt.Sprintf("%v 00:00:00", from)
	newTo := fmt.Sprintf("%v 23:59:59", to)
	const layout = "20060102 15:04:05"
	t := time.Now()
	timex := t.Format(layout)
	tNow, _ := time.Parse(layout, timex)

	tFrom, err := time.Parse(layout, newFrom)
	if err != nil {
		return false
	}
	tBefore := tFrom.Before(tNow)

	tTo, err := time.Parse(layout, newTo)
	if err != nil {
		return false
	}
	tAfter := tTo.After(tNow)

	if !tAfter || !tBefore {
		return false
	}
	return true
}
