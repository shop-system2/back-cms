package usecase

import (
	"context"
	"errors"
	"fmt"
	"os"
	"reflect"

	"github.com/tealeg/xlsx/v3"
)

type (
	objData struct {
		Name  string
		Price string
	}
)

func ImportFile(ctx context.Context, action string, file *os.File) (*[]interface{}, error) {

	outChan, doneChan, err := reactExcelFile(file, objData{})
	if err != nil {
		return nil, err
	}
	fmt.Println(doneChan)
	fmt.Println(outChan)
	var result []interface{}
	for {
		select {
		case outInf, ok := <-outChan:
			if !ok {
				return &result, nil
			}
			if outInf != nil {
				fmt.Println("append value:", outInf)
				result = append(result, outInf)
			}
		case <-doneChan:
			return &result, nil
		}
	}
}

func reactExcelFile(file *os.File, obj interface{}) (<-chan interface{}, <-chan bool, error) {
	var (
		sheet *xlsx.Sheet
		ok    bool
	)
	xlslFile, err := xlsx.OpenFile(file.Name())
	if err != nil {
		return nil, nil, err
	}
	if sheet, ok = xlslFile.Sheet["Data"]; !ok {
		return nil, nil, errors.New("not exist sheet data")
	}
	outChan := make(chan interface{})
	doneChan := make(chan bool)
	rowVisitor := func(r *xlsx.Row) error {
		newStructInstance := reflect.New(reflect.TypeOf(obj)).Interface()
		err := r.ReadStruct(newStructInstance)
		if err != nil {
			fmt.Println(" error while ReadStruct")
			doneChan <- true
			return err
		}
		outChan <- newStructInstance
		return nil
	}
	go func() {
		err = sheet.ForEachRow(rowVisitor)
		if err != nil {
			fmt.Println("error while processing row")
		}
		close(outChan)
		close(doneChan)
	}()
	return outChan, doneChan, nil
}
