package usecase

import (
	"context"
	"errors"
	"fmt"
	"go-libs/logger"
	"shop-back-cms/config"
	"shop-back-cms/internal/common"
	"shop-back-cms/internal/dto"
	"shop-back-cms/internal/helper"
	"shop-back-cms/internal/helper/cache"
	"shop-back-cms/internal/repository"
	"shop-back-cms/internal/repository/model"
	"time"
)

//
type (
	VendorUsecase interface {
		GetListVendor(ctx context.Context, req *dto.GetListVendorRequestDTO) (res *dto.GetListVendorResponseDTO, err error)
		SearchVendor(ctx context.Context, req *dto.SearchVendorRequestDTO) (res *dto.GetListVendorResponseDTO, err error)
		GetVendorDetail(ctx context.Context, req *dto.GetVendorDetailRequestDTO) (res *dto.GetVendorDetailResponseDTO, err error)
		PostNewVendor(ctx context.Context, req *dto.PostNewVendorRequestDTO) (res *dto.PostNewVendorResponseDTO, err error)
		PutUpdateVendor(ctx context.Context, req *dto.PutUpdateVendorRequestDTO) (res *dto.PutUpdateVendorResponseDTO, err error)
		DeleteVendor(ctx context.Context, req *dto.DeleteVendorRequestDTO) (res *dto.DeleteVendorResponseDTO, err error)
		DoActiveVendor(ctx context.Context, req *dto.DoActiveVendorRequestDTO) error
	}
	vendorUsecase struct {
		cfg              *config.Config
		modelCopier      helper.ModelCopier
		vendorRepository repository.VendorRepository
		helperRedis      cache.CacheHelper
	}
)

// NewVendorUsecase create instance Comment Usecase
func NewVendorUsecase(
	cfg *config.Config,
	modelCopier helper.ModelCopier,
	vendorRepository repository.VendorRepository,
	helperRedis cache.CacheHelper,
) VendorUsecase {
	return &vendorUsecase{
		cfg:              cfg,
		vendorRepository: vendorRepository,
		modelCopier:      modelCopier,
		helperRedis:      helperRedis,
	}
}

func (u *vendorUsecase) DoActiveVendor(ctx context.Context,
	req *dto.DoActiveVendorRequestDTO) (err error) {
	isActive := uint32(1)
	if !req.IsActive {
		isActive = uint32(0)
	}
	err = u.vendorRepository.DoActiveVendor(ctx, req.VendorID, isActive)
	if err != nil {
		logger.GlobaLogger.Errorf("%v,usecase.vendorUseCase/DoActiveVendor: Error while active vendor, %v", req.TransactionID, err)
		return errors.New(common.ReasonDBError.Code())
	}
	return nil
}

func (u *vendorUsecase) PostNewVendor(ctx context.Context,
	req *dto.PostNewVendorRequestDTO) (res *dto.PostNewVendorResponseDTO, err error) {
	err = u.vendorRepository.PostNewVendor(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.comment/PostNewVendor:  Error while add new vendor", req.TransactionID, err)
		return nil, errors.New(common.ReasonDBError.Code())
	}
	return res, err
}

func (u *vendorUsecase) GetListVendor(ctx context.Context,
	req *dto.GetListVendorRequestDTO) (res *dto.GetListVendorResponseDTO, err error) {
	var (
		vendors = []*model.VendorModel{}
	)
	res = &dto.GetListVendorResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Vendor.CacheKeyListVendor)
	valueInterface, err := u.helperRedis.GetInterface(ctx, keyCache, vendors)
	if err == nil {
		logger.GlobaLogger.Infof("%v: Get list vendor in redis.", req.TransactionID)
		vendors = valueInterface.([]*model.VendorModel)
	} else {
		vendors, err = u.vendorRepository.GetListVendor(ctx, req)
		if err != nil {
			logger.GlobaLogger.Errorf("%v: Error while get list vendor: err = %v", req.TransactionID, err)
			return nil, errors.New((common.ReasonDBError.Code()))
		}
		if vendors == nil {
			logger.GlobaLogger.Errorf("%v: Get list vendor not found:", req.TransactionID)
			return nil, errors.New((common.ReasonNotFound.Code()))
		}
		u.helperRedis.SetInterface(ctx, keyCache, vendors, time.Duration(u.cfg.Vendor.CacheTimeListVendor)*time.Second)

	}
	res.Vendors = make([]dto.VendorsDTO, len(vendors))
	for i, item := range vendors {
		item := dto.VendorsDTO{
			ID:          item.ID,
			Name:        item.Name,
			Code:        item.Code,
			Description: item.Description.String,
			CreatedAt:   item.CreatedAt,
			UpdatedAt:   item.UpdatedAt,
			IsActive:    item.IsActive == "1",
			Address:     item.Address,
			PhoneNumber: item.PhoneNumber,
		}
		res.Vendors[i] = item
	}
	return res, nil
}

func (u *vendorUsecase) SearchVendor(ctx context.Context,
	req *dto.SearchVendorRequestDTO) (res *dto.GetListVendorResponseDTO, err error) {
	var (
		vendors = []model.VendorModel{}
	)
	res = &dto.GetListVendorResponseDTO{}
	valueInterface, err := u.helperRedis.GetInterface(ctx, "ducnp", vendors)
	if err == nil {
		vendors = valueInterface.([]model.VendorModel)
		u.modelCopier.CopyFromModel(&res.Vendors, vendors)
		return res, nil
	}
	vendors, err = u.vendorRepository.SearchVendor(ctx, req)
	if err != nil {
		return nil, err
	}
	u.helperRedis.SetInterface(ctx, "ducnp", vendors, time.Duration(1000)*time.Second)
	u.modelCopier.CopyFromModel(&res.Vendors, vendors)
	return res, nil
}

func (u *vendorUsecase) GetVendorDetail(ctx context.Context,
	req *dto.GetVendorDetailRequestDTO) (res *dto.GetVendorDetailResponseDTO, err error) {
	var (
		vendors *model.VendorModel
	)

	res = &dto.GetVendorDetailResponseDTO{}
	keyCache := fmt.Sprintf(u.cfg.Vendor.CacheKeyDetailVendor, req.ID)
	resInter, err := u.helperRedis.GetInterface(ctx, keyCache, vendors)
	if resInter != nil && err == nil {
		logger.GlobaLogger.Infof("%v: Get detail vendor in redis.", req.TransactionID)
		vendors = resInter.(*model.VendorModel)
	} else {
		vendors, err = u.vendorRepository.GetVendorDetail(ctx, req.ID)
		if err != nil {
			logger.GlobaLogger.Errorf("%v: Error while get detail vendor: err = %v", req.TransactionID, err)
			return nil, errors.New((common.ReasonDBError.Code()))
		}
		u.helperRedis.Set(ctx, keyCache, vendors, time.Duration(u.cfg.Vendor.CacheTimeDetailVendor)*time.Second)
	}
	u.modelCopier.CopyFromModel(&res, vendors)
	return res, nil
}

func (u *vendorUsecase) PutUpdateVendor(ctx context.Context,
	req *dto.PutUpdateVendorRequestDTO) (res *dto.PutUpdateVendorResponseDTO, err error) {
	err = u.vendorRepository.PutUpdateVendor(ctx, req)
	if err != nil {
		logger.GlobaLogger.Errorf("%v: Error while update vendor: err = %v", req.TransactionID, err)
		return nil, errors.New((common.ReasonDBError.Code()))
	}
	return res, nil
}

func (u *vendorUsecase) DeleteVendor(ctx context.Context,
	req *dto.DeleteVendorRequestDTO) (res *dto.DeleteVendorResponseDTO, err error) {
	if req.Id == 0 {
		logger.GlobaLogger.Errorf("%v,usecase.vendor/DeleteVendor: Filed ID = 0 invalid", req.TransactionID)
		return nil, errors.New(common.ReasonVendorFieldIDInvalid.Code())
	}
	err = u.vendorRepository.DeleteVendor(ctx, uint32(req.Id))
	if err != nil {
		logger.GlobaLogger.Errorf("%v, usecase.vendor/DeleteVendor: Error while add new vendor, err=%v", req.TransactionID, err)
		return nil, errors.New((common.ReasonDBError.Code()))
	}
	res = &dto.DeleteVendorResponseDTO{}
	return res, nil
}
