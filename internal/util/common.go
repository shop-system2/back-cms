package util

import "strings"

// ContentInArray check whether or not item in array using EqualFold
func ContentInArray(array []string, item string) bool {
	for _, value := range array {
		if strings.EqualFold(value, item) {
			return true
		}
	}
	return false
}

// ContentInArrayUsingStringContains check whether or not item in array using Contains
func ContentInArrayUsingStringContains(array []string, item string) bool {
	for _, value := range array {
		if strings.Contains(value, item) {
			return true
		}
	}
	return false
}
