package util

// FormatNumberToAmount format number to amount, ex: 123123 -> 123,123
func FormatNumberToAmount(number string) string {
	var result string = ""
	for len(number) > 3 {
		result = "," + number[(len(number)-3):] + result
		number = number[0 : len(number)-3]
	}
	result = number + result
	return result
}
