package util

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"math/rand"
	"strings"
	"time"
)

var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func NowAsUnixSecondNano() int64 {
	return time.Now().UnixNano()
}

func nowAsUnixSecond() int64 {
	return time.Now().Unix()
}

func getRefreshToken() string {
	var id strings.Builder
	dest, _ := hex.DecodeString(fmt.Sprintf("%d", NowAsUnixSecondNano()))
	encode := base64.StdEncoding.EncodeToString(dest)
	//rand.Seed(nowAsUnixSecondNano())
	id.WriteString(encode)
	id.WriteString(RandString(10))
	return strings.Replace(id.String(), ".", RandString(10), 1)
}

func GetID() string {

	dests, _ := hex.DecodeString(fmt.Sprintf("%d", nowAsUnixSecond()))
	var ids strings.Builder
	encodes := base64.StdEncoding.EncodeToString(dests)
	rand.Seed(time.Now().UnixNano())
	ids.WriteString(encodes)
	ids.WriteString(RandString(4))
	return (strings.Replace(ids.String(), "=", RandString(1), 1))

}

var lettersOTP = []rune("0123456789")

func randString(n int, letters []rune) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
func CreateOtp(lengthOtp int) string {
	return randString(lengthOtp, lettersOTP)
}

func HashOtp(otp string) string {
	hasher := md5.New()
	hasher.Write([]byte(otp))
	return hex.EncodeToString(hasher.Sum(nil))
}
func GenProductSKU(lengthToken int) string {
	return randString(lengthToken, letters)
}
