package util

import (
	"context"
	"errors"
	"reflect"
	"regexp"
	"shop-back-cms/internal/common"
	"strconv"
	"strings"
	"time"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
	"google.golang.org/grpc/metadata"
)

func SetErrorToResponse(err error, in interface{}) {
	if err == nil || reflect.TypeOf(in).Kind() != reflect.Ptr {
		return
	}
	reason := common.ParseError(err)
	values := reflect.ValueOf(in)
	vIndirect := reflect.Indirect(values)
	reasonCode := vIndirect.FieldByName("ReasonCode")
	status := vIndirect.FieldByName("StatusCode")
	reasonMessage := vIndirect.FieldByName("ReasonMessage")
	if reasonCode.CanSet() {
		reasonCode.SetString(reason.Code())
	}
	if status.CanSet() {
		status.SetString(common.FAILED)
	}
	if reasonMessage.CanSet() {
		reasonMessage.SetString(reason.Message())
	}
}
func GetUDomainFromCtx(ctx context.Context) string {
	user, ok := metadata.FromIncomingContext(ctx)
	if ok && len(user[common.DomainMDKey]) > 0 {
		return user[common.DomainMDKey][0]
	}
	return ""
}
func GetUserIDFromCtx(ctx context.Context) uint32 {
	var userID uint32
	user, ok := metadata.FromIncomingContext(ctx)
	if ok && len(user["userid"]) > 0 {
		userID = ConvertStringToUint32(user["userid"][0])
	}
	return userID
}

func ConvertStringToInt(intStr string) int {
	val, err := strconv.ParseInt(intStr, 10, 64)
	if err != nil {
		return 0
	}
	return int(val)
}

// func ConvertSnakeCaseToCamelCase(in string) string {
// 	return link.ReplaceAllStringFunc(in, func(s string) string {
// 		return strings.ToUpper(strings.Replace(s, "_", "", -1))
// 	})
// }

func SnakeCaseToCamelCase(inputUnderScoreStr string) (camelCase string) {
	isToUpper := false
	regex := regexp.MustCompile("[^A-Za-z0-9]")

	for index, char := range inputUnderScoreStr {
		if index == 0 && !regex.MatchString(string(char)) {
			camelCase += strings.ToUpper(string(char))
		} else {
			if isToUpper {
				if !regex.MatchString(string(char)) {
					camelCase += strings.ToUpper(string(char))
					isToUpper = false
				}
			} else {
				if regex.MatchString(string(char)) {
					isToUpper = true
				} else {
					camelCase += strings.ToLower(string(char))
				}
			}
		}
	}
	return camelCase
}

func FormatMoney(num float64) string {
	str := strconv.FormatFloat(num, byte('f'), 3, 64)
	money := strings.Split(str, ".")

	re := regexp.MustCompile(`(\\d+)(\\d{3})`)
	for n := ""; n != money[0]; {
		n = money[0]
		money[0] = re.ReplaceAllString(money[0], "$1,$2")
	}

	return strings.Join(money, ".")
}

func ConvertDateForInternal(currentTime, formatType string) (string, error) {
	if currentTime == "" {
		return "", nil
	}

	timeFormat, err := time.Parse(formatType, currentTime)
	if err != nil {
		return "", errors.New("can't match format type")
	}

	return timeFormat.Format("2006-01-02"), nil
}

func ConvertDateForExternal(currentTime, typeFormat string) (string, error) {
	if currentTime == "" {
		return "", nil
	}

	timeFormat, err := time.Parse("2006-01-02", currentTime)
	if err != nil {
		return "", errors.New("can't match format type")
	}
	return timeFormat.Format(typeFormat), nil
}

var runsetInstance runes.Set = &runSet{}

type (
	runSet struct {
	}
)

func (rs *runSet) Contains(r rune) bool {
	return unicode.Is(unicode.Mn, r)
}

func RemoveStringPunctuation(in string) (result string, err error) {

	t := transform.Chain(norm.NFD, runes.Remove(runsetInstance), norm.NFC)
	result, _, err = transform.String(t, in)
	return result, err
}

func ConvertTimeToYYYYMMDD(input, layout string) (out string) {
	if input == "" {
		return ""
	}
	t, err := time.Parse(layout, input)
	if err != nil {
		return input
	}
	out = t.Format("20060102")
	return out
}
