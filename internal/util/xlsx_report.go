package util

import (
	"fmt"
	"shop-back-cms/internal/dto"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
)

func WriteTitleXlsxExcelize(streamWriter *excelize.StreamWriter,
	req *dto.FileDTO) int {
	rowIndex := 1
	var axis string
	axis = fmt.Sprintf("A%v", rowIndex)
	styleFileName := []interface{}{excelize.Cell{StyleID: req.Style, Value: req.TitleName}}
	_ = streamWriter.SetRow(axis, styleFileName)
	rowIndex++
	userCreate := fmt.Sprintf("Created by: %v", req.DomainUser)
	axis = fmt.Sprintf("A%v", rowIndex)
	itemValue := []interface{}{excelize.Cell{StyleID: req.Style, Value: userCreate}}
	_ = streamWriter.SetRow(axis, itemValue)
	rowIndex++
	timeCreateReports := fmt.Sprintf("Created Time: %v", time.Now().Format("2006/01/02 15:04:05"))
	axis = fmt.Sprintf("A%v", rowIndex)
	valye := []interface{}{excelize.Cell{StyleID: req.Style, Value: timeCreateReports}}
	_ = streamWriter.SetRow(axis, valye)
	return rowIndex
}

func WriteHeaderXlsxExcelize(streamWriter *excelize.StreamWriter, req *dto.FileDTO, indexRow int) int {
	newIndexRow := indexRow + 1
	newHeader := make([]interface{}, len(req.HeaderFile))
	for i, v := range req.HeaderFile {
		newHeader[i] = excelize.Cell{StyleID: req.Style, Value: fmt.Sprint(v)}
	}
	_ = streamWriter.SetRow(fmt.Sprintf("A%v", newIndexRow), newHeader)
	return newIndexRow
}

func WriteValueXlsxExcelize(streamWriter *excelize.StreamWriter,
	cellValues *[]interface{}, rowIndex int) (num int, err error) {
	newIndexRow := rowIndex
	var item []interface{}
	for i := 0; i < len(*cellValues); i++ {
		item = (*cellValues)[i].([]interface{})
		newIndexRow++
		err = streamWriter.SetRow(fmt.Sprintf("A%v", newIndexRow), item)
		if err != nil {
			return newIndexRow, err
		}
	}
	return newIndexRow, nil
}
