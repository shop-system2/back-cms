create table product (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    short_description VARCHAR(250),
    price VARCHAR(10),
    sell_price  VARCHAR(10),
    tag  VARCHAR(10),
    vote VARCHAR(10),
    description VARCHAR(1000),
    category_id int not null,
    quantity SMALLINT(6) NOT NULL DEFAULT 0,
    sku VARCHAR(100) NULL, -- not null
    -- domain VARCHAR(25) NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by VARCHAR(25),
    updated_by VARCHAR(25),
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    status_of_product VARCHAR(15) DEFAULT 'NORMAL' not null,
    url_image VARCHAR(100) NOT NULL ,
    PRIMARY KEY (id),
    CONSTRAINT PRODUCT_SKU_UNIQUE UNIQUE (sku)
);

----
create table category (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    code CHAR(4),
    description VARCHAR(255) null,
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    constraint code_unique UNIQUE (code)
);
---
create table client (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    email VARCHAR(100),
    phone_number VARCHAR(11),
    address VARCHAR(255),
    account_bank VARCHAR(20),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
     is_active CHAR(1) DEFAULT '1' NOT NULL,
   description VARCHAR(255) null,
    PRIMARY KEY (id),
    constraint phone_number_unique UNIQUE (phone_number),

    constraint email_unique UNIQUE (email)
);
----
create table comment (
    id int NOT NULL AUTO_INCREMENT,
    title VARCHAR(100),
    content VARCHAR(255),
    user_id int,
    product_id int not null,
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

---

create table vendor (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    code CHAR(4),
    phone_number VARCHAR(11),
    description VARCHAR(255) null,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    address VARCHAR(255),
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    PRIMARY KEY (id),
    constraint code_unique UNIQUE (code)
);
----
create table orders (
    id int NOT NULL AUTO_INCREMENT,
    user_id int not null,
    first_name VARCHAR(50) null,
    last_name VARCHAR(50) null,
    full_name VARCHAR(50) null,
    email VARCHAR(50) null,
    address_line_1 VARCHAR(255) null, 
    address_line_2 VARCHAR(255) null, 
    city VARCHAR(255) null, 
    country VARCHAR(255) null, 
    description text null,
    total VARCHAR(15) not null, 
    phone_number VARCHAR(15) null,
    status VARCHAR(15) DEFAULT 'PENDING' not null, -- SHIPPING, DONE
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

----

create table order_item (
    id int NOT NULL AUTO_INCREMENT,
    product_id int not null,
    order_id int not null,
    quantity SMALLINT(6) NOT NULL,
    sku VARCHAR(100) NULL,
    price VARCHAR(10),
    sell_price  VARCHAR(10),
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    description text,
    created_by VARCHAR(25),
    status VARCHAR(15) DEFAULT 'PENDING' not null, -- SHIPPING, DONE
    PRIMARY KEY (id)
);
---
create table carts (
    id int NOT NULL AUTO_INCREMENT,
    user_id int not null,
    first_name VARCHAR(50) null,
    last_name VARCHAR(50) null,
    full_name VARCHAR(50) null,
    email VARCHAR(50) null,
    address_line_1 VARCHAR(255) null, 
    address_line_2 VARCHAR(255) null, 
    city VARCHAR(255) null, 
    country VARCHAR(255) null, 
    description text null,
    total VARCHAR(15) not null, 
    phone_number VARCHAR(15) null,
    status VARCHAR(15) DEFAULT 'PENDING' not null, -- SHIPPING, DONE
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

---
create table cart_item (
    id int NOT NULL AUTO_INCREMENT,
    product_id int not null,
    cart_id int not null,
    quantity SMALLINT(6) NOT NULL,
    sku VARCHAR(100) NULL,
    price VARCHAR(10),
    sell_price  VARCHAR(10),
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    created_by VARCHAR(25),
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    description text,
    PRIMARY KEY (id),
    status CHAR(1) DEFAULT 'N' not null -- N: DONE add new, A: ordered, R : delete 
);
--
create table intruction (
    id int NOT NULL AUTO_INCREMENT,
    description VARCHAR(255) NOT NULL,
    type VARCHAR(10),
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    PRIMARY KEY (id)
);
---
create table product_intruction (
    id int NOT NULL AUTO_INCREMENT,
    product_id int not null,
    intruction_id int not null,
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    PRIMARY KEY (id)
);

-- 
create table review_common (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL,
    icon_url VARCHAR(255) NOT NULL,
    image_url VARCHAR(255) NOT NULL,
    major VARCHAR(255) NULL,
    description text,
    tag VARCHAR(255) NOT NULL,
    number_like int  null,
    comment int  null,
    star int null,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    PRIMARY KEY (id)
);

--
create table feature_flag (
    id int NOT NULL AUTO_INCREMENT,
    path VARCHAR(50) NOT NULL,
    value VARCHAR(20) NULL,
	from_date VARCHAR(20)  NULL, 
	to_date VARCHAR(20)  NULL, 
   description VARCHAR(255) null,
    is_active CHAR(1) DEFAULT '1' NOT NULL,
    created_by VARCHAR(20) NULL,
    updated_by VARCHAR(20) NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY (id)
);

--
create table notification_template (
    id int NOT NULL AUTO_INCREMENT,
    template VARCHAR(255), 
	subject VARCHAR(255), 
	body VARCHAR(255), 
	pattern VARCHAR(255), 
	is_active CHAR(1) DEFAULT '1' NOT NULL,
    created_by VARCHAR(20) NULL,
    updated_by VARCHAR(20) NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY (id)
);

---
 create table report (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NULL DEFAULT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
---

  create table report_parameters (
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NULL DEFAULT NULL,
    type VARCHAR(25) NOT NULL,
    default_value VARCHAR(255),
    is_multiple CHAR(1) default '0', -- 1: multiple, 0 normal
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    report_id int not NULL,
    column value VARCHAR(255),
    PRIMARY KEY (id)
);