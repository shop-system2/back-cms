insert into feature_flag (path ) value ('/system/redis');

insert into report (name) values ('danh sách sản phẩm');
insert into report (name) values ('danh sach khach hang');
insert into report (name) values ('danh sach nguoi dung');

insert into report_parameters (report_id, name, type, default_value) values ('1', 'bao cao tien', 'select', 'A');
insert into report_parameters (report_id, name, type, default_value) values ('1', 'bao cao tien redfs', 'text', 'sin A');
insert into report_parameters (report_id, name, type, default_value) values ('1', 'bao cao tien redfs', 'date', '20210205');
insert into report_parameters (report_id, name, type, default_value) values ('2', 'bao cao nhan vien', 'date', '20210205');
insert into report_parameters (report_id, name, type, default_value) values ('3', 'bao cao hoat dong', 'text', 'xin chao');
insert into report_parameters (report_id, name, type, default_value, is_multiple) values ('4', 'bao cao tien', 'select'

-- category
insert into category (name, code, description) values ('Son', 'son', 'son moi');
insert into category (name, code, description)  values ('nuoc hoa', 'choa', 'nuoc hoa abc');

-- product
insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image)
values('new','san pham perferentail 2',1,1,'PERFERENTAIL',100000,0 ,'123123123','san pham ho tro', 'san pham ho tro abc abc description', 'root');
insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image) 
values('new','san pham perferentail 2',1,1,'PERFERENTAIL',110000,0 ,'123123124','san pham ho tro', 'san pham ho tro abc abc description', 'root');
insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image)
values('new','san pham perferentail 2',1,1,'PERFERENTAIL',120000,0 ,'123123125','san pham ho tro', 'san pham ho tro abc abc description', 'root');
insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image)
values('new','san pham perferentail 2',1,1,'PERFERENTAIL',130000,0 ,'123123126','san pham ho tro', 'san pham ho tro abc abc description', 'root');

insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image) 
values('new','san pham perferentail 2',1,1,'TRENDING',111000,0 ,'123121123','san pham ho tro', 'san pham ho tro abc abc description', 'root');
insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image) 
values('new','san pham perferentail 2',1,1,'TRENDING',112000,0 ,'123122124','san pham ho tro', 'san pham ho tro abc abc description', 'root');
insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image) 
values('new','san pham perferentail 2',1,1,'TRENDING',124000,0 ,'1231243125','san pham ho tro', 'san pham ho tro abc abc description', 'root');
insert into product 
(tag,name,category_id,is_active, status_of_product,price,sell_price,sku,short_description,description, url_image) 
values('new','san pham perferentail 2',1,1,'TRENDING',135000,0 ,'1231231126','san pham ho tro', 'san pham ho tro abc abc description', 'root');

-- review common
insert into review_common (name, title,  major, tag, number_like, comment, star, icon_url, image_url,  description) 
values('Garam','Danh gia cao cap', 'Super Star', 'Da kho, sach', 4, 5, 3,
 'https://i.pinimg.com/474x/9f/36/88/9f3688e2d2c869751d39777d424025b9.jpg', 'https://i.pinimg.com/474x/9f/36/88/9f3688e2d2c869751d39777d424025b9.jpg', 
 'Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
          Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
          Các bạn da khô nên tham khảo qua dòng này nhé');


insert into review_common (name, title,  major, tag, number_like, comment, star, icon_url, image_url,  description) 
values('Garam','Danh gia cao cap', 'Super Star', 'Da kho, sach', 1,2, 3,
 'https://i.pinimg.com/474x/8d/3f/47/8d3f4773631b072aaef4de9e8b1ae025.jpg', 'https://i.pinimg.com/474x/8d/3f/47/8d3f4773631b072aaef4de9e8b1ae025.jpg', 
 'Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
          Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
          Các bạn da khô nên tham khảo qua dòng này nhé');


insert into review_common (name, title,  major, tag, number_like, comment, star, icon_url, image_url,  description) 
values('Garam','Danh gia cao cap', 'Super Star', 'Da kho, sach', 3, 3, 3,
 'https://i.pinimg.com/474x/04/c6/d7/04c6d7153ba74eaeda2b575392e2a8de.jpg', 'https://i.pinimg.com/474x/04/c6/d7/04c6d7153ba74eaeda2b575392e2a8de.jpg', 
 'Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
          Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
          Các bạn da khô nên tham khảo qua dòng này nhé');


insert into review_common (name, title,  major, tag, number_like, comment, star, icon_url, image_url,  description) 
values('Garam','Danh gia cao cap', 'Super Star', 'Da Kho, Lam Sach,Duong Am', 5,6, 5,
 'https://i.pinimg.com/474x/af/46/5c/af465c1d980e4a18d8d2f5d9a027cc1d.jpg', 'https://i.pinimg.com/474x/af/46/5c/af465c1d980e4a18d8d2f5d9a027cc1d.jpg', 
 'Đây là dòng sản phẩm dành riêng cho các bạn da thường đến da khô, mình khuyến nghị các bạn da dầu nên suy nghĩ trước khi dùng nhé
          Độ làm sạch thì em í vẫn làm tốt nhiệm vụ của mình, nhưng sau khi làm xong thì da cực kì mềm mượt luôn. Nhưng với mình là người có làn da dầu thì sau khi dùng 1 thời gian tình trạng đổ dầu sẽ nhiều hơn nên bộ Olive này không hợp với mình lắm 😢😢😢
          Các bạn da khô nên tham khảo qua dòng này nhé');
