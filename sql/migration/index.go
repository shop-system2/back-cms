package migration

import (
	"fmt"
	"shop-back-cms/internal/repository/model"

	"gorm.io/gorm"
)

func MigrationDb(db *gorm.DB) error {
	err := db.Migrator().CreateTable(model.Product{})
	if err != nil {
		fmt.Println(err)
	}
	err = db.Migrator().CreateTable(model.Category{})
	if err != nil {
		fmt.Println(err)

	}
	// err = migration.CreateTablePermission(db)
	// if err != nil {
	// 	zap.S().Error("migrator create table permission err %s", err)
	// }
	// err = migration.CreateTableRole(db)
	// if err != nil {
	// 	zap.S().Error("migrator create table role err %s", err)
	// }

	// err = migration.CreateTableUserRole(db)
	// if err != nil {
	// 	zap.S().Error("migrator create table user role err %s", err)
	// }

	// err = migration.CreateTableRolePermission(db)
	// if err != nil {
	// 	zap.S().Error("migrator create table role perrmission err %s", err)
	// }

	// err = migration.CreateTableAccess(db)
	// if err != nil {
	// 	zap.S().Error("migrator create table accept err %s", err)
	// }
	return nil
}
