CREATE VIEW v_product AS
SELECT id, name, price, vote,
category_id, created_at, updated_at, created_by, updated_by,
is_active, description 
FROM product
WHERE 1=1
ORDER BY id desc; 

-- view product preferential
CREATE VIEW sbl_v_product_perferentail AS
SELECT id, name, description, tag,  price, sell_price, short_description,url_image
FROM product
WHERE 1 = 1 and is_active = '1' and status_of_product = 'PERFERENTAIL'
ORDER BY id desc LIMIT 16;

-- view product trending
CREATE VIEW sbl_v_product_trending AS
SELECT id, name, description, tag,  price, sell_price,url_image
FROM product
WHERE 1 = 1 and is_active = '1' and status_of_product = 'TRENDING'
ORDER BY id desc LIMIT 16;


CREATE VIEW v_client AS
SELECT id, name, email, phone_number, address, account_bank, created_at, updated_at
FROM client
WHERE 1=1
ORDER BY id desc; 


SHOW FULL TABLES  WHERE table_type = 'VIEW';